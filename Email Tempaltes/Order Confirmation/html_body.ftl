<table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
  <tr>
       <td>
            <img src = "${data.storefrontUrl}/file/general/fabEmailerImg.jpg" style = "width:745px;"/>
        </td>
    </tr>
  <tr>
      <td>
        <h1 style = "width: 23%;margin:0 auto;padding:28px 0 28px 0;font-size: 20px;font-weight: 700;font-family: Verdana, Geneva, sans-serif;;text-align: center;margin-bottom: 0;" >ORDER CONFIRMATION</h1>
      </td>
  </tr>
  <tr>
    <td>
      <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding: 0 13%;">
        ${getString("ORDER_PLACED_SALUTATION", data.shippingAddress.firstName)}</p>
      <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding: 0 13%;">
        ${getString("ORDER_PLACED_INTRO")}</p>
      <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding: 0 13%;">     
          <#if data.currencyCode == "INR">
              ${getString("ORDER_PLACED_BODY_START")}<b>${getString("ORDER_PLACED_BODY_MID_ONE_BOLD", data.orderId)}</b>
              ${getString("ORDER_PLACED_BODY_MID_TWO_PLAIN")}
             <b>${getString("ORDER_PLACED_BODY_MID_THREE_BOLD", data.orderDate)}</b>
              will be dispatched within <b>${getString("ORDER_PLACED_BODY_END_BOLD")}</b>. We will keep you updated.<br/></p>
              <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight: 600;padding: 0 13%;">Note: We try to ship items as soon as they are available,so it is possible that you may receive multiple shipments for your order.Please write to us if you need any assistance,<br/></p>
              <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding: 0 13%;">Please note that Bespoke (custom kurta) & Furniture items may take 10-12 business days for shipment.</p>
          <#else>
              ${getString("ORDER_PLACED_BODY_START")}<b>${getString("ORDER_PLACED_BODY_MID_ONE_BOLD", data.orderId)}</b>
              ${getString("ORDER_PLACED_BODY_MID_TWO_PLAIN")}
             <b>${getString("ORDER_PLACED_BODY_MID_THREE_BOLD", data.orderDate)}</b>
              are as below,we will ship your ordered product within
            <b>5 business days.</b></p>
          </#if>
    </td>
  </tr>
  <tr>
    <td>
      <p style="font-family: Verdana, Geneva, sans-serif; background-color: #f4f2e5; font-size: 17px;font-weight: 800;padding-left: 20px;">Order Summary</p>
    </td>
  </tr>

  <#--  FIRST INNER TABLE   -->
  <tr>
    <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px;">
      <tr>
         <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 13px;padding: 20px 5%;font-weight: 700;background-color: #f9f8f1;">
              ${getString("ORDER_SUMMARY_HEADER_ONE_BOLD",  data.orderId)}
          </td>
          <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding:0 12px;font-weight:800;background-color: #f9f8f1;">
              ${getString("ORDER_SUMMARY_HEADER_TWO_BOLD")}
          </td>
          <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;font-weight:800;background-color: #f9f8f1;">
              ${getString("ORDER_SUMMARY_HEADER_THREE_BOLD")}
          </td>
      </tr>
    </table>
  </tr>
  <tr>
    <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px;">
        <#list data.orderItems as product>

		  <!-- Fetching bespoke product json data -->
		  <#if product.dynamicProperties?has_content>
			<#list product.dynamicProperties as dynProperty>
				<#if dynProperty.propertyId == "x_bespokeProductSummary" && dynProperty.propertyValue?has_content>
					<#assign bespoke_product_json_string = dynProperty.propertyValue>
					<#if bespoke_product_json_string?has_content>
						<#assign bespoke_product_map = bespoke_product_json_string?eval>
					</#if>
				<#elseif dynProperty.propertyId == "x_bespokeFinalImageUrl" && dynProperty.propertyValue?has_content>
					<#assign bespoke_product_image_url = dynProperty.propertyValue>
				</#if>
			</#list>
		  </#if>

		  <tr>
            <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding:13px 10px;font-weight:400;background-color: #f9f8f1;margin-right:5px">
				<#if bespoke_product_image_url?has_content>
					<img src="${bespoke_product_image_url}" alt="${product.title!}" style = "width:100px;height:auto;">
				<#else>
					<img src="${product.imageLocation}" alt="${product.title!}" style = "width:100px;height:auto;">
				</#if>
            </td>
            <td style = "font-family: Verdana,Geneva,sans-serif;font-size: 12px;background-color: #f9f8f1;">

				<#if bespoke_product_map?has_content && bespoke_product_map.FinalLook?has_content>
					${getString("ORDER_PLACED_ITEM_TAG",  bespoke_product_map.FinalLook)}<br/>
				<#else>
					${getString("ORDER_PLACED_ITEM_TAG",  product.productId)}<br/>
				</#if>
                ${getString("ORDER_PLACED_QUANTITY_TAG",  product.quantity)}</br>

                <!-- Variants -->
                <#if product.variants??>
					<#list product.variants as variant>
						<#if variant.optionValue??>
							<br/>
							${variant.optionName}: ${variant.optionValue}
						</#if>
					</#list>
                </#if>

				<!-- Bespoke Product Details -->
				<#if bespoke_product_map?has_content && bespoke_product_map.ProductSummary?has_content>
					<br/><br/>
					<#list bespoke_product_map.ProductSummary as summary>
						<#if summary.Product?has_content && summary.Feature?has_content>
							<#if summary.Product != "Fabric" || (summary.Area?? && summary.Area == "All")>
								${summary.Product}: ${summary.Feature} <br/>
							</#if>
						</#if>
					</#list>
				</#if>

			</td>
            <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding:0 34px;font-weight:400;background-color: #f9f8f1;">
                ${product.price}
            </td>
            <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding:0 45px;font-weight:400;background-color: #f9f8f1;">
                ${product.price}
            </td>
          </tr>
        </#list>
    </table>
  </tr>
  <tr>
    <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px;">
      

      <tr>    
      <!-- Showing Loyalty Points -->
      <#if data.payments?has_content>
        <#list data.payments as pay>
          <#if pay.billingAddress?? && pay.billingAddress.dynamicProperties?has_content>
            <#list pay.billingAddress.dynamicProperties as dynProp>
               <#if dynProp.propertyId?has_content && dynProp.propertyId == 'x_discount' && dynProp.propertyValue?has_content>
                  <#assign payuDiscount = dynProp.propertyValue>
                </#if>
                <#if dynProp.propertyId?has_content && dynProp.propertyId == 'x_netAmountDebited' && dynProp.propertyValue?has_content>
                  <#assign netAmount = dynProp.propertyValue>
               </#if> 
            </#list>
          </#if>
        </#list>
      </#if>
                                         
        
      <#if data.dynamicProperties?has_content>
        <#list data.dynamicProperties as dynProp>
          
              <#if dynProp.propertyId?has_content && dynProp.propertyId == 'x_payUEmiInterest' && dynProp.propertyValue?has_content >
                <#assign emi_val = dynProp.propertyValue>
               </#if>                                                
                                           
        </#list>                
      </#if>
      <#if data.payments?has_content>
        <#list data.payments as pay>
          <#if pay.billingAddress?? && pay.billingAddress.dynamicProperties?has_content>
            <#list pay.billingAddress.dynamicProperties as dynProp>
              <#if dynProp.propertyId?has_content && dynProp.propertyId == 'x_loyaltyPointsRedeemed' && dynProp.propertyValue?has_content >
                <#assign loyalty_val = dynProp.propertyValue>
               </#if>                                                
            </#list>
          </#if>                                 
        </#list>                
      </#if>
          <td style = "font-family: Verdana, Geneva, sans-serif;display:block;text-align:right;font-size: 15px;padding:23px 10% 23px 0;font-weight:400;background-color: #f9f8f1;">
              ${getString("ORDER_PLACED_SUBTOTAL_TITLE")}:<br/><br/>
              <#if data.discount??>
                ${getString("ORDER_PLACED_DISCOUNT_TITLE")}:<br/><br/>
              </#if>
              <#if payuDiscount?has_content>
                ${getString("ADDITIONAL_DISCOUNT")}:<br/><br/> 
              </#if>
              <#if loyalty_val?has_content>
                ${getString("LOYALTY_REDEEM_POINTS")}:<br/><br/> 
              </#if>
              ${getString("ORDER_PLACED_SHIPPING_TITLE")}:<br/><br/>
          </td>
         <td style = "font-family: Verdana, Geneva, sans-serif;font-size: 15px;font-weight:400;background-color: #f9f8f1;color:#8f2c34">
              ${data.subtotal}<br/><br/>
              <#if data.discount??>
                ${data.discount}<br/><br/>
              </#if>
              <#if payuDiscount?has_content>
                ${data.currencyCode} ${payuDiscount}<br/><br/> 
              </#if>
              <#if loyalty_val?has_content>
                ${loyalty_val}<br/><br/> 
              </#if>
              ${data.shipping}
          </td>
      </tr>
    </table>
    </tr>
      <tr>
        <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px;">    
            <tr>
                  <td style = "font-family: Verdana, Geneva, sans-serif;text-align: right;width:52%;font-weight: 700;background-color: #f9f8f1;font-size: 18px;" >
                      ${getString("ORDER_PLACED_TOTAL_TITLE")}:
                  </td>
                  <td style = "font-family:Helvetica;color:#8f2c34;background-color: #f9f8f1;padding:21px 37px;font-size: 18px;">
                    <#if netAmount?has_content>
                      ${data.currencyCode} &nbsp; ${netAmount}
                    <#else>
                    <#if emi_val?has_content>
                      ${data.currencyCode}  &nbsp; ${emi_val}
                    <#else>
                      ${data.total}
                    </#if>
                    </#if>                      
                  </td>
            </tr>
        </table>
  </tr>

  <tr>
    <td>
       <p style="font-family:Verdana, Geneva, sans-serif; background-color: f9f8f1; margin: 0 auto; width: 730px; background-color: #f9f8f1; font-size: 17px; font-weight: 800; padding-left: 20px; padding-left: 20px;">Shipping Address</p>
    </td>
  </tr>

  <tr>
    <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px;">
      <tr>
         <td style = "font-family: Verdana, Geneva, sans-serif;padding:21px 0 25px 23px;font-weight: 400;background-color: #f9f8f1;">
                    ${data.shippingAddress.firstName}
                    ${data.shippingAddress.lastName}<br/>
                    ${data.shippingAddress.address1},<br/>                
                    <#if (data.shippingAddress.address2? length > 0)>
                                      ${data.shippingAddress.address2},<br/>   
                    </#if>
                    ${data.shippingAddress.city} 
                    ${data.shippingAddress.state} - ${data.shippingAddress.postalCode}<br/>
                    ${data.shippingAddress.country} <br/>
                   
          </td>
      </tr>
    </table>
  </tr>
  <tr>
    <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 750px; background-color: #f9f8f1;">
      <tr>
        <td>
          <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding:0 2%">${getString("ORDER_PLACED_FOOTER_START")} <strong> ${getString("ORDER_PLACED_FOOTER_PHONE_BOLD")} </strong>${getString("ORDER_PLACED_FOOTER_MID")} <strong> ${getString("ORDER_PLACED_FOOTER_EMAIL_BOLD")} </strong></p>
          <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding:0 2%">${getString("ORDER_PLACED_FOOTER_END")}</p>
          <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding:0 2%;margin-bottom: 6px;">Warm Regards,</p>
          <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 14px;padding:0 2%;margin-top: 3px;">Fabindia Team</p>
          <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 13px;border-top: 1px solid grey;padding: 9px 13%;">${getString("ORDER_PLACED_POST_SCRIPT")}</p>
        </td>
      </tr>
    </table>
  </tr>
</table>
  