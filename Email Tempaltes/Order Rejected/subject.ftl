<#if data.sitename??>
  ${getString("ORDER_REJECTED_SUBJECT", data.sitename, data.orderId)}
<#else>
  ${getString("ORDER_REJECTED_SUBJECT", "", data.orderId)}
</#if>