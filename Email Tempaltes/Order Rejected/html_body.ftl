
  <!-- Start of Social Icons Header -->
<table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table border = "0" cellspacing = "0" style = "margin: 0 auto;width: 600px;" align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody>
                      <!-- Spacing -->
                      <tr>
                        <td width="100%" height="10"></td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td>
                          <table width="100" align="left" border="0"
                            cellpadding="0" cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="middle"
                                  style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #666666"
                                  st-content="viewonline" class="mobile-hide">
                                  <!-- This may not be needed in Stratus but is a good feature to have <a href="#" style="text-decoration: none; color: #666666">View Online </a>  -->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table width="100" align="right" border="0"
                            cellpadding="0" cellspacing="0" class="devicewidth">
                            <tbody>
                              <tr>
                                <td width="30" height="30" align="right">
                                  <div class="imgpop">
                                    <a target="_blank" href="#"> <img
                                      src="${data.storefrontUrl}/img/facebook.png"
                                      alt="" border="0" width="30" height="30"
                                      style="display: block; border: none; outline: none; text-decoration: none;">
                                    </a>
                                  </div>
                                </td>
                                <td align="left" width="20"
                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                <td width="30" height="30" align="center">
                                  <div class="imgpop">
                                    <a target="_blank" href="#"> <img
                                      src="${data.storefrontUrl}/img/twitter.png"
                                      alt="" border="0" width="30" height="30"
                                      style="display: block; border: none; outline: none; text-decoration: none;">
                                    </a>
                                  </div>
                                </td>
                                <td align="left" width="20"
                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                <td width="30" height="30" align="center">
                                  <div class="imgpop">
                                    <a target="_blank" href="#"> <img
                                      src="${data.storefrontUrl}/img/pinterest.png"
                                      alt="" border="0" width="30" height="30"
                                      style="display: block; border: none; outline: none; text-decoration: none;">
                                    </a>
                                  </div>
                                </td>
                                <td align="left" width="20"
                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                <td width="30" height="30" align="center">
                                  <div class="imgpop">
                                    <a target="_blank" href="#"> <img
                                      src="${data.storefrontUrl}/img/instagram.png"
                                      alt="" border="0" width="30" height="30"
                                      style="display: block; border: none; outline: none; text-decoration: none;">
                                    </a>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td width="100%" height="10"></td>
                      </tr>
                      <!-- Spacing -->
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of Social Icons Header -->


  <!-- Start of header -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0"
            align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody>
                      <!-- Spacing -->
                      <tr>
                        <td height="20"
                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td>
                          <!-- logo -->
                          <table width="400" align="center" border="0"
                            cellpadding="0" cellspacing="0" class="devicewidth">
                            <tbody>
                              <tr>
                                <td width="400" height="83" align="center">
                                  <div class="imgpop">
                                    <a target="_blank" href="#"> <img src = "${data.storefrontUrl}/file/general/fab_india_logo.png" 
                                      alt="" border="0"  width="200"
                                      style="display: block; width: 200px; border: none; outline: none; text-decoration: none;">
                                    </a>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table> <!-- end of logo -->
                        </td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td height="20"
                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                      </tr>
                      <!-- Spacing -->
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of Header -->

  <!-- Start of separator -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" align="center" cellspacing="0" cellpadding="0"
            border="0" class="devicewidth">
            <tbody>
              <tr>
                <td align="center" height="20"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of separator -->


  <!-- Start Intro Text -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0"
            align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody>
                      <!-- Spacing -->
                      <tr>
                        <td height="20"
                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td>
                          <table width="560" align="center" cellpadding="0"
                            cellspacing="0" border="0" class="devicewidthinner">
                            <tbody>
                              <!-- Title -->
                              <tr>
                                <td>
                                  <h1 style="width:23%;margin:0 auto;padding:28px 0 28px 0;font-size:20px;font-weight:700;font-family:Verdana,Geneva,sans-serif;text-align:center;margin-bottom:0">
                                  ${getString("ORDER_REJECTED_TITLE")}</td>
                              </tr>
                              <!-- End of Title -->
                              <!-- spacing -->
                              <tr>
                                <td width="100%" height="20"
                                  style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- End of spacing -->
                              <#if data.approverMessages??> 
							  <tr>
                                <td
                                  style="font-family: Helvetica, arial, sans-serif; font-size: 20px; color: #333333; text-align: center; line-height: 30px;"
                                  st-title="fulltext-heading">
                                  <#list data.approverMessages as
                                          message> ${message!}<br />
                                          </#list></td>
                              </tr>					  
							  <!-- spacing -->
                              <tr>
                                <td width="100%" height="20"
                                  style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- End of spacing -->
							  </#if>
                              <!-- content -->
                              <tr>
                                <td
                                  style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align: center; line-height: 30px;"
                                  st-content="fulltext-content">
                                  ${getString("ORDER_PLACED_ORDER_LINE_START")}<#if data.orderLocation??><a href="${data.orderLocation}">${data.orderId}</a><#else>${data.orderId}</#if>${getString("ORDER_PLACED_ORDER_LINE_END",data.organization.name)}
                                </td>
                              </tr>
                              <!-- End of content -->							 
                            </tbody>
                          </table>
                        </td>
                      </tr>                      
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of Intro text -->


  <!-- Start of separator -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" align="center" cellspacing="0" cellpadding="0"
            border="0" class="devicewidth">
            <tbody>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td width="550" align="center" height="1" 
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of separator -->
  <!-- 3 Start of Columns -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0"
            align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody>
                      <tr>
                        <td>
                          <!-- col 1 -->
                          <table width="186" align="left" border="0"
                            cellpadding="0" cellspacing="0" class="devicewidth">
                            <tbody>
                              <tr>
                                <td>
                                  <!-- start of text content table -->
                                  <table width="186" align="center" border="0"
                                    cellpadding="0" cellspacing="0"
                                    class="devicewidthinner">
                                    <tbody>
                                      <!-- title2 -->
                                      <tr>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align: left; line-height: 24px;"
                                          st-title="3col-title1">
                                          ${getString("ORDER_PLACED_SHIPPING_ADDRESS_TITLE")}
                                        </td>
                                      </tr>
                                      <!-- end of title2 -->
                                      <!-- content2 -->
                                      <tr>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: left; line-height: 24px;"
                                          st-content="3col-content1">
                                          ${data.shippingAddress.firstName} 
                                          ${data.shippingAddress.lastName}<br>
                                          ${data.shippingAddress.address1},<br>
                                          <#if (data.shippingAddress.address2? length > 0)>
                                            ${data.shippingAddress.address2},<br>
                                          </#if>
                                          ${data.shippingAddress.city} 
                                          ${data.shippingAddress.state}<br>
                                          ${data.shippingAddress.postalCode}<br> 
                                          ${data.shippingAddress.country} 
                                        </td>
                                      </tr>
                                      <!-- end of content2 -->
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <!-- end of text content table -->
                            </tbody>
                          </table> <!-- spacing -->
                          <table width="20" align="left" border="0"
                            cellpadding="0" cellspacing="0" class="removeMobile">
                            <tbody>
                              <tr>
                                <td width="100%" height="15"
                                  style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table> <!-- end of spacing --> <!-- col 2 -->
                          <#if data.isPaymentTypeDeferred == true??>
	                          <table width="186" align="left" border="0"
	                            cellpadding="0" cellspacing="0" class="devicewidth">
	                            <tbody>
	                              <tr>
	                                <td>
	                                  <!-- start of text content table -->
	                                  <table width="186" align="center" border="0"
	                                    cellpadding="0" cellspacing="0"
	                                    class="devicewidthinner">
	                                    <tbody>
	                                      <!-- title2 -->
	                                      <tr>
	                                        <td
	                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align: center; line-height: 24px;"
	                                          st-title="3col-title2">
	                                          ${getString("ORDER_PLACED_PAYMENT_METHODS_TITLE")}
	                                        </td>
	                                      </tr>
	                                      <!-- end of title2 -->
	                                      <!-- Spacing -->
	                                      <tr>
	                                        <td width="100%" height="15"
	                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
	                                      </tr>
	                                      <!-- Spacing -->
	                                      <!-- content2 -->
	                                      <tr>
	                                        <td
	                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: center; line-height: 24px;"
	                                          st-content="3col-content2">
	                                          <#list data.paymentMethods as
	                                          paymentMethod> ${paymentMethod!}<br />
	                                          </#list>
	                                        </td>
	                                      </tr>
	                                      <!-- end of content2 -->
	                                    </tbody>
	                                  </table>
	                                </td>
	                              </tr>
	                              <!-- end of text content table -->
	                            </tbody>
	                          </table>
                          </#if><!-- end of col 2 --> <!-- spacing -->
                          <table width="1" align="left" border="0"
                            cellpadding="0" cellspacing="0" class="removeMobile">
                            <tbody>
                              <tr>
                                <td width="100%" height="15"
                                  style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table> <!-- end of spacing --> <!-- col 3 -->
                          <table width="186" align="right" border="0"
                            cellpadding="0" cellspacing="0" class="devicewidth">
                            <tbody>
                              <tr>
                                <td>
                                  <!-- start of text content table -->
                                  <table width="186" align="center" border="0"
                                    cellpadding="0" cellspacing="0"
                                    class="devicewidthinner">
                                    <tbody>
                                      <!-- title -->
                                      <tr>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align: center; line-height: 24px;"
                                          st-title="3col-title3">
                                          ${getString("ORDER_PLACED_SHIPPING_METHODS_TITLE")}
                                        </td>
                                      </tr>
                                      <!-- end of title -->
                                      <!-- Spacing -->
                                      <tr>
                                        <td width="100%" height="15"
                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                      </tr>
                                      <!-- Spacing -->
                                      <!-- content -->
                                      <tr>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: center; line-height: 24px;"
                                          st-content="3col-content3">
                                          <#list data.shippingMethods as
                                          shippingMethod> ${shippingMethod!}<br />
                                          </#list>
                                        </td>
                                      </tr>
                                      <!-- end of content -->

                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <!-- end of text content table -->
                            </tbody>
                          </table>
                        </td>
                        <!-- spacing -->
                        <!-- end of spacing -->
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- end of 3 Columns -->


  <!-- Start of separator -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" align="center" cellspacing="0" cellpadding="0"
            border="0" class="devicewidth">
            <tbody>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td width="550" align="center" height="1"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of separator -->


  <!-- Start of Cart -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0"
            align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody style="background: #f9f8f1; color: #000;">
                      <tr>
                        <td>
                          <!-- col 1 -->
                          <table width="100%" align="left" border="0"
                            cellpadding="0" cellspacing="0" class="devicewidth">
                            <tbody>

                              <tr>
                                <td>
                                  <!-- start of text content table -->
                                  <table width="100%" align="center" border="0"
                                    cellpadding="0" cellspacing="0"
                                    class="devicewidthinner">
                                    <tbody>

                                      <!-- title2 -->
                                      <tr>
                                        <td width="30%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-align: left; line-height: 24px; background: #f9f8f1; color: #000; padding: 5px 10px 5px 10px;"
                                          st-title="3col-title1">
                                          ${getString("ORDER_PLACED_ITEM_TITLE")}
                                        </td>
                                        <td width="40%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-align: center; line-height: 24px; background: #f9f8f1; color: #000; padding: 5px 10px 5px 10px;"
                                          st-title="3col-title1">&nbsp;</td>
                                        <td width="10%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-align: center; line-height: 24px; background: #f9f8f1; color: #000; padding: 5px 10px 5px 10px;"
                                          st-title="3col-title1">
                                          ${getString("ORDER_PLACED_QUANTITY_TITLE")}
                                        </td>
                                        <td width="20%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-align: right; line-height: 24px; background: #f9f8f1; color: #000; padding: 5px 10px 5px 10px;"
                                          st-title="3col-title1">
                                          ${getString("ORDER_PLACED_PRICE_TITLE")}
                                        </td>
                                      </tr>
                                      <!-- Spacing -->
                                      <tr>
                                        <td width="30%" height="15"
                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                        <td width="40%"
                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                        <td width="10%"
                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                        <td width="20%"
                                          style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                      </tr>

                                      <!--  Start of order items list-->
                                      <#list data.orderItems as product>
                                      <tr>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: left; line-height: 24px; padding: 5px 10px 5px 10px;"
                                          st-content="3col-content1" width="30%">
                                          <img src="${product.imageLocation}"
                                          alt="${product.title!}" width="100px" style="display: block; width: 100px;">
                                        </td>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: left; line-height: 24px; padding: 5px 10px 5px 10px;"
                                          st-content="3col-content1" width="40%">
                                          <a href="${product.location}">${product.title!}</a>
                                          <!-- Variants -->
                                          <#if product.variants??>
                                            <br />
                                            <#list product.variants as variant>
                                              ${variant.optionName}: <#if variant.optionValue??>${variant.optionValue}</#if>
                                               <br /> 
                                             </#list>
                                          </#if>
                                        </td>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: center; line-height: 24px; padding: 5px 10px 5px 10px;"
                                          st-content="3col-content1" width="10%">
                                          ${product.quantity}</td>
                                        <td
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: right; line-height: 24px; padding: 5px 10px 5px 10px;"
                                          st-content="3col-content1" width="20%">
                                          ${product.price}</td>
                                      </tr>
                                      </#list>
                                      <!--  End of order items list -->
                                    </tbody>
                                  </table> 
                                  
                                  <!-- Start of separator -->
                                  <table width="100%"
                                    cellpadding="0" cellspacing="0" border="0"
                                    id="backgroundTable" st-sortable="separator">
                                    <tbody>
                                      <tr>
                                        <td>
                                          <table width="600" align="center"
                                            cellspacing="0" cellpadding="0"
                                            border="0" class="devicewidth">
                                            <tbody>
                                              <tr>
                                                <td align="center" height="30"
                                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td width="550" align="center"
                                                  height="1"
                                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td align="center" height="30"
                                                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table> <!-- End of separator -->


                                  <table width="100%" align="center" border="0"
                                    cellpadding="0" cellspacing="0"
                                    class="devicewidthinner">
                                    <tbody background: #f9f8f1;color: #000;>

                                      <!-- title2 -->
                                      <!-- end of title2 -->

                                      <!-- content2 -->
                                      <tr>
                                        <td width="50%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: left; line-height: 24px;"
                                          st-content="3col-content1">&nbsp;</td>

                                        <td width="35%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: right; line-height: 24px; padding: 5px;"
                                          st-content="3col-content1">
                                          ${getString("ORDER_PLACED_SUBTOTAL_TITLE")}:<br/>
                                          ${getString("ORDER_PLACED_DISCOUNT_TITLE")}:<br/>
                                          ${getString("ORDER_PLACED_TAX_TITLE")}:<br/>
                                          ${getString("ORDER_PLACED_SHIPPING_TITLE")}:<br/>
                                          <strong>${getString("ORDER_PLACED_TOTAL_TITLE")}:</strong>
                                        </td>
                                        <td width="15%"
                                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #687078; text-align: right; line-height: 24px; padding: 5px;"
                                          st-content="3col-content1">
                                          ${data.subtotal}<br/>
                                          ${data.discount}<br/>
                                          ${data.tax}<br/>
                                          ${data.shipping}<br/>
                                          <strong>${data.total}</strong>
                                        </td>
                                      </tr>
                                      <!-- end of content2 -->
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <!-- end of text content table -->
                            </tbody>
                          </table>
                    </tbody>
                  </table>
                </td>
                <!-- spacing -->
                <!-- end of spacing -->
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- end of Cart -->




  <!-- Start of separator -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" align="center" cellspacing="0" cellpadding="0"
            border="0" class="devicewidth">
            <tbody>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td width="550" align="center" height="1"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" height="30"
                  style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of separator -->
  <!-- Start of Email Footer -->
  <table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;color:#000">
    <tbody>
      <tr>
        <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0"
            align="center" class="devicewidth">
            <tbody>
              <tr>
                <td width="100%">
                  <table width="600" cellpadding="0" cellspacing="0" border="0"
                    align="center" class="devicewidth">
                    <tbody>
                      <tr>
                        <td align="center" valign="middle"
                          style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #666666"
                          st-content="postfooter">
                          <#if data.sitename??>
                            ${getStringNotEscaped("ORDER_PLACED_THANKS_TEXT", '<a href="'+data.storefrontUrl+'">'+ data.sitename +'</a>')}
                          <#else>
                            ${getStringNotEscaped("ORDER_PLACED_THANKS_TEXT", '<a href="'+data.storefrontUrl+'"/>')}
                          </#if>
                        </td>
                      </tr>
                      <!-- Spacing -->
                      <tr>
                        <td width="100%" height="20"></td>
                      </tr>
                      <!-- Spacing -->
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- End of Email Footer -->