<table style = "margin: 0 auto;width: 750px;background-color:#f4f2e5;"> 
    <tr>
        <td>
        <div style=" text-align: center;"> <img src = "${data.storefrontUrl}/file/general/fab_india_logo.png" alt="" border="0" width="400" height="83" style="width:200px;text-align: center;border:none;margin: 0 auto;text-decoration:none;"></div>
            <h1 style = "padding:28px 0 28px 0;font-size: 20px;font-weight: 700;font-family: Verdana, Geneva, sans-serif;;text-align: center;margin-bottom: 0;background-color: #f9f8f1;">FORGOT PASSWORD</h1>
            <p style = "font-size: 14px;font-family: Verdana, Geneva, sans-serif;padding: 0 13%;">${getString("PASSWORD_RESET_SALUTATION", data.firstName)}</p></br>
            <p style = "font-family: Verdana, Geneva, sans-serif;padding: 0 13%;">Greetings from <a href = "www.fabindia.com">www.fabindia.com</a></p>
            <p style = "font-family: Verdana, Geneva, sans-serif;padding: 0 13%;">We have received a request to reset the password for your account</p>
            <p style = "font-family: Verdana, Geneva, sans-serif;padding: 0 13%;">Please reset your password by clicking <a style = "font-family: verdana;font-size: 14px;" href="${data.resetPasswordLink}">here</a></p>
            <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding: 0 13%;">If the link is not clickable, please copy the url shown below and paste in your browser address bar to visit the reset page. </p>
            <a style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding: 0 13%;" href="${data.resetPasswordLink}">resetPassword</a></br>
           <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding: 0 13%;">If you do not wish to reset your password, please disregard this message.</p></br>
           <b style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding: 0 13%;">Hope this helps!</b>
            <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;margin-top:0;padding: 0 13%;">Best Regards, </p>
            <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;padding: 0 13%;">Team <a style = "font-family: verdana;font-size: 14px;" href = "www.fabinida.com">www.fabindia.com</a></p>
            <p style = "font-family: Verdana, Geneva, sans-serif;font-size: 12px;text-align: center;border-top: 1px solid grey;width: 81%;margin: 33px auto;">This is an auto-generated email. Please do not reply to the same </p>
        </td>
    </tr>
    </table>

