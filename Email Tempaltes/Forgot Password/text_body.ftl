${getString("PASSWORD_RESET_SALUTATION", data.firstName)}

${getString("PASSWORD_RESET_LINE_1")}

${data.resetPasswordLink}

${getString("PASSWORD_RESET_LINE_2")}

${getString("PASSWORD_RESET_SENT_SIGNATURE_TEXT")}
