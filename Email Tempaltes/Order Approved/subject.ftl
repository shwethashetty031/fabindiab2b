<#if data.sitename??>
  ${getString("ORDER_APPROVED_SUBJECT", data.sitename, data.orderId)}
<#else>
  ${getString("ORDER_APPROVED_SUBJECT", "", data.orderId)}
</#if> 