<#if data.sitename??>
  ${getString("ORDER_PLACED_FOR_APPROVAL_SUBJECT", data.sitename, data.orderId)}
<#else>
  ${getString("ORDER_PLACED_FOR_APPROVAL_SUBJECT", "", data.orderId)}
</#if>