/**
 * @fileoverview Shopping Cart Summary Widget.
 * 
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'notifier', 'jquery','CCi18n'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, pubsub,  notifier, $, CCi18n) {

    "use strict";
    var widgetModel;
    var effectiveCreditLimit;

    return {
      WIDGET_ID: 'b2bCart',
      bannerUrl : ko.observable(),
      balanceCreditLimit: ko.observable(),
      availableCreditLimit: ko.observable(),
      showLoader: ko.observable(false),
      reservedArray :ko.observable(),
      creditLimit :ko.observable(),
      // Sends a message to the cart to remove this product
      handleRemoveFromCart: function (data) {
        $.Topic(pubsub.topicNames.CART_REMOVE).publishWith(
          this.productData(), [{ "message": "success", "commerceItemId": this.commerceItemId }]);
          
        //GTM implementation - setCartCookie method is called to update the cookie value after removing product from cart
        widgetModel.setCartCookie("cartDetails", widgetModel.setCartCookieObject(), 30);
        
      },

      //this function increases the quantity of an item.
      quantityIncrementer: function (data, event, id) {
        var qty = data.updatableQuantity();
        qty = qty + 1;
        data.updatableQuantity(qty);
        widgetModel.updateQuantity(data, event, id, 'add');
      },

      //this function decreases the quantity of an item.
      quantityDecrementer: function (data, event, id) {
        var qty = data.updatableQuantity();
        qty = qty - 1;
        if (qty > 1) {
          data.updatableQuantity(qty);
        }
        else {
          data.updatableQuantity(1);
        }
        widgetModel.updateQuantity(data, event, id, 'remove');
      },

      updateQuantity: function (data, event, id, quantityStatus) {

        if ('click' === event.type || ('keypress' === event.type && event.keyCode === 13)) {
          if (data.updatableQuantity && data.updatableQuantity.isValid()) {

            $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY).publishWith(
              data.productData(), [{ "message": "success", "commerceItemId": data.commerceItemId }]);
            
    
            var button = $('#' + id);
            button.focus();
            button.fadeOut();

            //GTM implementation - setCartCookie method is called to update the cookie value after adding or reducing quantity of product.
            widgetModel.setCartCookie("cartDetails", widgetModel.setCartCookieObject(), 30);
          }
        } else {
          this.quantityFocus(data, event);
        }

        return true;
      },

      // for International restrictions
      

      quantityFocus: function (data, event) {
        var field = $('#' + event.target.id);
        var button = field.siblings("p").children("button");
        button.fadeIn();
      },

     

      setExpandedFlag: function (element, data) {
        if (data.expanded()) {
          data.expanded(false);
        } else {
          data.expanded(true);
        }
      },

      onLoad: function (widget) {
        widgetModel = widget;
        ko.bindingHandlers.showQtyErrorHandler = {
            init: function(element, valueAccessor, allBindings, viewModel, bindingContext){},
            update: function(element, valueAccessor, allBindings, viewModel, bindingContext){
                var data = ko.unwrap(valueAccessor());
                var errorMessage = "";
                var $el = $(element);
                if(widgetModel.reservedArray() && Object.keys(widgetModel.reservedArray()).length > 0){
                    var reserveQtyForSku = ko.unwrap(widgetModel.reservedArray()[data.catRefId]);
                    var availableQty = parseInt(data.orderableQuantity) - parseInt((reserveQtyForSku));
                    if(availableQty < 1) {
                        errorMessage = widgetModel.translate('itemNotAvailable');
                        ko.applyBindingsToNode(element, { html: errorMessage });
                        $(element).attr("data-valid", false);
                        $el.show();
                        return;
                    }
                    if(availableQty < data.updatableQuantity()) {
                        errorMessage = CCi18n.t('ns.b2bshoppingcartsummary:resources.itemExceededAvailability', { availableQty: availableQty });
                        ko.applyBindingsToNode(element, { html: errorMessage });
                        $el.show();
                        $(element).attr("data-valid", false);
                    } else {
                        $el.hide();
                        $(element).attr("data-valid", true);
                    }  
                }
            }
        }
        ko.validation.makeBindingHandlerValidatable('showQtyErrorHandler');
        
        widget.creditLimit.subscribe(function(cartTotal){
            var balanceLimit = Number(widgetModel.availableCreditLimit()) - Number(cartTotal);
                if(balanceLimit){
                widgetModel.balanceCreditLimit(balanceLimit.toFixed(2));
                $.Topic("CREDIT_LIMIT_UPDATED.memory").publishWith([{ "message": "success", "balanceCreditLimit": widgetModel.balanceCreditLimit()}]);
                }
        });
        widget.totalUpdate = ko.computed(function() {
            widget.creditLimit(widget.cart().total());
            return widget.cart().total() || widgetModel.availableCreditLimit();
            }, widget);
      },
      getCreditLimitValues: function(){
        $.ajax({
            url: "/ccstorex/custom/getOrdersForHistory"  + '?q=organizationId="' + widgetModel.user().parentOrganization.id() + '" AND (state="PENDING_APPROVAL")&fields=items.priceInfo.total',
            dataType: "json",
            type: 'GET',
            contentType: "application/json",
            headers: {
              'X-CCOrganization': widgetModel.user().parentOrganization.id(),
              'x-ccsite': widgetModel.site().siteInfo.repositoryId
            },
            // async: false,
            success: function(data) {
                var pendingOrders=0;
                data.items.forEach(function(item,index){
                    pendingOrders += Number(item.priceInfo.total);
                });
                widgetModel.availableCreditLimit((Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(pendingOrders)).toFixed(2));
                widgetModel.balanceCreditLimit((Number(widgetModel.availableCreditLimit()) - Number(widgetModel.cart().total())).toFixed(2));
                $.Topic("CREDIT_LIMIT_UPDATED.memory").publishWith([{ "message": "success", "balanceCreditLimit": widgetModel.balanceCreditLimit() }]);
            }
        });
      },
      
      //GTM implementation - This method is responsible to create the cookie with all products data in cart.
      setCartCookie: function(cookieName,cookieValue,cookieExpDays) {
        var date = new Date();
        var cookieExpires = "expires=" + date.toUTCString();
      var cookiepath  = "path=/";
      if(cookieName !== null && cookieValue !== null && cookieExpDays !== null){
        date.setTime(date.getTime() + (cookieExpDays * 24 * 60 * 60 * 1000));
          document.cookie = cookieName + "=" + cookieValue; 
      }
      },

      //GTM implementation - This method is responsible to create the cookieObject which is passed while creating cookie as its value.
      setCartCookieObject : function() {
        var cartCookieObject = "[";
        var itemLengthCount = 0;
            
        if(widgetModel.cart().items().length > 0) {

            widgetModel.cart().items().forEach(function(val,i) {
                var itemTotal = widgetModel.cart().currency.currencyCode === "INR" ? Math.round(widgetModel.cart().items()[i].detailedItemPriceInfo()[0].detailedUnitPrice * widgetModel.cart().items()[i].quantity()) : (widgetModel.cart().items()[i].detailedItemPriceInfo()[0].detailedUnitPrice * widgetModel.cart().items()[i].quantity());

                if(itemLengthCount > 0){
                   cartCookieObject = cartCookieObject + ",{\"p\":\"" + widgetModel.cart().items()[i].productId + "\",";
                }else{
                    cartCookieObject = cartCookieObject + "{\"p\":\"" + widgetModel.cart().items()[i].productId + "\",";
                }

                cartCookieObject = cartCookieObject + "\"v\":\"" + widgetModel.cart().items()[i].catRefId + "\",";
                cartCookieObject = cartCookieObject + "\"q\":" + widgetModel.cart().items()[i].quantity() + ",";
                cartCookieObject = cartCookieObject + "\"f\":{}" + ",";
                cartCookieObject = cartCookieObject + "\"t\":\"" + itemTotal  + "\",";
                cartCookieObject = cartCookieObject + "\"d\":\"" + "H\"" + "}";

                itemLengthCount++;

            });
        }

        cartCookieObject = cartCookieObject + "]";
        
        return cartCookieObject;
      },
      
      beforeAppear : function(page) {
        //GTM implementation - setCartCookie method is called when cart page loads.
        widgetModel.setCartCookie("cartDetails", widgetModel.setCartCookieObject(), 30);
        widgetModel.getCreditLimitValues();
        widgetModel.getReservedQtyForCart();
         $.Topic("RESERVED_QUANTITY_UPDATED.memory").subscribe(function(data){
            widgetModel.reservedArray(data);
        });
      },
      getReservedQtyForCart: function(){
        setTimeout(function(){
            var skuArray = [];
            widgetModel.cart().items().forEach(function(val,i) {
                skuArray.push(val.catRefId);
            });   
            var inputData = {skus: skuArray.join()};
            $.ajax({
                url: widgetModel.translate('getReservedUrl'),
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(inputData),
                success: function(resp) {
                  var reservedArrayJS = {};
                  for (var j = 0; j < resp.inventories.length; j++) {
                    reservedArrayJS[resp.inventories[j].skuId] = resp.inventories[j].quantity;
                  }
                  widgetModel.reservedArray(ko.mapping.fromJS(reservedArrayJS));
                  widgetModel.showLoader(false);
                }
            });
        },300); 
      },
    };
  }
);
