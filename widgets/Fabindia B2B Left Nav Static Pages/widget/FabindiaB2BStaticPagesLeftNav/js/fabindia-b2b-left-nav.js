/**
 * @fileoverview footer Widget.
 * 
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub','pageLayout/site'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, pubsub, SiteViewModel) {
  
    "use strict";
    var widgetModel;
        
    return {
      footerSection:   ko.observableArray(),
      
      onLoad: function(widget) {

        if(widget.leftNavLinks() !== ""){
          if(SiteViewModel.getInstance().selectedPriceListGroup().id.toLowerCase() === "inr"){
            widget.footerSection(JSON.parse(widget.leftNavLinks()));
                }else{
                    
                 var parsedObj = JSON.parse(widget.leftNavLinks());
                  for(var i in parsedObj){
                  if(parsedObj[i] == JSON.parse(widget.site().extensionSiteSettings.globalSiteSettings.payuOffer).amexOffer.footerLink){
                      console.log(parsedObj[i])
                      parsedObj.splice(i,1);
                    }
                     widget.footerSection(parsedObj);
                 }
             }
        }
      },
      
      beforeAppear: function(page){}
    };
  }
);
