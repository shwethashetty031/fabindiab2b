<!-- ko if: initialized() && $data.elements.hasOwnProperty('b2b-search') -->
  <form role="form" data-bind="submit: function(data, event) { $data['elements']['b2b-search'].handleSearch.bind($data['elements']['b2b-search'], $data['elements']['b2b-search'], event)() }, event: { keydown : $data['elements']['b2b-search'].handleKeyPress }">
    <!-- ko with: $data['elements']['b2b-search'] -->
      <label for="CC-headerWidget-Search" class="label-hidden" data-bind="widgetLocaleText :'searchLabel'">
        Search
      </label>
      <div class="col-xs-10 input-group search">
        <input id="CC-headerWidget-Search" type="text" class="form-control search-query" autocomplete="off"
          data-bind="widgetLocaleText: {attr: 'placeholder', value: 'searchPlaceholder'}, value: searchText, valueUpdate: 'afterkeydown', onRender:initializeSearch.bind($parent), event: { focus: searchSelected}" />
        <div class="input-group-btn">
          <button type="submit" id="searchSubmit" class="btn btn-default form-control" data-bind="event: { keydown : hideSearchDropdown}">
            <span class="sr-only" data-bind="widgetLocaleText :'searchButtonText'"></span>
            	<span class="search_icon"><img src="/file/general/magnifying-glass_n.svg" alt="search_icon"</span>
          </button>
        </div>
      </div>
    <!-- /ko -->
  </form>
<!-- /ko -->
