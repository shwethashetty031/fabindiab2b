define(
	// -------------------------------------------------------------------
	// DEPENDENCIES
	// -------------------------------------------------------------------
	[ 'jquery', 'knockout', 'notifier', 'ccPasswordValidator', 'pubsub',
		'CCi18n', 'ccConstants', 'navigation', 'ccLogger',
		'notifications', 'storageApi', 'ccRestClient'],

	// -------------------------------------------------------------------
	// MODULE DEFINITION
	// -------------------------------------------------------------------
	function($, ko, notifier, CCPasswordValidator, pubsub, CCi18n,
		CCConstants, navigation, ccLogger, notifications,  storageApi, CCRestClient) {

	    "use strict";
	    
	    var HEADER_ORGANIZATION_PICKER = '#headerOrganizationPicker';

	    return {
		elementName : 'b2b-contact-login-for-managed-accounts',

		modalMessageType : ko.observable(''),
		modalMessageText : ko.observable(''),
		showErrorMessage : ko.observable(false),
		userCreated : ko.observable(false),
		ignoreBlur : ko.observable(false),
		defaultOrganization : ko.observable(),
		secondaryOrganizations : ko.observableArray([]),
		organizationDropdownVisible : ko.observable(false),
		currentOrganizationPosition : ko.observable(),
		organizationName : ko.observable(),
		noMatchFound : ko.observable(false),
		showPicker : ko.observable(false),

		onLoad : function(widget) {
		    var self = this;
		    $.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(self.loadPicker.bind(self, widget.user()));
		    self.successFunc = function(data) {
			self.secondaryOrganizations.removeAll();
			if(data.items.length == 0){
			  self.noMatchFound(true);
			}
			for ( var i = 0; i < data.items.length; i++) {
			  self.noMatchFound(false);
			  self.secondaryOrganizations.push(data.items[i]);
			}
		    }
		    self.errorFunc = function(error) {
		    	notifier.sendError(widget.WIDGET_ID,
				error.message, true);
		    }
		    self.organizationName.subscribe(function(pOrgName) {
				if (pOrgName.length >= 1) {
					var inputData = {};
					inputData[CCConstants.OFFSET] = 0;
					inputData[CCConstants.LIMIT] = 20;
					inputData[CCConstants.Q] = 'name co \"' + pOrgName + '\"';
					CCRestClient.request(CCConstants.ENDPOINT_B2B_ADMINISTRATION_LIST_ORGANIZATIONS,
						inputData, self.successFunc.bind(this),
						self.errorFunc.bind(this));
				} else {
					self.noMatchFound(false);
					self.loadPicker(widget.user());
				}
		    }, this);
		    
		    self.loadPicker(widget.user());
		    widget.user().ignoreEmailValidation(false);
		    
		    self.handleOrganizationChange = function(orgPosition) {
			var selectedOrganization = this;
			var displayedOrganization = widget.user().currentOrganization();

			// check if the cart contains any item with child-items
			// (i.e. check for Configurable Product)
			self.currentOrganizationPosition(orgPosition);
			if (self.getRepositoryId(selectedOrganization) != self.getRepositoryId(displayedOrganization)) {
			    self.storeOrganizationInLocalStorage(selectedOrganization);
			    widget.cart().clearCartForProfile();
			    var newURL = navigation.getBaseURL();
			    window.location.assign(newURL);
			}
			  self.hideOrganizationDropDown();
		    }

		    // To display success notification after redirection from
		    // customerProfile page.
		    if (widget.user().delaySuccessNotification()) {
		      notifier.sendSuccess(widget.WIDGET_ID, widget
				.translate('updateSuccessMsg'), true);
			  widget.user().delaySuccessNotification(false);
		    }
		},

		loadPicker : function(pUserData) {

		    var self = this;
		    self.organizationDropdownVisible(false);
		    
		    if(pUserData.loggedIn() && !pUserData.isUserLoggedOut() && !pUserData.isUserSessionExpired()){
		    	
		      self.secondaryOrganizations.removeAll();
		      var inputData = {};
		      inputData[CCConstants.OFFSET] = 0;
		      inputData[CCConstants.LIMIT] = 20;
		      CCRestClient.request(CCConstants.ENDPOINT_B2B_ADMINISTRATION_LIST_ORGANIZATIONS,
		      inputData,function(data) {
			    if (data && data .items) {
				  var displayedOrgId = self.getRepositoryId(pUserData.currentOrganization());
			      self.secondaryOrganizations.removeAll();
			      if(data.items.length>1){
			    	  self.showPicker(true);
			      }
			      else{
			    	  self.showPicker(false);
			      }
			      for ( var i = 0; i < data.items.length; i++) {
			        if (self.getRepositoryId(data.items[i]) !== displayedOrgId){
			          self.secondaryOrganizations.push(data.items[i]);
			        }
			      }
			     }
			   }, 
			  function(error) {
				 notifier.sendError("header", error.message, true);
			  });
		    }
		 },

		removeMessageFromPanel : function() {
		    var message = this;
		    var messageId = message.id();
		    var messageType = message.type();
		    notifier.deleteMessage(messageId, messageType);
		},

	/**
       * Invoked when Logout method is called
       */
      handleB2BLogout: function (data) {

        $.Topic(pubsub.topicNames.USER_CLEAR_CART).publish([{
          message: "success"
        }]);
        var successFunc = function () {
          $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).publish([{
            message: "success"
          }]);
          storageApi.getInstance().removeItem(CCConstants.LOCAL_STORAGE_SHOPPER_CONTEXT);
          storageApi.getInstance().removeItem(CCConstants.LOCAL_STORAGE_CURRENT_CONTEXT);
        //   ccRestClient.clearStoredValue(CCConstants.LOCAL_STORAGE_PRICELISTGROUP_ID);
          data.clearUserData();
          data.profileRedirect();
          // if (data.refreshPageAfterContactLogout()) {
          //   data.refreshPageAfterContactLogout(false);
          //   //window.location.assign(data.contextData.global.links.home.route);
          // } else {
          //Refreshing layout to set Content Variation Slots, if any.
          var eventData = {
            'pageId': navigation.getPath()
          };
          $.Topic(pubsub.topicNames.PAGE_VIEW_CHANGED).publish(eventData);
          //}
        };
        var errorFunc = function (pResult) {
          data.clearUserData();
          data.profileRedirect();
          //window.location.assign(self.contextData.global.links.home.route);
        };
        if (data.loggedIn()) {
          if (data.parentOrganization && data.parentOrganization.name()) {
            data.refreshPageAfterContactLogout(true);
          }
          data.client().logout(successFunc, errorFunc);
        } else {
          data.clearUserData();
          $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).publish([{
            message: "success"
          }]);
          data.profileRedirect();
        }
        // returns if the profile has unsaved changes.
        if (data.isUserProfileEdited()) {
          return true;
        }
        // Clearing the auto-login success message
        notifier.clearSuccess(this.WIDGET_ID);
        // Clearing any other notifications
        notifier.clearError(this.WIDGET_ID);
        data.updateLocalData(data.loggedinAtCheckout(), false);
        // $.Topic(pubsub.topicNames.USER_LOGOUT_SUBMIT).publishWith([{
        //   message: "success"
        // }]);
      },

		/**
		 * Shows the Organization dropdown based on visible flag
		 */
		showOrganizationDropDown : function(data) {
		    var self = this;
		    self.organizationName('');
		    $(HEADER_ORGANIZATION_PICKER).addClass('active');

		    // Tell the template its OK to display the currency picker.
		    self.loadPicker(data);
		    notifications.emptyGrowlMessages();

		    $(document).on('mouseleave', HEADER_ORGANIZATION_PICKER,
			    function() {
				self.hideOrganizationDropDown();
			    });

		    // to handle the mouseout/mouseleave events for ipad for
		    // currency-picker
		    var isiPad = navigator.userAgent
			    .match(CCConstants.IPAD_STRING) != null;
		    if (isiPad) {
			$(document)
				.on(
					'touchend',
					function(event) {
					    if (!($(event.target)
						    .closest(HEADER_ORGANIZATION_PICKER).length)) {
						self.hideOrganizationDropDown();
					    }
					});
		    }
		    self.organizationDropdownVisible(true);
		},

		/**
		 * Hides the organization dropdown based on visible flag
		 */
		hideOrganizationDropDown : function() {
		    // Tell the template the currency should no longer be
		    // visible.
		    this.organizationDropdownVisible(false);
		    $(HEADER_ORGANIZATION_PICKER).removeClass('active');
		    return true;
		},

		/**
		 * Adds the passed price list group to window local storage
		 */
		storeOrganizationInLocalStorage : function(pOrganization) {
		    var self = this;
		    var displayedOrgId = self.getRepositoryId(pOrganization);
		    CCRestClient.setStoredValue(CCConstants.LOCAL_STORAGE_ORGANIZATION_ID, 
		    		ko.toJSON(displayedOrgId));
		},

		/**
		 * Toggles the currency dropdown to show/hide it upon click on
		 * link
		 */
		toggleOrganizationsDropDown : function(data) {
		    if ($(HEADER_ORGANIZATION_PICKER).hasClass('active')) {
			this.hideOrganizationDropDown();
		    } else {
			this.showOrganizationDropDown(data);
		    }
		},

		/**
		 * fetches the repositoryId of an object
		 */
		getRepositoryId : function(pObject) {
		    var repoId = null;
		    if(pObject != null){
		      if (ko.isObservable(pObject.repositoryId)) {
			    repoId = pObject.repositoryId();
		      } else {
			    repoId = pObject.repositoryId;
		      }
		    }
		    return repoId;
		  },
	    };
	});
