/**
 * @fileoverview B2B Reports Widget.
 * 
 * @author Adapty
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'notifier', 'pubsub',
		'CCi18n', 'ccConstants', 'navigation', 'ccLogger',
		'notifications', 'storageApi', 'ccRestClient', 'ccStoreConfiguration', 'viewModels/purchaseList', 'viewModels/purchaseListListing'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, notifier, pubsub, CCi18n,
		CCConstants, navigation, ccLogger, notifications,  storageApi, CCRestClient, CCStoreConfiguration, PurchaseListViewModel, PurchaseListListingViewModel) {
    
    "use stricts";
    
    var widgetModel;
    
    return {
        
      WIDGET_ID:"b2bReports",
      
      accounts: ko.observableArray([]),
      purchaseLists: ko.observableArray([]),
      storeConfiguration: CCStoreConfiguration.getInstance(),
      
      beforeAppear: function (page) {

      },

      onLoad: function (widget) {
          
        widgetModel = widget;
        
        widget.purchaseListViewModel = ko.observable();
        widget.purchaseListViewModel(new PurchaseListViewModel());
        widget.purchaseListListingViewModel = ko.observable();
        widget.purchaseListListingViewModel(new PurchaseListListingViewModel());
      },
        
      hideReportSection: function(data){
          var localArr = [];
          widgetModel.user().roles().forEach(function(val,i){
              localArr.push(val.function);
          });
          if(($.inArray( "admin", localArr) === -1) && ($.inArray( "approver", localArr) === -1) && ($.inArray( "profileAddressManager", localArr) === -1) && ($.inArray( "accountAddressManager", localArr) === -1)){
              $('#verticalTabs-profileStack_1-tab-7').css("display", "none");
          }else{
              if(widgetModel.user().loggedIn() && !widgetModel.user().isUserLoggedOut() && !widgetModel.user().isUserSessionExpired()){
		    	
		      var inputData = {}, ids = [];
		      inputData[CCConstants.OFFSET] = 0;
		      inputData[CCConstants.LIMIT] = 20;
		      CCRestClient.request(CCConstants.ENDPOINT_B2B_ADMINISTRATION_LIST_ORGANIZATIONS,
        	      inputData,function(data) {
        		    if (data && data.items) {
        		        data.items.forEach(function(val, i){
        		            widgetModel.accounts.push(val);
          			        ids[i] = val.id;
        		        });
          			        widgetModel.fetchAllPurchaselist(ids);
        		     }
			     
        		   }, 
        		  function(error) {
        			 notifier.sendError("header", error.message, true);
        		  });
        	    }
            }
      },
      fetchAllPurchaselist: function(data){
            self = this;
            var url, queryString = "",inputData = {};
            queryString += '( siteId eq "' + widgetModel.site().siteInfo.id + '"' + " or siteId eq null )";
            inputData.sort = "name:asc"
            if (data) {
                queryString = queryString + ' ( accountId eq "' + data + '"' + " or accountId eq null )"
            }
            inputData[CCConstants.OFFSET] = 0;
            inputData[CCConstants.PURCHASE_LIST_TYPE] = "owner";
        
            inputData["q"] = queryString;
            var contextObj = {};
            contextObj[CCConstants.ENDPOINT_KEY] = CCConstants.ENDPOINT_LIST_PURCHASE_LISTS;
            var filterKey = widgetModel.storeConfiguration.getFilterToUse(contextObj);
            if (filterKey) {
                inputData[CCConstants.FILTER_KEY] = filterKey
            }
            url = CCConstants.ENDPOINT_LIST_PURCHASE_LISTS;
            if (CCRestClient.profileType === CCConstants.PROFILE_TYPE_AGENT) {
                widgetModel.agentContextmanager = require("agentViewModels/agent-context-manager");
                if (true != storageApi.getInstance().readFromMemory(CCConstants.CC_IS_SSO_LOGIN_UNDER_PROGRESS) && widgetModel.agentContextmanager.getInstance().getShopperProfileId()) {
                    CCRestClient.request(url, inputData, function(successData){
                                    var group = [], groubedById;
                                    var key = 'accountId' ;
                                    group = function (xs, key) {
                                        return xs.reduce(function (rv, x) {
                                          (rv[x[key]] = rv[x[key]] || []).push(x);
                                          return rv;
                                        }, {});
                                      };
                                    groubedById = Object.values(group(successData.items, 'accountId'))
                                    widgetModel.purchaseLists(groubedById);
                                    widgetModel.purchaseLists.valueHasMutated();
                                }, function(error){
                                    console.log("ERROR :: ",error);
                                });
                                
                }
            }else{
                if (true != storageApi.getInstance().readFromMemory(CCConstants.CC_IS_SSO_LOGIN_UNDER_PROGRESS)) {
                CCRestClient.request(url, inputData, function(successData){
                                var group = [], groubedById;
                                var key = 'accountId' ;
                                group = function (xs, key) {
                                    return xs.reduce(function (rv, x) {
                                      (rv[x[key]] = rv[x[key]] || []).push(x);
                                      return rv;
                                    }, {});
                                  };
                                groubedById = Object.values(group(successData.items, 'accountId'))
                                widgetModel.purchaseLists(groubedById);
                                widgetModel.purchaseLists.valueHasMutated();
                            }, function(error){
                                console.log("ERROR :: ",error);
                            });
                            
            }
            }
        },
        
     
    };
  }
);