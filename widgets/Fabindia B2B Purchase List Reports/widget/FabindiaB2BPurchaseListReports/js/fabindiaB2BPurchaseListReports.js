/**
 * @fileoverview B2B Purchase List Reports Widget.
 * 
 * @author Adapty
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'notifier', 'pubsub',
    'CCi18n', 'ccConstants', 'navigation', 'ccLogger',
    'notifications', 'storageApi', 'ccRestClient', 'ccStoreConfiguration'
  ],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, notifier, pubsub, CCi18n,
    CCConstants, navigation, ccLogger, notifications, storageApi, CCRestClient, CCStoreConfiguration) {

    var widgetModel;

    return {

      listData: ko.observable(),
      accountId: ko.observable(),
      storeConfiguration: CCStoreConfiguration.getInstance(),

      beforeAppear: function (page) {
        if (page.contextId) {
          widgetModel.accountId(page.contextId);
        }
        widgetModel.listData([]);
        widgetModel.fetchAllPurchaselist();
      },

      onLoad: function (widget) {
        widgetModel = widget;
        if (!widgetModel.accountId()) {
          widgetModel.accountId(window.location.pathname.split("/").pop());
        }
      },

      fetchAllPurchaselist: function () {
        self = this;
        var url, queryString = "",
          inputData = {};
        queryString += '( siteId eq "' + widgetModel.site().siteInfo.id + '"' + " or siteId eq null )";
        inputData.sort = "name:asc"
        if (widgetModel.accountId()) {
          queryString = queryString + ' ( accountId eq "' + widgetModel.accountId() + '"' + " or accountId eq null )"
        }
        inputData[CCConstants.OFFSET] = 0;
        inputData[CCConstants.PURCHASE_LIST_TYPE] = "owner";

        inputData["q"] = queryString;
        var contextObj = {};
        contextObj[CCConstants.ENDPOINT_KEY] = CCConstants.ENDPOINT_LIST_PURCHASE_LISTS;
        var filterKey = widgetModel.storeConfiguration.getFilterToUse(contextObj);
        if (filterKey) {
          inputData[CCConstants.FILTER_KEY] = filterKey
        }
        url = CCConstants.ENDPOINT_LIST_PURCHASE_LISTS;
        if (true != storageApi.getInstance().readFromMemory(CCConstants.CC_IS_SSO_LOGIN_UNDER_PROGRESS)) {
          CCRestClient.request(url, {}, function (successData) {
               var itemsArray = successData.items.filter(function(item, index){
                      return item.accountId === widgetModel.accountId();
               });
               widgetModel.listData(itemsArray);
          }, function (error) {
            console.log("ERROR :: ", error);
          }, widgetModel.accountId());
        }
      },
    };
  }
);