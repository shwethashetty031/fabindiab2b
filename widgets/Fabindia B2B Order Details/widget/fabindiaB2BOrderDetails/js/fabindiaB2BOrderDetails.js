/**
 * @fileoverview Order History Details.
 * 
 * @author shsatpat
 */
define(
 
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'notifier', 'CCi18n', 'ccConstants', 'navigation', 'ccRestClient','spinner',
   'viewModels/paymentsViewModel'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, notifier, CCi18n, CCConstants, navigation,  ccRestClient, spinner, paymentsContainer) {
  
    "use strict";

    return {
      /** Widget root element id */
      WIDGET_ID: 'orderDetailsRegion',
      /** Default options for creating a spinner on price*/
      orderDetailsBodyIndicator: '#CC-orderDetails-body',
      selectedOrder: ko.observable(),
      orderDetailsBodyIndicatorOptions : {
        parent:  '#CC-orderDetails-body',
        posTop: '50%',
        posLeft: '50%'
      },
      display : ko.observable(true),
	  //The array to store all the checked product
	  selectedProduct : ko.observableArray([]),

        onLoad : function(widget) {

          widget.secondaryCurrency = ko.observable(null);

          widget.showSecondaryShippingData = ko.pureComputed(function(){
            return widget.orderDetails().payShippingInSecondaryCurrency &&
                     (null != widget.secondaryCurrency());
          });
          
          widget.showSecondaryTaxData = ko.pureComputed(function(){
            return widget.cart().payTaxInSecondaryCurrency &&
                     (null != widget.secondaryCurrency());
          });
        	
        	
        	var isModalVisible = ko.observable(false);
        	var isModalNoClicked = ko.observable(false);
        	var isModalYesClicked = ko.observable(false);
        	
        	widget.cancelReasons=ko.observable();
        	widget.selectedCancelReason=ko.observable();
        	widget.isCancelAllowed=ko.observable(false);
        	widget.isReturnAllowed=ko.observable(false);
        	
        	widget.display(false);
        	widget.hideModal = function () {
                if(isModalVisible()) {
                  $("#CC-orderDetails-modal").modal('hide');
                  $('body').removeClass('modal-open');
                  $('.modal-backdrop').remove();
                }
              };
              
              widget.showModal = function () {
            	if($("#CC-orderDetails-modal").length){
                   $("#CC-orderDetails-modal").modal('show');
                   $('#CC-orderDetails-modal').on('hidden.bs.modal', function () {
                      if((!isModalYesClicked() && !isModalNoClicked() && isModalVisible())) {
                	  $("#CC-orderDetailsContextChangeMsg").show();
                      }
                   });
                   isModalVisible(true);
            	}
            	else{
            	   setTimeout(widget.showModal, 50);
            	}
              };
             
              widget.handleModalYes = function () {
                  isModalYesClicked(true);
                  widget.cart().clearCartForProfile();
                  ccRestClient.setStoredValue(
        				  CCConstants.LOCAL_STORAGE_ORGANIZATION_ID, ko
        			    	.toJSON(this.orderDetails().organizationId));
                  widget.hideModal();
                  window.location.assign(window.location.href);
                  widget.cart().loadCartForProfile();
                 };
                 
              widget.handleModalNo = function () {
            	   isModalNoClicked(true);
                   $("#CC-orderDetailsContextChangeMsg").show();
                   widget.hideModal();
                  };

          // Define a create spinner function with spinner options
          widget.createSpinner = function(pSpinner, pSpinnerOptions) {
            $(pSpinner).css('position', 'fixed');
            $(pSpinner).addClass('loadingIndicator');
            spinner.create(pSpinnerOptions);
          };

          // Define a destroy spinner function with spinner id
          widget.destroySpinner = function(pSpinnerId) {
            $(widget.orderDetailsBodyIndicator).css('position', 'relative');
            $(pSpinnerId).removeClass('loadingIndicator');
            spinner.destroyWithoutDelay(pSpinnerId);
          };
          widget.isGiftCardUsed = ko.computed(
                  function() {
                    if (widget.orderDetails()) {
                      var payments = widget.orderDetails().payments;
                      for ( var i = 0; i < payments.length; i++) {
                        if (payments[i].paymentMethod == CCConstants.GIFT_CARD_PAYMENT_TYPE && payments[i].paymentState == CCConstants.PAYMENT_GROUP_STATE_AUTHORIZED) {
                            return true;
                        }
                      }  
                    }
                  }, widget);
          widget.isScheduledOrder = ko.computed(
              function() {
                if (widget.orderDetails()) {
                  if(widget.orderDetails().scheduledOrderName){
                    return true;
                  }
                }
              }, widget);
          
          widget.totalAmount = ko.computed(
                  function() {
                    if (widget.orderDetails()) {
                      var payments = widget.orderDetails().payments;
                      var remainingTotal = 0;
                      for ( var i = 0; i < payments.length; i++) {
                        if (payments[i].paymentMethod != CCConstants.GIFT_CARD_PAYMENT_TYPE) {
                          remainingTotal += payments[i].amount;
                        }
                      }
                    }
                    return remainingTotal;
                  }, widget);

          widget.approverName = ko.computed(
            function(){
              if(widget.orderDetails() && widget.orderDetails().approvers){
                var approver=widget.orderDetails().approvers[0];
                if(approver.lastName != null){
                  return approver.firstName + " " + approver.lastName;
                } else{
                  return approver.firstName;
            }
              }
              return null;
            },widget);

          widget.approverMessage=ko.computed(
            function(){
              if(widget.orderDetails() && widget.orderDetails().approverMessages && widget.orderDetails().approverMessages.length > 0){
                return widget.orderDetails().approverMessages[0];
              }
              return CCConstants.NO_COMMENTS;
            },widget);

          widget.claimedCouponMultiPromotions = ko.pureComputed(
            function() {
              var coupons = new Array();
              if (widget.orderDetails()) {
                if (widget.orderDetails().discountInfo) {
                  for (var prop in widget.orderDetails().discountInfo.claimedCouponMultiPromotions) {
                    var promotions = widget.orderDetails().discountInfo.claimedCouponMultiPromotions[prop];
                    var couponMultiPromotion = [];
                    couponMultiPromotion.code = prop;
                    couponMultiPromotion.promotions = promotions;
                    coupons.push(couponMultiPromotion);
                  }
                }
              }
            return coupons;
          }, widget);

          /**
           * Function to check if complete Payment button should be displayed.
           */
          widget.isEligibleToCompletePayment = ko.computed(
            function(){
                //identify pending authorization payment group existence first for payU case
                var pendingAuthPaymentGroup = false;
                if(widget.orderDetails() && widget.orderDetails().payments){
                  var payments = widget.orderDetails().payments;
                  for ( var i = 0; i < payments.length; i++) {
                    if(payments[i].paymentState == CCConstants.PENDING_AUTHORIZATION){
                      pendingAuthPaymentGroup = true;
                      break;
                    }
                  }
                }
                // If order is pending payment and belongs to current user and does not
                //have pending authorization Payment Group then allow him to make payments
                if(widget.orderDetails() && widget.user() && widget.orderDetails().priceInfo &&
                   widget.orderDetails().state == CCConstants.ORDER_STATE_PENDING_PAYMENT &&
                   widget.orderDetails().orderProfileId == widget.user().id() &&
                   !pendingAuthPaymentGroup && widget.orderDetails().priceInfo.total > 0){
                  return true;
                }
              return false;
            },widget);
          
          // To append locale for scheduled orders link
          widget.detailsLinkWithLocale = ko.computed(
            function() {
              return navigation.getPathWithLocale('/scheduledOrders/');
          }, widget);
          
          widget.populatePaymentsViewModel = function() {
            var widget = this;
            var authorizedAmount = 0;
            var loyaltyAuthorizedAmount = 0;
            var completedPayments = [];
            var completedLoyaltyPayments = [];
            var isChargeShippingAndTaxInSecondaryCurrency = false;
            var currencySymbol = widget.orderDetails().priceListGroup.currency.symbol;

            if (widget.orderDetails().payShippingInSecondaryCurrency || widget.orderDetails().payTaxInSecondaryCurrency) {
              isChargeShippingAndTaxInSecondaryCurrency = true;
              currencySymbol = widget.secondaryCurrency() && widget.secondaryCurrency().symbol ? widget.secondaryCurrency().symbol : widget.orderDetails().priceListGroup.currency.symbol;
            }

            for (var i=0; i<widget.orderDetails().payments.length; i++) {
              if (widget.orderDetails().payments[i].paymentState == CCConstants.PAYMENT_GROUP_STATE_AUTHORIZED ||
                  widget.orderDetails().payments[i].paymentState == CCConstants.PAYMENT_GROUP_STATE_PAYMENT_REQUEST_ACCEPTED ||
            	    widget.orderDetails().payments[i].paymentState == CCConstants.PAYMENT_GROUP_PAYMENT_DEFERRED ||
            	    widget.orderDetails().payments[i].paymentState == CCConstants.PAYMENT_GROUP_STATE_SETTLED) {
                if (widget.orderDetails().payments[i].type === CCConstants.LOYALTY_POINTS_PAYMENT_TYPE && isChargeShippingAndTaxInSecondaryCurrency) {
                  loyaltyAuthorizedAmount = parseFloat(loyaltyAuthorizedAmount) + parseFloat(widget.orderDetails().payments[i].amount);
                  completedLoyaltyPayments.push(widget.generateCompletedPaymentsText(widget.orderDetails().payments[i], widget.orderDetails().priceListGroup.currency.symbol));
                } else {
                  authorizedAmount = parseFloat(authorizedAmount) + parseFloat(widget.orderDetails().payments[i].amount);
                  completedPayments.push(widget.generateCompletedPaymentsText(widget.orderDetails().payments[i], currencySymbol));
                }
              }
            }
            var dueAmount = 0;
            var loyaltyDueAmount = 0;
            if(isChargeShippingAndTaxInSecondaryCurrency){
              dueAmount = parseFloat(widget.orderDetails().priceInfo.secondaryCurrencyTotal) - parseFloat(authorizedAmount);
              loyaltyDueAmount = parseFloat(widget.orderDetails().priceInfo.primaryCurrencyTotal) - parseFloat(loyaltyAuthorizedAmount);
            } else {
              dueAmount = parseFloat(widget.orderDetails().priceInfo.total) - parseFloat(authorizedAmount);
            }
            // Update Payments View Model
            var paymentsViewModel = paymentsContainer.getInstance();
            // Reset the view model before adding the completed payments
            paymentsViewModel.resetPaymentsContainer();
            paymentsViewModel.paymentDue(dueAmount);
            paymentsViewModel.loyaltyPaymentDue(loyaltyDueAmount);
            paymentsViewModel.historicalCompletedPayments(completedPayments);
            paymentsViewModel.historicalCompletedLoyaltyPayments(completedLoyaltyPayments);
            widget.user().isHistoricalOrder = true;
          };
            
          widget.generateCompletedPaymentsText = function(pPaymentData, currencySymbol) {
            var type;
            var maskedNumber;
            if (pPaymentData.paymentMethod == CCConstants.TOKENIZED_CREDIT_CARD || pPaymentData.paymentMethod == CCConstants.CREDIT_CARD) {
              if(pPaymentData.cardType) {
                type = pPaymentData.cardType;
              } else {
              	type = CCConstants.CREDIT_CARD_TEXT;
              }
              maskedNumber = pPaymentData.cardNumber;
            } else if (pPaymentData.paymentMethod == CCConstants.GIFT_CARD_PAYMENT_TYPE) {
              type = CCConstants.GIFT_CARD_TEXT;
              maskedNumber = pPaymentData.maskedCardNumber;
            } else if (pPaymentData.paymentMethod == CCConstants.CASH_PAYMENT_TYPE) {
              type = CCConstants.CASH_PAYMENT_TYPE;
              maskedNumber = "";
            } else if(pPaymentData.paymentMethod == CCConstants.INVOICE_PAYMENT_METHOD) {
              type = CCConstants.INVOICE_PAYMENT_TYPE;
              maskedNumber = pPaymentData.PONumber;
            } else if(pPaymentData.paymentMethod == CCConstants.CUSTOM_CURRENCY_PAYMENT_TYPE) {
              type = CCConstants.LOYALTY_POINTS_PAYMENT_TYPE;
            }

            return (currencySymbol + pPaymentData.amount + widget.translate("toBeChargedPaymentText", {type: type, number: maskedNumber}));
          };

          widget.pendingApprovalReasons = ko.computed(
            function() {
              if (widget.orderDetails()) {
                var orderDetails = widget.orderDetails();
                if(orderDetails.approvalSystemMessages){
                  return orderDetails.approvalSystemMessages;
                }
              }
              return null;
          }, widget);

          widget.isAddToPurchaseListDisabled = ko.computed(
              function() {
                if (widget.getSelectedProducts().length > 0) {
                  return false;
                } else {
                  return true;
                }
              }, widget);

          //Keep updated in select all checkbox
          widget.selectedAllProduct= ko.pureComputed({
            read: function () {
              return widget.selectedProduct().length === widget.orderDetails().order.items.length;
            },
            
            write: function(value){
              widget.selectedProduct(value ? widget.orderDetails().order.items.slice(0) : []);
            }
          });


       /**
         * The method returns the value of isPurchaseListEnable flag as per configuration
         * specified in config.json
         */
         widget.checkPurchaseListEnabledFlag = function(){
           return widget.isPurchaseListEnable && widget.isPurchaseListEnable();

         };
        /**
          * The method returns the value of isShopperInitiatedReturnEnable flag as per configuration
          * specified in config.json
          */
         widget.checkShopperInitiatedReturnEnabledFlag = function(){
           return widget.isShopperInitiatedReturnEnable && widget.isShopperInitiatedReturnEnable();
         };
         /**
          * The method returns the value of isShopperInitiatedCancelEnable flag as per configuration
          * specified in config.json
          */          
        widget.checkShopperInitiatedCancelEnabledFlag = function(){
           return widget.isShopperInitiatedCancelEnable && widget.isShopperInitiatedCancelEnable();
         };

        },

         completePayment: function(){
           var widget = this;
           widget.populatePaymentsViewModel();

           this.cart().currentOrderId(this.orderDetails().id);
           this.cart().currentOrderState(this.orderDetails().state);
           this.user().validateAndRedirectPage(this.links().checkout.route+"?orderId="+this.orderDetails().id); 
         },
      
      beforeAppear: function (page) {
        var widget = this;
        widget.selectedProduct([]);
        var items = [];
        if (widget.orderDetails && widget.orderDetails() && widget.orderDetails().order) {
          items = widget.orderDetails().order.items;
        }
        $.when (widget.site().siteLoadedDeferred).done(function() {
          if (widget.orderDetails()) {
            var secondaryCurrency = widget.site().getCurrency(widget.orderDetails().secondaryCurrencyCode);
            if (widget.secondaryCurrency() || secondaryCurrency) {
              if (widget.secondaryCurrency() && secondaryCurrency && (widget.secondaryCurrency().currencyCode == secondaryCurrency.currencyCode)) {
                return;
              }
              widget.secondaryCurrency(secondaryCurrency);
            }
          }
        });
        var createExpandFlag = function (item) {
          for (var j = 0; j < item.childItems.length; j++) {
            item.childItems[j].expanded = ko.observable(false);
            if (item.childItems[j].childItems && item.childItems[j].childItems.length > 0) {
              createExpandFlag(item.childItems[j]);
            }
          }
        };
        for (var i = 0; items && i < items.length; i++) {
          var item = items[i];
          if (item.childItems && item.childItems.length > 0) {
            createExpandFlag(item);
          }
        }
        $("body").attr("id" ,"CC-orderDetails-body");
        if (!widget.orderDetails() || !widget.user().loggedIn() || widget.user().isUserSessionExpired()) {
          navigation.doLogin(navigation.getPath(), widget.links().home.route);
        }

        if(widget.user().currentOrganization()&&this.orderDetails().organizationId!=
        	widget.user().currentOrganization().repositoryId){
        	widget.display(false);
        	widget.showModal();
        }
        else if(widget.orderDetails() && widget.orderDetails().state == CCConstants.PENDING_APPROVAL){
        	widget.display(true);
          widget.createSpinner(widget.orderDetailsBodyIndicator,widget.orderDetailsBodyIndicatorOptions);
          var order = {};
          order[CCConstants.ORDER_ID] = widget.orderDetails().id;
          order[CCConstants.REPRICE]=true;
          ccRestClient.request(CCConstants.ENDPOINT_ORDERS_PRICE_ORDER, order,
           function(data) {
             data.order = {};
             data.order = data.shoppingCart;
             widget.orderDetails(data);
             widget.destroySpinner(widget.orderDetailsBodyIndicator);
           }, function (result) {
              widget.destroySpinner(widget.orderDetailsBodyIndicator);
              widget.display(true);
              notifier.sendError(widget.WIDGET_ID, result.message, true);
          }, order)
        }
        else{
        	widget.display(true);
        }

        widget.resetOrderDetails = function() {
          if (!(arguments[0].data.page.orderDetails && arguments[0].data.page.orderDetails.id)) {
            widget.orderDetails(null);
            $.Topic(pubsub.topicNames.PAGE_LAYOUT_LOADED).unsubscribe(widget.resetOrderDetails);
            $.Topic(pubsub.topicNames.PAGE_METADATA_CHANGED).unsubscribe(widget.resetOrderDetails);
          }
        };
        
        $.Topic(pubsub.topicNames.PAGE_LAYOUT_LOADED).subscribe(widget.resetOrderDetails);
        $.Topic(pubsub.topicNames.PAGE_METADATA_CHANGED).subscribe(widget.resetOrderDetails);

        if(widget.orderDetails() && widget.orderDetails().errorMessages != undefined) {
          notifier.clearError(widget.WIDGET_ID);
          notifier.clearSuccess(widget.WIDGET_ID);
          notifier.sendError(widget.WIDGET_ID, widget.orderDetails().errorMessages, true);
        }
        if (widget.orderDetails && widget.orderDetails() && widget.orderDetails().order) {
          var data={};
          ccRestClient.request(CCConstants.ENDPOINT_VALID_ACTIONS_ON_ORDER, data,
          // Success callback
           function(pResult) {
             widget.isCancelAllowed(pResult.cancel.isCancelAllowed);
             widget.isReturnAllowed(pResult.returns.isReturnAllowed);
           }.bind(widget),
           // Error callback
           function() {
             //nothing
           }, widget.orderDetails().id);
          }

        
      },
      /**
       * Function to get the display name of a state 
       * countryCd - Country Code
       * stateCd - State Code
       */
      getStateDisplayName: function(countryCd, stateCd) {
        if (this.shippingCountries()) {
          for (var i in this.shippingCountries()) {
            var countryObj = this.shippingCountries()[i];
            if (countryObj.countryCode === countryCd) {
              for (var j in countryObj.regions) {
                var stateObj = countryObj.regions[j];
                if (stateObj.abbreviation === stateCd) {
                  return stateObj.displayName;
                }
              }
            }
          }
        }
        return "";
      },
      
      /**
       * Function to get the display name of a state 
       */
      getStateName: function() {
        if (this.orderDetails && this.orderDetails() && this.orderDetails().shippingAddress && this.orderDetails().shippingAddress.regionName) {
          return this.orderDetails().shippingAddress.regionName;
        }
        return "";
      },
      
      /**
       * Function to get the display name of a Country 
       * countryCd - Country Code
       */
      getCountryDisplayName: function(countryCd) {
        if (this.shippingCountries()) {
          for (var i in this.shippingCountries()) {
            var countryObj = this.shippingCountries()[i];
            if (countryObj.countryCode === countryCd) {
              return countryObj.displayName;
            }
          }
        }
        return "";
      },
      
      /**
       * Function to get the display name of a Country 
       */
      getCountryName: function() {
        if (this.orderDetails && this.orderDetails() && this.orderDetails().shippingAddress && this.orderDetails().shippingAddress.countryName) {
          return this.orderDetails().shippingAddress.countryName;
        }
        return "";
      },

      /**
       * Function to check if the order is quoted or not
       */
      isQuoted: function() {
        if (this.orderDetails && this.orderDetails() && (this.orderDetails().state == CCConstants.QUOTED_STATES)) {
          return true;
        }
        return false;
      },

      /**
       * Function to check if the address object contains atleast 1 not null field
       */
      isAddressAvailable: function() {
        if (this.orderDetails && this.orderDetails() && this.orderDetails().shippingAddress) {
          for (var i in this.orderDetails().shippingAddress) {
            if (this.orderDetails().shippingAddress[i]) {
              return true;
            }
          }
          return false;
        }
      },

      /**
       * Function to toggle the expanded flag for a configurable child item.
       */
      toggleExpandedFlag : function(element, data) {
        if (data.expanded()) {
          data.expanded(false);
        } else {
          data.expanded(true);
        }
      },
      
      /**
      * Returns the selected product details to the purchase list element.
      */
      getSelectedProducts:function(){
        var widget = this;
        var productItemArray = [];
        ko.utils.arrayForEach(widget.selectedProduct(),function(item){
          var productItem = {
            "productId": item.productId,
            "catRefId": item.catRefId,
            "quantityDesired":item.quantity,
            "displayName":item.displayName
          };
          productItemArray.push(productItem);
        });
        return productItemArray;
      },  
      
             
        
        /**
         * Method to redirect the request to Return Page
         * so that return can be initiated
         */
        
        clickReturnItems :function(data,event){
            var widget = this;
            widget.user().validateAndRedirectPage(this.links().returnItems.route+'?'+"order" + "=" + data.id);
        },
        /**
         * Method to display the cancel model
         * on click of cancel button
         *

         */
        handleCancelOrderClick : function(data,event) {
          var data={};	
          ccRestClient.request(CCConstants.ENDPOINT_ORDERS_CANCEL_GET_CANCEL_REASONS, data,
                    // Success callback
                    function(pResult) {
        	        //suppressing the the irrelevant cancel reason for storefront.
        	         var i;
        	         for(i=0;i<pResult.items.length;i++){
        	        	 if(pResult.items[i].key=='paymentNotReceived'){
        	        		 pResult.items.splice(i,1);
        	        		 break;
        	        	 }
        	         }
        	        
        	         
                  	 this.cancelReasons(pResult.items)
                    }.bind(this),
                    // Error callback
                    function() {
                  	  notifier.sendError(this.WIDGET_ID, CCi18n.t('ns.common:resources.cancelOrderFail'), true)
                    }, this.orderDetails().id);
        	
        	$('#CC-cancel-order-modal').one('show.bs.modal', function() {
             // $('#CC-cancelOrderModel').show();
            });	
        	
        	$('#CC-cancel-order-modal').modal('show');
        	
        	$('#CC-cancel-order-modal').one('hide.bs.modal', function() {
              //  $('#CC-cancelOrderModel').hide();
            });

        },
        
        /**
         * Method to invoke the cancel the order 
         * on click of continue button in Cancel model

         */
        handleClickContinue : function(data,event){
          $('#CC-cancel-order-modal').modal('hide');
          var orderRequest = {};
          if(this.selectedCancelReason() == null){
        	  notifier.sendError(this.WIDGET_ID, CCi18n.t('ns.common:resources.enterCancelReason'), true);
          }else{
              orderRequest["cancelReason"] = this.selectedCancelReason().key;	
              
              orderRequest["orderId"]= this.orderDetails().id;
              ccRestClient.request(CCConstants.ENDPOINT_ORDERS_CANCEL_ORDER, orderRequest,
                  // Success callback
                  function(pResult) {
                	 notifier.sendSuccess(this.WIDGET_ID, CCi18n.t('ns.common:resources.cancelOrderSuccess'), true)
                	 this.orderDetails(pResult.order);
                  }.bind(this),
                  // Error callback
                  function(pResult ) {
                	  notifier.sendError(this.WIDGET_ID, CCi18n.t('ns.common:resources.cancelOrderFail'), true)
                  }.bind(this));
          }
          

        	
        },
        
        /**
         * Method to hide the cancel model
         * on click of cancel button

         */
        handleClickCancel:function(data,event){
          $('#CC-cancel-order-modal').modal('hide');

        }
    }
  }
);
