/**
 * @fileoverview fabindiaB2BFabconnectOrderDetails.js.
 *
 * @author 
 */
define(
  //---------------------
  // DEPENDENCIES
  //---------------------
  ['knockout', "jquery", 'pubsub', 'notifier', 'CCi18n', 'ccConstants', 'navigation', 'ccRestClient', 'ccLogger'],

  //-----------------------
  // MODULE DEFINITION
  //-----------------------
  function (ko, $, pubsub, notifier, CCi18n, CCConstants, navigation, ccRestClient, logger) {
 
    "use strict";
    var widgetModel;
    
    return {
        
      WIDGET_ID: 'b2bFabconnectOrderDetails',
      GET_ORDER_DETAILS: 'getFabconnectOrderDetailsUrl',
      orderFabconnectDetails: ko.observable(),
      orderDetails: ko.observable(),
      showLoader: ko.observable(false),
      
      onLoad: function(widget) {
        widgetModel = widget;
      }, 
      
      loadOrderDetails: function(orderId){
        widgetModel.showLoader(true);
        widgetModel.getOrderDetails(orderId).then(function(success){
            widgetModel.orderFabconnectDetails(success[0]);
            widgetModel.showLoader(false);
        },function(error){
            logger.info('ERROR:: ',error);
        });
      },
      
      getOrderDetails: function(orderId){
        var inputData = { id: orderId};
        ccRestClient.request(CCConstants.ENDPOINT_GET_ORDER, {}, widgetModel.successFunc.bind(this), widgetModel.errorFunc.bind(this),orderId);
        var orderDetails = {
            url: widgetModel.translate(widgetModel.GET_ORDER_DETAILS),
            dataType: "json",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(inputData),
        };
        return Promise.resolve($.ajax(orderDetails)); 
      },
      
      successFunc: function(result){
          if (result) {
              widgetModel.orderDetails(result);
          }
      },
      
      errorFunc: function(error){
        logger.info('ERROR:: ',error);
      },
      
       /**
       * Function to check if the address object contains atleast 1 not null field
       */
      isAddressAvailable: function() {
        if (this.orderDetails && this.orderDetails() && this.orderDetails().shippingAddress) {
          for (var i in this.orderDetails().shippingAddress) {
            if (this.orderDetails().shippingAddress[i]) {
              return true;
            }
          }
          return false;
        }
      },
      
      beforeAppear: function(page) {
        if(page.contextId){
            widgetModel.loadOrderDetails(page.contextId);
        }
        
        widgetModel.secondaryCurrency = ko.observable(null);
        
        $.when (widgetModel.site().siteLoadedDeferred).done(function() {
          if (widgetModel.orderDetails()) {
            var secondaryCurrency = widgetModel.site().getCurrency(widgetModel.orderDetails().secondaryCurrencyCode);
            if (widgetModel.secondaryCurrency() || secondaryCurrency) {
              if (widgetModel.secondaryCurrency() && secondaryCurrency && (widgetModel.secondaryCurrency().currencyCode == secondaryCurrency.currencyCode)) {
                return;
              }
              widgetModel.secondaryCurrency(secondaryCurrency);
            }
          }
        });
        
        widgetModel.showSecondaryShippingData = ko.pureComputed(function(){
            return widgetModel.orderDetails().payShippingInSecondaryCurrency &&
                     (null != widgetModel.secondaryCurrency());
          });
        widgetModel.showSecondaryTaxData = ko.pureComputed(function(){
            return widgetModel.cart().payTaxInSecondaryCurrency &&
                     (null != widgetModel.secondaryCurrency());
          });
      }
    };
  }
);