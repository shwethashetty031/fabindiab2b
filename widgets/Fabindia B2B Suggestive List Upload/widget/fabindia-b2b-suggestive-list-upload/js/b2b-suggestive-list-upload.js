
/**
 * @fileoverview Quick Order.
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery', 'knockout', 'pubsub', 'notifications', 'notifier', 'CCi18n', 'ccConstants', 'spinner', 'placeholderPatch', 'navigation', 'pageLayout/product',
    'ccRestClient', 'pageLayout/site', 'viewModels/purchaseList','viewModels/purchaseListListing','moment', 'ccLogger'
  ],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, pubsub, notifications, notifier, CCi18n, CCConstants, spinner, placeholder, navigation, ProductViewModel, ccRestClient, SiteViewModel,PurchaseListViewModel,PurchaseListListingViewModel, moment, ccLogger) {
    "use strict";
    
    var self;
    var newInputPurchaseListItems = [];
    var errorArrFile = [];
    var errorProdArr = [];
     
    return {

      WIDGET_ID: 'fabindia-b2b-suggestive-list-upload',
      ADD_PURCHASELIST_ID : "addSuggestiveListIDUrl",
      PUBLISH_SUGGESTIVEBUY_EMAIL: "sendSuggestivebuyMailUrl",
      rowsToAddToCart: ko.observableArray(),
      activeSearchBoxIndex: ko.observable(),
      skuList: ko.observableArray(),
      interceptedLink: ko.observable(null),
      isDirty: ko.observable(false),
      isModalVisible: ko.observable(false),
      popupId: "",
      disableAddToCartButton: ko.observable(false),
      enablePurchaseButton: ko.observable(false),
      skuIds: [],
      lines: [],
      invalidSKUs: [],
      newProducts: [],
      arrToDisplay: [],
      fileToUpload: ko.observable(null),
      nameOfPurchaseListPrepopulate: ko.observable(""),
      errorOfPurchaseListPrepopulate: ko.observable(""),
      checkRoleforApprover: ko.observable(false),
      windowClosed: ko.observable(false),
      spinnerOptions: {
        parent: ".modal-content",
        posTop: '30%'
      },
      prodArrMsg: ko.observableArray(),
      errorProdMsg: ko.observable("error"),
      ignoreValidation: ko.observable(false),
      errorArrNotEmpty: ko.observable(false),

      //For typeahead search
      MIN_CHARACTERS: 2,
      locale: "",
      MAX_RESULTS: 5,
      LEFT_MARGIN: 10,
      
      //Because listsku endpoint call cannot handle calls with
      //large no. of SKUs we send it in multiple batches.
      MAX_ITEMS_PER_BATCH: 50,

          exportToCsv: function (filename, rows) {
            var processRow = function (row) {
                // alert(row);
                var finalVal = '';
                for (var j = 0; j < row.length; j++) {
                    var innerValue = (row[j] === null || row[j] === undefined) ? '' : row[j].toString();
                    if (row[j] instanceof Date) {
                        innerValue = row[j].toLocaleString();
                    };
                    var result = innerValue.replace(/"/g, '""');
                    if (result.search(/("|,|\n)/g) >= 0)
                        result = '"' + result + '"';
                    if (j > 0)
                        finalVal += ',';
                    finalVal += result;
                }
                return finalVal + '\n';
            };

            var csvFile = '';
            for (var i = 0; i < rows.length; i++) {
                csvFile += processRow(rows[i]);
            }
            
            console.log(csvFile,"csvFile");

            var blob = new Blob([csvFile], { type: 'application/csv;charset=utf-8;' });
            // var blob =  new Blob([csvFile], {type: 'application/pdf'});
            if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(blob, filename);
            } else {
                var link = document.createElement("a");
                if (link.download !== undefined) { // feature detection
                    // Browsers that support HTML5 download attribute
                    var url = URL.createObjectURL(blob);
                    link.setAttribute("href", url);
                    link.setAttribute("download", filename);
                    link.style.visibility = 'hidden';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }
            
            // self.convertToExcel(self.translate('errorReportText'))
        },
        
     
        
        clickDownloadHandler : function() { 
             self.exportToCsv(self.getFormattedName(),errorProdArr);
            
        },
        
          getFormattedName : function(){
            var today = new Date();
            var date = today.getFullYear()+'-'+ (today.getMonth()+1) +'-'+today.getDate();
            var time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
            var dateTime = date+'-'+time;
            var fileName = self.user().currentOrganization().repositoryId +'-'+ dateTime +'.csv';
            return fileName; 
    },
    
      // validate the fields before adding to suggestive list
      createNewPurchaseList: function () {
        var self = this;
        if (self.nameOfPurchaseListPrepopulate.isValid() && self.prodArrMsg().length) {

          self.callNewPurchaseList(newInputPurchaseListItems)
        }
      },


      // method to invoke the purchase list endpoint

      callNewPurchaseList: function (pData) {

        if (pData.length > 0) {
          spinner.create(self.spinnerOptions);
          var data = {
            "name": self.nameOfPurchaseListPrepopulate(),
            "items": pData,
            "accountId": self.user().currentOrganization().repositoryId,
            "siteId": self.site().siteInfo.id,
            "description":"created"
          };
          ccRestClient.request(CCConstants.ENDPOINT_CREATE_PURCHASE_LIST, data,
            function (successData) {
              console.log("sucessssss");
              self.addSharedSettings(successData);
              self.addPurchaseLIistId(successData);
            },
            function (errorData) {
              self.errorOfPurchaseListPrepopulate(errorData.message);
              spinner.destroyWithoutDelay(self.spinnerOptions.parent);

            }, null);
        }
        else {
          self.enablePurchaseButton(false);
        }

      },

      // endpoint to make the suggestive list shared with all the contacts of the same account as and when it is created

      addSharedSettings: function (pData) {
        var self = this;
        var data = {

          "account": {
            "id": self.user().currentOrganization().repositoryId
          },
          "sharedResourceType": "purchaseList",
          "relativeTo": pData.id,
          "defaultEditEnabled": false,
          "organizationSharingEnabled": true

        }
        ccRestClient.request(CCConstants.ENDPOINT_CREATE_PURCHASE_LIST_SETTINGS, data,
          function (successData) {
            console.log("sucessssss");
            $.Topic("Purchase_List_Refresh.memory").publishWith({ messsage : true });
            self.removeData();
            $('#myModal').modal('hide');
            spinner.destroyWithoutDelay(self.spinnerOptions.parent);
            
          },
          function (errorData) {
            spinner.destroyWithoutDelay(self.spinnerOptions.parent);
          }, self.site().siteInfo.id);
      },
      
      purchaseItemsForList : function(newProdArr,errorProdArr,pItems){
          
          var prodArr = [];
           var index;
        var indexofError;
        var indexQty;
        var indexInvalid;
        var invalidProd;
          
            if(newProdArr && pItems){
                prodArr = pItems.filter(function (val) {
                    return newProdArr.indexOf(val.catRefId()) != -1;
                });
            }
            for(var i=prodArr.length-1; i >= 0 ; i--){
                if(prodArr[i].errorMessage() == 'Invalid Product' || prodArr[i].errorMessage() == self.translate('invalidSkuFileText') || prodArr[i].errorMessage() == 'Invalid SKU'){
                    prodArr.splice(i, 1)
                }
                if(prodArr[i].errorMessageQuantity() == 'There are invalid inputs.'){
                    prodArr.splice(i, 1)
                }
            }
                  self.arrToDisplay = self.arrToDisplay.concat(prodArr)
            
            self.errorProdMsg(errorProdArr.length-1);
            if(self.errorProdMsg() == '0'){
                self.errorArrNotEmpty(false)
            }
            else{
                self.errorArrNotEmpty(true)
            }
            console.log(prodArr,"newprodArr");
            self.prodArrMsg(self.arrToDisplay);
            
            prodArr.forEach(function (item) {
              var newInputItem = {};
              newInputItem.productId = item.catRefId();
              newInputItem.catRefId = item.productDisplay ? item.productDisplay() : "";
              newInputItem.quantityDesired = item.productQuantity ? Number(item.productQuantity()) : "";
              newInputPurchaseListItems.push(newInputItem);
            });
            
            if(!newInputPurchaseListItems.length){
                self.enablePurchaseButton(false);
            }
      },

      // check the uploaded file contains the valid collection type and category type
      // collection type in the product attribute should be the one present in the B2B site settings JSON
      // category type in the product attribute should be same as Suggestive List name prefixed with '_'

      createInputPurchaseListItems: function (pItems) {
        var newArr = [];
        var inputPurchaseListItems = [];
        


        var collectionValue = self.site().extensionSiteSettings.b2bSiteSettings.collections ? JSON.parse(self.site().extensionSiteSettings.b2bSiteSettings.collections) : '';
        if (collectionValue && collectionValue.upcoming && collectionValue.upcoming.collectionType) {
          collectionValue = collectionValue.upcoming.collectionType.toLowerCase();
        }
        var categoryType = self.nameOfPurchaseListPrepopulate().split('_') ? self.nameOfPurchaseListPrepopulate().split('_')[0] : '';


        pItems.forEach(function (item) {
          var inputItem = {};
          newArr.push(item.catRefId());
          inputItem.productId = item.productDisplay();
          inputItem.catRefId = item.catRefId ? item.catRefId() : "";
          inputItem.quantityDesired = item.productQuantity ? Number(item.productQuantity()) : "";
          inputPurchaseListItems.push(inputItem);
        });
        var input = {};
        var newArrayFinal = [];
        var newProdArr = [];
        
        // var matchedSuccessData = [];
        input[CCConstants.PROD_STATE] = true;
        input[CCConstants.PRODUCT_IDS] = newArr;
        input[CCConstants.FIELDS_QUERY_PARAM] = ["id", "x_categoryType", "x_collectionType"];
        input[CCConstants.CATALOG] = self.cart().catalogId();
        input[CCConstants.CONTINUE_ON_MISSING_PRODUCT] = true;
        input[CCConstants.STORE_PRICELISTGROUP_ID] = SiteViewModel.getInstance().selectedPriceListGroup().id;
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input,
          function (successData) {
              
                for (var i = 0; i < newArr.length; i++) {
                    var match = false; // we haven't found it yet
                    for (var j = 0; j < successData.length; j++) {
                        if (newArr[i] == successData[j].id) {
                            match = true;
                            if ((collectionValue.toLowerCase().indexOf(successData[j].x_collectionType.toLowerCase()) > -1)){
                                if((categoryType.toLowerCase().indexOf(successData[j].x_categoryType.toLowerCase()) > -1)) {
                                    newProdArr.push(successData[j].id);
                                }
                            else {
                                errorProdArr.push([errorProdArr.length, successData[j].id ,self.translate("incorrectCategoryText")]);
                            }
                        }
                        else {
                            errorProdArr.push([errorProdArr.length, successData[j].id, self.translate("incorrectCollectionText")]);
                        }
                            break;
                        }
                    }
                    if (!match) {
                        self.arrToSend.forEach(function(arrItem,arrIndex){
                        if(arrItem.catRefId() == newArr[i]){
                        errorProdArr.push([errorProdArr.length, newArr[i], "Invalid Product"]);
                        arrItem.errorMessage('Invalid Product')
                    }
            });
                
                    }
                    
                }
                    
                
            // errorArrFile = errorArrFile.concat(errorProdArr);
            self.purchaseItemsForList(newProdArr,errorProdArr,pItems);
            // spinner.destroyWithoutDelay(self.spinnerOptions.parent);
        
             
          },
          function (errorData) {
            if(errorData.status == '400'){
                errorData.errors.forEach(function(item,index){
                   if(item.errorCode == '20031'){
                      errorProdArr.push([errorArrFile.length,item.moreInfo, item.message]);  
                   } 
                });
            errorArrFile = errorArrFile.concat(errorProdArr);
            }
            self.purchaseItemsForList('',errorProdArr,'');
            // spinner.destroyWithoutDelay(self.spinnerOptions.parent);
          },
          null);

      },

      beforeAppear: function () {
        var widget = this;
        self.prodArrMsg.removeAll();
        widget.skuList.removeAll();
      },



      removeData: function () {
        self.fileToUpload(null);
        self.nameOfPurchaseListPrepopulate(''),
          self.prodArrMsg().length = 0;
        self.errorProdMsg('error');
        self.prodArrMsg().length = 0;
        self.skuList().length = 0;
        self.lines.length = 0;
        self.skuIds.length = 0;
        self.invalidSKUs.length = 0;
        $(csvFileInput)[0].value = "";
        self.errorOfPurchaseListPrepopulate('');
        self.ignoreValidation(true);
        newInputPurchaseListItems.length = 0
      },

      onLoad: function (widget) {
        self = widget;
        //   console.log("PurchaseListViewModel====>",new PurchaseListViewModel());
        var clickedElementId = ko.observable(null);
        widget.locale = ccRestClient.getStoredValue(CCConstants.LOCAL_STORAGE_USER_CONTENT_LOCALE);
        if (widget.locale != null) {
          widget.locale = JSON.parse(widget.locale)[0].name;
        } else {
          widget.locale = $(':root').attr('lang');
        }
        // Configuring chinese locales to support typeahead with minimum one character length
        if (widget.locale == "zh_CN" || widget.locale == "zh_TW") {
          widget.MIN_CHARACTERS = 1;
        }

        widget.nameOfPurchaseListPrepopulate.extend({
          required: {
            params: true, message: CCi18n.t('ns.common:resources.listNameMandatoryText'), onlyIf: function () { return !widget.ignoreValidation(); },
            maxLength: { params: 256, message: CCi18n.t('ns.common:resources.maxAllowedListNameLength') },
          },
           pattern : {
              params : "(?=\S*['-])([a-zA-Z'-]+)",
              message : "Please follow the correct pattern"
          }

        });

        widget.nameFieldBlured = function () {
          widget.ignoreValidation(false);
        };
        widget.nameFieldFocused = function () {
          widget.ignoreValidation(true);
        };

        // handler for anchor click event.
        widget.handleUnsavedChanges = function(e, linkData) {
          var usingCCLink = linkData && linkData.usingCCLink;
          // If URL is changed explicitly from profile.
          if (!usingCCLink && !navigation.isPathEqualTo(widget.links().profile.route)) {
            widget.removeEventHandlersForAnchorClick();
            return true;
          }
          if (widget.user().loggedIn()) {
            clickedElementId = this.id;
            if (clickedElementId === "CC-header-cart-total") {
              return true;
            }
            widget.interceptedLink = e.currentTarget.pathname;
            if (widget.isDirty()) {
              widget.showModal();
              usingCCLink && (linkData.preventDefault = true);
              return false;
            }
          }
        };
        
        widget.showModal = function() {
          $("#CC-quickOrder-modal").modal('show');
          widget.isModalVisible(true);
        };

        widget.navigateAway = function() {
          if (clickedElementId === "CC-header-checkout" || clickedElementId === "CC-loginHeader-logout" || clickedElementId === "CC-customerAccount-view-orders" ||
            clickedElementId === "CC-header-language-link" || clickedElementId.indexOf("CC-header-languagePicker") != -1) {
            widget.removeEventHandlersForAnchorClick();
            // Get the DOM element that was originally clicked.
            var clickedElement = $("#" + clickedElementId).get()[0];
            clickedElement.click();
          } else if (clickedElementId === "CC-loginHeader-myAccount") {
            // Get the DOM element that was originally clicked.
            var clickedElement = $("#" + clickedElementId).get()[0];
            clickedElement.click();
          } else {
            if (!navigation.isPathEqualTo(widget.interceptedLink)) {
              navigation.goTo(widget.interceptedLink);
              widget.removeEventHandlersForAnchorClick();
            }
          }
        };


        /**
         *  Adding event handler for anchor click.
         */

        widget.handleModalCancelUpdateDiscard = function() {
          $("#CC-quickOrder-modal").modal('hide');
          widget.navigateAway();
        };

        widget.addEventHandlersForAnchorClickSetPopupId = function () {
          //   widget.popupId = popupId;
          widget.user().roles().forEach(function (item, index) {
            if (((item.function == "approver") && (item.relativeTo.repositoryId == widget.user().currentOrganization().repositoryId)) || ((item.function == "admin") && (item.relativeTo.repositoryId == widget.user().currentOrganization().repositoryId))) {
              widget.checkRoleforApprover(true);
                var uploadSuggestiveBuyDate = widget.site().extensionSiteSettings.b2bSiteSettings.uploadSuggestiveBuyDate ? moment(widget.site().extensionSiteSettings.b2bSiteSettings.uploadSuggestiveBuyDate,"DD-MM-YYYY") : '';
                    uploadSuggestiveBuyDate = uploadSuggestiveBuyDate.format('YYYY-MM-DD')
                    var uploadSuggestiveEndDate =  widget.site().extensionSiteSettings.b2bSiteSettings.uploadSuggestiveEndDate ? moment(widget.site().extensionSiteSettings.b2bSiteSettings.uploadSuggestiveEndDate,"DD-MM-YYYY") : '';
                    uploadSuggestiveEndDate = uploadSuggestiveEndDate.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                    if(moment(currentDate).isAfter(uploadSuggestiveEndDate,'day') || moment(currentDate).isBefore(uploadSuggestiveBuyDate,'day')){
                        widget.windowClosed(true);
                        return;
                    } 
            }
            else if((item.function == "buyer") && (item.relativeTo.repositoryId == widget.user().currentOrganization().repositoryId)){
                if(widget.user().roles().length ==1){
                    widget.checkRoleforApprover(false);
                }
            }
          })
          $("body").on("click.cc.unsaved", "a", widget.handleUnsavedChanges);
          $.Topic("UPLOAD_WINDOW.memory").publishWith(widget.windowClosed(), [{message: "success"}]);
        };

        /**
         *  removing event handlers explicitly that has been added when anchor links are clicked.
         */
        widget.removeEventHandlersForAnchorClick = function() {
          $("body").off("click.cc.unsaved", "a", widget.handleUnsavedChanges);
        };


        /**
         *  Navigates window location to the interceptedLink OR clicks checkout/logout button explicitly.
         */
        widget.navigateAway = function() {

          if (clickedElementId === "CC-header-checkout" || clickedElementId === "CC-loginHeader-logout" || clickedElementId === "CC-customerAccount-view-orders" ||
            clickedElementId === "CC-header-language-link" || clickedElementId.indexOf("CC-header-languagePicker") != -1) {
            widget.removeEventHandlersForAnchorClick();
            // Get the DOM element that was originally clicked.
            var clickedElement = $("#" + clickedElementId).get()[0];
            clickedElement.click();
          } else if (clickedElementId === "CC-loginHeader-myAccount") {
            // Get the DOM element that was originally clicked.
            var clickedElement = $("#" + clickedElementId).get()[0];
            clickedElement.click();
          } else {
            if (!navigation.isPathEqualTo(widget.interceptedLink)) {
              navigation.goTo(widget.interceptedLink);
              widget.removeEventHandlersForAnchorClick();
            }
          }
          widget.isDirty(false);
        }

        widget.hideModal = function() {
          if (widget.isModalVisible() || widget.user().isSearchInitiatedWithUnsavedChanges()) {
            $("#CC-quickOrder-modal").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
          }
        }
      },

      sendPublishMail: function(){
          var message = $("#CC-Text-Area-Message").val();
          var formData = {
              "text": message
          };
          $.ajax({
                url : self.translate(self.PUBLISH_SUGGESTIVEBUY_EMAIL),
                type: 'POST',
                dataType : "json",
                data: formData,
                success : function(successData){
                    notifier.sendSuccess(self.WIDGET_ID, successData.Data ,true);
                    ccLogger.info("SUCCESS:: Purchase List ID api call to add in database");
                },
                error : function error(error){
                    ccLogger.info('ERROR:: While Addinng purchase list id',error);
                }
          });
          $('#publishModal').modal('hide');
      },
      addPurchaseLIistId : function(data) {
           var purchaselIstId = data.id;
           var formData = { 
               "purchaseListId": purchaselIstId
           }
           $.ajax({
                    url : self.translate(self.ADD_PURCHASELIST_ID),
                    type: 'POST',
                    dataType : "json",
                    data: formData,
                    success : function(successData){
                      ccLogger.info("SUCCESS:: Purchase List ID api call to add in database");
                    },
                    error : function error(error){
                            ccLogger.info('ERROR:: While Addinng purchase list id',error);
                        }
                });
      },


		  handleFiles: function(widget, pEvent) {
		    var target = pEvent.target ? pEvent.target : pEvent.srcElement;
           var file, fileName;
           if(target.files){
             file = target.files[0];
             if(file.name.split('.').pop()!=="csv"){
   		        notifier.sendError("quickOrderWidget", widget.translate('invalidFileText'), true);
                 return;   
   		      }else{
   		    	  widget.fileToUpload(file);
   		      }
              target.value='';
           }
		  },
		  
		  getAsText: function() {
		  var widget = this;
		  widget.errorOfPurchaseListPrepopulate('');
		  var errors = ko.validation.group([this.nameOfPurchaseListPrepopulate], {
          deep: true
        });
        errors.showAllMessages(true);
        if(widget.nameOfPurchaseListPrepopulate.isValid()){
	      var reader = new FileReader();
	      // Read file into memory as UTF-8      
	      reader.readAsText(widget.fileToUpload());
	      // Handle errors load
	      reader.onload = widget.loadHandler.bind(this);
	      reader.onerror = widget.errorHandler.bind(this);
        }
	    },

	   loadHandler: function(event) {
	      var widget = this;    
	      var csv = event.target.result;
	      widget.processData.call(widget,csv);
	    },

      processData: function (csv) {
        var widget = this;
        var allTextLines = csv.split(/\r\n|\n/);
        var index;
        var floor;
        var partSKUList;
        var partArrToSend;
        widget.skuIds = [];
        widget.lines = [];
        widget.invalidSKUs = [];
        widget.fileToUpload(null);
        self.prodArrMsg.removeAll();
        errorArrFile.length = 0;
        errorProdArr.length = 0;
        newInputPurchaseListItems.length = 0
        
       var floor;
       var floorToSend;
        widget.arrToSend = [];
        var rowToAddFromCSV = {};
        errorProdArr.push(['Sr.No.','Error Values','Error description'])
        

        for (var i = 1; i < allTextLines.length; i++) {
            if(allTextLines[i] !== ""){
          var data = allTextLines[i].split(';');
          var rowFromFile = [];
          for (var j = 0; j < data.length; j++) {
            rowFromFile.push(data[j]);
              rowToAddFromCSV = {
                    productName: ko.observable(""),
                    productDisplay: ko.observable(rowFromFile[0].split(',')[0]),
                    catRefId: ko.observable(rowFromFile[0].split(',')[1]),
                    productQuantity: ko.observable(rowFromFile[0].split(',')[2]).extend({
	             	          required: {
	             	            params: true,
	             	            message:  widget.translate('quantityRequireMsg')
	             	          },
	             	          digit: {
	             	            params: true,
	             	            message:  widget.translate('quantityNumericMsg')
	             	         
	             	          },
	             	          min: {
	             	            params: 1,
	             	            message: widget.translate('quantityGreaterThanMsg', {
	             	              quantity: 0
	             	           })
	             	           
	             	          }
	             	        }).isModified(true),
                    errorMessage: ko.observable(""),
                    errorMessageQuantity: ko.observable("")
                  };
            if (rowFromFile[i] !== "") {
              if (rowFromFile[0].split(',')[0] !== "") {
                widget.skuIds.push(rowFromFile[0].split(',')[0]);
                widget.lines.push(rowFromFile);
              }
              else
              {
                  
                  widget.invalidSKUs = widget.invalidSKUs.concat(rowFromFile[0].split(',')[1])
                  errorProdArr.push([errorProdArr.length,'null', "Empty SKU field"]);
                  rowToAddFromCSV.errorMessage(widget.translate('invalidSKUText'))
                  
              }
              
            
                  if(!rowToAddFromCSV.productQuantity.isValid()){
                        errorProdArr.push([errorProdArr.length,rowToAddFromCSV.productQuantity(),widget.translate('invalidInputs')])
                        widget.invalidSKUs = widget.invalidSKUs.concat(rowToAddFromCSV.productDisplay()),
                        rowToAddFromCSV.errorMessageQuantity(widget.translate('invalidInputs'))
                        // console.log(rowToAddFromCSV.errorMessageQuantity())
                  }
                  
                //   if (widget.invalidSKUs.indexOf(rowToAddFromCSV.productDisplay()) !== -1) {
                //     rowToAddFromCSV.errorMessage(widget.translate('invalidSKUText'));
                //   }
                  
                    widget.arrToSend.push(rowToAddFromCSV);
                
            }
          }
        }
        }
        widget.total = widget.skuIds.length;
        floor = widget.total - widget.MAX_ITEMS_PER_BATCH;
        if (floor < 0) {
          floor = 0;
        }
        widget.totalToSend = widget.arrToSend.length;
        floorToSend = widget.totalToSend - widget.MAX_ITEMS_PER_BATCH;
        if (floorToSend < 0) {
          floorToSend = 0;
        }
        partSKUList = widget.skuIds.slice(floor, widget.total);
        partArrToSend = widget.arrToSend.slice(floorToSend, widget.totalToSend)
        widget.initialSKUCall(partSKUList,partArrToSend);
        spinner.create(widget.spinnerOptions);
      },

      initialSKUCall: function (partSKUList,partArrToSend) {
        var widget = this;
        var input = {};
       
        var floor;
      
        
        input[CCConstants.CATALOG] = widget.cart().catalogId();
        input[CCConstants.SKU_IDS] = partSKUList;
        input[CCConstants.PROD_STATE] = true;
        input[CCConstants.STORE_PRICELISTGROUP_ID] = SiteViewModel.getInstance().selectedPriceListGroup().id;
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_SKUS, input,
          function (data) {
            widget.invalidSKUs = widget.invalidSKUs.concat(data.deletedSkus);
            widget.arrToSend.forEach(function(arrItem,arrIndex){
                data.deletedSkus.forEach(function(delItem,delIndex){
                    if(arrItem.productDisplay() == delItem){
                        errorProdArr.push([errorProdArr.length,delItem, self.translate("invalidSkuFileText")]);
                        arrItem.errorMessage(widget.translate('invalidSkuFileText'))
                    }
                });
            });
              self.createInputPurchaseListItems(partArrToSend);
              widget.enablePurchaseButton(true);
            
            if (widget.total > 0 && widget.total - widget.MAX_ITEMS_PER_BATCH > 0) {
              widget.total = widget.total - widget.MAX_ITEMS_PER_BATCH;
              floor = widget.total - widget.MAX_ITEMS_PER_BATCH;
              if (floor < 0) {
                floor = 0;
              }
              partSKUList = widget.skuIds.slice(floor, widget.total);
              partArrToSend = widget.arrToSend.slice(floor, widget.total);
              widget.initialSKUCall(partSKUList,partArrToSend);
            } else {
              spinner.destroyWithoutDelay(widget.spinnerOptions.parent);
              widget.fileToUpload(null);
            }
          }, function () {
            notifier.sendError("quickOrderWidget", widget.translate('fileParsingErrorText'), true);
            widget.enablePurchaseButton(false);
            spinner.destroyWithoutDelay(widget.spinnerOptions.parent);
            return;
          }, partSKUList);
      },
      errorHandler: function (evt) {
        if (evt.target.error.name == "NotReadableError") {
        //   alert("Cannot read file !");
        }
      },

      clearAll: function () {
        var widget = this;
        widget.beforeAppear();
        $(csvFileInput)[0].value = "";
        widget.fileToUpload(null);
      }

    }
  }
);