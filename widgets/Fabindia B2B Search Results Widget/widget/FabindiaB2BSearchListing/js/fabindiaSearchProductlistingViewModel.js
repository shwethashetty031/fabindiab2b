/**
 * @fileoverview Fab India Product Listing Search View Model.
 */
define(['knockout', 'jquery', 'ccConstants', 'pubsub', 'viewModels/productListingSearchViewModel', 'pageLayout/site'],

  function(ko, $, CCConstants, pubsub, ProductListingSearchViewModel, SiteViewModel) {

    "use strict";
	
	ProductListingSearchViewModel.prototype.itemsPerRow = ko.observable(3);
    ProductListingSearchViewModel.prototype.itemsPerRowInDesktopView = ko.observable(3);
    ProductListingSearchViewModel.prototype.itemsPerRowInLargeDesktopView = ko.observable(3);
    ProductListingSearchViewModel.prototype._internalViewportMode = ko.observable(3);
    ProductListingSearchViewModel.prototype.viewportMode = ko.observable(3);
	ProductListingSearchViewModel.prototype.itemsPerRowInTabletView = ko.observable(3);
	
    /**
     * Submits the search .
     * Builds the search parameter object, and publishes the SEARCH_CREATE event with the search parameters.
     * The search is taken care of by the 'pageLayout/search' view model.
     * @see module:pageLayout/search
     * Overriden Submit Search method to add Endeca Filter Parameters.
     */
    ProductListingSearchViewModel.prototype.submitSearch = function(){
      var searchTerm = '';
      var recSearchType = '';
      if (this.searchText().trim()) {
        searchTerm = CCConstants.PRODUCT_DISPLAYABLE + CCConstants.SEARCH_PROPERTY_SEPARATOR + this.searchText().trim();
      }

      if (this.parameters && this.parameters.searchType) {
        recSearchType = this.parameters.searchType;
      }

      //this.blockSize = this.itemsPerPage;

      var searchParams = { getFromUrlParam : true,
        newSearch : false,
        recordsPerPage : this.blockSize,
        recordOffSet : this.recordOffSet,
        recDYMSuggestionKey : this.recDYMSuggestionKey,
        searchType : recSearchType};

      if (this.parameters.Ns) {
        var sortValues = decodeURIComponent(this.parameters.Ns).split("|");

        if (sortValues.length === 2) {
          searchParams.sortDirectiveProperty = sortValues[0];
          searchParams.sortDirectiveOrder = sortValues[1] == "1" ? "desc" : "asc";
        }
      }
      else {
        this.sortDirectiveProp('displayName');
      }

      if (!this.useGenericSort && ProductListingSearchViewModel.prototype.idToSearchIdMap[this.sortDirectiveProp()] !== 'product.relevance') {
        searchParams.sortDirectiveProperty = ProductListingSearchViewModel.prototype.idToSearchIdMap[this.sortDirectiveProp()];
        searchParams.sortDirectiveOrder = (searchParams.sortDirectiveProperty == "product.relevance") ? "none" : this.sortDirectiveOrder();
      }

      // Added Endeca Filter parameter specific to India & USA
    //   if(SiteViewModel.getInstance().selectedPriceListGroup().id === 'INR'){
    //     searchParams.recFilter = this.widget.translate("filterParamINR");
    //   }else{
    //     searchParams.recFilter = this.widget.translate("filterParamUSA");
    //   }

      $.Topic(pubsub.topicNames.SEARCH_CREATE).publishWith(
        searchParams,
        [{message:"success"}]);
    };
    
    //var originalInitialize = ProductListingSearchViewModel.prototype.initialize; 
    
   /* ProductListingSearchViewModel.prototype.initialize = function() { 

	var self = this; 

	originalInitialize.call(self); 

	/** 
	* Handle widget responses when the search result 
	
		$.Topic(pubsub.topicNames.SEARCH_RESULTS_FOR_CATEGORY_UPDATED).subscribe(function(obj) { 
		self.noSearchResultsText(''); 
		if (obj.message === CCConstants.SEARCH_MESSAGE_FAIL) { 
			self.searchFailed(true); 
			navigation.goTo(self.widget.links().noSearchResults.route, false, true); 
			$.Topic(pubsub.topicNames.SEARCH_FAILED_TO_PERFORM).publish(); 
		} else { 
			if (this.totalRecordsFound === 0 && !this.searchAdjustments.suggestedSearches) { 
			navigation.goTo(self.widget.links().noSearchResults.route, false, true); 
			$.Topic(pubsub.topicNames.SEARCH_TERM).publishWith( 
			this.searchAdjustments.originalTerms, 
				[ { 
					message : "success" 
				} ]); 
				self.display(false); 
				return; 
			} else { 
				self.display(true); 
			} 
		
		//self.buildResultsPerPage(this.totalRecordsFound, self.prevTotalNumber, self.selectedResultsPerPageOption());
		
		//self.totalNumber(this.totalRecordsFound); 
		//self.searchTerms(this.searchAdjustments.originalTerms); 
		if (this.searchAdjustments.adjustedSearches) { 
			self.adjustedSearchTerms(this.searchAdjustments.adjustedSearches[self.getSearchTerms()]); 
		} else { 
			self.adjustedSearchTerms([]); 
		} 
		if (this.searchAdjustments.suggestedSearches) { 
			self.formatSuggestedSearches(this.searchAdjustments.suggestedSearches[self.getSearchTerms()]); 
			self.suggestedSearches(this.searchAdjustments.suggestedSearches[self.getSearchTerms()]); 
		} else { 
			self.suggestedSearches([]); 
		} 
		self.searchFailed(false); 
		if (this.pagingActionTemplate) { 
			self.navigationDescriptors(self.getParameterByName( 
			this.pagingActionTemplate.navigationState, 
			CCConstants.SEARCH_NAV_DESCRIPTORS_KEY)); 
		} 
		if (this.isNewSearch === true) { 
			self.clearOnLoad = true; 
			if (self.sortDirectiveProp() !== "displayName") { 
				self.selectedSort(self.sortOptions()[0]); 
			} 
		} else { 
			self.clearOnLoad = false; 
			// if (obj.requestor) { 
			// var selectedSort = 
			// getSelectedSort(obj.requestor); 
			// 
			// if (selectedSort) { 
			// self.selectedSort(selectedSort); 
			// } 
			// else { 
			// self.selectedSort(self.sortOptions()[0]); 
			// } 
			// } 
		} 
		if (this.searchResults && this.searchResults.length > 0) { 
			self.addData(this.searchResults, self.totalNumber(), this.recordOffSet); 
		} else { 
			self.targetPage = 1; 
			if (!this.searchAdjustments.suggestedSearches) { 
				// Passing in true for noHistory param (2nd 
				// param), we don't want the url to change 
				// on 404 pages. 
				navigation.goTo(self.widget.links()['404'].route, true, true); 
			} 
		} 
		self.titleText(self.getTitleText(this.searchResults.length)); 
		self.resultsText(self.getResultsText()); 
		self.pageLoadedText(self.getPageLoadedText()); 
		} 
}; 
	}); */

    return ProductListingSearchViewModel;
  });
