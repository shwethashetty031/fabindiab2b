/**
 * @fileoverview hero Widget.
 * 
 * @author 
 */
define(
  //-------------------------------------------------------------------
  // MODULE DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'navigation', 'notifications', 'pubsub', 'ccConstants'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, navigation, notifications, pubsub, CCConstants) {

    "use strict";
    var currSlide;
    var widgetModel;
    return {
      searchText: ko.observable(''),
      SEARCH_SELECTOR: '.search-query',

      onLoad: function (widget) {
        widgetModel = widget;
        var collections = widget.site().extensionSiteSettings.b2bSiteSettings.collections ? widget.site().extensionSiteSettings.b2bSiteSettings.collections : '';
        widgetModel.bannerJsonObject = ko.computed(function() {
                    if (collections) {
                        return JSON.parse(collections);
                    }
            }, widget); 
      },
      // publishes a message to say create a search 
      handleSearch: function(data){
        // Executing a full search, cancel any search typeahead requests
        $.Topic(pubsub.topicNames.SEARCH_TYPEAHEAD_CANCEL).publish([{message:"success"}]);
        
         var collection = this.toLowerCase();
         
            switch(collection){
              case 'upcoming':
                data.searchText(widgetModel.bannerJsonObject().upcoming.collectionType ? widgetModel.bannerJsonObject().upcoming.collectionType : "");
                break;
              case 'current' :
                data.searchText(widgetModel.bannerJsonObject().current.collectionType ? widgetModel.bannerJsonObject().current.collectionType : "");
                  break;
              case 'perennials' : 
                data.searchText(widgetModel.bannerJsonObject().perennials.collectionType ? widgetModel.bannerJsonObject().perennials.collectionType : "");
                  break;
              case 'food' : 
                data.searchText(widgetModel.bannerJsonObject().food.collectionType ? widgetModel.bannerJsonObject().food.collectionType : "");
                  break;
              case 'beauty':
                data.searchText(widgetModel.bannerJsonObject().beauty.collectionType ? widgetModel.bannerJsonObject().beauty.collectionType : "");
                  break;
              case '':
                  return false;
            }
            
            var trimmedText = $.trim(data.searchText());
            var searchTerm = 'product.x_collectionType:' +trimmedText;
            if (trimmedText.length != 0){
              
              // Send the search results and the related variables for the Endeca query on the URI
              navigation.goTo("/searchresults" + "?"
                + CCConstants.SEARCH_NAV_RECORD_FILTER_KEY + "="
                + encodeURIComponent(searchTerm) + "&"
                + CCConstants.SEARCH_DYM_SPELL_CORRECTION_KEY + "="
                + encodeURIComponent(CCConstants.DYM_ENABLED) + "&"
                + CCConstants.SEARCH_NAV_ERECS_OFFSET + "=0&"
                + CCConstants.SEARCH_REC_PER_PAGE_KEY + "="
                + CCConstants.DEFAULT_SEARCH_RECORDS_PER_PAGE+ "&"
                + CCConstants.SEARCH_RANDOM_KEY + "=" + Math.floor(Math.random()*1000) + "&"
                + CCConstants.SEARCH_TYPE + "=" + CCConstants.SEARCH_TYPE_SIMPLE + "&"
                + CCConstants.PARAMETERS_TYPE + "=" + CCConstants.PARAMETERS_SEARCH_QUERY);
              data.searchText('');
            }
      },

    }
  }
);