/**
 * @fileoverview No Search Results Widget. 
 * 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout','ccConstants','ccRestClient'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko,CCConstants,ccRestClient) {  
  
    "use strict";
    var widgetModel;
  
    return {


      collectionArray : ko.observableArray([]),

      onLoad: function(widget) {
        widgetModel = widget;
        var self = this;
        widget.getCollection();
      },

      //gets the list of collections from category id which are provided in widget. 
      getCollection : function (){
        var url, data;
        var categoryIds = widgetModel.catId1()+","+widgetModel.catId2()+","+widgetModel.catId3()+","+widgetModel.catId4();
        url = CCConstants.ENDPOINT_LIST_COLLECTIONS;
        data= {};
        data["categoryIds"]= categoryIds;
        data["fields"]= "id,displayName,route";
        
        //calling listCollection api for all categoryIds
        ccRestClient.request(url, 
                             data,
    	                     this.successFunc.bind(this),
                             this.errorFunc.bind(this));
        },

        //puts the category response in 'collectionArray' observableArray
        successFunc : function(data){
            data.forEach(function(element){
                var categoryObject = {};
                categoryObject['category'] = element;
                if(element.id == widgetModel.catId1()){
                    categoryObject['image'] = widgetModel.catImg1;
                }
                if(element.id == widgetModel.catId2()){
                    categoryObject['image'] = widgetModel.catImg2;
                }
                if(element.id == widgetModel.catId3()){
                    categoryObject['image'] = widgetModel.catImg3;
                }
                if(element.id == widgetModel.catId4()){
                    categoryObject['image'] = widgetModel.catImg4;
                }
                widgetModel.collectionArray.push(categoryObject);
            });
        },

        errorFunc : function(data){
            console.log('Error in FabindiaNoSearchResults: ', data);
        }
    };
  }
);
