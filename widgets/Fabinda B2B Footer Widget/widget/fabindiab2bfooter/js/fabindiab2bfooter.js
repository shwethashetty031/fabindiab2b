/**
 * @fileoverview footer Widget.
 * 
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub','pageLayout/site'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, pubsub,SiteViewModel) {
  
    "use strict";
     var widgetModel;
     var parsedFooterHeader1;    
    return {
      footerSection1:   ko.observableArray(),
      footerSection2:   ko.observableArray(),
      footerSection3:   ko.observableArray(),
      
      onLoad: function(widget) {
        widgetModel = widget;
        

        if(widget.footerHeader1() !== ""){
            widget.footerSection1(JSON.parse(widget.footerHeader1()));
        }

        if(widget.footerSection2() !== ""){
            if(SiteViewModel.getInstance().selectedPriceListGroup().id.toLowerCase() === "inr"){
            widget.footerSection2(JSON.parse(widget.footerHeader2()));
                }else{
                 var parsedObj = JSON.parse(widget.footerHeader2());
                  for(var i in parsedObj){
                    widget.footerSection2(parsedObj);
                 }
             }
        }
        
    $('body').on('click', '[data-collapse-group="footer_mobile_button"]', function () {
        
            var $this = $(this);
            $("[data-collapse-group='footer_mobile_button']:not([data-target='" + $this.data("target") + "'])").each(function () {
                $($(this).data("target")).removeClass("in").addClass('collapse');
                $($(this).data("target")).parent().find('a.footer_collapse').addClass('collapsed');
            });
            
        });
      },
    showChatApp: function() {
            var q = document.createElement('script');
            q.type = 'text/javascript';
            q.async = true;
            var proto = ('https:' == document.location.protocol?'https://':'http://');
            q.src = proto + "chat.trixchat.com/chatadmin/js_chat/check_for_valid_license.php?lk=6163a3404141b800cda93fe22ebaf031&is_agent=no";
            document.getElementsByTagName('head')[0].appendChild(q);
        },
      beforeAppear : function(){
        $('body,html').animate({
          scrollTop: 0
      }, 500);
    },
    };
  }
);
