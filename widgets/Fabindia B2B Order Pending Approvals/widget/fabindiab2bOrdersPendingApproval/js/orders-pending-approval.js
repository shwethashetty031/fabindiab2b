/**
 * @fileoverview Orders Pending Approval Widget.
 * 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'viewModels/ordersPendingApprovalViewModel','notifier', 'CCi18n', 'ccConstants', 'navigation', 'ccRestClient', 'pageLayout/currency'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, OrdersPendingApprovalViewModel, notifier, CCi18n, CCConstants, navigation, ccRestClient, currencyViewModel) {
  
    "use strict";
    var widgetModel;
    return {

      WIDGET_ID: "ordersPendingApproval",
      fetchSize: ko.observable(15),
      searchText: ko.observable(),
      sortDirections: ko.observable({"lastModifiedDate":"both"}),
      startIndex : ko.observable(),
      endIndex: ko.observable(),
      currencyViewModel: currencyViewModel.getInstance(),
      subscriptions: [],
      contextManager: null,
      fieldValues: null,
      homeRoute: "",
      beforeAppear: function(page) {
        var widget = this;
         if (widget.user().loggedIn() == false || widget.user().isUserSessionExpired()) {
           navigation.doLogin(navigation.getPath(), widget.homeRoute);
           return;
         } else {
           if(widget.user().roles().length == 0 && widget.user().id() == "") {
             widget.user().isApprover(true);
             return;
           }
           if (!widget.user().isApprover()) {
             notifier.clearError(widget.WIDGET_ID);
             notifier.clearSuccess(widget.WIDGET_ID);
           } else {
             widget.sortDirections({"lastModifiedDate":"both"});
             if(!widget.approvalViewModel) {
               widget.approvalViewModel = ko.observable();
               widget.approvalViewModel(new OrdersPendingApprovalViewModel());
             }
             widget.approvalViewModel().user = widget.user();
             widget.approvalViewModel().clearOnLoad = true;
             widget.populateSiteAndOrganization();
             widget.approvalViewModel().refinedFetch();
             widget.homeRoute = widget.links().home.route;
             if (ccRestClient.profileType === CCConstants.PROFILE_TYPE_AGENT) {
               widget.homeRoute = widget.links().agentHome.route;
               widget.subscriptions.push(widget.contextManager.selectedSite.subscribe(function(pValue){
                  widget.approvalViewModel().siteFilter = pValue;
                  widget.approvalViewModel().refinedFetch();
               }));
               widget.subscriptions.push(widget.contextManager.currentOrganizationId.subscribe(function(pValue){
                   widget.approvalViewModel().organizationFilter = pValue;
                   widget.approvalViewModel().refinedFetch();
               }));
             }
           }
         }
       },
       populateSiteAndOrganization : function () {
         var widget = this;
         if(widget.approvalViewModel() && widget.contextManager) {
             widget.approvalViewModel().siteFilter = widget.contextManager.getSelectedSite();
             widget.approvalViewModel().organizationFilter = widget.contextManager.getCurrentOrganizationId();  
         }        
      },
      onLoad: function(widget) {
         var self = this;
         widgetModel = widget;
         widget.approvalViewModel = ko.observable();
         widget.approvalViewModel(new OrdersPendingApprovalViewModel(self.fieldValues));
         widget.approvalViewModel().user = widget.user();
         widget.approvalViewModel().itemsPerPage = widget.fetchSize();
         widget.approvalViewModel().blockSize = widget.fetchSize();
   widget.approvalViewModel().sortProperty ="lastModifiedDate:desc";
         if (ccRestClient.profileType === CCConstants.PROFILE_TYPE_AGENT) {
            widget.contextManager = require("agentViewModels/agent-context-manager").getInstance();
            widget.cacheHandler = require("agentViewModels/agentUtils/simple-cache-handler");
            widget.populateSiteAndOrganization();
            $.Topic(pubsub.topicNames.PAGE_CHANGED).subscribe(widget.triggerPageChangeEvent.bind(widget));
         }
         widget.fetchSize.subscribe(function(value) {
             widget.approvalViewModel().itemsPerPage = widget.fetchSize();
             widget.approvalViewModel().blockSize = widget.fetchSize();
             widget.approvalViewModel().refinedFetch();
         });
         widget.getCurrency = function(currencyCode) {
             var self = this;
             return self.currencyViewModel.currencyMap[currencyCode];
         };
         //Create approvalGrid computed for the widget
         widget.approvalGrid = ko.computed(function() {
             var numElements, start, end, width;
             var rows = [];
             var orders;
             if (($(window)[0].innerWidth || $(window).width()) > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
               var startPosition, endPosition;
               // Get the orders in the current page
               startPosition = (widget.approvalViewModel().currentPage() - 1) * widget.approvalViewModel().itemsPerPage;
               this.startIndex(startPosition+1);
               endPosition = startPosition + Number(widget.approvalViewModel().itemsPerPage);
               orders = widget.approvalViewModel().data.slice(startPosition, endPosition);
             } else {
               orders = widget.approvalViewModel().data();
               this.startIndex(1);
             }
             if (!orders) {
               return;
             }
             numElements = orders.length;
             width = parseInt(widget.approvalViewModel().itemsPerRow(), 10);
             start = 0;
             end = start + width;
             while (end <= numElements) {
               rows.push(orders.slice(start, end));
               start = end;
               end += width;
             }
             if (end > numElements && start < numElements) {
               rows.push(orders.slice(start, numElements));
             }
             this.endIndex((rows.length-1)+this.startIndex());
             if(rows.length==0){
               this.startIndex(0);
             }

             for(var i=0; i<orders.length; i++) {
               if(!orders[i]['priceListGroup']) {
                 orders[i].priceListGroup = {};
                 $.when(widget.currencyViewModel.siteCurrenciesLoaded).done(function() {
                     orders[i]['priceListGroup'] = {"currency": widget.getCurrency(orders[i].priceInfo.currencyCode)};
                 });
                 //AgentApplication
                 orders[i]['primaryCurrencyTotal'] = orders[i].priceInfo.primaryCurrencyTotal;
                 orders[i]['secondaryCurrencyTotal'] = orders[i].priceInfo.secondaryCurrencyTotal;
                 orders[i].total = orders[i].priceInfo.total;
                 orders[i].creationDate = new Date(orders[i].creationTime).toISOString();
                 orders[i].orderId = orders[i].id;
               }
              //orders[i].exchangeRate && --check it is working fine
               if ((ccRestClient.profileType === CCConstants.PROFILE_TYPE_AGENT)) {
                 orders[i]['secondaryOrderDataAvailable'] = orders[i].payTaxInSecondaryCurrency && orders[i].payShippingInSecondaryCurrency &&
                                                          orders[i].secondaryCurrencyCode ? true : false;
               } else {
                 orders[i]['secondaryOrderDataAvailable'] = orders[i].payTaxInSecondaryCurrency && orders[i].payShippingInSecondaryCurrency &&
                                                         orders[i].exchangeRate && orders[i].secondaryCurrencyCode;
               }
               if(orders[i].secondaryOrderDataAvailable) {
                 $.when (widget.site().siteLoadedDeferred).done(function() {
                   orders[i]['secondaryCurrency'] = ko.observable(widget.getCurrency(orders[i].secondaryCurrencyCode));
                 });
               }
             }
             return rows;
           }, widget);
           
          $.Topic(pubsub.topicNames.ORDERS_GET_PENDING_APPROVAL_LIST_FAILED).subscribe(function(data) {
          if (this.status == CCConstants.HTTP_UNAUTHORIZED_ERROR) {
            widget.user().handleSessionExpired();
            if (navigation.isPathEqualTo(widget.links().profile.route) || navigation.isPathEqualTo(widget.links().ordersPendingApproval.route)) {
              navigation.doLogin(navigation.getPath(), widget.homeRoute);
            }
          }
         });
      },
      
      hideOrderPendingSection: function(){
            var localArr = [];
            widgetModel.user().roles().forEach(function(val,i){
                localArr.push(val.function);
            });
            if(($.inArray( "admin", localArr) === -1) && ($.inArray( "approver", localArr) === -1) && ($.inArray( "profileAddressManager", localArr) === -1) && ($.inArray( "accountAddressManager", localArr) === -1)){
                $('#verticalTabs-profileStack_1-tab-3').css("display", "none");
            }
      },
      clickToSort: function(sortOrder,sortTerm){
          var widget=this;
         widget.sortDirections()[sortTerm]=sortOrder;
         var sortString=sortTerm+":"+sortOrder;
          widget.approvalViewModel().sortProperty=sortString;
          widget.sortDirections.valueHasMutated();
          widget.approvalViewModel().refinedFetch();
        },
        
        onClickFunc: function(){
           if(this.searchText()) {
             this.approvalViewModel().inputqparam="id co \"" + this.searchText() + "\" AND (state eq \"pending_approval\" OR state eq \"pending_approval_template\")";
           } else {
             this.approvalViewModel().inputqparam="state eq \"pending_approval\" OR state eq \"pending_approval_template\"";
           }
           this.approvalViewModel().refinedFetch();
        },

      clickOrderDetails: function (data, event) {
        var widget = this;

        if(ccRestClient.profileType === CCConstants.PROFILE_TYPE_AGENT) {
          widget.cacheHandler.addItemToCache("isPendingApprovalPage", true);
          widget.contextManager.setProperty("isPendingApprovalPage", true);
          widget.contextManager.includedAgentContextProperties.push("isPendingApprovalPage");
          // Check if the Values can be read
          if(data.state == CCConstants.PENDING_APPROVAL) {
            var params = {};
            params[CCConstants.ORDER_PENDING_APPROVAL] = "true";
            params[CCConstants.AGENT_PARAM_APPROVER_ID] = widget.user().id();
            var url  = widget.contextManager.getAgentContextURL(widget.links().AgentOrderDetails.route+'/'+data.orderId, params);
            widget.user().navigateToPage(url);

          } else if(data.state == CCConstants.PENDING_APPROVAL_TEMPLATE){
            // The scheduledOrderId needs to be determined here.
            var obj = {};
            // end point checks on the fielsd query parameters from client.
            obj[CCConstants.FIELDS_QUERY_PARAM] = "scheduledOrderId";
            obj[CCConstants.INCLUDE_RESULT] = CCConstants.INCLUDE_RESULT_FULL;
            ccRestClient.request(CCConstants.ENDPOINT_GET_ORDER,obj,
              function(pSuccess){
                if(pSuccess.scheduledOrderId) {
                  widget.cacheHandler.addItemToCache("showScheduled",true);
                  var id =  pSuccess.scheduledOrderId ? pSuccess.scheduledOrderId : data.scheduledOrderId
                  var params = {};
                  params[CCConstants.ORDER_PENDING_APPROVAL] = "true";
                  params[CCConstants.AGENT_PARAM_APPROVER_ID] = widget.user().id();
                  params[CCConstants.AGENT_PARAM_IS_SCHEDULED_ORDER] = true;
                  var url  = widget.contextManager.getAgentContextURL(widget.links().AgentOrderDetails.route+'/'+id, params);
                  widget.user().navigateToPage(url);
                }
              },
              function(pResult) {
              },
            data.orderId);
          }
        } else if(data.state == CCConstants.PENDING_APPROVAL){
          widget.user().validateAndRedirectPage(this.links().orderDetails.route+'/'+ data.orderId); 
        }else if(data.state == CCConstants.PENDING_APPROVAL_TEMPLATE){
          widget.user().validateAndRedirectPage(this.links().scheduledOrders.route+'/'+ data.scheduledOrderId); 
        }
        return false;
      },
      
      triggerPageChangeEvent: function () {
        var widget = this;
        var length = widget.subscriptions.length;
        for (var i =0; i<length; i++){
           widget.subscriptions[i].dispose();
        }  
        widget.subscriptions = [];
      }
    };
  }
);
