/**
* @fileoverview webContent Widget. 
* Helps to display static template files.
*/
define(

    //-------------------------------------------------------------------
    // DEPENDENCIES
    //-------------------------------------------------------------------
    ['knockout', 'ccConstants', 'notifications', 'pubsub', 'CCi18n', 'spinner', 'ccStoreConfiguration', 'notifier', 'pageLayout/site', 'navigation', 'ccRestClient', 'ccLogger','moment'],

    //-------------------------------------------------------------------
    // MODULE DEFINITION
    //-------------------------------------------------------------------
    function (ko, CCConstants, notifications, pubsub, CCi18n, spinner, CCStoreConfiguration, notifier, SiteViewModel, navigation, CCRestClient,logger,moment) {
        "use strict";
        var products = [];
        var updateObj = {};
        var widgetModel;
        var skuQuantity;
        var checkData;
        var qtyLPTotal;
        var qtyMRPTotal;
        var qtyTotal;
        var effectiveCreditLimit;
        return {
            
            WIDGET_ID: 'fabindia-b2b-selection-list',
            GET_ORDER_SSE: 'getOrdersForAccountURL',
            emptySelectionList: ko.observable(true),
            webStyleArr: ko.observableArray(),
            updateSelectData: ko.observable(),
            selectedWebStyle: ko.observableArray(),
            listTotalQty:ko.observable(0),
            listLPValue: ko.observable(0),
            listMRPValue: ko.observable(0),
            effectiveCreditLimit:ko.observable(),
            availableCreditLimit: ko.observable(),
            creditLimitCheck: ko.observable(true),
            disableSave:ko.observable(true),
            creditLimit: ko.observable(),
            productLink: ko.observable(),
            categoryName: ko.observable(),
            GET_RECORDS_FOR_SELECTION_URL : "getRecordsForShowList",
            UPDATE_RECORDS_FOR_SELECTION_URL: "updateRecordsForShowList",
            windowClosed:ko.observable(true),
            windowClosedForBuyer:ko.observable(false),
            windowClosedForRM:ko.observable(false),
            isHovered:ko.observable(false),
            
            spinnerOptions: {
                parent: ".selection-list-body",
                posTop: '30%'
            },
            
            getCreditLimitValues: function(){
                
                 $.ajax({
                    url: widgetModel.translate(widgetModel.GET_ORDER_SSE)  + '?q=organizationId="' + widgetModel.user().parentOrganization.id() + '" AND (state="PENDING_APPROVAL")&fields=items.priceInfo.total',
                    dataType: "json",
                    type: 'GET',
                    contentType: "application/json",
                    headers: {
                      'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                      'x-ccsite': widgetModel.site().siteInfo.repositoryId
                    },
                    async: false,
                    success: function(data) {
                        var sum=0;
                        data.items.forEach(function(item,index){
                            sum += Number(item.priceInfo.total);
                        });
                        effectiveCreditLimit = Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(sum);
                        widgetModel.effectiveCreditLimit(effectiveCreditLimit.toFixed(2));
                        if(widgetModel.effectiveCreditLimit() > 0){
                            widgetModel.creditLimitCheck(false);
                        } else {
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('creditLimitMsg'), true);
                            widgetModel.creditLimitCheck(true);
                        }
                    }
                });
            },
            
            
            beforeAppear: function(page){
                widgetModel.windowClosed(true),
                widgetModel.windowClosedForBuyer(false),
                widgetModel.windowClosedForRM(false),
                widgetModel.checkWindowClosed();
                widgetModel.creditLimit(Number(widgetModel.user().currentOrganization().x_creditLimit));
                 if(window.location.href.split("/").pop() === "selection"){
                    setTimeout(function(){
                        $("#main").addClass("selection-main-page");
                        $(".selection-main-page > .row > .redBox > div[id^=region-] ").addClass("selection-main-page-below-row");  
                    }, 0); 
                }
                
                $(window).scroll(function(event) {
                    function footer(){
                        var scroll = $(window).scrollTop(); 
                        if(scroll>20 && !widgetModel.emptySelectionList()){ 
                            $(".sticky_footer").fadeIn("slow").addClass("show");
                        } else {
                            $(".sticky_footer").fadeOut("slow").removeClass("show");
                        }
                        
                        clearTimeout($.data(window, 'scrollTimer'));
                        $.data(window, 'scrollTimer', setTimeout(function() {
                            if ($('.sticky_footer').hasClass('hover')) {
                                footer();
                            } else {
                                $(".sticky_footer").fadeOut("slow").removeClass("show");
                            }
                        }, 2000));
                    }
                    footer();
                });
            },
            
            captureSelectUpdate: function(selectSkuData,selectProductData,selectData,event){
                var self = this,
                str = selectData.quantity,
                patt = new RegExp("^[0-9]*$"),
                res = patt.test(str);
                if(!res || selectData.quantity == ''){
                    selectData.quantity = 0;
                    event.target.value = 0;
                }
                products.push({
                  productId : selectSkuData.productId ? selectSkuData.productId : '',      
                  skuId : selectData.skuId ? selectData.skuId : '',      
                  listPrice : selectSkuData.listPrice ? selectSkuData.listPrice : '',         
                  salePrice :selectSkuData.salePrice ? selectSkuData.salePrice : '',      
                  size : selectData.size ? selectData.size : '',         
                  webStyleId : selectSkuData.webstyleId ? selectSkuData.webstyleId : '', 
                  pscStyleCode: selectSkuData.pscStyleCode ? selectSkuData.pscStyleCode : '',
                  parentCategory :selectSkuData.parentCategory ? selectSkuData.parentCategory : '',    
                  x_filterColor: selectSkuData.color ? selectSkuData.color : '',
                  quantity :event.target.value ? event.target.value : '',       
                  displayName :selectSkuData.displayName ? selectSkuData.displayName : ''
                })
                
                
               
              
                 updateObj = {
                    accountId:    widgetModel.user().parentOrganization.id(),
                    emailId:      widgetModel.user().emailAddress(),
                    isPDP : false,
                    categoryType : selectProductData.categoryType ? selectProductData.categoryType : '',
                    products: products   
                };
                
                
                
                if(selectData && selectData.skuPrice[0] && selectData.skuPrice[0].listPrice && selectSkuData.sku){
                    
                    selectSkuData.totalMRPQty = 0
                    for(var i=0;i<selectSkuData.sku.length;i++){
                        selectSkuData.totalMRPQty += Number(selectSkuData.sku[i].quantity ? parseInt(selectSkuData.sku[i].quantity) : 0)*Number(selectData.skuPrice[0].listPrice)
                    }
                    $('#updateMRPQuantity_'+selectSkuData.productId).text(selectSkuData.totalMRPQty.toFixed(2))
                }
                if(selectData && selectData.skuPrice[0] && selectData.skuPrice[0].salePrice && selectSkuData.sku){
                    selectSkuData.totalQty = 0
                    for(var i=0;i<selectSkuData.sku.length;i++){
                        selectSkuData.totalQty += Number(selectSkuData.sku[i].quantity ? parseInt(selectSkuData.sku[i].quantity) : 0)*Number(selectData.skuPrice[0].salePrice)
                    }
                    
                    $('#updateQuantity_'+selectSkuData.productId).text(selectSkuData.totalQty.toFixed(2))
                }
                if(selectSkuData){
                    selectSkuData.totalSKUQty = 0
                    for(var i=0;i<selectSkuData.sku.length;i++){
                        selectSkuData.totalSKUQty += selectSkuData.sku[i].quantity ? parseInt(selectSkuData.sku[i].quantity) : 0;
                    }
                    
                    $('#updateSKUQuantity_'+selectSkuData.productId).text(selectSkuData.totalSKUQty)
                }
                
                var totalSKUQty = 0;
                var totalMRPQty = 0;
                var totalQty = 0;
                
                widgetModel.webStyleArr().forEach(function(val,index){
                    val.forEach(function(val,index){
                        val.productData.forEach(function(val,index){
                           totalSKUQty += val.totalSKUQty
                           totalMRPQty += val.totalMRPQty
                           totalQty += val.totalQty
                        })    
                    })
                })
                 widgetModel.listTotalQty(totalSKUQty);
                 widgetModel.listLPValue(totalQty.toFixed(2));
                 widgetModel.listMRPValue(totalMRPQty.toFixed(2));
                 
                var availableCreditLimit =  widgetModel.effectiveCreditLimit() - widgetModel.listLPValue()
                widgetModel.availableCreditLimit(availableCreditLimit.toFixed(2));
                 
                if(totalQty > widgetModel.effectiveCreditLimit()){
                    notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    var creditLimitMsg = widgetModel.translate('creditLimitMsg')
                    notifier.sendError(widgetModel.WIDGET_ID,creditLimitMsg, true);
                    widgetModel.creditLimitCheck(true);
                    widgetModel.disableSave(true);
                }else{
                    notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    widgetModel.creditLimitCheck(false)
                    widgetModel.disableSave(false);
                }
                
            },
            
            
            checkWindowClosed: function(){
             var buyDateFranchise = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise,"DD-MM-YYYY") : '';
                    buyDateFranchise = buyDateFranchise.format('YYYY-MM-DD')
                    var endDateFranchise =  widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise,"DD-MM-YYYY") : '';
                    endDateFranchise = endDateFranchise.format('YYYY-MM-DD')
                    var buyDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM,"DD-MM-YYYY") : '';
                    buyDateRM = buyDateRM.format('YYYY-MM-DD')
                    var endDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM,"DD-MM-YYYY") : '';
                    endDateRM = endDateRM.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                for(var i=0;i< widgetModel.user().roles().length; i++){
                    if(moment(currentDate).isAfter(endDateFranchise,'day') || moment(currentDate).isBefore(buyDateFranchise,'day')){
                        if((widgetModel.user().roles()[i].function == "approver") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId) || (widgetModel.user().roles()[i].function == "admin") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(moment(currentDate).isAfter(endDateRM,'day') || moment(currentDate).isBefore(buyDateRM,'day')){
                                widgetModel.windowClosedForRM(true);
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('regionalManagerError'), true);
                                break;
                            }
                            else{
                                widgetModel.windowClosedForRM(false);
                            }
                        }
                        else if((widgetModel.user().roles()[i].function == "buyer") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(widgetModel.user().roles().length == 1){
                            widgetModel.windowClosedForBuyer(true);
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('buyerError'), true);
                        }
                            
                        }
                    }
                else{
                    widgetModel.windowClosedForBuyer(false); 
                }
        }
        
               if(widgetModel.windowClosedForBuyer() || widgetModel.windowClosedForRM()){
                   widgetModel.windowClosed(true);
                   return;
               }
               else{
                   widgetModel.windowClosed(false);
               }
          
      },
            
            handleUpdateSelect: function(continueClick,self,selfData,event){
                // var self = this;
                if(Object.keys(updateObj).length){
                 spinner.create(self.spinnerOptions);
                 widgetModel.addToSelection(widgetModel.translate(widgetModel.UPDATE_RECORDS_FOR_SELECTION_URL), updateObj).then(function(successData){
                   
                    spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                    notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    if(continueClick == "continue" && widgetModel.listTotalQty() == 0){
                        notifier.sendError(widgetModel.WIDGET_ID,widgetModel.translate('continueError'), true);
                    } else if(continueClick == "continue") {
                       navigation.goTo(widgetModel.translate('summaryLinkText')); 
                    } else {
                        notifier.sendSuccess(widgetModel.WIDGET_ID,widgetModel.translate('successfulUpdateText'), true);   
                    }
                    products = [];
                    updateObj = {};
                 },function(error){
                     
                     spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                     notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    notifier.sendError(widgetModel.WIDGET_ID,widgetModel.translate('errorUpdateText'), true);
              });
            }
            else{
                if(continueClick == "continue" && widgetModel.listTotalQty() == 0){
                    notifier.sendError(widgetModel.WIDGET_ID,widgetModel.translate('continueError'), true);
                }  else if(continueClick == "continue") {
                    navigation.goTo(widgetModel.translate('summaryLinkText')); 
                }
            }
            },
            handleClearSelectDialog:function(){
                notifier.clearError(widgetModel.WIDGET_ID);
                notifier.clearSuccess(widgetModel.WIDGET_ID);
                $("#CC-selectionList-delete-modal-1").modal('show');
                $('body').addClass('modal-open');
            },
        handleClearSelect: function(){
            $("#CC-selectionList-delete-modal-1").modal('hide');
             setTimeout(function() {
               $('body').removeClass('modal-open');
               $('.modal-backdrop').remove();
             },300);
            spinner.create(widgetModel.spinnerOptions);
            var deleteObj = {
                    accountId:    widgetModel.user().parentOrganization.id(),
                    emailId:      widgetModel.user().emailAddress(),
                    clearSuggestiveBuy: true
            };
            widgetModel.deleteSelection(deleteObj).then(function(successData){
                    spinner.destroyWithoutDelay(widgetModel.spinnerOptions.parent);
                    notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    notifier.sendSuccess(widgetModel.WIDGET_ID,widgetModel.translate('clearSuccess'), true);
                    widgetModel.emptySelectionList(true);
                 },function(error){
                     
                     spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                     notifier.clearError(widgetModel.WIDGET_ID);
                    notifier.clearSuccess(widgetModel.WIDGET_ID);
                    notifier.sendError(widgetModel.WIDGET_ID,widgetModel.translate('errorDelete'), true);
              });
            
        },   
            
            
            onLoad: function (widget) {
            widgetModel = widget;
            
            widgetModel.getCreditLimitValues()
            
            ko.bindingHandlers.styleHandler = {
                init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
                    var data = ko.unwrap(valueAccessor());
                    var $el = $(element)
                    var _href = $('#linkDetails'+viewModel.webstyleId + viewModel.parentCategory).attr('href')
                    
                    $el.on('click', addStyleData);
                    $el.on('load', addStyleData);
                    
                    function addStyleData(){
                        $el.addClass('.selected')
                        if(viewModel.displayName){
                            $('#displayName'+viewModel.webstyleId + viewModel.parentCategory).text(viewModel.displayName);
                        }
                        if(viewModel.productImg){
                            $('#prodImage'+viewModel.webstyleId+ viewModel.parentCategory).attr('src',viewModel.productImg);
                        }
                        if(viewModel.productLink){
                            $('#linkDetails'+viewModel.webstyleId+ viewModel.parentCategory).attr('href',_href + viewModel.productLink);
                        }
                    }
                   
                }
            },
            ko.bindingHandlers.quantityHandler = {
                init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
                    var data = ko.unwrap(valueAccessor());
                    var $el = $(element)
                    var sum = 0
                        for(var i=0;i< viewModel.sku.length; i++){
                            if(viewModel.sku[i] && viewModel.sku[i].quantity && viewModel.sku[i].skuPrice[0] && viewModel.sku[i].skuPrice[0].salePrice){
                                bindingContext.$data.totalQty += Number(viewModel.sku[i].quantity)*Number(viewModel.sku[i].skuPrice[0].salePrice)
                                $el.text(Number(bindingContext.$data.totalQty.toFixed(2)))
                            }
                        }
                        qtyLPTotal += Number(bindingContext.$data.totalQty.toFixed(2));
                        widgetModel.listLPValue(qtyLPTotal.toFixed(2));
                        var availableCreditLimit =  Number(effectiveCreditLimit.toFixed(2)) - Number(qtyLPTotal)
                        widgetModel.availableCreditLimit(availableCreditLimit.toFixed(2));
                        
                
                        
                }
            },
            
             ko.bindingHandlers.quantityMRPHandler = {
                init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
                    var data = ko.unwrap(valueAccessor());
                    var $el = $(element)
                    var sum = 0
                        for(var i=0;i< viewModel.sku.length; i++){
                            if(viewModel.sku[i] && viewModel.sku[i].quantity && viewModel.sku[i].skuPrice[0] && viewModel.sku[i].skuPrice[0].listPrice){
                                bindingContext.$data.totalMRPQty += Number(viewModel.sku[i].quantity)*Number(viewModel.sku[i].skuPrice[0].listPrice)
                                $el.text(Number(bindingContext.$data.totalMRPQty.toFixed(2)))
                        
                            }    
                        }
                        qtyMRPTotal += Number(bindingContext.$data.totalMRPQty.toFixed(2));
                        widgetModel.listMRPValue(qtyMRPTotal.toFixed(2));
                        
                }
            },
             ko.bindingHandlers.quantitySKUHandler = {
                init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
                    var data = ko.unwrap(valueAccessor());
                    var $el = $(element)
                   
                    var sum = 0
                        for(var i=0;i< viewModel.sku.length; i++){
                            if(viewModel.sku[i] && viewModel.sku[i].quantity){
                                bindingContext.$data.totalSKUQty += Number(viewModel.sku[i].quantity)
                                $el.text(Number(bindingContext.$data.totalSKUQty))
                            }
                        }
                        qtyTotal += Number(bindingContext.$data.totalSKUQty);
                        widgetModel.listTotalQty(qtyTotal);
                }
            }
            
        },
            
         
           
            
            // for sticky footer hide show functionality on scroll
                // $(document).scroll(function() {
                //     var y = $(this).scrollTop();
                //     if (y > 480) {
                //         $('.sticky_footer_main_pdp').fadeIn();
                //     } else {
                //         $('.sticky_footer_main_pdp').fadeOut();
                //     }
                // });
            // sticky footer hide show functionality on scroll ends   
            // },
            
            handleShowSelection: function(){
                self = this;
                qtyLPTotal = 0;
                qtyMRPTotal = 0;
                qtyTotal = 0;
                widgetModel.webStyleArr.removeAll();
                widgetModel.emptySelectionList(false);
                // widgetModel.creditLimitCheck(true)
                spinner.create(self.spinnerOptions);
                var json = {
                    accountId:    self.user().parentOrganization.id(),
                    emailId:      self.user().emailAddress()
                };
                
                 widgetModel.getSelection(widgetModel.translate(widgetModel.GET_RECORDS_FOR_SELECTION_URL), json).then(function(successData){
                    
                    widgetModel.getWebStyleArr(successData.categories);
                 },function(error){
                     spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                    
              });
                
            },
            toggleClassHover: function(data, event){
                if(event.type == "mouseover"){
                    widgetModel.isHovered(true);
                } else {
                    widgetModel.isHovered(false);
                }
            },
            
            getWebStyleArr: function(categoryArr){
                var self = this;
                if(categoryArr && categoryArr.length > 0){
                    categoryArr.forEach(function(item, index){
                        if(item.sub_category){
                            if(item.webStyle){
                                item.webStyle.forEach(function(item,index){
                                item.categoryType  ? widgetModel.categoryName(item.categoryType) : '';
                                if(item.productData[0] && item.productData[0].parentCategory){
                                    item['catId'] = item.productData[0].parentCategory;
                                    item.productData.forEach(function(item,index){
                                        item["totalQty"] = 0;
                                        item["totalMRPQty"] = 0;
                                        item["totalSKUQty"] = 0;
                            
                                    })
                                }
                            })
                        widgetModel.webStyleArr.push(item.webStyle);
                        }
                        widgetModel.getWebStyleArr(item.sub_category);           
                        }
                        else if(!$.isEmptyObject(item) && !item.sub_category){
                               
                            item.webStyle.forEach(function(item,index){
                                item.categoryType  ? widgetModel.categoryName(item.categoryType) : '';
                               
                                if(item.productData[0] && item.productData[0].parentCategory){
                                    item['catId'] = item.productData[0].parentCategory;
                                    item.productData.forEach(function(item,index){
                                        item["totalQty"] = 0;
                                        item["totalMRPQty"] = 0;
                                        item["totalSKUQty"] = 0;
                                    })
                                }
                            })
                            widgetModel.webStyleArr.push(item.webStyle)
                            spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                            return;
                        } else {
                            spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                            return;    
                        }
                    });
                }
                else
                {
                    widgetModel.emptySelectionList(true);
                }
            },
            
            addToSelection: function(addToSelectUrl, jsonData) {
                if(addToSelectUrl && jsonData){
                    var selectedProductDetails = {
                        url: addToSelectUrl,
                        dataType: "json",
                        type: 'POST',
                        contentType: "application/json",
                        data: JSON.stringify(jsonData)
                    };
                    
                    return Promise.resolve($.ajax(selectedProductDetails));
                }
            },
            deleteSelection: function(jsonData) {
                if(jsonData){
                    var selectedProductDetails = {
                        url: widgetModel.translate('deleteSelectionUrl'),
                        dataType: "json",
                        type: 'POST',
                        contentType: "application/json",
                        data: JSON.stringify(jsonData)
                    };
                    
                    return Promise.resolve($.ajax(selectedProductDetails));
                }
            },
            getSelection: function(getRecordsUrl, jsonData) {
                if(getRecordsUrl && jsonData){
                    var getDetails = {
                        url: getRecordsUrl + "?accountId=" + jsonData.accountId + "&emailId=" + jsonData.emailId,
                        dataType: "json",
                        type: 'GET',
                        contentType: "application/json"
                    };
                  
                    return Promise.resolve($.ajax(getDetails));
                }
            }
        };
    }
);
