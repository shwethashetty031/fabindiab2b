/**
 * @fileoverview Order Approval Widget.
 * 
 * @author 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'notifier', 'CCi18n', 'ccConstants', 'navigation', 'ccRestClient', 'pageLayout/scheduled-order','moment', 'ccLogger'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, notifier, CCi18n, CCConstants, navigation, ccRestClient, ScheduledOrderViewModel, moment, logger) {

    "use strict";
    var widgetModel;
    var inventoryArray = [];
  return {
    WIDGET_ID: 'approvalDetails',
    approverComments : ko.observable(),
    scheduledOrder : ko.observable(),
    approvalResponseData: ko.observable(),
    pageId:"",
    PAYMENT_API: "paymentApi",
    PAYMENT_GATEWAY_TYPE : "paymentGatewayType",
    PAYMENT_SERVICE: "paymentService",
    OAUTH_TOKEN: "oAuthToken",
    approvalWindowClosed: ko.observable(true),
    showLoader: ko.observable(false),
    
    beforeAppear: function(widget){
      this.approverComments("");
      this.pageId = this.pageContext().pageType.id;
      if(this.pageId == CCConstants.SCHEDULED_ORDER_PAGE_TYPE ){
        this.scheduledOrder(ScheduledOrderViewModel.getInstance());
        }
        widgetModel.checkApprovalWindow();
    },
     checkApprovalWindow: function(){
      var approveStartDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyOrderPendingApprovalDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyOrderPendingApprovalDateRM,"DD-MM-YYYY").format('YYYY-MM-DD') : '';
      var approveEndDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endOrderPendingApprovalDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endOrderPendingApprovalDateRM,"DD-MM-YYYY").format('YYYY-MM-DD') : '';
      var currentDate = moment().format("YYYY-MM-DD");
      if(moment(currentDate).isAfter(approveEndDateRM,'day') || moment(currentDate).isBefore(approveStartDateRM,'day')){
        widgetModel.approvalWindowClosed(true);
      } else {
        widgetModel.approvalWindowClosed(false);
      }
      if(widgetModel.approvalWindowClosed()){
        setTimeout(function(){ 
         notifier.clearSuccess(widgetModel.WIDGET_ID);
         notifier.clearError(widgetModel.WIDGET_ID);
         notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('approvalMessage'), true);
        }, 2000);
      }
    },
    onLoad: function(widget){
        widgetModel = widget;
      widget.validationModel = ko.validatedObservable({
        approverComments:  widget.approverComments
      });
      
     widget.approverComments.extend({
      validation: [{
      validator: function(obj) {
      if(obj){
        return obj.length <= 254;
      }
        return true;
      },
      message: widget.translate('messageLengthExceeded'),
    }]
  });
    widget.releaseInventory= function(){
        inventoryArray = [];
        this.orderDetails().order.items.forEach(function(item, index){
            var inventoryObj = {};
            inventoryObj.skuId = item.catRefId;
            inventoryObj.orderId = this.orderDetails().id;
            inventoryObj.quantity = item.quantity;
            inventoryArray.push(inventoryObj);
        },this);
        var inventoryData = {"inventories": inventoryArray};
        var releaseInventoryDetails = {
                url: this.translate('releaseInventoryUrl'),
                data : JSON.stringify(inventoryData),
                dataType: "json",
                type: 'POST',
                contentType: "application/json"
            };
            return Promise.resolve($.ajax(releaseInventoryDetails));   
      }
    widget.approveOrder = function(widget){
        widgetModel.showLoader(true);
        if (this.validationModel.isValid()) {
            this.releaseInventory().then(function(releaseSuccess){
              var data = {};
              data[CCConstants.ORDER_ID] = widget.orderDetails().id;
              data[CCConstants.APPROVER_MESSAGE] = widget.approverComments();
          data[CCConstants.IGNORE_PRICE_WARNINGS] = true;
              ccRestClient.request(CCConstants.ENDPOINT_APPROVE_ORDER, data, widget.approveOrderSuccess, widget.approveOrderFailure);
            },function(error){
                log.error("Unable to release inventory ",widget.orderDetails().id);
            });
        } else{
          this.validationModel.errors.showAllMessages(true);
          notifier.clearError(this.WIDGET_ID);
          notifier.clearSuccess(this.WIDGET_ID);
          notifier.sendError(this.WIDGET_ID, this.translate('validationModalErrorMessage'), true);
        }
      }

      widget.approveOrderSuccess = function(pData){
        widgetModel.approvalResponseData(pData);
        var paymentsURL = widget.translate(widget.PAYMENT_API);
        var paymentPayload = null;
        var result = {};
        result[widget.translate(widget.PAYMENT_SERVICE)] = widget.translate(widget.PAYMENT_GATEWAY_TYPE);
        paymentPayload = {
          orderId: widget.orderDetails().id,
          uuid: widget.orderDetails().uuid,
          payments: [{
            "amount": widget.orderDetails().priceInfo.total,
            "customProperties": result,
            "paymentMethodType": widget.translate(widget.PAYMENT_GATEWAY_TYPE)
          }]
        };
        var finalPayload = {
            paymentBody: paymentPayload,
            siteId : widget.site().siteInfo.id,
            accountId: widget.user().currentOrganization().repositoryId
        };
        widget.invokePaymentAPI(finalPayload, paymentsURL).then(widget.paymentAPISuccess, widget.paymentAPIError);
      }
     widget.invokePaymentAPI = function(finalPayload, paymentsURL) {
          var ajaxArgument = {
            type: "post",
            url:  paymentsURL,
            dataType : "json",
            contentType: "application/json",
            data : JSON.stringify(finalPayload)
          };
          // Call the /payment API to submit order
          return Promise.resolve($.ajax(ajaxArgument));
        }
        widget.paymentAPISuccess = function(data){

          widgetModel.approvalResponseData().state = "SUBMITTED";
          widgetModel.approvalResponseData().orderStatus = "SUBMITTED";          
          widget.orderDetails(widgetModel.approvalResponseData());
          widgetModel.showLoader(false);
        }
        
        widget.paymentAPIError = function(data){
            widgetModel.showLoader(false);
        }
      widget.approveOrderFailure = function(pData){
        if(pData.errorCode == CCConstants.UNLINKED_ADD_ON_PRODUCT) {
          var moreInfo = JSON.parse(pData.moreInfo);
          notifier.sendError(widget.WIDGET_ID, widget.translate('unLinkedAddonValidationError', {productId:moreInfo.productId, skuId:moreInfo.catRefId }), true);
        } else {
          notifier.sendError(widget.WIDGET_ID, pData.message, true);
        }
        widgetModel.showLoader(false);
      }

      widget.rejectOrder = function(widget){
        if (this.validationModel.isValid()) {
            widgetModel.releaseInventory().then(function(releaseSuccess){
              var data = {};
              if(widget.pageId == CCConstants.ORDER_DETAILS_PAGE_TYPE){
                data[CCConstants.ORDER_ID] = widget.orderDetails().id;
              } else{
                data[CCConstants.ORDER_ID] = widget.scheduledOrder().templateOrderId();
              }
              data[CCConstants.APPROVER_MESSAGE] = widget.approverComments();
              ccRestClient.request(CCConstants.ENDPOINT_REJECT_ORDER, data, widget.rejectOrderSuccess,
                widget.rejectOrderFailure);
            },function(error){
                log.error("Unable to release inventory ",widget.orderDetails().id);
            });
        } else{
          this.validationModel.errors.showAllMessages(true);
          notifier.clearError(this.WIDGET_ID);
          notifier.clearSuccess(this.WIDGET_ID);
          notifier.sendError(this.WIDGET_ID, this.translate('validationModalErrorMessage'), true);
        }
      }

      widget.rejectOrderSuccess = function(pData){
        if(widget.pageId == CCConstants.ORDER_DETAILS_PAGE_TYPE){
          widget.orderDetails(pData);
        } else{
          widget.scheduledOrder().templateOrder(pData);
        }
      }

      widget.rejectOrderFailure = function(pData){
        notifier.sendError(widget.WIDGET_ID, pData.message, true);
      }
    }
  }
})
