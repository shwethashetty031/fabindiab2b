/**
 * @fileoverview purchase list Widget.
 *
 */
define(
    //-------------------------------------------------------------------
    // DEPENDENCIES
    //-------------------------------------------------------------------
    ['knockout', 'pubsub', 'navigation', 'notifier', 'ccConstants', 'CCi18n', 'viewModels/purchaseList', 'viewModels/purchaseListListing', 'notifications',
        'ccNumber', 'pageLayout/product', 'pageLayout/site', 'ccRestClient', 'paginated', 'ccStoreUtils','moment', 'ccLogger','jquery'
    ],

    //-------------------------------------------------------------------
    // MODULE DEFINITION
    //-------------------------------------------------------------------
    function (ko, PubSub, navigation, notifier, CCConstants, CCi18n, PurchaseListViewModel, PurchaseListListingViewModel,
        notifications, ccNumber, ProductViewModel, SiteViewModel, ccRestClient, Paginated, StoreUtils, moment, ccLogger,$) {

    "use strict";

        var self;
        var widgetModel;
        var listId;
        var showDetail = {};
  var productsWebStyleId = [];

        function PurchaseListItem(item) {

      var self = this;
      self.productName = item.productName;
      self.displayName = item.productName;
      self.productId = item.productId;
      self.catRefId = item.catRefId;
      self.quantityDesired = ko.observable(item.quantityDesired);
      self.selectedOptions = item.selectedOptions;
      self.thumbnailUrl = item.thumbnailUrl;
      self.path = item.path;
      self.isProductDeleted = item.isProductDeleted;
      self.route = item.path
      self.x_actualSize = item.x_actualSize,
        self.longDescription = item.longDescription
    }
    return {
    
        GET_ORDER_SSE: "getOrderSSE",
      WIDGET_ID: "purchaseLists",
            ADD_RECORDS_TO_SELECTION_URL: "addRecordsToSelectionUrl",
      DELETE_PURCHASELIST_ID: "deleteSuggestiveListIDUrl",
      //For typeahead search
      MIN_CHARACTERS: 2,
      locale: "",
      MAX_RESULTS: 5,
      SEARCH_DELAY: 300, // milliseconds
      LEFT_MARGIN: 10,
      
      productSkuData : [],
      categoryType : '',
      popUpId: ko.observable(""),
      showErrorMessage: ko.observable(),
      errorMessage: ko.observable(),
      originalAccountSharing: ko.observable(),
      subscriptions: [],
      listIdSelect:'',
      contextManager: null,
      confirmDeletePurchageListMessageAgent: ko.observable(),
      confirmAddToCartPurchageListMessageAgent: ko.observable(),
      homeRoute: "",
      pDeleteData: ko.observable(),
      pProductData: ko.observableArray([]),
      displayMultipleSkus: ko.observableArray([]),
      product: ko.observableArray([]),
      purchaseSkus: ko.observableArray([]),
      quantity: ko.observable(),
      showLoaderList: ko.observable(false),
      showLoaderDetail: ko.observable(false),
       windowClosed:ko.observable(true),
        windowClosedForBuyer:ko.observable(false),
        windowClosedForRM:ko.observable(false),
        effectiveCreditLimit:ko.observable(0),
        availableCreditLimit: ko.observable(),
        creditLimitExceeded: ko.observable(true),
        disableDelete:  ko.observable(false),
    
      MAX_ITEMS_PER_BATCH : 10,
      ADD_TO_SELECT_BATCH: 1200,
      PRODUCTS_BATCH: 50,
      
        /**
       * Called when the user presses "Enter" key for search after the search term is entered:
       */
      handleSearchKeypress: function(pListingContext, pData, pEvent) {
        var widget = this;
        if (pEvent.which === 13) {
          widget.handleSearch(pListingContext);
          return false;
        }
        return true;
      },
      
        /**
       * Handler to perform search
       * @param pListingContext Context of the purchase list in which user is currently in
       */
      handleSearch : function(pListingContext) {
        if(pListingContext.searchTerm()) {
          pListingContext.searchTerm(pListingContext.searchTerm().trim());
        }
        pListingContext.refinedFetch();
      },
      
           onLoad: function (widget) {
          self = widget;
          widgetModel = widget;

        widget.purchaseListViewModel = ko.observable();
        widget.purchaseListViewModel(new PurchaseListViewModel());
        widget.purchaseListListingViewModel = ko.observable();
        widget.purchaseListListingViewModel(new PurchaseListListingViewModel());


        widget.sharedPurchaseListListingViewModel = ko.observable();
        widget.accountSharingEnabled = ko.observable();
        widget.currentAccountId = widget.user().parentOrganization ? widget.user().parentOrganization.id() : null;
        widget.sharedPurchaseListListingViewModel(new PurchaseListListingViewModel({
          listType: CCConstants.PURCHASE_LIST_TYPE_SHARED,
          sortKeyAndDirections: {
            name: "both",
            "owner.email": "both",
            "owner.lastName": "both"
          },
          spinnerId: "#purchaseLists-info"
        })
        );
        widget.filterOptions = ko.observableArray([{
          label: CCi18n.t("ns.purchaseLists:resources.sharedViaEmail"),
          value: CCConstants.EMAIL_ADDRESS_TEXT
        },
        {
          label: CCi18n.t("ns.purchaseLists:resources.sharedViaThisAccount"),
          value: CCConstants.PROFILE_ACCOUNT
        },
        {
          label: CCi18n.t("ns.purchaseLists:resources.allPurchaseLists"),
          value: CCConstants.ALL_PURCHASE_LISTS
        }

        ]);

        widget.sharedPurchaseListListingViewModel().selectedListOption(CCConstants.EMAIL_ADDRESS_TEXT);
        widget.isSharedListLoaded = ko.observable(false);
        widget.purchaseListViewModel().profileId = widget.user().id();
        widget.displaySection = ko.observable("list");
        widget.hideEditAndCreateSection = ko.observable(true);
        widget.isDirty = ko.observable(false);
        widget.isModalVisible = ko.observable(false);
        widget.nameOfPurchaseListPrepopulate = ko.observable("");
        widget.nameOfPurchaseListPrepopulate.extend({
          required: { params: true, message: CCi18n.t('ns.common:resources.listNameMandatoryText') },
          maxLength: { params: 256, message: CCi18n.t('ns.common:resources.maxAllowedListNameLength') }
        });
        if (widget.purchaseListViewModel && widget.purchaseListViewModel().dynamicPropertyMetaInfo &&
          !widget.purchaseListViewModel().dynamicPropertyMetaInfo.dynamicPropertyMetaCache.hasOwnProperty(CCConstants.ENDPOINT_PURCHASE_LIST_TYPE)) {
          widget.purchaseListViewModel().populateDynamicPropertiesMetaData();
        }
        
        // To append locale for purchase List link
        widget.detailsLinkWithLocale = ko.computed(function() {
          return navigation.getPathWithLocale('/purchaseList/');
        }, widget);
        
         $.Topic("Purchase_List_Refresh.memory").subscribe(function(){
             
            //  if (page.path.split("/")[0] == "profile") {
          widget.displaySection("list");
          widget.purchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
            widget.sharedPurchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
            widget.sharedPurchaseListListingViewModel().selectedListOption("account");
          if(widget.user().isB2BUser())
          {
            widget.purchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
              widget.sharedPurchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
          }
          widget.purchaseListListingViewModel().fetchAllPurchaseLists();
            if(widget.isSharedListLoaded()) {
              widget.sharedPurchaseListListingViewModel().refinedFetch();
            }
          widget.hideEditAndCreateSection(true);
          widget.purchaseListViewModel().items.removeAll();
        //   widget.selectedPurchaseListItems.removeAll();
           
        // }
         })
         
                 //create purchaseList grid computed for the widget
        widget.purchaseListGrid = ko.computed(function() {
          var numElements, start, end, width;
          var rows = [];
          var purchaseLists;
          if (($(window)[0].innerWidth || $(window).width()) > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
            var startPosition, endPosition;
            // Get the orders in the current page
              purchaseLists = widget.purchaseListListingViewModel().data().sort(function (a, b) {
            var dateA = new Date(a.creationDate), dateB = new Date(b.creationDate);
            return dateB - dateA;
          })
            startPosition = (widget.purchaseListListingViewModel().currentPage() - 1) * widget.purchaseListListingViewModel().itemsPerPage;
            endPosition = startPosition + widget.purchaseListListingViewModel().itemsPerPage;
            purchaseLists = purchaseLists.slice(startPosition, endPosition);
          } else {
            purchaseLists = widget.purchaseListListingViewModel().data().sort(function (a, b) {
                var dateA = new Date(a.creationDate), dateB = new Date(b.creationDate);
                return dateB - dateA;
              });
          }
          if (!purchaseLists) {
            return;
          }
          numElements = purchaseLists.length;
          width = parseInt(widget.purchaseListListingViewModel().itemsPerRow(), 10);
          start = 0;
          end = start + width;
          while (end <= numElements) {
            rows.push(purchaseLists.slice(start, end));
            start = end;
            end += width;
          }
          if (end > numElements && start < numElements) {
            rows.push(purchaseLists.slice(start, numElements));
          }
          return rows;
        }, widget);

        widget.sharedPurchaseListGrid = ko.computed(function () {
          var numElements, start, end, width;
          var rows = [];
          var purchaseLists;
          if (($(window)[0].innerWidth || $(window).width()) > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
            var startPosition, endPosition;
            // Get the orders in the current page
            purchaseLists = widget.sharedPurchaseListListingViewModel().data().sort(function (a, b) {
            var dateA = new Date(a.creationDate), dateB = new Date(b.creationDate);
            return dateB - dateA;
          })
            startPosition = (widget.sharedPurchaseListListingViewModel().currentPage() - 1) * widget.sharedPurchaseListListingViewModel().itemsPerPage;
            endPosition = startPosition + widget.sharedPurchaseListListingViewModel().itemsPerPage;
            purchaseLists = purchaseLists.slice(startPosition, endPosition);
          } else {
            purchaseLists =  widget.sharedPurchaseListListingViewModel().data().sort(function (a, b) {
                var dateA = new Date(a.creationDate), dateB = new Date(b.creationDate);
                return dateB - dateA;
              });
          }
          if (!purchaseLists) {
            return;
          }
          numElements = purchaseLists.length;
          width = parseInt(widget.sharedPurchaseListListingViewModel().itemsPerRow(), 10);
          start = 0;
          end = start + width;
          while (end <= numElements) {
            rows.push(purchaseLists.slice(start, end));
            start = end;
            end += width;
          }
          if (end > numElements && start < numElements) {
            rows.push(purchaseLists.slice(start, numElements));
          }
          return rows;
        }, widget);

        //array containing the checked items of purchase list 
        widget.selectedPurchaseListItems = ko.observableArray();

        widget.selectedAllPurchaseListItems = ko.pureComputed({
          read: function() {
            return widget.selectedPurchaseListItems().length === widget.purchaseListViewModel().items().length;
          },

          write: function(value) {
            widget.selectedPurchaseListItems(value ? widget.purchaseListViewModel().items.slice(0) : []);
          }
        });
        
         widget.dirtyCheck = ko.computed(function() {
         
          if(widget.purchaseListViewModel().purchaseListName.isModified())
           return true ;
        
          for(var i=0;i<widget.purchaseListViewModel().items().length;i++)
          {
            if(widget.purchaseListViewModel().items()[i].quantityDesired.isModified())
            return true;
          }
          
          if(widget.isDirty())
          return true;
        
          return false;
        });
        
        widget.invalidQuantityCheck = ko.computed(function() {
        
          var items = widget.selectedPurchaseListItems();
         
          for(var i=0;i<items.length;i++)
          {
            if(!widget.isNumeric(items[i].quantityDesired())|| items[i].quantityDesired()==0)
            return  true;
          }
          
          return false;
        });
        
        // hide the challange dialog box
        widget.hideModal = function () {
          if(this.isModalVisible()) {
            $("#CC-purchaseList-delete-modal-1").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            this.isModalVisible(false);
          }
        };
        
        widget.showModal = function () {
          $("#CC-purchaseList-delete-modal-1").modal('show');
          this.isModalVisible(true);
        };
     
        
        PurchaseListViewModel.prototype.fetchSkusDetailsList = function() {
        var self = this;
        var url = CCConstants.ENDPOINT_PRODUCTS_LIST_SKUS;
        var contextObj = {};
        var inputData = {};
        contextObj[CCConstants.ENDPOINT_KEY] = CCConstants.ENDPOINT_PRODUCTS_LIST_SKUS;
        contextObj[CCConstants.IDENTIFIER_KEY] = "skuListing";
        var filterKey = this.storeConfiguration.getFilterToUse(contextObj);
        if (filterKey) {
            inputData[CCConstants.FILTER_KEY] = filterKey
        }
        inputData[CCConstants.SKU_IDS] = self.skuIds;
        if (self.skuIds.length > 0) {
            var skuData = new Object;
            skuData["deletedSkus"] = [];
            skuData["items"] = [];
            var deferredArray = new Array;
            var batchSize = StoreUtils.getEntityCountLimitInGetRequest(10, self.skuIds.length);
            if (batchSize < 0) {
                batchSize = self.skuIds.length
            }
            var individualInputData = Object.create(inputData);
            for (var i = 0; i < self.skuIds.length; i) {
                var individualBatchIds = [];
                for (var j = 0; j < batchSize && i < self.skuIds.length; j++,
                i++) {
                    individualBatchIds.push(self.skuIds[i])
                }
                individualInputData[CCConstants.SKU_IDS] = individualBatchIds;
                deferredArray.push(self.getDataBatch(url, individualInputData))
            }
            $.when.apply(null, deferredArray).done(function() {
                for (var i = 0; i < arguments.length; i++) {
                    var partialSkuData = arguments[i];
                    skuData["deletedSkus"] = partialSkuData["deletedSkus"] != null ? skuData["deletedSkus"].concat(partialSkuData["deletedSkus"]) : skuData["deletedSkus"];
                    skuData["items"] = skuData["items"].concat(partialSkuData["items"] || partialSkuData)
                }
                this.fetchSkuDetailsListSuccess.bind(this, skuData)()
            }
            .bind(this)).fail(function() {
                var errorData;
                for (var i = 0; i < arguments.length; i++) {
                    errorData = arguments[i]
                }
                this.fetchSkuDetailsListError.bind(this, errorData)()
            }
            .bind(this))
        } else {
            $.Topic(pubsub.topicNames.PURCHASE_LIST_FETCH_SUCCESS).publish()
        }
    }
        
        PurchaseListViewModel.prototype.populateAdditionalInfoForItems = function(items) {
        // var self = this;
        var purchaseListItems = [];
        for(var i=0;i < this.itemsFromFetchPurchaseListCall().length; i++) {
        if(this.invalidSkus.indexOf(this.itemsFromFetchPurchaseListCall()[i].catRefId) != -1) {
            var purchaseListItemData = {
                  catRefId:this.itemsFromFetchPurchaseListCall()[i].catRefId,
                  quantityDesired:this.itemsFromFetchPurchaseListCall()[i].quantityDesired,
                  productId: this.itemsFromFetchPurchaseListCall()[i].productId,                  
                  productName:"",
                  displayName:"",
                  thumbnailUrl:null,
                  selectedOptions: null,
                  path: null,
                  route: null,
                  isProductDeleted: true,
                  x_actualSize: null,
                  longDescription : ""
               };

              var productItem = new PurchaseListItem(purchaseListItemData);
              productItem.quantityDesired.extend({
                required: { params: true, message: CCi18n.t('ns.common:resources.emptyCheck') },
                digit: { params: true, message: CCi18n.t('ns.common:resources.validQuantityCheck') },
                min: { params: 1, message: CCi18n.t('ns.common:resources.minQuantityCheck') }
              });
              purchaseListItems.push(productItem);
            } else {
              for (var itemIndex = 0; itemIndex < items.length; itemIndex++) {
                if (items[itemIndex].repositoryId === this.itemsFromFetchPurchaseListCall()[i].catRefId) {
                  var giftItem = items[itemIndex];
                  var purchaseListItemData = {
                    catRefId: giftItem.repositoryId,
                    quantityDesired: this.itemsFromFetchPurchaseListCall()[i].quantityDesired,
                    productId: giftItem.parentProducts[0].id,
                    productName: giftItem.parentProducts[0].displayName,
                    thumbnailUrl: giftItem.parentProducts[0].thumbImageURLs[0],
                    selectedOptions: giftItem.productVariantOptions ? giftItem.productVariantOptions[0] : null,
                    path: giftItem.parentProducts[0].route,
                    isProductDeleted: false,
                      x_actualSize : giftItem.x_actualSize,
                    longDescription: giftItem.parentProducts[0].longDescription
                  };

                  var productItem = new PurchaseListItem(purchaseListItemData);
                  productItem.quantityDesired.extend({
                    required: { params: true, message: CCi18n.t('ns.common:resources.emptyCheck') },
                    digit: { params: true, message: CCi18n.t('ns.common:resources.validQuantityCheck') },
                    min: { params: 1, message: CCi18n.t('ns.common:resources.minQuantityCheck') }
                  });
                  purchaseListItems.push(productItem);
                }
              }
            }
          }
          this.items(purchaseListItems);
        }
                var collections = widget.site().extensionSiteSettings.b2bSiteSettings.collections ? widget.site().extensionSiteSettings.b2bSiteSettings.collections : '';
                widgetModel.bannerJsonObject = ko.computed(function () {
                  if (collections) {
                    return JSON.parse(collections);
                  }
                }, widget);
                widgetModel.getCreditLimitValues()
                
                $.Topic("UPLOAD_WINDOW.memory").subscribe(function(obj){
                    if(this === true){
                        widgetModel.disableDelete(true);
                    } else if(this === false) {
                        widgetModel.disableDelete(false);
                    }
                });
                
      },
      
         getCreditLimitValues: function(){
                
             $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE)  + '?fields=items.priceInfo.total&q=organizationId="' + widgetModel.user().parentOrganization.id() + '" AND (state="PENDING_APPROVAL")',
                dataType: "json",
                type: 'GET',
                contentType: "application/json",
                headers: {
                  'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                  'x-ccsite': widgetModel.site().siteInfo.repositoryId
                },
            async: true,
            success: function(data) {
                var pendingOrders = data.items ? data.items : '';
                var orderTotal = pendingOrders.reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.priceInfo.total)},0);
                // if(widgetModel.isUpcomingCollection()){
                    widgetModel.getCurrentSelectionTotal().then(function(successData){
                        widgetModel.effectiveCreditLimit(Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(orderTotal) - Number(successData.totalPrice));
                        console.log('widgetModel.effectiveCreditLimit >>>>',widgetModel.effectiveCreditLimit());
                        if(widgetModel.effectiveCreditLimit() > 0){
                            widgetModel.creditLimitExceeded(false);
                        } else {
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                            widgetModel.creditLimitExceeded(true);
                        }
                    }, function(errorData){
                        logger.log(errorData);
                    });
                    
                // }
               
            }
        });
        },
        getCurrentSelectionTotal: function(){
          var getDetails = {
                url: widgetModel.translate('selectionTotalUrl') + "?accountId=" + widgetModel.user().parentOrganization.id() + "&emailId="  + widgetModel.user().emailAddress() ,
                dataType: "json",
                type: 'GET',
                contentType: "application/json"
            };
          
            return Promise.resolve($.ajax(getDetails));  
        },
        
        getProductsBatch : function(productIds){
            return new Promise(function(resolve,reject){
                var productsBatch = [];
                for( var i = 0; i < productIds.length; i = i + self.PRODUCTS_BATCH ){
                    productsBatch.push(productIds.slice(i, i + self.PRODUCTS_BATCH)); 
                }
                var ctr = 0;
                productsBatch.forEach(function(item,index){
                    var input = {};
                    input[CCConstants.PRODUCT_IDS] = item;
                    input[CCConstants.FIELDS_QUERY_PARAM] = ["id","x_webStyleId"];
                    ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input, function (successData) {
                        var sucessCtr = 0;
                        successData.forEach(function (val, i) {
                            if (val.x_webStyleId) {
                                productsWebStyleId.push("product.x_webStyleId:"+val.x_webStyleId);
                            }
                            sucessCtr++;
                            if(sucessCtr == successData.length ){
                                ctr++;
                            }
                            if(ctr == productsBatch.length ){
                                resolve (true);
                            }
                        });
                        
                    },item);    
                });    
            })
                    
        },
  processSelectionData: function(){
            self.getProductsBatch(self.productsId).then(function (data) {
                    var productsUniqueWebStyleId = [];
                    for (var i = 0; i < productsWebStyleId.length; i++) {
                        if (productsUniqueWebStyleId.indexOf(productsWebStyleId[i]) === -1) {
                            productsUniqueWebStyleId.push(productsWebStyleId[i]);
                        }
                    }
                    self.multipleProductByWebStyleId(productsUniqueWebStyleId).then(function(successData){
                        var currentTotal = self.productSkuData.reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.listPrice)},0);
                        var availableCreditLimit =  widgetModel.effectiveCreditLimit() - parseInt(currentTotal, 10);
                        if( parseInt(availableCreditLimit, 10) < 1 ){
                            widgetModel.creditLimitExceeded(true);
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                            $('.overlay_wrapper').css('display','none')
                            $('#CC-purchaseLists #checkout_loader').css('display','none')
                            widgetModel.showLoaderList(false)
                            widgetModel.showLoaderDetail(false)
                            return;
                        } else {
                            var productsBatchArray = [];
                            var purchaseListId = null; 
                            for(i = 0; i < self.productSkuData.length; i = i+ self.ADD_TO_SELECT_BATCH){
                                productsBatchArray.push(self.productSkuData.slice(i,i+ self.ADD_TO_SELECT_BATCH));  
                            }
                            var productsBatchLength = productsBatchArray.length;
                            productsBatchArray.reduce(function(childBatch, values, i){
                                return childBatch.then(function(success){
                                    if( i == productsBatchLength - 1) {
                                        purchaseListId = self.listIdSelect; 
                                    }
                                  return self.addDataToSelection(values,purchaseListId);
                                });
                            }, Promise.resolve()).then(function(successData){
                                self.purchaseSkus.removeAll();
                                self.productSkuData = [];
                                productsWebStyleId = [];
                                notifier.sendSuccess(self.WIDGET_ID,successData.Data, true);
                                $('.overlay_wrapper').css('display','none')
                                $('#CC-purchaseLists #checkout_loader').css('display','none')
                                widgetModel.showLoaderList(false)
                                widgetModel.showLoaderDetail(false)
                            },function(error){
                                console.log("ERROR:: ", error);
                                notifier.sendError(self.WIDGET_ID,error.responseJSON.message, true);
                                $('.overlay_wrapper').css('display','none')
                                $('#CC-purchaseLists #checkout_loader').css('display','none')
                                widgetModel.showLoaderList(false)
                                widgetModel.showLoaderDetail(false)
                                self.purchaseSkus.removeAll();
                                self.productSkuData = [];
                                productsWebStyleId = [];
                                logger.error(error)});  
                        }
                    },function(error){
                        logger.log(error);    
                    });
                    
                },function (errorData) {
                    logger.log(errorData);
                });    
        },
             // Add purchase list to Selection list from profile page
             handleAddPurchaseToSelection: function (data) {
                $('.overlay_wrapper').css('display','block')
                $('#CC-purchaseLists #checkout_loader').css('display','block')
                widgetModel.showLoaderList(true);
                
                listId = this.id;
                self.listIdSelect = this.id;
                self.productsId = []
                self.categoryType = this.name.split('-')[0].trim();
              
                this.items.forEach(function (val, i) {
                    self.productsId.push(val.productId);
                    self.purchaseSkus.push({
                        catRefId: val.catRefId,
                        quantityDesired: val.quantityDesired
                    });
                });
                self.processSelectionData();
                
            },
            
            //Add purchase list to Selection list
            handleAddToSelection: function (data) {
               
                widgetModel.showLoaderDetail(true)
                self.listIdSelect = data.purchaseListViewModel().id();
                self.productsId = [];
                data.pProductData().forEach(function (value, index) {
                    value.forEach(function(val, i){
                    self.productsId.push(val.productId);
                    self.purchaseSkus.push({
                        catRefId: val.catRefId,
                        quantityDesired: val.quantityDesired
                    });
                });
                });
                
                self.categoryType = self.purchaseListGrid().length > 0 ? self.purchaseListGrid()[0].name.split('-') ? self.purchaseListGrid()[0].name.toLowerCase().split('-')[0].trim() : self.purchaseListGrid()[0].name.toLowerCase() : self.purchaseListViewModel().purchaseListName().toLowerCase() ? self.purchaseListViewModel().purchaseListName().split('-') ? self.purchaseListViewModel().purchaseListName().split('-')[0].trim() : self.purchaseListViewModel().purchaseListName().toLowerCase() : self.purchaseListViewModel().purchaseListName().toLowerCase();
                self.processSelectionData();
                
                            },
            //Get the all products by there webStyleId
            multipleProductByWebStyleId: function (webStyleIds) {
                return new Promise(function(resolve,reject){
                var webStyleBatch = [];
                for( var i = 0; i < webStyleIds.length; i = i + self.MAX_ITEMS_PER_BATCH ){
                    webStyleBatch.push(webStyleIds.slice(i, i+self.MAX_ITEMS_PER_BATCH)); 
                }
                var prdctCtr = 0;
                webStyleBatch.forEach(function(item,index){
                    var i, input = {},productArrForSku = [];
                    input[CCConstants.SEARCH_NAV_RECORD_FILTER_KEY] = 'OR('+ item +')';
                    input[CCConstants.SEARCH_NAV_DESCRIPTORS_KEY] = 0,
                    input[CCConstants.SEARCH_REC_PER_PAGE_KEY] = 250,
                    input[CCConstants.FIELDS_QUERY_PARAM] = ["product.repositoryId"];
                    ccRestClient.request(CCConstants.ENDPOINT_SEARCH_SEARCH,
                        input,
                        function (successData) {
                            // noOfArrays++;
                            successData.resultsList.records.forEach(function (rValue, index) {
                                    productArrForSku.push(rValue.records[0].attributes["product.repositoryId"][0]);
                            });
                            // var productSkuData = [];
                            var input = {};
                            
                            input[CCConstants.PRODUCT_IDS] = productArrForSku;
                            input[CCConstants.FIELDS_QUERY_PARAM] = ["id","displayName","primaryFullImageURL","route","childSKUs.repositoryId","childSKUs.x_actualSize","childSKUs.salePrice","childSKUs.listPrice","childSKUs.x_fabLongDesc","parentCategories","parentCategory","listPrice","salePrice","x_collectionType","x_categoryType","x_webStyleId","x_primaryCategory","x_filterColor","x_pseStyleCode"];
                                ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input, function (successData) {
                                    var skuCtr = 0; 
                                    successData.forEach(function (pValue, pI) {
                                        var childCtr = 0;
                                        pValue.childSKUs.forEach(function (skuValue, skuIndex) {
                                            // self.quantity(0);
                                            self.purchaseSkus().forEach(function (listVal, listI) {
                                                if (listVal.catRefId === skuValue.repositoryId) {
                                                  var qty = parseInt(listVal.quantityDesired) ? parseInt(listVal.quantityDesired) : listVal.quantityDesired();
                                                  skuValue.quantity = qty ;
                                                } 
                                            });
                                            
                                            var parentCategoryArr = [];
                                            pValue.parentCategories.forEach(function(item,index){
                                                parentCategoryArr.push(item.repositoryId)
                                            })
                                            
                                            if((pValue.x_collectionType.toLowerCase() === self.bannerJsonObject().upcoming.collectionType.toLowerCase()) && (pValue.x_categoryType.toLowerCase() === self.categoryType.toLowerCase())){
                                                self.productSkuData.push({
                                                    productId: pValue.id,
                                                    skuId: skuValue.repositoryId,
                                                    listPrice: skuValue.listPrice + '',
                                                    salePrice: skuValue.salePrice + '',
                                                    size: skuValue.x_actualSize,
                                                    categoryType: pValue.x_categoryType,
                                                    webStyleId: pValue.x_webStyleId,
                                                    pscStyleCode: pValue.x_pseStyleCode,
                                                    parentCategory: pValue.x_primaryCategory ? 'b2b-'+ pValue.x_primaryCategory : pValue.parentCategory.repositoryId,
                                                    parentCategories : parentCategoryArr,
                                                    x_filterColor : pValue.x_filterColor,
                                                    quantity:skuValue.quantity ? skuValue.quantity +'' : 0 +'',
                                                    displayName: pValue.displayName,
                                                    productImg: pValue.primaryFullImageURL,
                                                    productLink: pValue.route,
                                                    productListPrice: pValue.listPrice,
                                                    productSalePrice: pValue.salePrice,
                                                    longDescription: skuValue.x_fabLongDesc
                                                });
                                            }
                                            childCtr++;
                                            if(childCtr ==  pValue.childSKUs.length){
                                                skuCtr++;    
                                            }
                                            })
                                            if(successData.length == skuCtr){
                                                prdctCtr++;
                                            }
                                            if(prdctCtr == webStyleBatch.length  ){
                                                resolve(true);
                                            }
                                    });
                                    
                            },function(error){
                                console.log("ERROR:: List is Empty", error);
                            });
                        });
                    });
                });
            },
            //Add the products group by webStyleId with their child sku's to selection list
            addDataToSelection: function (productSKUs,purchaseListId) {
                return new Promise(function(resolve,reject){
                    var json;
                    json = {
                        isPDP : false,
                        accountId: self.user().parentOrganization.id(),
                        emailId: self.user().emailAddress(),
                        categoryType: self.categoryType.toUpperCase(),
                        products: productSKUs,
                        purchaseListId: purchaseListId
                    };
                    self.addToSelection(self.translate(self.ADD_RECORDS_TO_SELECTION_URL), json).then(function (successData) {
                        resolve(successData);
                       }, function (error) {
                        reject(error);
                    });
                    
                });
                
            },

            addToSelection: function (addToSelectUrl, jsonData) {
                var selectedProductDetails = {
                    url: addToSelectUrl,
                    dataType: "json",
                    type: 'POST',
                    contentType: "application/json",
                    data: JSON.stringify(jsonData)
                };
                return Promise.resolve($.ajax(selectedProductDetails));
            },
            
               
           checkWindowClosed: function(){
             var buyDateFranchise = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise,"DD-MM-YYYY") : '';
                    buyDateFranchise = buyDateFranchise.format('YYYY-MM-DD')
                    var endDateFranchise =  widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise,"DD-MM-YYYY") : '';
                    endDateFranchise = endDateFranchise.format('YYYY-MM-DD')
                    var buyDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM,"DD-MM-YYYY") : '';
                    buyDateRM = buyDateRM.format('YYYY-MM-DD')
                    var endDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM,"DD-MM-YYYY") : '';
                    endDateRM = endDateRM.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                for(var i=0;i< widgetModel.user().roles().length; i++){
                    if(moment(currentDate).isAfter(endDateFranchise,'day') || moment(currentDate).isBefore(buyDateFranchise,'day')){
                        if((widgetModel.user().roles()[i].function == "approver") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId) || (widgetModel.user().roles()[i].function == "admin") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(moment(currentDate).isAfter(endDateRM,'day') || moment(currentDate).isBefore(buyDateRM,'day')){
                                widgetModel.windowClosedForRM(true);
                                setTimeout(function(){
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('regionalManagerError'), true);
                                })
                                break;
                            }
                            else{
                                widgetModel.windowClosedForRM(false);
                            }
                        }
                        else if((widgetModel.user().roles()[i].function == "buyer") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(widgetModel.user().roles().length == 1){
                            widgetModel.windowClosedForBuyer(true);
                            setTimeout(function(){
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('buyerError'), true);
                            });
                        }
                            
                        }
                    }
                else{
                    widgetModel.windowClosedForBuyer(false); 
                }
        }
        
               if(widgetModel.windowClosedForBuyer() || widgetModel.windowClosedForRM()){
                   widgetModel.windowClosed(true);
                   return;
               }
               else{
                   widgetModel.windowClosed(false);
               }
          
      },
        
            beforeAppear: function (page) {
                var widget = this;
                widget.windowClosed = ko.observable(true),
                widget.windowClosedForBuyer = ko.observable(false),
                widget.windowClosedForRM = ko.observable(false),
                widget.checkRoleforBuyer();
                    widgetModel.checkWindowClosed()
                widgetModel.pProductData.removeAll();
                if (widget.user().loggedIn() == false) {
                    navigation.doLogin(navigation.getPath(), widget.links().home.route);
                } else {
                    widget.purchaseListListingViewModel().clearOnLoad = true;
                    widget.sharedPurchaseListListingViewModel().clearOnLoad = true;
                    widget.purchaseListViewModel().profileId = widget.user().id();
                }
                //logic to show display sections of widget according to the page
                if (page.path.split("/")[0] == "profile") {
                    widget.displaySection("list");
                    widget.purchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
                    widget.sharedPurchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
                    widget.sharedPurchaseListListingViewModel().selectedListOption("account");
                    if (widget.user().isB2BUser()) {
                        widget.purchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
                        widget.sharedPurchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
                    }
                    widget.purchaseListListingViewModel().fetchAllPurchaseLists();
                    if (widget.isSharedListLoaded()) {
                        widget.sharedPurchaseListListingViewModel().refinedFetch();
                    }
                    widget.hideEditAndCreateSection(true);
                    widget.purchaseListViewModel().items.removeAll();
                    widget.selectedPurchaseListItems.removeAll();

                } else if (page.path.split("/")[0] == "purchaseList") {
                    if (page.path.split("/")[1]) {
                        widget.purchaseListViewModel().items.removeAll();
                        widget.selectedPurchaseListItems.removeAll();
                        widget.editPurchaseList(page.path.split("/")[1]);
                    } else {
                        widget.purchaseListViewModel().items.removeAll();
                        widget.nameOfPurchaseListPrepopulate("");
                        $('#search-bar-create').val('').change();
                        widget.displaySection("create");
                    }
                    widget.hideEditAndCreateSection(false);
                }
            },

            //check the contact role to change the view of suggestive buy

            checkRoleforBuyer: function () {

                for (var i = 0; i < self.user().roles().length; i++) {
                    if (self.user().roles()[i].function == "buyer") {
                        if (self.user().roles().length == 1)
                            self.handleFilterSelection(self.sharedPurchaseListListingViewModel(), self);
                    }
                }
                widgetModel.showLoaderList(false)
                widgetModel.showLoaderDetail(false)
                
                $('.overlay_wrapper').css('display','none')
                $('#CC-purchaseLists #checkout_loader').css('display','none')
               
            },

            handleFilterSelection: function (pListingContext, pData) {

                // if(pEvent.originalEvent) {
                pListingContext.refinedFetch();
                self.isSharedListLoaded(true);
                // }
            },

            //called on pressing the delete button on purchase list page
            handleDeleteSelectedPurchaseList: function () {
                var widget = this;
                widget.hideModal();
                if (widget.pDeleteData()) {
                    ccRestClient.request(CCConstants.ENDPOINT_DELETE_PURCHASE_LIST, {},
                        function (successData) {
                            // console.log("sucessssss");
                            widget.displaySection("list");
                            widget.purchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
                            widget.sharedPurchaseListListingViewModel().siteId(SiteViewModel.getInstance()['siteInfo'].id);
                            widget.sharedPurchaseListListingViewModel().selectedListOption("account");
                            if (widget.user().isB2BUser()) {
                                widget.purchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
                                widget.sharedPurchaseListListingViewModel().accountId(widget.user().currentOrganization().repositoryId);
                            }
                            widget.purchaseListListingViewModel().fetchAllPurchaseLists();
                            if (widget.isSharedListLoaded()) {
                                widget.sharedPurchaseListListingViewModel().refinedFetch();
                            }
                            widget.hideEditAndCreateSection(true);
                            widget.purchaseListViewModel().items.removeAll();
                            widget.selectedPurchaseListItems.removeAll();
                        },
                        function (errorData) {
                            // spinner.destroyWithoutDelay(self.spinnerOptions.parent);
                        }, widget.pDeleteData());

                } else {
                    widget.purchaseListViewModel().deletePurchaseList(widget.purchaseListViewModel().purchaseListId,
                        widget.deletePurchaseListSuccess.bind(widget), widget.deletePurchaseListError.bind(widget));
                }
            },

      //called on pressing the delete button on purchase list page
      deleteSelectedPurchaseList: function (pData) {
        var widget = this;
        var formData = { 
           "purchaseListId": pData
        }
        //To check is Purchase List exist in Selection List or Not
        $.ajax({
            url : widgetModel.translate(widgetModel.DELETE_PURCHASELIST_ID),
            type: 'POST',
            dataType : "json",
            data: formData,
            success : function(successData){
                if(successData.Status !== 200){
                    notifier.clearError(widget.WIDGET_ID);
                    notifier.clearSuccess(widget.WIDGET_ID);
                    notifier.sendError(widget.WIDGET_ID,successData.Data, true);
                }else{
                    widget.pDeleteData(pData);
                    widget.showModal();
                }
            },
            error : function error(error){
                    ccLogger.info('ERROR:: While Deleting purchase list id',error);
                }
        });
      },

      //called on pressing the delete icon against each purchaselist item
      removeItem: function(data) {
        var widget = this;
        widget.isDirty(true);
        widget.purchaseListViewModel().items.remove(function(toDelete) {
          return toDelete.catRefId == data.catRefId;
        });
        widget.selectedPurchaseListItems.remove(function(toDelete) {
          return toDelete.catRefId == data.catRefId;
        });
      },
      
      isNumeric: function(value) {
        return /^\d+$/.test(value);
      },

      // callback for delete purchaseList error
      deletePurchaseListError: function(pResult) {
        var widget = this;
        var errorMsg = widget.translate(pResult.message);
        notifier.clearError(widget.WIDGET_ID);
        notifier.clearSuccess(widget.WIDGET_ID);
        notifier.sendError(widget.WIDGET_ID,errorMsg, true);
      },

      // callback for delete purchaseList success
      deletePurchaseListSuccess: function() {
        var widget = this;
        var successMsg = widget.translate('deleteSuccessMsg');
        notifier.clearError(widget.WIDGET_ID);
        notifier.clearSuccess(widget.WIDGET_ID);
        widget.purchaseListListingViewModel().fetchAllPurchaseLists();
        navigation.goTo('/profile');
        setTimeout(function(){notifier.sendSuccess(widget.WIDGET_ID,successMsg, true);},500);
      },
      
      //TypeAhead Functions


      editPurchaseList: function(id) {
        var widget = this;
        widget.purchaseListViewModel().purchaseListId = id;
        widget.purchaseListViewModel().items.removeAll();
        widget.purchaseListViewModel().fetchPurchaseList(id);
        $.Topic(PubSub.topicNames.PURCHASE_LIST_FETCH_SUCCESS).subscribe(function() {
          widget.selectedPurchaseListItems(widget.purchaseListViewModel().items().slice(0));
          widget.displaySection("edit");
          widget.purchaseListViewModel().purchaseListName.isModified(false);
          for (var i = 0; i < widget.purchaseListViewModel().items().length; i++) {
            widget.purchaseListViewModel().items()[i].quantityDesired.isModified(false);
          }


          // custom code to group the products of the same sku
          var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x, index) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          };

          var groubedByTeam = Object.values(groupBy(widget.purchaseListViewModel().items(), 'productId'))
          widget.pProductData(groubedByTeam);
          widget.pProductData.valueHasMutated();

        });
      },

      clickPurchaseListDetails: function (data, event) {
        widgetModel.user().validateAndRedirectPage(widgetModel.links().purchaseList.route+'/'+ data.id);
      }
    };
  }
);
