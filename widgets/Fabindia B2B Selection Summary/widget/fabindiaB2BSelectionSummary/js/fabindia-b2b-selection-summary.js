/**
 * @fileoverview Payment Gateway Option Widget.
 * 
 * @author 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'ccConstants', 'pageViewTracker','ccRestClient','pubsub','jquery','notifier','navigation', 'moment','ccLogger'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, CCConstants, pageViewTracker,ccRestClient, pubsub,$,notifier,navigation, moment, log) {

    "use strict";
    var downloadArrFile = [];
    var widgetModel;    
    var authorizationHeader;
    var cartData = {};
    var selectSkuData = [];
    var displayData = [];
    var totalWebstyle = 0;
    var totalSkuCount = 0;
    var totalQTY = 0;
    var totalLP = 0;
    var totalProductsCount = 0;
    var summaryDataArr = [];
    return {
        
    
    webStyleArr: ko.observableArray(),
      invoiceOrder: ko.observable(true),
      isInvoicePaymentEnabled : ko.observable(true),
      GET_RECORDS_FOR_SELECTION_URL: "selectionUrl",
      DELETE_SELECTION_URL: "deleteUrl",
      OAUTH_TOKEN: "oAuth",
      ADD_TO_CART_URL:"createOrder",
      poNumber: ko.observable(),
      selectionData:ko.observableArray([]),
      emptySelectionList:ko.observable(false), 
      totalWebstyeVal: ko.observable(),
      totalProductsCount: ko.observable(),
      totalSkuCount: ko.observable(),
      totalQuantity:ko.observable(),
      totalLPQTY:ko.observable(),
      showLoader: ko.observable(false),
      windowClosed:ko.observable(true),
      windowClosedForBuyer:ko.observable(false),
      windowClosedForRM:ko.observable(false),
      showLoader: ko.observable(false),
      
      onLoad : function(widget) {
        widgetModel = widget;
        
      },
      clearTotalValues: function(){
        totalWebstyle = 0;
        totalSkuCount = 0;
        totalQTY = 0;
        totalLP = 0;
        totalProductsCount = 0; 
        widgetModel.webStyleArr.removeAll();
        summaryDataArr = [];
      },
      beforeAppear: function(page){
        widgetModel.windowClosed(true),
        widgetModel.windowClosedForBuyer(false),
        widgetModel.windowClosedForRM(false),
        widgetModel.clearTotalValues();
        widgetModel.showLoader(true);
        widgetModel.checkWindowClosed();
        widgetModel.prepareInvoiceOrder();   
      },
     
         checkWindowClosed: function(){
             var buyDateFranchise = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise,"DD-MM-YYYY") : '';
                    buyDateFranchise = buyDateFranchise.format('YYYY-MM-DD')
                    var endDateFranchise =  widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise,"DD-MM-YYYY") : '';
                    endDateFranchise = endDateFranchise.format('YYYY-MM-DD')
                    var buyDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM,"DD-MM-YYYY") : '';
                    buyDateRM = buyDateRM.format('YYYY-MM-DD')
                    var endDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM,"DD-MM-YYYY") : '';
                    endDateRM = endDateRM.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                for(var i=0;i< widgetModel.user().roles().length; i++){
                    if(moment(currentDate).isAfter(endDateFranchise,'day') || moment(currentDate).isBefore(buyDateFranchise,'day')){
                        if((widgetModel.user().roles()[i].function == "approver") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId) || (widgetModel.user().roles()[i].function == "admin") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(moment(currentDate).isAfter(endDateRM,'day') || moment(currentDate).isBefore(buyDateRM,'day')){
                                widgetModel.windowClosedForRM(true);
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('regionalManagerError'), true);
                                break;
                            }
                            else{
                                widgetModel.windowClosedForRM(false);
                            }
                        }
                        else if((widgetModel.user().roles()[i].function == "buyer") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(widgetModel.user().roles().length == 1){
                            widgetModel.windowClosedForBuyer(true);
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('buyerError'), true);
                        }
                            
                        }
                    }
                else{
                    widgetModel.windowClosedForBuyer(false); 
                }
        }
        
               if(widgetModel.windowClosedForBuyer() || widgetModel.windowClosedForRM()){
                   widgetModel.windowClosed(true);
                   return;
               }
               else{
                   widgetModel.windowClosed(false);
               }
          
      },
      resourcesLoaded : function(widget) {
      },
      
      goToSelection : function(){
          navigation.goTo(widgetModel.translate('selectionLinkText'));
      },
      
      //csv download start
      exportToCsv: function (filename, rows) {
            var processRow = function (row) {
                var finalVal = '';
                for (var j = 0; j < row.length; j++) {
                    var innerValue = (row[j] === null || row[j] === undefined) ? '' : row[j].toString();
                    if (row[j] instanceof Date) {
                        innerValue = row[j].toLocaleString();
                    };
                    var result = innerValue.replace(/"/g, '""');
                    if (result.search(/("|,|\n)/g) >= 0)
                        result = '"' + result + '"';
                    if (j > 0)
                        finalVal += ',';
                    finalVal += result;
                }
                return finalVal + '\n';
            };

            var csvFile = '';
            for (var i = 0; i < rows.length; i++) {
                csvFile += processRow(rows[i]);
            }
            
            console.log(csvFile,"csvFile")

            var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
            if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(blob, filename);
            } else {
                var link = document.createElement("a");
                if (link.download !== undefined) { // feature detection
                    // Browsers that support HTML5 download attribute
                    var url = URL.createObjectURL(blob);
                    link.setAttribute("href", url);
                    link.setAttribute("download", filename);
                    link.style.visibility = 'hidden';
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }
        },
        
         getWebStyleArrCSV: function(){
                var self = this;
                for(var i = 0; i < widgetModel.webStyleArr().length; i ++){
                    for(var m = 0; m< widgetModel.webStyleArr()[i].length; m++){
                        for(var j=0; j < widgetModel.webStyleArr()[i][m].productData.length; j++){
                            for(var k = 0; k < widgetModel.webStyleArr()[i][m].productData[j].sku.length; k++){
                                for(var l = 0; l < widgetModel.webStyleArr()[i][m].productData[j].sku[k].skuPrice.length; l++){
                                    downloadArrFile.push([
                                        downloadArrFile.length +1,
                                        widgetModel.webStyleArr()[i][m].productData[j].productId,
                                        widgetModel.webStyleArr()[i][m].productData[j].displayName,
                                        widgetModel.webStyleArr()[i][m].productData[j].productImg,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].longDescription,
                                        widgetModel.webStyleArr()[i][m].productData[j].productLink,
                                        widgetModel.webStyleArr()[i][m].categoryType,
                                        widgetModel.webStyleArr()[i][m].productData[j].webstyleId,
                                        widgetModel.webStyleArr()[i][m].productData[j].color,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].size,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].quantity ? widgetModel.webStyleArr()[i][m].productData[j].sku[k].quantity : 0,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].skuId,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].skuPrice[l].listPrice,
                                        widgetModel.webStyleArr()[i][m].productData[j].sku[k].skuPrice[l].salePrice,
                                        widgetModel.webStyleArr()[i][m].productData[j]["sumListPrice"].toFixed(2),
                                        widgetModel.webStyleArr()[i][m].productData[j]["sumSalePrice"].toFixed(2)
                                    ])
                                }
                            }
                        }
                    }
                }
            },
            
             clickHandler : function() {
        widgetModel.getWebStyleArrCSV();
        downloadArrFile.unshift(['Sr. No',
                                'product.id',
                                'product.name',
                                'product.image',
                                'product.longDescription',
                                'product.url',
                                'product.categoryType',
                                'product.webStyleID',
                                'product.color',
                                'product.sku.size',
                                'product.sku.quantity',
                                'product.sku.skuid',
                                'product.sku.MRP',
                                'product.sku.billPrice',
                                'product.sku.totalMRP',
                                'product.sku.totalBillPrice'
                                ])
        widgetModel.exportToCsv(widgetModel.getFormattedName(), downloadArrFile);
        downloadArrFile.length = 0;
    },
    
     getFormattedName : function(){
        var today = new Date();
        var date = today.getFullYear()+'-'+ (today.getMonth()+1) +'-'+today.getDate();
        var time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
        var dateTime = date+'-'+time;
        var fileName = widgetModel.user().currentOrganization().repositoryId +'-'+ dateTime +'.csv';
        return fileName; 
    },
      
      
     
        
      prepareInvoiceOrder: function(){
        if (widgetModel.order().user().loggedIn()) {
      var cookie = document.cookie;
      var cookies = cookie.split(';');
      var storeOauthToken = '';
      var oauthTokenId = widgetModel.translate(widgetModel.OAUTH_TOKEN);
      for (var i = 0; i < cookies.length; i++) {
        if (cookies[i].indexOf(oauthTokenId) >= 0) {
          var storeOauthTokenPair = cookies[i].split('=');
          storeOauthToken = storeOauthTokenPair[1];
          break;
        }
      }
      var decodedToken = decodeURIComponent(storeOauthToken);
      var tokens = decodedToken.split('"');
      authorizationHeader = 'Bearer ' + tokens[1];
      widgetModel.authorizationHeader = authorizationHeader;
    }
        var jsonData = {
            accountId:    widgetModel.user().parentOrganization.id(),
            emailId:      widgetModel.user().emailAddress()
        };
        var shippingAddress = {};
        var billingAddress = {};
        var emailAddress;
        
        shippingAddress = widgetModel.user().contactShippingAddress;
        shippingAddress.selectedCountry = widgetModel.user().contactShippingAddress.country;
        var firstName = widgetModel.user().contactShippingAddress.firstName ? widgetModel.user().contactShippingAddress.firstName : widgetModel.user().firstName();
        var lastName = widgetModel.user().contactShippingAddress.lastName ? widgetModel.user().contactShippingAddress.lastName : widgetModel.user().lastName();
        shippingAddress.firstName = firstName;
        shippingAddress.lastName = lastName;
        if (widgetModel.user().emailAddress()) {
        shippingAddress.email = widgetModel.user().emailAddress()
        }
        cartData.shippingAddress = shippingAddress;
        
        billingAddress = widgetModel.user().contactBillingAddress;
        billingAddress.selectedCountry = widgetModel.user().contactBillingAddress.country;
        billingAddress.firstName = firstName;
        billingAddress.lastName = lastName;
        cartData.billingAddress = billingAddress;
        
        cartData.shippingMethod = {"value": widgetModel.user().parentOrganization.shippingMethods()[0].repositoryId()};
        cartData.isAnonymousCheckout = !(widgetModel.user().loggedIn());
        
        var payments = [];
        payments.push({type : "invoice"});
        cartData.payments = payments;
        
        
        widgetModel.getSelection(widgetModel.translate(widgetModel.GET_RECORDS_FOR_SELECTION_URL), jsonData).then(function(successData){
            cartData.shoppingCart = {items: []}; 
            displayData = widgetModel.getWebStyleArr(successData.categories);
            // widgetModel.totalValuesCalc(successData.categories);
            widgetModel.selectionData(displayData);
            cartData['x_selectionSummary'] = summaryDataArr;
            
            
           
            
        },function(error){
           console.log("error here",error );
        });
      },
   
      getSelection: function(getRecordsUrl, jsonData) {
        if(getRecordsUrl && jsonData){
            var getDetails = {
                url: getRecordsUrl + "?accountId=" + jsonData.accountId + "&emailId=" + jsonData.emailId,
                dataType: "json",
                type: 'GET',
                contentType: "application/json"
            };
          
            return Promise.resolve($.ajax(getDetails));
        }
      },
      deleteSelectionRecord:function(delRecordUrl,jsonData){
        var deleteDetails = {
            url: delRecordUrl,
            dataType: "json",
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify(jsonData)
        };
        return Promise.resolve($.ajax(deleteDetails));
      },
      getWebStyleArr: function(categoryArr){
        var self = this;
        var childCat = [];
        if(categoryArr && categoryArr.length > 0){
            categoryArr.forEach(function(item, index){
                var childObj = {};
                childObj['webstyleCount'] = 0;
                childObj['productCount'] = 0;
                childObj['skuCount'] = 0;
                childObj['totQuantity'] = 0;
                childObj['totPrice'] = 0;
                var skuCount = 0;
                var totQuantity = 0;
                var totPrice = 0;
                var totListPrice = 0
                var productCount = 0;
                if(!item.catDisplayName && item.name) {
           childObj['name'] = item.name;  }
        //         } else if(item.name){
        //   childObj['name'] =  item.name;  
        //   }
          childCat.push(childObj);
                if(item.sub_category){
                    if(item.webStyle  && item.webStyle.length > 0 ){
                    var tempWebStyleCount = 0;
                    item.webStyle.forEach(function(item,index){
                        if(item.productData && item.productData.length > 0){
                            var tempProductCount = 0;
                            item.productData.forEach(function(pItem,index){
                                pItem["sumSalePrice"] = 0;
                                pItem["sumListPrice"] = 0;
                                if(pItem.sku && pItem.sku.length > 0) {
                                    var tempSkuCount = 0;
                                    pItem.sku.forEach(function(sItem,index){
                                        if(sItem.quantity && sItem.quantity > 0){
                                            tempSkuCount++; 
                                            selectSkuData.push({productId: pItem.productId,quantity:sItem.quantity, catRefId: sItem.skuId, x_actualSize : sItem.size});
                                            summaryDataArr.push({productId: pItem.productId,quantity:sItem.quantity, catRefId: sItem.skuId});
                                            totQuantity = totQuantity +  Number(sItem.quantity);
                                            sItem.skuPrice.forEach(function(priceItem,index){
                                                if(priceItem.salePrice && priceItem.salePrice > 0){
                                                    totPrice += Number(sItem.quantity) * Number(priceItem.salePrice)
                                                    pItem["sumSalePrice"] += Number(sItem.quantity) * Number(priceItem.salePrice)
                                                }
                                                if(priceItem.listPrice && priceItem.listPrice > 0){
                                                    totListPrice += Number(sItem.quantity) * Number(priceItem.listPrice)
                                                    pItem["sumListPrice"] += Number(sItem.quantity) * Number(priceItem.listPrice)
                                                }
                                           })
                                        }
                                    });
                                    skuCount = skuCount+ tempSkuCount;
                                    if(tempSkuCount > 0){
                                        tempProductCount++;
                                    }
                                }
                            });
                            productCount = productCount + tempProductCount;
                            if(tempProductCount > 0){
                                tempWebStyleCount++;
                            }
                        }
                    })
                    // widgetModel.emptySelectionList(false)
                    widgetModel.webStyleArr.push(item.webStyle);
                    childCat[index]['webstyleCount'] = tempWebStyleCount;
                    totalWebstyle += tempWebStyleCount;
                    widgetModel.totalWebstyeVal(totalWebstyle);
                    childCat[index]['skuCount'] = skuCount;
                    totalSkuCount += skuCount
                    widgetModel.totalSkuCount(totalSkuCount);
                    childCat[index]['productsCount'] = productCount;
                    totalProductsCount += productCount;
                    widgetModel.totalProductsCount(totalProductsCount);
                    childCat[index]['totQuantity'] = totQuantity;
                    totalQTY += totQuantity;
                    widgetModel.totalQuantity(totalQTY);
                    childCat[index]['totPrice'] = totPrice;
                    totalLP += totPrice;
                    widgetModel.totalLPQTY(totalLP);
                }
                childCat[index]['child'] = widgetModel.getWebStyleArr(item.sub_category); 

                } else if(!$.isEmptyObject(item) && !item.sub_category && item.webStyle && item.webStyle.length > 0 ){
                    var tempWebStyleCount = 0;
                    item.webStyle.forEach(function(item,index){
                        if(item.productData && item.productData.length > 0){
                            var tempProductCount = 0;
                            item.productData.forEach(function(pItem,index){
                                pItem["sumSalePrice"] = 0;
                                pItem["sumListPrice"] = 0;
                                if(pItem.sku && pItem.sku.length > 0) {
                                    var tempSkuCount = 0;
                                    pItem.sku.forEach(function(sItem,index){
                                        if(sItem.quantity && sItem.quantity > 0){
                                           tempSkuCount ++; 
                                           selectSkuData.push({productId: pItem.productId,quantity:sItem.quantity, catRefId: sItem.skuId, x_actualSize : sItem.size  });
                                           summaryDataArr.push({productId: pItem.productId,quantity:sItem.quantity, catRefId: sItem.skuId});
                                           totQuantity = totQuantity +  Number(sItem.quantity);
                                           sItem.skuPrice.forEach(function(priceItem,index){
                                                if(priceItem.salePrice && priceItem.salePrice > 0){
                                                    totPrice += Number(sItem.quantity) * Number(priceItem.salePrice)
                                                    pItem["sumSalePrice"] += Number(sItem.quantity) * Number(priceItem.salePrice)
                                                }
                                                if(priceItem.listPrice && priceItem.listPrice > 0){
                                                    totListPrice += Number(sItem.quantity) * Number(priceItem.listPrice)
                                                    pItem["sumListPrice"]+= Number(sItem.quantity) * Number(priceItem.listPrice)
                                                }
                                           })
                                        }
                                    });
                                    skuCount = skuCount+ tempSkuCount;
                                    if(tempSkuCount > 0){
                                        tempProductCount++;
                                    }
                                }
                            });
                           productCount = productCount + tempProductCount;
                           if(tempProductCount > 0){
                                tempWebStyleCount++;
                            }
                        }
                    })
                    childCat[index]['webstyleCount'] = tempWebStyleCount;
                    totalWebstyle += tempWebStyleCount;
                    widgetModel.totalWebstyeVal(totalWebstyle);
                    childCat[index]['skuCount'] = skuCount;
                    totalSkuCount += skuCount
                    widgetModel.totalSkuCount(totalSkuCount);
                    childCat[index]['productsCount'] = productCount;
                    totalProductsCount += productCount;
                    widgetModel.totalProductsCount(totalProductsCount);
                    childCat[index]['totQuantity'] = totQuantity;
                    totalQTY += totQuantity
                    widgetModel.totalQuantity(totalQTY);
                    childCat[index]['totPrice'] = totPrice
                    totalLP += totPrice
                    widgetModel.totalLPQTY(totalLP);
                    // console.log('cartData');console.log(cartData);
                    cartData.shoppingCart.items =selectSkuData;
                    // widgetModel.emptySelectionList(false)
                    widgetModel.webStyleArr.push(item.webStyle);
                    
                }
            });
        } else {
            widgetModel.emptySelectionList(true);
            navigation.goTo(widgetModel.translate('selectionLinkText'));
            widgetModel.showLoader(false);
        }
        widgetModel.showLoader(false);
        setTimeout(function(){if(widgetModel.totalQuantity() == 0){
            navigation.goTo(widgetModel.translate('selectionLinkText'));
        }},3000)
        var tempChildCat = [];
        if(childCat.length > 0) {
            childCat.forEach(function(item,index){
                if((item.hasOwnProperty('child') && item.child.length > 0 )|| item.webstyleCount > 0) {
                    tempChildCat.push(item);
                }
            })
        }
        return tempChildCat;
    },
      addToCart: function(){
        if(cartData){
            var addCartDetails = {
                url: widgetModel.translate(widgetModel.ADD_TO_CART_URL),
                data : JSON.stringify(cartData),
                dataType: "json",
                type: 'POST',
                contentType: "application/json",
                headers: {
                  'Authorization': authorizationHeader,
                  'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                  'x-ccsite': widgetModel.site().siteInfo.repositoryId
                }
                
            };
            return Promise.resolve($.ajax(addCartDetails));
        }
      },
      handlePlaceOrder: function(){
        widgetModel.showLoader(true);
        widgetModel.addToCart().then(function(successData){
            if (successData.state !== 'PENDING_APPROVAL') {
                var message = successData.payments[0].paymentState;
                notifier.sendError(widgetModel.WIDGET_ID, message, true);
                log.error("Error adding payment to Cart Order ",message );
              widgetModel.showLoader(false);
            } else {
                var jsonData = {
                    accountId:    widgetModel.user().parentOrganization.id(),
                    emailId:      widgetModel.user().emailAddress(),
                    clearSuggestiveBuy: false
                };
                widgetModel.deleteSelectionRecord(widgetModel.translate(widgetModel.DELETE_SELECTION_URL),jsonData).then(function(deleteSuccess){
                // Redirecting to order confirmation page
                $.Topic(pubsub.topicNames.ORDER_SUBMISSION_SUCCESS).publish([ {
                  message : "success",
                  id : successData.id,
                  uuid : successData.uuid
                } ]);
                $.Topic("SHOW_CONFIRMATION_DATA.memory").publishWith(widgetModel.selectionData());
                widgetModel.clearTotalValues();
                },function(error){
                  widgetModel.showLoader(false);
                    log.error("Error deleting selection Order ",error );
                });
                
            }
        },function(error){
           log.error("Error adding selection to Order ",error );
        });  
      },
    };
  }
);
