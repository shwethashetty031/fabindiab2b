/**
 * @fileoverview Customer Profile Widget.
 *
 * @author
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'jquery', 'pubsub', 'navigation', 'viewModels/address', 'notifier', 'ccConstants', 'ccLogger', 'ccPasswordValidator', 'CCi18n', 'swmRestClient', 'ccRestClient', 'pageLayout/site', 'pageLayout/user'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, $, pubsub, navigation, Address, notifier, CCConstants, logger, CCPasswordValidator, CCi18n, swmRestClient, CCRestClient, SiteViewModel) {

    "use strict";
    var widgetModel;
    var remoteErrorMessage;
    return {

      WIDGET_ID: "b2bLoginWidget",

      login: ko.observable(''),
      password: ko.observable(''),
      ignoreBlur: ko.observable(''),
      ignoreEmailForgotPasswordValidation: ko.observable(false),
      emailForUpdatePassword: ko.observable(),
      enableUserAuthButton: ko.observable(true),
      userLoginPane: ko.observable(true),
      updatePasswordPane: ko.observable(false),
      remoteUnregisterIdError: ko.observable(false),
      ignorePasswordValidation: ko.observable(false),
      showErrorMessage: ko.observable(false),
      modalMessageText: ko.observable(''),
      modalMessageType: ko.observable(''),
      passwordReset: ko.observable(''),
      resetPasswordPane: ko.observable(false),
      latestPassword: ko.observable(''),
      confirmLatestPassword: ko.observable(''),
      savePasswordRemoteError: ko.observable(false),
      customIgnoreConfirmPasswordValidation: ko.observable(true),
      customIgnorePasswordValidation: ko.observable(true),

      onLoad: function (widget) {
        widgetModel = widget;
        if (widget.user().loggedIn()) {
          navigation.goTo(widget.links().home.route);
        }

        widget.login.extend({
          required: {
            params: true,
            message: CCi18n.t('ns.common:resources.emailAddressRequired'),
            onlyIf: function () {
              return !widget.ignoreEmailForgotPasswordValidation()
            }
          },
          pattern: {
            params: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
            onlyIf: function () {
              return (!widget.ignoreEmailForgotPasswordValidation());
            },
            message: CCi18n.t('ns.common:resources.emailAddressInvalid')
          }
        });

        widget.password.extend({
          required: {
            params: true,
            onlyIf: function () {
              return (!widget.ignorePasswordValidation());
            },
            message: CCi18n.t('ns.common:resources.passwordRequired')
          }
        });

        widget.emailForUpdatePassword.extend({
          required: {
            params: true,
            message: CCi18n.t('ns.common:resources.emailAddressRequired'),
            onlyIf: function () {
              return !widget.ignoreEmailForgotPasswordValidation()
            }
          },
          maxLength: {
            params: CCConstants.CYBERSOURCE_EMAIL_MAXIMUM_LENGTH,
            message: CCi18n.t('ns.common:resources.maxLengthEmailAdd', {
              maxLength: CCConstants.CYBERSOURCE_EMAIL_MAXIMUM_LENGTH
            })
          },
          pattern: {
            params: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
            onlyIf: function () {
              return (!widget.ignoreEmailForgotPasswordValidation());
            },
            message: CCi18n.t('ns.common:resources.emailAddressInvalid')
          },
          validation: {
            validator: function () {
              return !widget.remoteUnregisterIdError();
            },
            onlyIf: function () {
              return widget.remoteUnregisterIdError() && !widget.ignoreEmailForgotPasswordValidation();
            },
            message: widget.translate('emailIdNotRegistered')
          }
        });

        widget.latestPassword.extend({
          required: {
            params: 'true',
            message: CCi18n.t('ns.common:resources.passwordRequired')
          },
          password: {
            params: {
              policies: widget.user().passwordPolicies,
              login: widget.user().emailAddressForForgottenPwd,
              observable: widget.latestPassword,
              includePreviousNPasswordRule: widget.user().isChangePassword
            },
            onlyIf: function () {
              return !widget.customIgnorePasswordValidation();
            },
            message: CCi18n.t('ns.common:resources.passwordPoliciesErrorText')
          },
          validation: {
            validator: function () {
              return !widget.savePasswordRemoteError();
            },
            message: function () {
              return remoteErrorMessage
            },
            onlyIf: function () {
              return widget.savePasswordRemoteError() && !widget.customIgnorePasswordValidation()
            }
          }
        });

        widget.confirmLatestPassword.extend({
          required: {
            params: true,
            message: CCi18n.t('ns.common:resources.confirmPasswordRequired')
          },
          match: {
            params: widget.latestPassword,
            onlyIf: function () {
              return !widget.customIgnoreConfirmPasswordValidation();
            },
            message: CCi18n.t('ns.common:resources.passwordUnmatched')
          }
        });

        $.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function (obj) {
          var user = widget.user();
          navigation.goTo(widget.links().home.route);
          widgetModel.showErrorMessage(false);
        });

        $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).subscribe(function(){
           widgetModel.login('');
           widgetModel.password('');
           widgetModel.ignoreEmailForgotPasswordValidation(true);
           widgetModel.ignorePasswordValidation(true);
         });

        $.Topic(pubsub.topicNames.PAGE_PARAMETERS).subscribe(function () {
          var token = this.parameters.occsAuthToken;
          // Proceed only if there is a token on the parameters
          if (token) {
            // Validate the token to make sure that it is valid
            // before proceeding to update the password.
            widget.user().validateTokenForPasswordUpdate(token,
              // Success callback
              function (data) {
                // Let's try and update the password.
                widgetModel.userLoginPane(false);
                widgetModel.resetPasswordPane(true);
                widgetModel.hideAllSections();
              },
              // Error callback
              function (data) {
                // Error function - show error message
                notifier.clearSuccess(widget.WIDGET_ID);
                notifier.clearError(widget.WIDGET_ID);
                notifier.sendError(widget.WIDGET_ID, widgetModel.translate('resetPasswordErrorMessage'), true);

              });
          }
        });

        widget.emailAddressFieldBlured = function () {
          widget.ignoreEmailForgotPasswordValidation(false);
        };

        widget.emailAddressFieldFocused = function () {
          widget.ignoreEmailForgotPasswordValidation(true);
          widget.remoteUnregisterIdError(false);
        };

        widget.savePasswordFieldFocused = function () {
          widget.customIgnorePasswordValidation(true);
          widget.savePasswordRemoteError(false);
        };

        widget.savePasswordFieldBlured = function () {
          widget.customIgnorePasswordValidation(false);
        };

        widget.saveConfirmPasswordFieldFocused = function () {
          widget.customIgnoreConfirmPasswordValidation(true);
          widget.savePasswordRemoteError(false);
        };

        widget.saveConfirmPasswordFieldBlured = function () {
          widget.customIgnoreConfirmPasswordValidation(false);
        };

        $.Topic(pubsub.topicNames.USER_LOGIN_FAILURE).subscribe(function (obj) {
          widgetModel.modalMessageType("error");

          if (obj.errorCode && obj.errorCode === CCConstants.ACCOUNT_ACCESS_ERROR_CODE) {
            widgetModel.modalMessageText(CCi18n.t('ns.common:resources.accountError'));
          } else {
            widgetModel.modalMessageText(CCi18n.t('ns.common:resources.loginError'));
          }

          widgetModel.showErrorMessage(true);
        });
      },

      /**
       * Ignores the blur function when mouse click is up
       */
      handleMouseUp: function () {
        this.ignoreBlur(false);
        this.user().ignorePasswordValidation(false);
        return true;
      },

      /**
       * Ignores the blur function when mouse click is down
       */
      handleMouseDown: function () {
        this.ignoreBlur(true);
        this.user().ignorePasswordValidation(true);
        return true;
      },

      /**
       * Invoked when Login method is called
       */
      handleLogin: function (data, event, dd) {
        data.user().login(data.login());
        data.user().password(data.password());
        if ('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          var errors = ko.validation.group([data.login, data.password], {
            deep: true
          });
          errors.showAllMessages(true);
          notifier.clearError(this.WIDGET_ID);
          if (data.user().validateLogin()) {
            data.user().updateLocalData(false, false);
            $.Topic(pubsub.topicNames.USER_LOGIN_SUBMIT).publishWith(data.user(), [{
              message: "success"
            }]);
          }
        }
        return true;
      },

      showForgotPasswordSection: function (data) {
        widgetModel.userLoginPane(false);
        widgetModel.resetPasswordPane(false);
        widgetModel.ignorePasswordValidation(true);
        if (data.user().login()) {
          data.user().emailAddressForForgottenPwd(data.user().login());
        } else {
          data.user().emailAddressForForgottenPwd('');
          data.user().emailAddressForForgottenPwd.isModified(false);
        }
      },

      /**
       * Resets the password for the entered email id.
       */
      resetForgotPassword: function (data, event) {
        widgetModel.passwordReset(false);
        if ('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          widgetModel.ignoreEmailForgotPasswordValidation(false);
          if (data.emailForUpdatePassword.isValid()) {
            widgetModel.enableUserAuthButton(false);
            data.user().login(widgetModel.emailForUpdatePassword());
            widgetModel.checkEmailIdRegistered(data, event);
          }
        }
        return true;
      },

      checkEmailIdRegistered: function (data, event) {
        const url = '/ccstorex/custom/v1/verifyEmail';
        CCRestClient.request(url, {
            email: widgetModel.emailForUpdatePassword()
          },
          function (result) {
            if (result['response'] === "success") {
              widgetModel.remoteUnregisterIdError(false);
              data.user().resetForgotPassword(widgetModel.emailForUpdatePassword());
              widgetModel.passwordReset(true);
            } else if (result['response'] === "failure") {
              widgetModel.remoteUnregisterIdError(true);
              widgetModel.emailForUpdatePassword.valueHasMutated()
              widgetModel.enableUserAuthButton(true);
              widgetModel.passwordReset(false);
            };
          },
          function (error) {
            logger.info('Error:: Could not fire api get profile', error);
            widgetModel.enableUserAuthButton(true);
          })
      },

      handleCancelForgottenPassword: function (data, event) {
        widgetModel.userLoginPane(true);
        if ('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          widgetModel.ignoreEmailForgotPasswordValidation(true);
          widgetModel.passwordReset(false);
          notifier.clearError(this.WIDGET_ID);
        }
        return true;
      },

      savePassword: function (data, event) {
        var errors = ko.validation.group([this.emailForUpdatePassword, this.latestPassword, this.confirmLatestPassword], {
          deep: true
        });
        errors.showAllMessages(true);
        if ('click' === event.type || (('keydown' === event.type || 'keypress' === event.type) && event.keyCode === 13)) {
          notifier.clearError(this.WIDGET_ID);
          this.user().newPassword(this.latestPassword());
          this.user().confirmPassword(this.confirmLatestPassword());
          this.user().emailAddress(this.emailForUpdatePassword());
          widgetModel.customIgnorePasswordValidation(false);
          widgetModel.ignoreEmailForgotPasswordValidation(false);
          widgetModel.emailForUpdatePassword.isModified(true);
          if (this.user().newPassword.isValid() && this.user().confirmPassword.isValid() && this.user().emailAddress.isValid()) {
            widgetModel.enableUserAuthButton(false);
            this.user().updateExpiredPasswordUsingToken(this.user().token, this.user().emailAddress(), this.user().newPassword(), this.user().confirmPassword(),
              function (retData) {
                // Success function
                $('#CC-updatePasswordPane').hide();
                $('#CC-updatePasswordMessagePane').show();
                widgetModel.enableUserAuthButton(true);
              },
              function (retData) {
                // Error function - show error message
                widgetModel.enableUserAuthButton(true);
                if (retData.errorCode === '83037') {
                  $('#CC-updatePasswordPane').hide();
                  $('#CC-updatePasswordErrorMessagePane').show();
                } else if (retData.errorCode != '83037' && retData.hasOwnProperty('message') && retData.message) {
                  remoteErrorMessage = retData.message;
                  widgetModel.savePasswordRemoteError(true);
                }
              }
            );
          }
        }
        return true;
      },

      updatePassword: function () {
        widgetModel.userLoginPane(true);
      },

      /**
       * Hides all the sections of  modal dialogs.
       */
      hideAllSections: function () {
        $('#CC-updatePasswordMessagePane').hide();
        $('#CC-updatePasswordErrorMessagePane').hide();
      },
    }
  }
);