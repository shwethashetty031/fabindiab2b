
/**
 * @fileoverview Fab India Product Listing View Model.
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'ccPaginated', 'pubsub', 'ccRestClient', 'ccConstants',
    'CCi18n', 'spinner', 'ccNumber', 'pageLayout/product', 'storageApi', 'pageLayout/site', 'navigation', 'ccStoreConfiguration', 'viewModels/productListingViewModel'],

  function(ko, Paginated, pubsub, ccRestClient, CCConstants, CCi18n, spinner, ccNumber, Product, storageApi, SiteViewModel, navigation, CCStoreConfiguration, ProductListingViewModel) {

    "use strict";

    var self = this;
	ProductListingViewModel.prototype.itemsPerRow = ko.observable(3);
	ProductListingViewModel.prototype.itemsPerRowInDesktopView = ko.observable(3);
	ProductListingViewModel.prototype.itemsPerRowInLargeDesktopView = ko.observable(3);
	ProductListingViewModel.prototype._internalViewportMode = ko.observable(3);
	ProductListingViewModel.prototype.viewportMode = ko.observable(3);
    ProductListingViewModel.prototype.itemsPerRowInTabletView = ko.observable(3);
    /**
     * Load products using the current sorting order and starting from the
     * specified starting index.
     * @param {integer} startingIndex the index of the first record to load.
     * Overridden this method to add filter parameters for browse pages.
     */
    ProductListingViewModel.prototype.fetchBlock = function(startingIndex) {
      var url, data;

      if(this.clearOnLoad) {
        this.titleText('');
        this.clearData();
      }

      data = {};
      data[CCConstants.TOTAL_RESULTS] = true;
      data[CCConstants.TOTAL_EXPANDED_RESULTS]=true;
      if(this.catalog) {
        data[CCConstants.CATALOG] = this.catalog();
      }
      if (this.widget.user && this.widget.user().catalogId){
        data[CCConstants.CATALOG] = this.widget.user().catalogId();
      }
      if ((this.widget.shouldUseStyleBased() || this.widget.shouldUseStyleBased()=="true") && window.history && window.history.pushState) {
        if (startingIndex == 0) { //first page data
          this.actualOffset = 0;
          this.expandedOffset = 0;
          data[CCConstants.OFFSET] = this.actualOffset;
        } else if (this.pageNumber == Math.ceil(this.totalExpandedResults/this.itemsPerPage)) { //last page data
          this.actualOffset = (this.totalResults - this.blockSize)<0?0:this.totalResults - this.blockSize;
          this.expandedOffset = -1 * this.totalExpandedResults;
          data[CCConstants.OFFSET] = this.actualOffset;
        } else {
          for (var index=1; index <= this.blockSize; index++) {
            if (this.data()[startingIndex + this.itemsPerPage - index]) {
              this.actualOffset = this.data()[startingIndex + this.itemsPerPage - index].offsetMap.actualOffset + this.blockSize;
              this.expandedOffset = startingIndex + this.itemsPerPage + 1 - index;
              data[CCConstants.OFFSET] = this.actualOffset;
              break;
            }
          }
          if (data[CCConstants.OFFSET] == undefined) {
            for (var index=1; index <= this.blockSize; index++) {
              if (this.data()[startingIndex + index]) {
                this.actualOffset = (this.data()[startingIndex + index].offsetMap.actualOffset - this.blockSize) < 0 ?
                  0 : (this.data()[startingIndex + index].offsetMap.actualOffset - this.blockSize);
                this.expandedOffset = -1 * (startingIndex + index -1);
                data[CCConstants.OFFSET] = this.actualOffset;
                break;
              }
            }
          }
        }

        if (data[CCConstants.OFFSET] == undefined) {
          var offsetMap, listingOffsetMap;

          listingOffsetMap = storageApi.getInstance().getItem(CCConstants.LISTING_OFFSET_MAP_KEY);
          if (listingOffsetMap && typeof listingOffsetMap === 'string') {
            listingOffsetMap = JSON.parse(listingOffsetMap);
          }

          if (listingOffsetMap) {
            offsetMap = listingOffsetMap[this.category().route + "/" + this.pageNumber];
          }
          if (offsetMap && offsetMap !== undefined) {
            this.actualOffset= offsetMap.actualOffset;
            this.expandedOffset = offsetMap.expandedOffset;
            data[CCConstants.OFFSET] = this.actualOffset;
          }
        }
      }

      if (data[CCConstants.OFFSET] == undefined) {
        this.actualOffset = startingIndex;
        this.expandedOffset = startingIndex;
        data[CCConstants.OFFSET] = this.actualOffset;
      }

      data[CCConstants.LIMIT] = this.blockSize;
      
      url = CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS;

      if(this.category()) {
		
		var cat = this.category();
		if(this.sortDirectiveProp() === 'creationDate'){

		   data[CCConstants.SORTS] = this.sortDirectiveProp()+":desc";

	    } else if (this.sortDirectiveProp() !== 'default') {
             data[CCConstants.SORTS] = this.sortDirectiveProp() + ":" + this.sortDirectiveOrder();

        }else {
            if(cat && cat.childCategories && cat.childCategories.length) {
                data[CCConstants.SORTS] = "creationDate:desc,x_categorySortOrderBase:asc"; 
            }
        }

        if (!this.requesting) {
          this.requesting = true;
          spinner.create(this.productLoadingOptions);

          data[CCConstants.CATEGORY] = this.category().id;
          data.includeChildren = true;
          if(this.showInactiveProducts != null && this.showInactiveSkus != null) {
            data.showInactiveProducts = this.showInactiveProducts;
            data.showInactiveSkus = this.showInactiveSkus;
          }

          if (this.widget && this.widget.fields && this.widget.fields()){
            data.fields = this.widget.fields();
          }
          this.requesting = true;
          data[CCConstants.STORE_PRICELISTGROUP_ID] = SiteViewModel.getInstance().selectedPriceListGroup().id;

          if (this.widget && this.widget.productListing && this.widget.productListing.filterKey) {
            data[CCConstants.FILTER_KEY] = this.widget.productListing.filterKey;
          }
          // Added isDomesticOnly filter check for USD
          if(SiteViewModel.getInstance().selectedPriceListGroup().id === 'USD'){
            data["q"] = 'x_isDomesticOnly eq "FALSE"';
          }

          ccRestClient.request(url, data,
        	  function(result){this.successFunc(result);}.bind(this),
//            this.successFunc.bind(this),
            this.errorFunc.bind(this));
        }
      }
    };

    return ProductListingViewModel;
});
