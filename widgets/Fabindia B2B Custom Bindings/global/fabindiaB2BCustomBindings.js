/**
 * @fileoverview Fabindia B2B Custom Bindings.
 *
 * @author
 */
define(//-------------------------------------------------------------------
// DEPENDENCIES
//-------------------------------------------------------------------
[
  "knockout",  "ccLogger",  "jquery",  "ccConstants",  "ccRestClient", "ccNumber"
], //-------------------------------------------------------------------
// MODULE DEFINITION
//-------------------------------------------------------------------
function(ko, ccLogger, $, CCConstants, ccRestClient, ccNumber) {
  "use strict";

  return {
    onLoad: function(widget) {
      /**
       * Product Tile Size Hover for Search Landing Page
       *
       */
      ko.bindingHandlers.SLPTileSizeOnHover = {
        init: function(
          element,
          valueAccessor,
          allBindings,
          viewModel,
          bindingContext
        ) {
          var data = ko.unwrap(valueAccessor());
          var thisContext = bindingContext.$parents[1];
          var categoryData = {};

          var pathParam = data.repositoryId()[0];
          var stockUrl = CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY;
          var productUrl = CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS;
          var collectionUrl = CCConstants.ENDPOINT_LIST_COLLECTIONS;
          var input = { skuId: "", catalogId: "" };
          var queryParam = { productIds: pathParam };
          queryParam["fields"] = "childSKUs,x_collectionType";
          // Intiallizing variables for product image animation
          var _index = 0;
          var toBeScrolledBy = 0;
          var imgArray = data["product.mediumImageURLs"]();
          var scroller = $(element)
            .find(".customized-slider")
            .eq(0);
          var StopAnimation;
          var skuSizeArray = [];

          if (imgArray.length) {
            //This will sort image URLs in specific order based on the letter contains in image URL.
            imgArray.sort(function(url1, url2) {
              return (
                getValueForMediumImageURL(url1) -
                getValueForMediumImageURL(url2)
              );
            });

            $($(element).find(".fab-product-images")[_index]).attr(
              "src",
              imgArray[_index]
            );
          }

          //invoke product api to get all childSKUs on SLP
          ccRestClient.request(
            productUrl,
            queryParam,
            function(successData) {
              var childSkus = [];
              var skus = "";
              var tempArr = [];
              var productChildSKUsLength = successData[0].childSKUs
                ? successData[0].childSKUs.length
                : 0;
              for (var index = 0; index < productChildSKUsLength; index++) {
                childSkus.push(successData[0].childSKUs[index]);
                tempArr.push(successData[0].childSKUs[index].repositoryId);
              }
              skus = tempArr.join();
              var inputData = { skus: skus };
              if(successData[0].x_collectionType && successData[0].x_collectionType == JSON.parse(thisContext.site().extensionSiteSettings.b2bSiteSettings.collections).upcoming.collectionType) { 
                ccRestClient.request(
                  stockUrl,
                  input,
                  function(successData) {
                    var childSkusLength = childSkus ? childSkus.length : 0;
                    for (var index = 0; index < childSkusLength; index++) {
                      var sku = childSkus[index];
                      var skuId = sku.repositoryId;                        
                      if (sku.x_actualSize) {
                        skuSizeArray.push(sku.x_actualSize);
                      }
                    }
                  },
                  //error callback
                  function(errorData) {
                    console.error("ERROR : ", errorData);
                  },
                  pathParam
                );
              } else {              
                $.ajax({
                  url: "/ccstorex/custom/getReservedQty",
                  dataType: "json",
                  type: "POST",
                  contentType: "application/json",
                  // async:true,
                  data: JSON.stringify(inputData),
                  success: function(resp) {
                    var reservedArray = [];
                    for (var j = 0; j < resp.inventories.length; j++) {
                      reservedArray[resp.inventories[j].skuId] =
                        resp.inventories[j].quantity;
                    }
                    ccRestClient.request(
                      stockUrl,
                      input,
                      function(successData) {
                        var instockSizeCount = 0;
                        var childSkusLength = childSkus ? childSkus.length : 0;
                        for (var index = 0; index < childSkusLength; index++) {
                          var sku = childSkus[index];
                          var skuId = sku.repositoryId;                        
                          if (sku.x_actualSize && successData[skuId] === "IN_STOCK" && (parseInt(successData.productSkuInventoryStatus[skuId]) - parseInt(reservedArray[skuId])) > 0) {
                            skuSizeArray.push(sku.x_actualSize);
                            instockSizeCount++;
                          }
                        }
                         if (instockSizeCount < 1) {
                            $(element).parent().parent().find(".new_sale_product").remove();
                            $(element).parent().parent().find(".new_product").remove();
                            $(element).parent().parent().next(".tag_change").html("Sold Out").css("display", "block");
                          }
                      },
                      //error callback
                      function(errorData) {
                        console.error("ERROR : ", errorData);
                      },
                      pathParam
                    );
                  }
                });
              }   
            },
            //error callback
            function(errorData) {
              console.error("ERROR : ", errorData);
            }
          );

          element.onmouseenter = function() {
            $.fn.mouseIsOver = function() {
              return (
                $(element)
                  .parent()
                  .find($(element).selector + ":hover").length > 0
              );
            };

            var isAddSkuSizeDivPresent = $(element)
              .parent()
              .parent()
              .siblings(".product-detail")
              .children(".cc-size.clearfix")
              .children(".addSkuSize").length;

            if ($(element).mouseIsOver() && isAddSkuSizeDivPresent <= 0) {
              $(element)
                .parent()
                .parent()
                .siblings(".product-detail")
                .children(".cc-size.clearfix")
                .append('<div class="addSkuSize"></div>');

              if (skuSizeArray && skuSizeArray.length > 0) {
                $.each(skuSizeArray, function(index, value) {
                  $(".addSkuSize").append("<span>" + value + "</span>");
                });
              }
            }

            //Start Animation of SLP tile Image Carousel
            StopAnimation = setInterval(function() {
              var totalSlides = scroller.children();
              var slideWidth = totalSlides[0] && totalSlides[0].clientWidth;
              _index++;
              scroller
                .find(".fab-product-images")
                .eq(_index)
                .attr("src", imgArray[_index]);
              if (_index >= imgArray.length) {
                _index = 0;
              }
              toBeScrolledBy = slideWidth * _index;
              scroller.css({
                transform: "translateX(-" + toBeScrolledBy + "px)"
              });
            }, 2500);
          };

          element.onmouseleave = function() {
            $(".addSkuSize").remove();
            //End of animation and reseting the index and div postion
            clearInterval(StopAnimation);
            _index = 0;
            $(scroller).css({
              transform: "translateX(0)"
            });
          };
        }
      };

      /**
       * Product Tile Size Hover for Browse Landing Page
       *
       */
      ko.bindingHandlers.PLPTileSizeOnHover = {
        init: function(
          element,
          valueAccessor,
          allBindings,
          viewModel,
          bindingContext
        ) {
          var data = ko.unwrap(valueAccessor());
          var thisContext = bindingContext.$parents[1];
          var input = { skuId: "", catalogId: "" };

          var pathParam = data.repositoryId();
          var stockUrl = CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY;
          // var collectionUrl = CCConstants.ENDPOINT_LIST_COLLECTIONS;
          // Intiallizing variables for product image animation
          var _index = 0;
          var toBeScrolledBy = 0;
          var imgArray = data.mediumImageURLs();
          var imgAltText = data.primaryImageAltText()
            ? data.primaryImageAltText()
            : data.displayName();
          var scroller = $(element)
            .find(".customized-slider")
            .eq(0);
          var StopAnimation;
          var skuSizeArray = [];

          $($(element).find(".fab-product-images")[_index]).attr({
            src: imgArray[_index],
            alt: imgAltText
          });
          var childSkusLength = data.childSKUs() ? data.childSKUs().length : 0;
          if(data.x_collectionType() && data.x_collectionType() == JSON.parse(thisContext.site().extensionSiteSettings.b2bSiteSettings.collections).upcoming.collectionType) {
            ccRestClient.request(
                  stockUrl,
                  input,
                  function(successData) {
                    
                    for (var index = 0; index < childSkusLength; index++) {
                      var sku = data.childSKUs()[index];
                      var skuId = sku.repositoryId();
                      if (sku.x_actualSize() ) {
                        skuSizeArray.push(sku.x_actualSize());                        
                      }
                    }                   
                  },
                  //error callback
                  function(errorData) {
                    console.error("ERROR : ", errorData);
                  },
                  pathParam
                );
          } else {        

            var skus = "";
            var tempArr = [];          
            for (var index = 0; index < childSkusLength; index++) {
              tempArr.push(data.childSKUs()[index].repositoryId());
            }
            skus = tempArr.join();
            var inputData = { skus: skus };

            $.ajax({
              url: "/ccstorex/custom/getReservedQty",
              dataType: "json",
              type: "POST",
              contentType: "application/json",
              // async:true,
              data: JSON.stringify(inputData),
              success: function(resp) {
                var reservedArray = [];
                for (var j = 0; j < resp.inventories.length; j++) {
                  reservedArray[resp.inventories[j].skuId] =
                    resp.inventories[j].quantity;
                }
                ccRestClient.request(
                  stockUrl,
                  input,
                  function(successData) {
                    var instockSizeCount = 0;
                    for (var index = 0; index < childSkusLength; index++) {
                      var sku = data.childSKUs()[index];
                      var skuId = sku.repositoryId();
                      if (sku.x_actualSize() && successData[skuId] == "IN_STOCK" && (parseInt(successData.productSkuInventoryStatus[skuId]) - parseInt(reservedArray[skuId])) > 0) {
                        skuSizeArray.push(sku.x_actualSize());
                        instockSizeCount++;
                      }
                    }
                    /**
                     * check for the OUT_OF_STOCK and add the Sold-Out tag
                     * check if the Skus are active or not.
                     */
                    if (instockSizeCount < 1) {
                      $(element).parent().parent().find(".new_sale_product").remove();
                      $(element).parent().parent().find(".new_product").remove();
                      $(element).parent().parent().next(".tag_change").html("Sold Out").css("display", "block");
                    }
                  },
                  //error callback
                  function(errorData) {
                    console.error("ERROR : ", errorData);
                  },
                  pathParam
                );
              }
            });
          }
          element.onmouseenter = function() {
            $.fn.mouseIsOver = function() {
              return ($(element).parent().find($(element).selector + ":hover").length > 0);
            };

            var isAddSkuSizeDivPresent = $(element).parent().parent().siblings(".product-detail").children(".cc-size.clearfix").children(".addSkuSize").length;

            if ($(element).mouseIsOver() && isAddSkuSizeDivPresent <= 0) {
              $(element).parent().parent().siblings(".product-detail").children(".cc-size.clearfix").append('<div class="addSkuSize"></div>');

              if (skuSizeArray && skuSizeArray.length > 0) {
                $.each(skuSizeArray, function(index, value) {
                  $(".addSkuSize").append("<span>" + value + "</span>");
                });
              }
            }

            //Start Animation of PLP tile Image Carousel
            StopAnimation = setInterval(function() {
              var totalSlides = scroller.children();
              var slideWidth = totalSlides[0] && totalSlides[0].clientWidth;
              _index++;
              scroller.find(".fab-product-images").eq(_index).attr({ src: imgArray[_index], alt: imgAltText });
              if (_index >= imgArray.length) {
                _index = 0;
              }
              toBeScrolledBy = slideWidth * _index;
              scroller.css({
                transform: "translateX(-" + toBeScrolledBy + "px)"
              });
            }, 2500);
          };

          element.onmouseleave = function() {
            $(".addSkuSize").remove();
            //End of the animation and reseting the index and div postion
            clearInterval(StopAnimation);
            _index = 0;
            $(scroller).css({
              transform: "translateX(0)"
            });
          };
        }
      };

      /**
        * Overriden Currency Custom Binding to remove fractionalDigits
        *
        */
       ko.bindingHandlers.fabindiaCurrency = {

        update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
          var currencySymbol;
          var value = ko.utils.unwrapObservable(valueAccessor());
          var numberValue = parseFloat(value.price);
          var nullReplace = value.nullReplace;
          var prependNull = value.prependNull;
          var prependSymbol = value.prependSymbol;
          var fractionalDigits = value.fractionalDigits;

          var formattedNumber = null;
    
          //Below if block is written to get the currency symbol
          if(value.currencyObj && value.currencyObj.symbol){
            currencySymbol = value.currencyObj.symbol;
          }else{
            currencySymbol = value.currencyObj;
          }

          if (prependSymbol) {
            currencySymbol = prependSymbol+' '+currencySymbol;
          }
          if ((numberValue || numberValue === 0) && !isNaN(parseFloat(numberValue))) {
            if (currencySymbol.match(/^[0-9a-zA-Z]+$/)) {
              currencySymbol = currencySymbol + ' ';
            }
            numberValue = numberValue.toFixed(fractionalDigits);
            // Send the precision along with the number
            formattedNumber = ccNumber.formatNumber(numberValue, true, fractionalDigits);
            var sign = formattedNumber.charAt(0);
            if (sign === '-' || sign === '+') {
              $(element).text(sign + currencySymbol + formattedNumber.slice(1));
            } else {
              $(element).text(currencySymbol + formattedNumber);
            }
          } else {
            $(element).text((currencySymbol && prependNull ? currencySymbol : '') + (nullReplace ? nullReplace : ''));
          }
        }
      }
    }
  };
});

var getValueForMediumImageURL = function(url) {
  if (/\.(f|F)\./g.test(url)) {
    return 0;
  } else if (/\.(b|B)\./g.test(url)) {
    return 1;
  } else if (/\.(d|D)\./g.test(url)) {
    return 2;
  } else if (/\.(r|R)\./g.test(url)) {
    return 3;
  } else if (/\.(l|L)\./g.test(url)) {
    return 4;
  } else if (/\.(m|M)\./g.test(url)) {
    return 5;
  } else {
    return 9;
  }
};
