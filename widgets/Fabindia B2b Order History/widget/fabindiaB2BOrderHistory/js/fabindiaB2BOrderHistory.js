/**
 * @fileoverview Order History Widget.
 * 
 * @author sukonda
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'viewModels/orderHistoryViewModel', 'notifier', 'CCi18n', 'ccConstants', 'navigation', 'ccRestClient', 'ccLogger', 'moment'],
    
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, pubsub, OrderHistoryViewModel, notifier, CCi18n, CCConstants, navigation, ccRestClient, logger, moment) {
  
    "use strict";
    var widgetModel;
    var offset;
    var productsWebStyleId = [];
     var orderTotalCount;

    return {

      WIDGET_ID: "orderB2BHistory",
      GET_ORDER_SSE: "getOrderSSE",
      ADD_RECORDS_TO_SELECTION_URL: "addRecordsToSelectionUrl",
      MAX_ITEMS_PER_BATCH : 10,
      ADD_TO_SELECT_BATCH: 1200,
      PRODUCTS_BATCH: 50,
      
      ordersArray: ko.observableArray([]),
      showLoader: ko.observable(false),
      GET_ORDER_SSE: 'getOrdersForAccountURL',
      sortDirections: ko.observable({"submittedDate":"both"}),
      Status : function(displayName, statuses) {
          this.selectedName = displayName;
          this.selectedStatuses = statuses;
      },
      searchText: ko.observable(),
      orderRecords : ko.observable(),
      status : ko.observable(),
      selectedOrderId: ko.observable(),
      purchaseSkus: ko.observableArray([]),
      productSkuData: ko.observableArray([]),
      productsId: ko.observableArray([]),
      effectiveCreditLimit:ko.observable(0),
      availableCreditLimit: ko.observable(),
      creditLimitExceeded: ko.observable(true),
      showLoader: ko.observable(false),
      windowClosed: ko.observable(true),
      windowClosedForRM: ko.observable(false),
      windowClosedForBuyer:ko.observable(false),
      
      beforeAppear: function (page) {
        var widget = this;
        widget.windowClosed = ko.observable(true),
        widget.windowClosedForBuyer = ko.observable(false),
        widget.windowClosedForRM = ko.observable(false),
        widgetModel.checkWindowClosed();
        if (widget.user().loggedIn() == false) {
          navigation.doLogin(navigation.getPath(), widget.links().home.route);
        } else {
          widget.status(null);
          widget.sortDirections({"submittedDate":"both"});
          widget.historyViewModel().clearOnLoad = true;
        }
        widgetModel.historyViewModel().data().length = 0;
        widget.fetchOrdersForAccount(0);
      },
      
      onClickFunc: function(){
          widgetModel.showLoader(true);
          if(this.searchText()){
               $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE) + '?fields=lastModifiedDate,id,state,priceInfo.currencyCode,priceInfo.total,x_selectionSummary&q=organizationId="'+ widgetModel.user().parentOrganization.id() +'" AND id="'+ this.searchText() +'"',
                    dataType: "json",
                    type: 'GET',
                    contentType: "application/json",
                    headers: {
                      'X-CCOrganization': widgetModel.user().currentOrganization().repositoryId,
                      'x-ccsite': widgetModel.site().siteInfo.repositoryId
                    },
                success: function(orderData) {
                    if(orderData.items.length > 0){
                        console.log("data.....",orderData.items);
                        orderData.items[0].priceInfo['currency'] = widgetModel.site().selectedPriceListGroup().currency;
                        widgetModel.historyViewModel().data().length = 0;
                        widgetModel.historyViewModel().data.push(orderData.items);
                        // $(window).off('scroll', widgetModel.scrollHandler)
                        widgetModel.showLoader(false);
                }else{
                        widgetModel.historyViewModel().data().length = 0;
                        widgetModel.historyGrid().length = 0;
                        widgetModel.historyViewModel().data.valueHasMutated();
                        widgetModel.showLoader(false);
                }
            }
            });
          }
          else {
              widgetModel.fetchOrdersForAccount(0)
          }
      },
      
    
    fetchOrdersForAccount: function(offsettoFetch) {
        offset = offsettoFetch;
        var limit = 20
        var records = []

            $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE) +'?offset='+offset+'&limit=20&sort=lastModifiedDate:desc&fields=lastModifiedDate,id,state,priceInfo.currencyCode,priceInfo.total,x_selectionSummary&q=organizationId="'+ widgetModel.user().parentOrganization.id() +'" AND (state!="INCOMPLETE")',
                    dataType: "json",
                    type: 'GET',
                    contentType: "application/json",
                    headers: {
                      'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                      'x-ccsite': widgetModel.site().siteInfo.repositoryId
                    },
                async: true,
                success: function(data) {
                    orderTotalCount = data.total;
                    if (offset <= data.total) {
                        for(var i=0;i<data.items.length;i++){
                            data.items[i].priceInfo['currency'] = widgetModel.site().selectedPriceListGroup().currency;
                        }
                        widgetModel.historyViewModel().data.splice(offset,0,data.items)
                    }
                    else if(offset > data.total){
                        for(var i=0;i<data.items.length;i++){
                            data.items[i].priceInfo['currency'] = widgetModel.site().selectedPriceListGroup().currency;
                        }
                        widgetModel.historyViewModel().data.splice(offset,0,data.items)
                    }
                    setTimeout(function(){
                    widgetModel.showLoader(false);
                    $(window).on('scroll', widgetModel.scrollHandler)
                        
                    })
                }
            });
    },



         scrollHandler : function(){
            var self = this;
            var scrollTop = $(window).scrollTop();
            var viewportHeight = $(window).height();
            var productListHeight = $(document).height();
            
            if(((scrollTop + viewportHeight) >= (productListHeight - 100)) && (offset < orderTotalCount)) {
                widgetModel.showLoader(true);
                $(window).off('scroll', widgetModel.scrollHandler)
                offset += 20
                widgetModel.fetchOrdersForAccount(offset)
            }
        },
    

      onLoad: function(widget) {
      var self = this;
      widgetModel = widget;
      widget.availableStatuses = ko.observableArray([
                                                  new widget.Status(CCi18n.t('ns.common:resources.ALL'), []),
                                                    new widget.Status(CCi18n.t('ns.common:resources.SUBMITTED'), ["SUBMITTED"]),
                                                    new widget.Status(CCi18n.t('ns.common:resources.PROCESSING'), ["PROCESSING"]),
                                                    new widget.Status(CCi18n.t('ns.common:resources.NO_PENDING_ACTION'), ["NO_PENDING_ACTION"])
                                                ]);
        widget.historyViewModel = ko.observable();
        widget.historyViewModel(new OrderHistoryViewModel());
        
        //Create historyGrid computed for the widget
        widget.historyGrid = ko.computed(function() {
          
          
          var numElements, start, end, width;
          var rows = [];
          var orders;
          if (($(window)[0].innerWidth || $(window).width()) > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
            var startPosition, endPosition;
            // Get the orders in the current page
            startPosition = (widget.historyViewModel().currentPage() - 1) * widget.historyViewModel().itemsPerPage;
            endPosition = startPosition + widget.historyViewModel().itemsPerPage;
            //If statement is to not load orders from orderHistoryViewModel which were alredy added to viewModel
            if(!Array.isArray(widget.historyViewModel().data()[0])){
                 widget.historyViewModel().data([]);
            }
            orders = widget.historyViewModel().data.slice(startPosition, endPosition);
          } else {
            orders = widget.historyViewModel().data();
          }
          if (!orders) {
            return;
          }
          numElements = orders.length;
          width = parseInt(widget.historyViewModel().itemsPerRow(), 10);
          start = 0;
          end = start + width;
          while (end <= numElements) {
            rows.push(orders.slice(start, end));
            start = end;
            end += width;
          }
          if (end > numElements && start < numElements) {
            rows.push(orders.slice(start, numElements));
          }

          for(var i=0; i<orders.length; i++) {
            orders[i]['secondaryOrderDataAvailable'] = orders[i].payTaxInSecondaryCurrency && orders[i].payShippingInSecondaryCurrency &&
                                                  orders[i].exchangeRate && orders[i].secondaryCurrencyCode;
            if(orders[i].secondaryOrderDataAvailable) {
              $.when (widget.site().siteLoadedDeferred).done(function() {
                orders[i]['secondaryCurrency'] = ko.observable(widget.site().getCurrency(orders[i].secondaryCurrencyCode));
              });
            }
          }

          return rows;
        }, widget);

        $.Topic(pubsub.topicNames.ORDERS_GET_HISTORY_FAILED).subscribe(function(data) {
          if (this.status == CCConstants.HTTP_UNAUTHORIZED_ERROR) {
            widget.user().handleSessionExpired();
            if (navigation.isPathEqualTo(widget.links().profile.route) || navigation.isPathEqualTo(widget.links().orderHistory.route)) {
              navigation.doLogin(navigation.getPath, widget.links().home.route);
            }
          } else {
            navigation.goTo('/profile');
          }
        });
        widgetModel.getCreditLimitValues();
        
      },
      
        checkWindowClosed: function(){
             var buyDateFranchise = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise,"DD-MM-YYYY") : '';
                    buyDateFranchise = buyDateFranchise.format('YYYY-MM-DD')
                    var endDateFranchise =  widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise,"DD-MM-YYYY") : '';
                    endDateFranchise = endDateFranchise.format('YYYY-MM-DD')
                    var buyDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM,"DD-MM-YYYY") : '';
                    buyDateRM = buyDateRM.format('YYYY-MM-DD')
                    var endDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM,"DD-MM-YYYY") : '';
                    endDateRM = endDateRM.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                for(var i=0;i< widgetModel.user().roles().length; i++){
                    if(moment(currentDate).isAfter(endDateFranchise,'day') || moment(currentDate).isBefore(buyDateFranchise,'day')){
                        if((widgetModel.user().roles()[i].function == "approver") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId) || (widgetModel.user().roles()[i].function == "admin") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(moment(currentDate).isAfter(endDateRM,'day') || moment(currentDate).isBefore(buyDateRM,'day')){
                                widgetModel.windowClosedForRM(true);
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('regionalManagerError'), true);
                                break;
                            }
                            else{
                                widgetModel.windowClosedForRM(false);
                            }
                        }
                        else if((widgetModel.user().roles()[i].function == "buyer") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(widgetModel.user().roles().length == 1){
                            widgetModel.windowClosedForBuyer(true);
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('buyerError'), true);
                        }
                            
                        }
                    }
                else{
                    widgetModel.windowClosedForBuyer(false); 
                }
        }
        
               if(widgetModel.windowClosedForBuyer() || widgetModel.windowClosedForRM()){
                   widgetModel.windowClosed(true);
                   return;
               }
               else{
                   widgetModel.windowClosed(false);
               }
          
      },
      
      getCreditLimitValues: function(){
             $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE)  + '?fields=items.priceInfo.total&q=organizationId="' + widgetModel.user().parentOrganization.id() + '" AND (state="PENDING_APPROVAL")',
                dataType: "json",
                type: 'GET',
                contentType: "application/json",
                headers: {
                  'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                  'x-ccsite': widgetModel.site().siteInfo.repositoryId
                },
            async: true,
            success: function(data) {
                var pendingOrders = data.items ? data.items : '';
                var orderTotal = pendingOrders.reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.priceInfo.total)},0);
                
                    widgetModel.getCurrentSelectionTotal().then(function(successData){
                        widgetModel.effectiveCreditLimit(Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(orderTotal) - Number(successData.totalPrice));
                        if(widgetModel.effectiveCreditLimit() > 0){
                            widgetModel.creditLimitExceeded(false);
                        } else {
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                            widgetModel.creditLimitExceeded(true);
                        }
                    }, function(errorData){
                        logger.error(errorData);
                    });
            }
        });
        },
    
      getCurrentSelectionTotal: function(){
          var getDetails = {
                url: widgetModel.translate('selectionTotalUrl') + "?accountId=" + widgetModel.user().parentOrganization.id() + "&emailId="  + widgetModel.user().emailAddress() ,
                dataType: "json",
                type: 'GET',
                contentType: "application/json"
            };
            return Promise.resolve($.ajax(getDetails));  
      },
      
      getFilteredStatus: function(valueSelected){
        var widget = this;
      var filtertext = valueSelected;
      widget.historyViewModel().filterArray=filtertext;
     widget.fetchOrdersForAccount();
      }, 
      
      clickOrderDetails: function (data, event) {
        var widget = this;
        widgetModel.user().validateAndRedirectPage(widgetModel.links().b2bOrderDetails.route+'/'+ data.id);
        return false;
      },      
      handleAddToSelection: function(data){
          var widget = this;
          widgetModel.checkWindowClosed();
          widget.showLoader(true);
          widgetModel.purchaseSkus(JSON.parse(data.x_selectionSummary));
          widgetModel.purchaseSkus().forEach(function(val,i){
              widgetModel.productsId().push(val.productId);
          });
          widgetModel.processSelectionData();
      },
      processSelectionData: function(){
            widgetModel.getProductsBatch(widgetModel.productsId()).then(function (data) {
                    var productsUniqueWebStyleId = [];
                    for (var i = 0; i < productsWebStyleId.length; i++) {
                        if (productsUniqueWebStyleId.indexOf(productsWebStyleId[i]) === -1) {
                            productsUniqueWebStyleId.push(productsWebStyleId[i]);
                        }
                    }
                    widgetModel.multipleProductByWebStyleId(productsUniqueWebStyleId).then(function(successData){
                        var currentTotal = widgetModel.productSkuData().reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.listPrice)},0);
                        var availableCreditLimit =  widgetModel.effectiveCreditLimit() - parseInt(currentTotal, 10);
                        if( parseInt(availableCreditLimit, 10) < 1 ){
                            widgetModel.creditLimitExceeded(true);
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                            widgetModel.showLoader(false);
                            return;
                        } else {
                            var productsBatchArray = [];
                            for(i = 0; i < widgetModel.productSkuData().length; i = i+ widgetModel.ADD_TO_SELECT_BATCH){
                                productsBatchArray.push(widgetModel.productSkuData().slice(i,i+ widgetModel.ADD_TO_SELECT_BATCH));  
                            }
                            var productsBatchLength = productsBatchArray.length;
                            productsBatchArray.reduce(function(childBatch, values, i){
                                return childBatch.then(function(success){
                                  return widgetModel.addDataToSelection(values);
                                });
                            }, Promise.resolve()).then(function(successData){
                                widgetModel.purchaseSkus.removeAll();
                                widgetModel.productSkuData.removeAll();
                                widgetModel.productsId.removeAll();
                                productsWebStyleId = [];
                                notifier.sendSuccess(widgetModel.WIDGET_ID,successData.Data, true);
                                widgetModel.showLoader(false);
                            },function(error){
                                logger.info("ERROR:: ", error);
                                widgetModel.showLoader(false);
                                widgetModel.purchaseSkus.removeAll();
                                widgetModel.productSkuData.removeAll();
                                widgetModel.productsId.removeAll();
                                productsWebStyleId = [];
                                notifier.sendError(widgetModel.WIDGET_ID,error.responseJSON.message, true);
                                logger.error(error)});  
                        }
                    },function(error){
                        logger.error(error);    
                    });
                },function (errorData) {
                    logger.error(errorData);
                });    
        },
      getProductsBatch : function(productIds){
            return new Promise(function(resolve,reject){
                var productsBatch = [];
                for( var i = 0; i < productIds.length; i = i + widgetModel.PRODUCTS_BATCH ){
                    productsBatch.push(productIds.slice(i, i + widgetModel.PRODUCTS_BATCH)); 
                }
                var ctr = 0;
                productsBatch.forEach(function(item,index){
                    var input = {};
                    input[CCConstants.PRODUCT_IDS] = item;
                    input[CCConstants.FIELDS_QUERY_PARAM] = ["id","x_webStyleId"];
                    ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input, function (successData) {
                        var sucessCtr = 0;
                        successData.forEach(function (val, i) {
                            if (val.x_webStyleId) {
                                productsWebStyleId.push("product.x_webStyleId:"+val.x_webStyleId);
                            }
                            sucessCtr++;
                            if(sucessCtr == successData.length ){
                                ctr++;
                            }
                            if(ctr == productsBatch.length ){
                                resolve (true);
                            }
                        });
                    },item);    
                });    
            })
        },
      //Get the all products by there webStyleId
        multipleProductByWebStyleId: function (webStyleIds) {
            return new Promise(function(resolve,reject){
            var webStyleBatch = [];
            for( var i = 0; i < webStyleIds.length; i = i + widgetModel.MAX_ITEMS_PER_BATCH ){
                webStyleBatch.push(webStyleIds.slice(i, i+widgetModel.MAX_ITEMS_PER_BATCH)); 
            }
            var prdctCtr = 0;
            webStyleBatch.forEach(function(item,index){
                var i, input = {},productArrForSku = [];
                input[CCConstants.SEARCH_NAV_RECORD_FILTER_KEY] = 'OR('+ item +')';
                input[CCConstants.SEARCH_NAV_DESCRIPTORS_KEY] = 0,
                input[CCConstants.SEARCH_REC_PER_PAGE_KEY] = 250,
                input[CCConstants.FIELDS_QUERY_PARAM] = ["product.repositoryId"];
                ccRestClient.request(CCConstants.ENDPOINT_SEARCH_SEARCH,
                    input,
                    function (successData) {
                        successData.resultsList.records.forEach(function (rValue, index) {
                                productArrForSku.push(rValue.records[0].attributes["product.repositoryId"][0]);
                        });
                        var input = {};
                        input[CCConstants.PRODUCT_IDS] = productArrForSku;
                        input[CCConstants.FIELDS_QUERY_PARAM] = ["id","displayName","primaryFullImageURL","route","childSKUs.repositoryId","childSKUs.x_actualSize","childSKUs.salePrice","childSKUs.listPrice","childSKUs.x_fabLongDesc","parentCategories","parentCategory","listPrice","salePrice","x_collectionType","x_categoryType","x_webStyleId","x_primaryCategory","x_filterColor","x_pseStyleCode"];
                            ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input, function (successData) {
                                var skuCtr = 0; 
                                successData.forEach(function (pValue, pI) {
                                    var childCtr = 0;
                                    pValue.childSKUs.forEach(function (skuValue, skuIndex) {
                                        widgetModel.purchaseSkus().forEach(function (listVal, listI) {
                                            if (listVal.catRefId === skuValue.repositoryId) {
                                              var qty = listVal.quantity;
                                              skuValue.quantity = qty ;
                                            } 
                                        });
                                        var parentCategoryArr = [];
                                        pValue.parentCategories.forEach(function(item,index){
                                            parentCategoryArr.push(item.repositoryId)
                                        })
                                            widgetModel.productSkuData().push({
                                                productId: pValue.id,
                                                skuId: skuValue.repositoryId,
                                                listPrice: skuValue.listPrice + '',
                                                salePrice: skuValue.salePrice + '',
                                                size: skuValue.x_actualSize,
                                                categoryType: pValue.x_categoryType,
                                                webStyleId: pValue.x_webStyleId,
                                                pscStyleCode: pValue.x_pseStyleCode,
                                                parentCategory: pValue.x_primaryCategory ? 'b2b-'+ pValue.x_primaryCategory : pValue.parentCategory.repositoryId,
                                                parentCategories : parentCategoryArr,
                                                x_filterColor : pValue.x_filterColor,
                                                quantity:skuValue.quantity ? skuValue.quantity +'' : 0 +'',
                                                displayName: pValue.displayName,
                                                productImg: pValue.primaryFullImageURL,
                                                productLink: pValue.route,
                                                productListPrice: pValue.listPrice,
                                                productSalePrice: pValue.salePrice,
                                                longDescription: skuValue.x_fabLongDesc
                                            });
                                        childCtr++;
                                        if(childCtr ==  pValue.childSKUs.length){
                                            skuCtr++;    
                                        }
                                        })
                                        if(successData.length == skuCtr){
                                            prdctCtr++;
                                        }
                                        if(prdctCtr == webStyleBatch.length  ){
                                            resolve(true);
                                        }
                                });
                        },function(error){
                            logger.info("ERROR:: List is Empty", error);
                        });
                    });
                });
            });
        },
        addDataToSelection: function (productSKUs) {
            return new Promise(function(resolve,reject){
                var json;
                json = {
                    isPDP : false,
                    accountId: widgetModel.user().parentOrganization.id(),
                    emailId: widgetModel.user().emailAddress(),
                    categoryType: productSKUs[0].categoryType,
                    products: productSKUs,
                    purchaseListId: 'null'
                };
                var url = '/ccstorex/custom/addRecordsToSelectionTable';
                widgetModel.addToSelection(url, json).then(function (successData) {
                    resolve(successData);
                   }, function (error) {
                    reject(error);
                });
            });
        },
        addToSelection: function (addToSelectUrl, jsonData) {
            var selectedProductDetails = {
                url: addToSelectUrl,
                dataType: "json",
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify(jsonData)
            };
            return Promise.resolve($.ajax(selectedProductDetails));
        },
      mergeToCart: function (data) {
          var widget = this;
          widget.showLoader(true);
          if(data.id){
            widget.selectedOrderId(data.id);
          }
          widget.cart().mergeCart(true);
           $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE) +'?q=id="'+ data.id +'"',
                    dataType: "json",
                    type: 'GET',
                    contentType: "application/json",
                    headers: {
                      'X-CCOrganization': widgetModel.user().currentOrganization().repositoryId,
                      'x-ccsite': widgetModel.site().siteInfo.repositoryId
                    },
                success: function(data) {
                        console.log("data.....",data);
                        var state = data.items[0].state;
              var success = function(){
                widget.user().validateAndRedirectPage("/cart");
                            widget.showLoader(false);
              };
              var error = function(errorBlock){
              var errMessages = "";
              var displayName;
              for(var k=0;k<errorBlock.length;k++){
                errMessages = errMessages + "\r\n" + errorBlock[k].errorMessage;
              }
                notifier.sendError("CartViewModel", errMessages, true);
                            widget.showLoader(false);
              };
                    widget.cart().addItemsToCart(data.items[0].commerceItems, success, error);
              // If not go 404
              }
            });
            
        },
        
        setSelectedOrderId: function (data, event) {
            this.selectedOrderId(data.orderId);
        }
    };
  }
);
