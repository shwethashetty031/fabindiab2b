/**
 * @fileoverview Mega Menu Widget.
 *
 */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'ccConstants', 'notifications', 'pubsub', 'CCi18n', 'spinner', 'ccStoreConfiguration', 'notifier', 'pageLayout/site', 'navigation', 'ccRestClient', 'storageApi'],
  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, CCConstants, notifications, pubsub, CCi18n, spinner, CCStoreConfiguration, notifier, SiteViewModel, navigation,CCRestClient,storageApi) {

    "use strict";

    var selfWidget;
    return {

      categories: ko.observableArray(),
      storeConfiguration: CCStoreConfiguration.getInstance(),
      mobileCustomerPointsValue : ko.observable(),
      showFabFamilyLink: ko.observable(true),
      showMyAccountBtn: ko.observable(false),

      // Spinner resources
      catalogMenuBlock: '#CC-megaMenu',
      menuOptions: {
        parent: '#CC-megaMenu',
        posTop: '40px',
        posLeft: '30%'
      },

      onLoad: function (widget) {

        selfWidget = widget;

        widget.menuName = 'CC-CategoryNav';
        widget.isMobile = ko.observable(false);

        /**
         * Handle null & undefined checks
        */
        widget.getJSON = function (data) {
          if (data != null && data != undefined && data != "") {
            return JSON.parse(data);
          }
          return null;
        };
        
        $.Topic("GET_POINTS_ON_PROFILE_PAGE.memory").subscribe(function(){
            widget.mobileCustomerPointsValue(this.points);
            // selfWidget.displayLoyaltyForInr();
        });

         widget.handlePageChanged = function (pageData) {
             selfWidget.displayLoyaltyForInr();
         }

         $.Topic(pubsub.topicNames.PAGE_CHANGED).subscribe(widget.handlePageChanged.bind(widget));

       
       

        $(window).resize(function () {
          widget.checkResponsiveFeatures($(window).width());
        });

        $('.dropdown-submenu-fab a.test').on("click", function (e) {
          $(this).next('ul').toggle();
          e.stopPropagation();
          e.preventDefault();
        });
        $(document).on('mouseover', 'li.cc-desktop-dropdown', function () {
          $(this).children('ul.dropdown-menu').css({ 'display': 'block', 'top': 'auto' });
          if (navigator.userAgent.indexOf("Firefox") != -1) {
            $("#CC-product-listing-sortby-controls select.form-control").hide();
          }
          else {
            $("#CC-product-listing-sortby-controls select.form-control").css('visibility', 'hidden');
          }
        });
        $(document).on('mouseout', 'li.cc-desktop-dropdown', function () {
          $(this).children('ul.dropdown-menu').css({ 'display': 'none' });
          if (navigator.userAgent.indexOf("Firefox") != -1) {
            $("#CC-product-listing-sortby-controls select.form-control").show();
          }
          else {
            $("#CC-product-listing-sortby-controls select.form-control").css('visibility', 'visible');
          }
        });
        $(document).on('keydown', 'a.Level1', function (event) {
          if (event.which === 9 && event.shiftKey) {
            $(this).next('ul.dropdown-menu').css({ 'display': 'none' });
          } else if (event.which == 27) {
            $(this).next('ul.dropdown-menu').css({ 'display': 'none' });
          } else if (event.which == 9) {
            $(this).next().children('a.Level2').parents('ul.dropdown-menu').css({ 'display': 'block', 'top': 'auto' });
          } else if (event.which == 13) {
            $(this).next('ul.dropdown-menu').css({ 'display': 'none' });
          }
        });
        $(document).on('keydown', 'a.Level2', function (event) {
          if (event.which == 27) {
            $(this).parents('ul.dropdown-menu').css({ 'display': 'none' });
          } else if (event.which == 9) {
            $(this).next().children('a.Level3').parents('ul.dropdown-menu').css({ 'display': 'block', 'top': 'auto' });
          } else if (event.which == 13) {
            $(this).parents('ul.dropdown-menu').css({ 'display': 'none' });
          }
        });
        $(document).on('keydown', 'a.Level3', function (event) {
          if (event.which == 27) {
            $(this).parents('ul.dropdown-menu').css({ 'display': 'none' });
          } else if (event.which == 9) {
            if ($(this).parent('li').next('li').children('a.Level3').length != 0 || $(this).parents('div.child-category-container').next('div.child-category-container').children('a.Level2').length != 0) {
            } else {
              $(this).parents('ul.dropdown-menu').parent('li.cc-desktop-dropdown').next('li.cc-desktop-dropdown').focus();
            }
          } else if (event.which == 13 && navigator.userAgent.indexOf("MSIE") == -1 && !navigator.userAgent.match(/Trident.*rv\:11\./)) {
            $(this).parents('ul.dropdown-menu').css({ 'display': 'none' });
          }
        });
        $(document).on('blur', 'a.Level3', function (event) {
          if ($(this).parent('li').next('li').children('a.Level3').length != 0 || $(this).parents('div.child-category-container').next('div.child-category-container').children('a.Level2').length != 0) {
          } else {
            $(this).parents('ul.dropdown-menu').css({ 'display': 'none' });
          }
        });
        $(document).on('focus', 'li.cc-desktop-dropdown', function () {
          $(this).children('ul.dropdown-menu').css({ 'display': 'block', 'top': 'auto' });
        });
        $(document).on('click', '.dropdown-menu', function (event) {
          if (!($('#' + event.target.id).children().length)) {
            $('body').removeClass('active');
            $('.overlay').removeClass('show');
          }
        });
        $(document).on('click', '.level2', function (event) {
          $('body').removeClass('active');
          $('.overlay').removeClass('show');
        });

        $(document).on('click', '#CC-loginHeader-myAccount', function (event) {
          $('body').removeClass('active');
          $('.overlay').removeClass('show');
        });

        if (widget.user() !== undefined && widget.user().catalogId) {
          widget.catalogId(widget.user().catalogId());
        }
        widget.setCategoryList();

      },

      /**
       * Updates categories if user catalog changes.
       */
      beforeAppear: function (page) {

        var widget = this;

        if (ko.isObservable(widget.user) && widget.user() &&
          ko.isObservable(widget.user().catalogId) && widget.user().catalogId()) {
          if (widget.user().catalogId() != widget.catalogId()) {
            widget.categories([]);
            widget.createSpinner();
            widget.catalogId(widget.user().catalogId());
            widget.setCategoryList();
            widget.destroySpinner();
          }
        }

        if ($(window).width() > 769) {
          widget.closeMegaMenu();
        } 
      },

      redirectToFabFamily: function () {
        Promise.resolve(navigation.goTo(selfWidget.links().profile.route)).then((function () {
            $("body").removeClass("active");
            $(".overlay").removeClass("show");
            $('#mobile-menu').removeClass("active in");
        }));
      },

      displayLoyaltyForInr: function () {
        if (SiteViewModel.getInstance().selectedPriceListGroup().id.toLowerCase() === 'inr') {
            if (!selfWidget.user().loggedIn()) {
                selfWidget.showFabFamilyLink(true);
                selfWidget.showMyAccountBtn(false);
            }else{
                
            }
        }

        if (SiteViewModel.getInstance().selectedPriceListGroup().id.toLowerCase() !== 'inr') {
          if (selfWidget.user().loggedIn()) {
            selfWidget.showMyAccountBtn(true);
          } else {
            selfWidget.showFabFamilyLink(false);
          }
        }

      },

      /**
       * Get the categories for the catalog and set it to the widget.
       */
      setCategoryList: function () {
        var widget = this;
        var params = {};
        var contextObj = {};
        contextObj[CCConstants.ENDPOINT_KEY] = CCConstants.ENDPOINT_COLLECTIONS_GET_COLLECTION;
        contextObj[CCConstants.IDENTIFIER_KEY] = "megaMenuNavigation";
        var filterKey = widget.storeConfiguration.getFilterToUse(contextObj);
        if (filterKey) {
          params[CCConstants.FILTER_KEY] = filterKey;
        }
        //Load the categoryList
        widget.load('categoryList',
          [widget.rootCategoryId(), widget.catalogId(), 1000], params,
          function (result) {

            widget.checkResponsiveFeatures($(window).width());

            var arraySize, maxElementCount;
            arraySize = result.length;
            maxElementCount = parseInt(widget.maxNoOfElements(), 1000);

            if (arraySize > maxElementCount) {
              arraySize = maxElementCount;
              result = result.slice(0, maxElementCount);
            }

            widget.categories.valueWillMutate();
            // Removing child categories initially to display only the first level
            // of categories on UI.
            for (var i = 0; i < result.length; i++) {
              var category = $.extend({}, result[i]);
              if (category.hasOwnProperty("childCategories")) {
                category.showCaret = true;
                delete category.childCategories;
              } else {
                category.showCaret = false;
              }
              widget.categories.push(category);
            }
            widget.categories.valueHasMutated();

            var categoriesSet = false;
            // This adds the child categories back to the parent/first level categories.
            $(document).one('mouseover focus', '#CC-megaMenu', function () {
              if (!categoriesSet) {
                widget.categories(result);
                categoriesSet = true;
              }
            });
          },
          widget);
      },

      closeMegaMenu: function () {
        //Close the Mega Menu on clicking of any of the links
        var links = $('.dropdown a');
        $(links).each(function (index) {
          var anchor_element = $(this);
          $(this).click(function () {
            $('.dropdown-menu').hide();
          });
        });
      },

      /**
       * Destroy spinner.
       */
      destroySpinner: function () {
        var widget = this;
        $(widget.catalogMenuBlock).removeClass('loadingIndicator');
        spinner.destroy(1);
      },

      /**
       * Creates spinner.
       */
      createSpinner: function () {
        var widget = this;
        $(widget.catalogMenuBlock).css('position', 'relative');
        widget.menuOptions.loadingText = 'Loading';
        spinner.createWithTimeout(widget.menuOptions, 5000);
      },

      /**
       * Menu items click event used to set focus to product listing result section
       * 
       * data - knockout data 
       * event - event data
       */
      catMenuClick: function (data, event) {
        $.Topic(pubsub.topicNames.OVERLAYED_GUIDEDNAVIGATION_HIDE).publish([{ message: "success" }]);
        $.Topic(pubsub.topicNames.UPDATE_FOCUS).publishWith({ WIDGET_ID: "productListing" }, [{ message: "success" }]);
        if(selfWidget.site().extensionSiteSettings.globalSiteSettings.enableWebengage){
          setTimeout(function(){
            $.Topic("GTM_CATEGORY_VIEWED.memory").publishWith(null,[{data: data, event_fired: "Sub-Category Viewed"}]);
            },5000);
        }
        return true;
      },
     
      checkResponsiveFeatures: function (viewportWidth) {
        if (viewportWidth > 978) {
          this.isMobile(false);
        }
        else if (viewportWidth <= 978) {
          this.isMobile(true);
        }
      },

      /**
       * sub sub menu click event - key press event handle
       * 
       * data - knockout data 
       * event - event data
       */
      navigationCategoryClick: function (data, event) {
        notifications.emptyGrowlMessages();
        var $this, parent;

        event.stopPropagation();

        $this = $(event.target).parent("li");
        parent = $this.parent().parent();

        if ($(event.target).parent().hasClass('dropdown-submenu')) {
          event.preventDefault();
        }

        if ($this.hasClass('open')) {
          // Closes previously open categories
          $this.removeClass('open').addClass('closed');

        } else {
          if (parent.hasClass('open')) {
            $('.dropdown-submenu.open').removeClass('open').addClass('closed');
            if ($this.hasClass('closed')) {
              // Opens a previously closed category
              $this.removeClass('closed').addClass('open');
              return false;
            } else {
              $this.removeClass('open').addClass('closed');
            }
          }
        }

        return true;
      },


      mobileOverlay: function () {
        /*$('#mobile-menu,body').toggleClass('active');
        console.log("mobile overlay guided");*/
        if ($('#mobile-menu').hasClass('active')) {
          $('#mobile-menu').removeClass('active');
        } else if (!($('#mobile-menu').hasClass('active'))) {
          $('#mobile-menu').addClass('active');
        }

        if ($('body').hasClass('active')) {
          $('body').removeClass('active');
          $('.overlay').removeClass('show');
        } else if (!($('body').hasClass('active'))) {
          $('body').addClass('active');
          $('.overlay').addClass('show');
        }
        if ($('body').hasClass('active')) {
          $('#mobile-menu').css({
            transform: "translateX(-100%)",
            maxHeight: 'auto'
          });
          $('body').css({
            //   overflow:"hidden"
          });
        }
        selfWidget.scrollToTop();
      },
      overlayDocument: function () {
        if ($('#mobile-menu').hasClass('active')) {
          $('#mobile-menu').removeClass('active');
        } else if (!($('#mobile-menu').hasClass('active'))) {
          $('#mobile-menu').addClass('active');
        }

        if ($('body').hasClass('active')) {
          $('body').removeClass('active');
          $('.overlay').removeClass('show');
          $('#mobile-menu').removeClass('in');
        } else if (!($('body').hasClass('active'))) {
          $('body').addClass('active');
        }
        if ($('body').hasClass('active')) {
          $('#mobile-menu').css({
            transform: "translateX(-100%)",
            maxHeight: 'auto'
          });

        }
        selfWidget.scrollToTop();
      },
      // Scroll to top of body
      scrollToTop: function () {
        $('body,html').animate({
          scrollTop: 0
        }, 500);
      },

      megaMenuClick: function (data, event) {
      if(selfWidget.site().extensionSiteSettings.globalSiteSettings.enableWebengage){
        setTimeout(function(){
          $.Topic("GTM_CATEGORY_VIEWED.memory").publishWith(null,[{data: data, event_fired: "Category Viewed"}]);
        },5000);
      }
        notifications.emptyGrowlMessages();
        $.Topic(pubsub.topicNames.OVERLAYED_GUIDEDNAVIGATION_HIDE).publish([{ message: "success" }]);
        event.stopPropagation();
        return true;
      },

      /**
       * Invoked when Logout method is called
       */ 
       handleB2BLogout: function (data) {
        $.Topic(pubsub.topicNames.USER_CLEAR_CART).publish([{
          message: "success"
        }]);
        var successFunc = function () {
          $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).publish([{
            message: "success"
          }]);
          storageApi.getInstance().removeItem(CCConstants.LOCAL_STORAGE_SHOPPER_CONTEXT);
          storageApi.getInstance().removeItem(CCConstants.LOCAL_STORAGE_CURRENT_CONTEXT);
          data.clearUserData();
          data.profileRedirect();
          var eventData = {
            'pageId': navigation.getPath()
          };
          $.Topic(pubsub.topicNames.PAGE_VIEW_CHANGED).publish(eventData);
        };
        var errorFunc = function (pResult) {
          data.clearUserData();
          data.profileRedirect();
        };
        if (data.loggedIn()) {
          if (data.parentOrganization && data.parentOrganization.name()) {
            data.refreshPageAfterContactLogout(true);
        }
          data.client().logout(successFunc, errorFunc);
        } else {
          data.clearUserData();
          $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).publish([{
            message: "success"
          }]);
          data.profileRedirect();
        }
        if (data.isUserProfileEdited()) {
          return true;
        }
        // Clearing the auto-login success message
        notifier.clearSuccess(this.WIDGET_ID);
        // Clearing any other notifications
        notifier.clearError(this.WIDGET_ID);
        data.updateLocalData(data.loggedinAtCheckout(), false);
      },

      //Redirects on login-register page after signIn link is clicked from mega menu overlay.
    //   clickSignIn: function () {
    //     window.location = "/login-registration" + "?isSignin=" + true;
    //     //GTM implementation - 'login' event will be fired when user clicks on signIn link from mega menu overlay.
    //     if (typeof window.dataLayer !== 'undefined') {
    //       window.dataLayer.push
    //         ({
    //           "event": "Login",
    //           "eventAction": "Click",
    //           "eventCategory": "Login",
    //           "eventLabel": "",
    //           "eventValue": ""
    //         });
    //     }
    //   },

      //Redirects on login-register page after signUp link is clicked from mega menu overlay.
    //   clickRegister: function () {
    //     window.location = "/login-registration" + "?isRegister=" + true;
    //     //GTM implementation - 'register' event will be fired when user clicks on signUp link from mega menu overlay.
    //     if (typeof window.dataLayer !== 'undefined') {
    //       window.dataLayer.push
    //         ({
    //           "event": "Register",
    //           "eventAction": "Click",
    //           "eventCategory": "Register",
    //           "eventLabel": "",
    //           "eventValue": ""
    //         });
    //     }
    //   },

      //Redirects user to login/profile when clicked on myAccount link
      clickMyAccount: function () {
        if (selfWidget.user().loggedIn() == false) {
          window.location = "/login-registration" + "?isSignin=" + true;
        } else {
          navigation.goTo(selfWidget.links().profile.route);        }
      }

    };
  });
