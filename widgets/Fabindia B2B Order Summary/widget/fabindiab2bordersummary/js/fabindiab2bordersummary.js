/**
 * @fileoverview Order Summary Widget. 
 * Calculates Shipping Cost and Order Total, given selected Shipping 
 * Option.
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'notifier', 'CCi18n', 'ccConstants', 'ccLogger', 'spinner', 'ccRestClient'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, pubsub, notifier, CCi18n, CCConstants, log, spinner, CCRestClient) {

    "use strict";
    var widgetModel;
    var authorizationHeader;
    var cartData = {}; 
    var inventoryArray = [];
    return {
      WIDGET_ID: 'b2bOrderSummary',
      
      includeShipping: ko.observable(false),
      includeTax: ko.observable(false),
      hasShippingInfo: ko.observable(false),
      hasTaxInfo: ko.observable(false),
      shippingOptions: ko.observableArray(),
      selectedShippingOption: ko.observable(),
      pricingAvailable: ko.observable(false),
      persistedLocaleName: ko.observable(),
      reservedArray: ko.observable(),
      paypalImageSrc: ko.observable("https://fpdbs.paypal.com/dynamicimageweb?cmd=_dynamic-image"),
      isCheckoutBtnDisable: ko.observable(true),
      OAUTH_TOKEN: "oAuthToken",
      ADD_TO_CART_URL:"createOrderUrl",
      SUCCESS_STATE:"successState",
      showLoader: ko.observable(false),

      // Stuff used by the spinner
      pricingIndicator: '#CC-orderSummaryLoadingModal',
      DEFAULT_LOADING_TEXT: "Loading...'",
      pricingIndicatorOptions: {
        parent: '#CC-orderSummaryLoadingModal',
        posTop: '10%',
        posLeft: '30%'
      },

      /**
       * Called when resources have been loaded
       */
      resourcesLoaded: function (widget) {

        // Create observable to mark the resources loaded, if it's not already there
        if (typeof widget.orderSummaryResourcesLoaded == 'undefined') {
          widget.orderSummaryResourcesLoaded = ko.observable(false);
        }

        // Notify the computeds relying on resources
        widget.orderSummaryResourcesLoaded(true);
      },

      /**
       * validate the cart items stock status as per the quantity. base on the
       * stock status of cart items redirect to checkout or cart
       */
      handleValidateCart: function (data, event) {
       // handlePlaceOrder
        if (data.cart().items().length > 0) {
            var errorArray = document.querySelectorAll(".cc_cart_error");
            var cartValid = true;
            errorArray.forEach(function(item, index) {
               if(item.getAttribute('data-valid') == 'false'){
                    cartValid = false;
                    return;
               }
            });
            widgetModel.getReservedQtyForCart().then(function(success){
                var reservedArrayJS = {};
                for (var j = 0; j < success.inventories.length; j++) {
                    reservedArrayJS[success.inventories[j].skuId] = success.inventories[j].quantity;
                }
                widgetModel.reservedArray(ko.mapping.fromJS(reservedArrayJS));
                $.Topic("RESERVED_QUANTITY_UPDATED.memory").publishWith(null,[widgetModel.reservedArray()]);
                widgetModel.cart().items().forEach(function(val,i) {
                    var calculatedSkuQuantity = val.orderableQuantity - widgetModel.reservedArray()[ko.unwrap(val.catRefId)]();
                    console.log("calculatedSkuQuantity>>>>",calculatedSkuQuantity);
                    if(calculatedSkuQuantity <= 0 ){
                        notifier.clearError(widgetModel.WIDGET_ID);
                        notifier.clearSuccess(widgetModel.WIDGET_ID);
                        notifier.sendError(widgetModel.WIDGET_ID, "Please remove the out of stock product from the cart to proceed placing the order", true);
                        return;
                    } else if(cartValid == true){
                widgetModel.showLoader(true);
                widgetModel.addToCart().then(function(successData){
                    if (successData.state !== widgetModel.translate(widgetModel.SUCCESS_STATE)) {
                        widgetModel.releaseInventory().then(function(releaseSuccess){
                            widgetModel.showLoader(false);
                            var message = successData.paymentResponses[0].paymentState + successData.paymentResponses[0].message;
                            notifier.clearError(widgetModel.WIDGET_ID);
                            notifier.clearSuccess(widgetModel.WIDGET_ID);
                            notifier.sendError(widgetModel.WIDGET_ID, message, true);
                            log.error("Inventory released, Error adding payment to Cart Order ",message );
                        },function(error){
                            log.error("Order was not placed but inventory was reserved for Order Id: ",widgetModel.order().user().orderId());
                        });
                        
                    } else {
                        // Redirecting to order confirmation page
                        $.Topic(pubsub.topicNames.ORDER_SUBMISSION_SUCCESS).publish([ {
                          message : "success",
                          id : successData.id,
                          uuid : successData.uuid
                        } ]);
                    }
                },function(error){
                   widgetModel.releaseInventory().then(function(releaseSuccess){
                        var message = error.responseJSON.message;
                        notifier.sendError(widgetModel.WIDGET_ID, message, true);
                        log.error("Inventory released, Error adding payment to Cart Order ",message );
                         widgetModel.showLoader(false);
                    },function(error){
                        log.error("Order was not placed but inventory was reserved for Order Id: ",widgetModel.order().user().orderId());
                    });
                   
                });
            } else {
                notifier.clearError(widgetModel.WIDGET_ID);
                notifier.clearSuccess(widgetModel.WIDGET_ID);
                notifier.sendError(widgetModel.WIDGET_ID, "Please remove the out of stock product from the cart to proceed placing the order", true);
                return;
            }
                }); 
            });
        }
      },
      getReservedQtyForCart: function(){
            var skuArray = [];
            widgetModel.cart().items().forEach(function(val,i) {
                skuArray.push(val.catRefId);
            });   
            var inputData = {skus: skuArray.join()};
            var reserveInventoryDetails = {
                url: "/ccstorex/custom/getReservedQty",
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(inputData),
            };
             return Promise.resolve($.ajax(reserveInventoryDetails)); 
      },
      loadCart: function(){
        var cartItems = widgetModel.order().cart().items;
        var items = [];
        inventoryArray = [];
        for (var i = 0; i < cartItems().length; i++) {
          var item = {};
          var inventoryOj = {};
          inventoryOj.skuId = cartItems()[i].catRefId;
          inventoryOj.orderId = widgetModel.order().user().orderId();
          inventoryOj.quantity = cartItems()[i].quantity();
          inventoryArray.push(inventoryOj);
          item.productId = cartItems()[i].productId;
          item.quantity = cartItems()[i].quantity();
          item.x_bespokeFinalImageUrl=  cartItems()[i].productData() ? cartItems()[i].productData().primaryThumbImageURL : null;
          item.x_bespokeProductSummary= cartItems()[i].productData() ? cartItems()[i].productData().route : null;
          item.x_actualSize= cartItems()[i].productData() ? cartItems()[i].productData().childSKUs[0].x_actualSize : null;;
                item.catRefId = cartItems()[i].catRefId;
                items.push(item);
        }
        cartData.shoppingCart = {items: []}; 
        cartData.shoppingCart.items = items;
      },
      prepareInvoiceOrder: function(){
        if (widgetModel.order().user().loggedIn()) {
          var cookie = document.cookie;
          var cookies = cookie.split(';');
          var storeOauthToken = '';
          var oauthTokenId = widgetModel.translate(widgetModel.OAUTH_TOKEN);
          for (var i = 0; i < cookies.length; i++) {
            if (cookies[i].indexOf(oauthTokenId) >= 0) {
              var storeOauthTokenPair = cookies[i].split('=');
              storeOauthToken = storeOauthTokenPair[1];
              break;
            }
          }
          var decodedToken = decodeURIComponent(storeOauthToken);
          var tokens = decodedToken.split('"');
          authorizationHeader = 'Bearer ' + tokens[1];
          widgetModel.authorizationHeader = authorizationHeader;
        }
        var jsonData = {
            accountId:    widgetModel.user().parentOrganization.id(),
            emailId:      widgetModel.user().emailAddress()
        };
        var shippingAddress = {};
        var billingAddress = {};
        var emailAddress;
        
        shippingAddress = widgetModel.user().contactShippingAddress;
        shippingAddress.selectedCountry = widgetModel.user().contactShippingAddress.country;
        var firstName = widgetModel.user().contactShippingAddress.firstName ? widgetModel.user().contactShippingAddress.firstName : widgetModel.user().firstName();
        var lastName = widgetModel.user().contactShippingAddress.lastName ? widgetModel.user().contactShippingAddress.lastName : widgetModel.user().lastName();
        shippingAddress.firstName = firstName;
        shippingAddress.lastName = lastName;

        if (widgetModel.user().emailAddress()) {
        shippingAddress.email = widgetModel.user().emailAddress()
        }
        cartData.shippingAddress = shippingAddress;
        
        billingAddress = widgetModel.user().contactBillingAddress;
        billingAddress.selectedCountry = widgetModel.user().contactBillingAddress.country;
        billingAddress.firstName = firstName;
        billingAddress.lastName = lastName;
        cartData.billingAddress = billingAddress;
        
        cartData.shippingMethod = {"value": widgetModel.user().parentOrganization.shippingMethods()[0].repositoryId()};
        
        cartData.isAnonymousCheckout = !(widgetModel.user().loggedIn());
        
        cartData.id = widgetModel.order().user().orderId();
        var payments = [];
        payments.push({type : "invoice"});
        cartData.payments = payments;
        widgetModel.loadCart();
      },
      reserveInventory: function(){
        var inventoryData = {"inventories": inventoryArray};
        var reserveInventoryDetails = {
                url: widgetModel.translate('reserveInventoryUrl'),
                data : JSON.stringify(inventoryData),
                dataType: "json",
                type: 'POST',
                contentType: "application/json"
            };
            return Promise.resolve($.ajax(reserveInventoryDetails));  
      },
      releaseInventory: function(){
        var inventoryData = {"inventories": inventoryArray};
        var releaseInventoryDetails = {
                url: widgetModel.translate('releaseInventoryUrl'),
                data : JSON.stringify(inventoryData),
                dataType: "json",
                type: 'POST',
                contentType: "application/json"
            };
            return Promise.resolve($.ajax(releaseInventoryDetails));   
      },
      addToCart: function(){
          return new Promise(function(resolve,reject){
            widgetModel.reserveInventory().then(function(reserveSuccess){
            if(cartData){
                var addCartDetails = {
                    url: widgetModel.translate(widgetModel.ADD_TO_CART_URL),
                    data : JSON.stringify(cartData),
                    dataType: "json",
                    type: 'POST',
                    contentType: "application/json",
                    headers: {
                        'Authorization': authorizationHeader,
                        'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                        'x-ccsite':widgetModel.site().siteInfo.id
                    }
                    
                };
                resolve($.ajax(addCartDetails));
            }    
        },function(error){
            log.error("Unable to reserve inventory",error);
            widgetModel.showLoader(false);
            notifier.clearError(widgetModel.WIDGET_ID);
            notifier.clearSuccess(widgetModel.WIDGET_ID);
            notifier.sendError("Unable to reserve inventory for your order. Please try again")
        });
            
              
          });
      },
      /**
       * Called when widget first loaded
       */
      onLoad: function (widget) {
        widgetModel = widget;
        
        
        $.Topic("CREDIT_LIMIT_UPDATED.memory").subscribe(function(data){
            notifier.clearError(widgetModel.WIDGET_ID);
             notifier.clearSuccess(widgetModel.WIDGET_ID);
            if(this[0].balanceCreditLimit > 0){
              widgetModel.isCheckoutBtnDisable(false); 
            } else {
              widgetModel.isCheckoutBtnDisable(true);
              notifier.sendError(widgetModel.WIDGET_ID,widgetModel.translate('getOrderHistoryMsg'), true);
              
            }
        });
        
        // Create observable to mark the resources loaded, if it's not already there
        if (typeof widget.orderSummaryResourcesLoaded == 'undefined') {
          widget.orderSummaryResourcesLoaded = ko.observable(false);
        }

       

        // These are not configuration options
        widget.selectedShippingOption.isData = true;

        // Initialise options for showing shipping and tax information
        widget.includeShipping(false);
        widget.hasShippingInfo(false);
        widget.includeTax(false);
        widget.hasTaxInfo(false);

        /**
         * Sets up shipping and sales tax observable fields.
         */
        widget.setupShippingAndTax = function (isPricingComplete) {
          // If no items in cart, don's show shipping and sales tax costs
          if (widget.cart().items().length <= 0) {
            widget.includeShipping(false);
            widget.hasShippingInfo(false);
            widget.includeTax(false);
            widget.hasTaxInfo(false);
          }
          // No shipping and tax amount available if shipping method has not been selected
          else if (widget.selectedShippingOption() == undefined) {
            widget.includeShipping(true);
            widget.hasShippingInfo(false);
            widget.includeTax(true);
            widget.hasTaxInfo(false);
          }
          // Work out shipping and tax amounts
          else {
            widget.includeShipping(true);
            widget.hasShippingInfo(isPricingComplete);
            widget.includeTax(true);
            widget.hasTaxInfo(isPricingComplete);

          }
        };

        /**
         * Resets the shipping and sales tax observable fields.
         */
        widget.resetShippingAndTax = function (obj) {
          widget.selectedShippingOption(null);
          if (widget.cart().items().length > 0) {
            widget.includeShipping(true);
            widget.includeTax(true);
          }
          else {
            widget.includeShipping(false);
            widget.includeTax(false);
          }
          widget.hasShippingInfo(false);
          widget.hasTaxInfo(false);
        };

        /**
         * Handle  cart pricing success/failure events
         */
        $.Topic(pubsub.topicNames.ORDER_PRICING_SUCCESS).subscribe(function (obj) {
          widget.pricingAvailable(true);
          widget.destroySpinner();
          widget.setupShippingAndTax(true);
        });

        $.Topic(pubsub.topicNames.ORDER_PRICING_FAILED).subscribe(function (obj) {
          widget.pricingAvailable(false);
          widget.destroySpinner();
          if (this && this.errorCode == CCConstants.INVALID_SHIPPING_METHOD) {
            widget.hasShippingInfo(false);
            widget.hasTaxInfo(false);
          }
          else if (this && this.errorCode == CCConstants.PRICING_TAX_REQUEST_ERROR) {
            widget.hasShippingInfo(false);
            widget.hasTaxInfo(false);
          }
          else {
            widget.resetShippingAndTax();
          }
        });

        // Handle shipping method load success/failure events
        $.Topic(pubsub.topicNames.SHIPPING_METHODS_LOADED).subscribe(function (obj) {
          // Handle case where no shipping methods have been loaded
          if (widget.shippingmethods().shippingOptions().length === 0) {
            widget.resetShippingAndTax();
          }
          else {
            // TODO - According to Nev this will cause issues with the observable model 
            // as we're assigning a new array to the observable array.
            // May have to iterate through widget.shippingmethods().shippingOptions() 
            // and push these onto widget.shippingOptions.
            // However if we make our own copies then this complicates pricing updates
            // as our copy won't have the updated pricing
            widget.shippingOptions(widget.shippingmethods().shippingOptions());

            // Make sure that if we have a currently selected option then re-price using that option
            if (widget.selectedShippingOption() != undefined &&
              widget.selectedShippingOption() != '') {
              var foundShippingOption = false;
              for (var i = 0; i < widget.shippingOptions().length; i++) {
                if (widget.selectedShippingOption().repositoryId === widget.shippingOptions()[i].repositoryId) {
                  widget.selectedShippingOption(widget.shippingOptions()[i]);
                  foundShippingOption = true;
                  break;
                }
              }
              if (!foundShippingOption && widget.cart().shippingMethod()) {
                widget.selectedShippingOption(null);
              }
            }

            // Refresh shipping and tax observables
            widget.setupShippingAndTax(widget.pricingAvailable());
          }
        });

        $.Topic(pubsub.topicNames.LOAD_SHIPPING_METHODS_FAILED).subscribe(function (obj) {
          widget.shippingOptions.removeAll();
          widget.resetShippingAndTax();
        });

        $.Topic(pubsub.topicNames.NO_SHIPPING_METHODS).subscribe(function (obj) {
          widget.shippingOptions.removeAll();
          widget.resetShippingAndTax();
        });


        // Handle case of re-pricing for a selected shipping option.
        $.Topic(pubsub.topicNames.CHECKOUT_SHIPPING_METHOD).subscribe(function (obj) {
          // Create spinner for re-pricing
          if (widget.cart() != undefined
            && widget.cart().items() != undefined
            && widget.cart().items().length > 0) {

            if (this && this.repositoryId && widget.cart().shippingMethod()) {
              widget.createSpinner();
            }

            // The selected shipping option must be in our list of shipping options
            if (this && this.repositoryId && widget.shippingOptions() != undefined) {
              var foundOption = false;
              for (var i = 0; i < widget.shippingOptions().length; i++) {
                if (this.repositoryId === widget.shippingOptions()[i].repositoryId) {
                  widget.selectedShippingOption(widget.shippingOptions()[i]);
                  foundOption = true;
                  break;
                }
              }

              if (!foundOption || !widget.cart().shippingMethod()) {
                widget.destroySpinner();
                widget.selectedShippingOption(null);
              }
            }
            // Refresh display options
            widget.setupShippingAndTax(widget.pricingAvailable());
          }
        });

        // Handle case when shipping method is reset
        $.Topic(pubsub.topicNames.CHECKOUT_RESET_SHIPPING_METHOD).subscribe(function (obj) {
          widget.selectedShippingOption(null);
          widget.setupShippingAndTax(false);
        });

        // Reset shipping and tax if shipping address is invalid
        $.Topic(pubsub.topicNames.CHECKOUT_SHIPPING_ADDRESS_INVALID).subscribe(function (obj) {
          widget.shippingOptions.removeAll();
          widget.resetShippingAndTax();
        });

        $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY).subscribe(function (obj) {
          if (widget.selectedShippingOption()) {
            widget.createSpinner();
          }
        });

        // Subscribe to changes in the total cost
        widget.cart().amount.subscribe(function (newValue) {
          // Refresh shipping, tax and total costs
          widget.setupShippingAndTax(false);
          widgetModel.loadCart();
        });

        // Functions to create and destroy the spinner
        widget.destroySpinner = function () {
          $(widget.pricingIndicator).removeClass('loadingIndicator');
          spinner.destroyWithoutDelay($(widget.pricingIndicator));
        };
        widget.createSpinner = function () {
          $(widget.pricingIndicator).css('position', 'relative');
          widget.pricingIndicatorOptions.loadingText = widget.translate('rePricingText', { defaultValue: this.DEFAULT_LOADING_TEXT });
          spinner.create(widget.pricingIndicatorOptions);
        };

        // Set up initial shipping and tax observables
        widget.setupShippingAndTax(false);
        widget.destroySpinner();

        widget.persistedLocaleName(JSON.parse(CCRestClient.getStoredValue(CCConstants.LOCAL_STORAGE_USER_CONTENT_LOCALE)));
        // If locale name is present in local storage, append it to the paypal image url
        if (widget.persistedLocaleName() && widget.persistedLocaleName()[0]) {
          widget.paypalImageSrc(widget.paypalImageSrc() + "&locale=" + widget.persistedLocaleName()[0].name);
        }
        //Mobile On click scroll to Order summary
        $('body').on('click touchstart', '.view_details_scroll', function () {
          $('html, body').animate({
            scrollTop: $(".scrollToOrderSummary").offset().top - 60
          }, 1000)
        }); 
        
        
        widgetModel.loadCart();
      }, // end of onLoad
      
      
      
      /**
       * Called each time the page appears
       */
      beforeAppear: function (page) {
        var widget = this;

        // Work out available and selected shipping options
        widget.shippingOptions(widget.shippingmethods().shippingOptions());
        var pricingAvailable = false;
        if (widget.cart().shippingMethod() != undefined && widget.cart().shippingMethod() != '') {
          for (var i = 0; i < widget.shippingOptions().length; i++) {
            if (widget.cart().shippingMethod() === widget.shippingOptions()[i].repositoryId) {
              widget.selectedShippingOption(widget.shippingOptions()[i]);
              // Ensure we have  pricing
              if (widget.cart().shipping() > 0)
                pricingAvailable = true;

              break;
            }
          }
        }


        // Set up observables for this widget
        widget.setupShippingAndTax(pricingAvailable);
        widgetModel.prepareInvoiceOrder();
      }
    
      
    }
  }
);
