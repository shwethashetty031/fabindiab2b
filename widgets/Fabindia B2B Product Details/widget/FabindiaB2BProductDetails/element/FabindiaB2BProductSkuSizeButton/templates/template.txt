<!-- ko foreach: variantOptionsArray -->

<!-- ko if: $data.optionValues -->
    
      <div data-bind="text: optionDisplayName"></div>
      <div data-bind="foreach: optionValues">
        <button>
          <li> 
             <span class="size_box" data-bind="text: key, click: $parent.selectedOption, attr: {id: 'CC-prodDetails-' + $parent.optionId}"></span>
          </li>
        </button>
      </div>

<!-- /ko -->

<!--/ko -->