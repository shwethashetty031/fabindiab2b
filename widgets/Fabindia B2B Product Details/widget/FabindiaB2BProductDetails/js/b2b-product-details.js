/**
 * @fileoverview Product Details Widget.
 * 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout', 'pubsub', 'ccConstants', 'koValidate', 'notifier', 'CCi18n', 'storeKoExtensions', 'swmRestClient', 'spinner', 'pageLayout/product', 'ccRestClient', 'pinitjs', 'js/jquery.tds', 'js/jsrender', 'pageLayout/cart', 'pageLayout/site', 'js/slick', 'storageApi','navigation','moment', 'ccLogger'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function (ko, pubsub, CCConstants, koValidate, notifier, CCi18n, storeKoExtensions, swmRestClient, spinner, product, ccRestClient, pinitjs, jqueryTds, jsRender, CartViewModel, SiteViewModel, slick, storageApi, navigation, moment, logger) {

    "use strict";
    var widgetModel;
    var bespokeProductObj;
    var lookProductJSON;
    var lookName;
    var bespokeProductSummary;
    var newData;
    var onLoad = true;
    var productClick = false;
    var isProductSelected = false;
    var categorySlider;
    var lookId;
    var fabricId;
    var actualSize;
    var isHideScroll = false;
    var fullPath;
    var LOADED_EVENT = "LOADED";
    var LOADING_EVENT = "LOADING";
    var arraySetFabric = {};
    var arrayTemp = [];
    var arraySetFabricIndex = 0;
    var productLoadingOptions = {
      parent: '#page',

    };
    var fabricIndexOnScroll = 0;
    var resourcesAreLoaded = false;
    var resourcesNotLoadedCount = 0;
    var resourcesMaxAttempts = 5;
    var fabProdURL = "https://www.fabindia.com/";
    var fabricTxt = "";

    var mySpacesComparator = function (opt1, opt2) {
      if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
        return 1;
      } else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
        return -1;
      } else {
        return 0;
      }
    };
    var joinedSpacesComparator = function (opt1, opt2) {
      if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
        return 1;
      } else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
        return -1;
      } else {
        return 0;
      }
    };

    return {

            
      ADD_RECORDS_TO_SELECTION_URL: "addRecordsToSelectionUrl",
      GET_ORDER_SSE: "getOrderSSE",
      GET_RESERVE_QUANTITY: "reserveQtyUrl",
      stockStatus: ko.observable(false),
      stockState: ko.observable(),
      showStockStatus: ko.observable(false),
      variantOptionsArray: ko.observableArray([]),
      itemQuantity: ko.observable(1),
      stockAvailable: ko.observable(),
      availabilityDate: ko.observable(),
      selectedSku: ko.observable(),
      disableOptions: ko.observable(false),
      priceRange: ko.observable(false),
      filtered: ko.observable(false),
      WIDGET_ID: 'productDetails',
      isAddToCartClicked: ko.observable(false),
      containerImage: ko.observable(),
      imgGroups: ko.observableArray(),
      mainImgUrl: ko.observable(),
      activeImgIndex: ko.observable(0),
      viewportWidth: ko.observable(),
      skipTheContent: ko.observable(false),
      listPrice: ko.observable(),
      salePrice: ko.observable(),
      backLinkActive: ko.observable(true),
      variantName: ko.observable(),
      variantValue: ko.observable(),
      listingVariant: ko.observable(),
      shippingSurcharge: ko.observable(),
      secondaryCurrencyShippingSurcharge: ko.observable(),
      imgMetadata: [],
      isMobile: ko.observable(false),
      quickViewFromPurchaseList: ko.observable(false),
      pincode: ko.observable(),
      codAvailable: ko.observable(false),
      furnitureServiceAvailbale: ko.observable(false),
      validentry: ko.observable(true),
      showMessage: ko.observable(false),
      imgPath: ko.observable(),
      sizeCHrtOb: ko.observable(),
      productDetailsArray: ko.observable([]),
      variants: ko.observable(),
      bespokeOptionValues: ko.observableArray([]),
      isSizeDropdown: ko.observable(),
      catInfo: ko.observable(),
      staticsizeCHrtOb: ko.observable(),
      showSizeMsg: ko.observable(),
      addToCartSuccessMsg: ko.observable(false),
      showLoader: ko.observable(false),
      displayMultipleSkus: ko.observableArray([]),
      isUpcomingCollection: ko.observable(false),

      // social
      showSWM: ko.observable(true),
      isAddToSpaceClicked: ko.observable(false),
      disableAddToSpace: ko.observable(false),
      spaceOptionsArray: ko.observableArray([]),
      spaceOptionsGrpMySpacesArr: ko.observableArray([]),
      spaceOptionsGrpJoinedSpacesArr: ko.observableArray([]),
      mySpaces: ko.observableArray([]),
      siteFbAppId: ko.observable(''),

      finalBespokePrice: ko.observable(),
      bespokeTotalPrice: ko.observable(),
      fabricHtmlOutput: ko.observable(),
      lookSummary: ko.observable(),
      finalDisplayImage: ko.observable(),
      selectedLookId: ko.observable(),
      componentIdAndCost: ko.observable(),
      buyButtonText: ko.observable(),
      continueBtnTxt: ko.observable(false),
      SKUDetailArr: ko.observableArray([]),
      allListPricesCheck: ko.observable(false),
        windowClosed:ko.observable(true),
        windowClosedForBuyer:ko.observable(false),
        windowClosedForRM:ko.observable(false),
       

    effectiveCreditLimit:ko.observable(0),
    availableCreditLimit: ko.observable(),
    creditLimitExceeded: ko.observable(true),
      reservedArray: ko.observableArray([]),
      resourcesLoaded: function (widget) {
        resourcesAreLoaded = true;
      },
      
    
        checkWindowClosed: function(){
             var buyDateFranchise = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateFranchise,"DD-MM-YYYY") : '';
                    buyDateFranchise = buyDateFranchise.format('YYYY-MM-DD')
                    var endDateFranchise =  widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateFranchise,"DD-MM-YYYY") : '';
                    endDateFranchise = endDateFranchise.format('YYYY-MM-DD')
                    var buyDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.buyDateRM,"DD-MM-YYYY") : '';
                    buyDateRM = buyDateRM.format('YYYY-MM-DD')
                    var endDateRM = widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM ? moment(widgetModel.site().extensionSiteSettings.b2bSiteSettings.endDateRM,"DD-MM-YYYY") : '';
                    endDateRM = endDateRM.format('YYYY-MM-DD')
                    var currentDate = moment().format("YYYY-MM-DD");
                if(widgetModel.product().x_collectionType().toLowerCase() === widgetModel.bannerJsonObject().upcoming.collectionType.toLowerCase()){
                for(var i=0;i< widgetModel.user().roles().length; i++){
                    if(moment(currentDate).isAfter(endDateFranchise,'day') || moment(currentDate).isBefore(buyDateFranchise,'day')){
                        if((widgetModel.user().roles()[i].function == "approver") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId) || (widgetModel.user().roles()[i].function == "admin") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(moment(currentDate).isAfter(endDateRM,'day') || moment(currentDate).isBefore(buyDateRM,'day')){
                                widgetModel.windowClosedForRM(true);
                                setTimeout(function(){
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('regionalManagerError'), true);
                                });
                                break;
                            }
                            else{
                                widgetModel.windowClosedForRM(false);
                            }
                        }
                        else if((widgetModel.user().roles()[i].function == "buyer") && (widgetModel.user().roles()[i].relativeTo.repositoryId == widgetModel.user().currentOrganization().repositoryId)){
                            if(widgetModel.user().roles().length == 1){
                            widgetModel.windowClosedForBuyer(true);
                            setTimeout(function(){
                                notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('buyerError'), true);
                            });
                        }
                            
                        }
                    }
                else{
                    widgetModel.windowClosedForBuyer(false); 
                }
        }
        
               if(widgetModel.windowClosedForBuyer() || widgetModel.windowClosedForRM()){
                   widgetModel.windowClosed(true);
                   return;
               }
               else{
                   widgetModel.windowClosed(false);
               }
          }
      },

      onLoad: function (widget) {
        widgetModel = widget;
        widget.defaultCartText();
        if (widget.product().type().toLowerCase() !== 'fbbespoke') {
          widget.indentDescription();
        }

        fullPath = window.location.protocol + '//' + window.location.hostname;

        var collections = widget.site().extensionSiteSettings.b2bSiteSettings.collections ? widget.site().extensionSiteSettings.b2bSiteSettings.collections : '';
        widgetModel.bannerJsonObject = ko.computed(function () {
          if (collections) {
            return JSON.parse(collections);
          }
        }, widget);


        $.Topic(pubsub.topicNames.UPDATE_LISTING_FOCUS).subscribe(function (obj) {
          widget.skipTheContent(true);
        });

        widget.showSecondaryShippingData = ko.pureComputed(function () {
          return widget.site().payShippingInSecondaryCurrency && (null != widget.site().exchangeRate()) &&
            (null != widget.site().siteSecondaryCurrency());
        });
        /* Disable Cuff for Half Sleeves */
        widget.lookSummary.subscribe(function (summary) {
          var proSummary = summary.ProductSummary;
          for (var i = 0; i < proSummary.length; i++) {
            if (proSummary[i].Product === "Sleeve") {
              (proSummary[i].Feature === "Half sleeve") ? $('.clickCuff').addClass('disable') : $('.clickCuff').removeClass('disable');
            }
          }
        });

        // for sticky footer hide show functionality on scroll

        $(document).scroll(function () {
          var y = $(this).scrollTop();
          if (y > 480) {
            $('.sticky_footer_main_pdp').fadeIn();
          } else {
            $('.sticky_footer_main_pdp').fadeOut();
          }
        });

        // sticky footer hide show functionality on scroll ends

        $.Topic(pubsub.topicNames.PAGE_READY).subscribe(function (obj) {
          var parameters = {};
          if (obj.parameters) {
            var param = obj.parameters.split("&");
            for (var i = 0; i < param.length; i++) {
              var tempParam = param[i].split("=");
              parameters[tempParam[0]] = tempParam[1];
            }
          }
          if (parameters.variantName && parameters.variantValue) {
            widget.variantName(decodeURI(parameters.variantName));
            widget.variantValue(decodeURI(parameters.variantValue));
          } else {
            widget.variantName("");
            widget.variantValue("");
          }
        });

        $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_SUCCESS).subscribe(function (obj) {
          if (obj.productUpdated) {
            widget.disableAddToSpace(true);
            setTimeout(function () { widget.disableAddToSpace(false); }, 3000);
          }
          else {
            widget.isAddToSpaceClicked(true);
            widget.disableAddToSpace(true);
            setTimeout(function () { widget.isAddToSpaceClicked(false); }, 3000);
            setTimeout(function () { widget.disableAddToSpace(false); }, 3000);
          }
        });

        $.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function (obj) {
          widget.getSpaces(function () { });
        });

        $.Topic(pubsub.topicNames.USER_AUTO_LOGIN_SUCCESSFUL).subscribe(function (obj) {
          widget.getSpaces(function () { });
        });

        $.Topic(pubsub.topicNames.SOCIAL_REFRESH_SPACES).subscribe(function (obj) {
          widget.getSpaces(function () { });
        });

        widget.itemQuantity.extend({
          required: { params: true, message: CCi18n.t('ns.common:resources.quantityRequireMsg') },
          digit: { params: true, message: CCi18n.t('ns.common:resources.quantityNumericMsg') },
          min: { params: 1, message: CCi18n.t('ns.productdetails:resources.quantityGreaterThanMsg', { quantity: 0 }) }
        });

        widget.stockAvailable.subscribe(function (newValue) {
          var max = parseInt(newValue, 10);
          widget.itemQuantity.rules.remove(function (item) {
            return item.rule == "max";
          });
          if (max > 0) {
            widget.itemQuantity.extend({
              max: {
                params: max,
                message: CCi18n.t('ns.productdetails:resources.quantityLessThanMsg',
                  { quantity: max })
              }
            });
          }
        });

        //Get the item quantity, available in cart.
        widget.itemQuantityInCart = function (pProduct) {
          var isCatRefId = pProduct.orderLimit ? false : true;
          return widget.cart().getItemQuantityInCart(widget.cart().items(), pProduct.id, pProduct.childSKUs[0].repositoryId, isCatRefId);
        };

        // initialize swm rest client
        swmRestClient.init(widget.site().tenantId, widget.isPreview(), widget.locale());

        // get FB app ID
        widget.fetchFacebookAppId();

        /**
         * Set up the popover and click handler 
         * @param {Object} widget
         * @param {Object} event
         */
        widget.shippingSurchargeMouseOver = function (widget, event) {
          // Popover was not being persisted between
          // different loads of the same 'page', so
          // popoverInitialised flag has been removed

          // remove any previous handlers
          $('.shippingSurchargePopover').off('click');
          $('.shippingSurchargePopover').off('keydown');

          var options = new Object();
          options.trigger = 'manual';
          options.html = true;

          // the button is just a visual aid as clicking anywhere will close popover
          options.title = widget.translate('shippingSurchargePopupTitle')
            + "<button id='shippingSurchargePopupCloseBtn' class='close btn pull-right'>"
            + widget.translate('escapeKeyText')
            + " &times;</button>";

          options.content = widget.translate('shippingSurchargePopupText');

          $('.shippingSurchargePopover').popover(options);
          $('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
          $('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);
        };

        widget.shippingSurchargeShowPopover = function (e) {
          // if keydown, rather than click, check its the enter key
          if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ENTER) {
            return;
          }

          // stop event from bubbling to top, i.e. html
          e.stopPropagation();
          $(this).popover('show');

          // toggle the html click handler
          $('html').on('click', widget.shippingSurchargeHidePopover);
          $('html').on('keydown', widget.shippingSurchargeHidePopover);

          $('.shippingSurchargePopover').off('click');
          $('.shippingSurchargePopover').off('keydown');
        };

        widget.shippingSurchargeHidePopover = function (e) {
          // if keydown, rather than click, check its the escape key
          if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ESCAPE) {
            return;
          }

          $('.shippingSurchargePopover').popover('hide');

          $('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
          $('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);

          $('html').off('click');
          $('html').off('keydown');

          $('.shippingSurchargePopover').focus();
        };

        $(window).resize(function () {
          // Optimizing the carousel performance, to not reload when only height changes
          var width = $(window)[0].innerWidth || $(window).width();
          if (widget.product && widget.product() && widget.product().primaryFullImageURL) {
            if (widget.viewportWidth() == width) {
              // Don't reload as the width is same
            } else {
              // Reload the things
              if (width > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
                if (widget.viewportWidth() <= CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
                  // Optionally reload the image place in case the view port was different
                  widget.activeImgIndex(0);
                  widget.mainImgUrl(widget.product().primaryFullImageURL);
                  $('#prodDetails-imgCarousel').carousel(0);
                  $('#carouselLink0').focus();
                }
              } else if (width >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
                if ((widget.viewportWidth() < CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) || (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH)) {
                  // Optionally reload the image place in case the view port was different
                  widget.activeImgIndex(0);
                  widget.mainImgUrl(widget.product().primaryFullImageURL);
                  $('#prodDetails-imgCarousel').carousel({ interval: 1000000000 });
                  $('#prodDetails-imgCarousel').carousel(0);
                  $('#carouselLink0').focus();
                }
              } else {
                if (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
                  // Optionally reload the carousel in case the view port was different
                  $('#prodDetails-mobileCarousel').carousel({ interval: 1000000000 });
                  $('#prodDetails-mobileCarousel').carousel(0);
                }
              }
            }
          }
          widget.viewportWidth(width);
          widget.checkResponsiveFeatures($(window).width());
        });

        widget.isAddToPurchaseListDisabled = ko.computed(
          function () {
            return (!widget.validateAddToSpace() || widget.disableAddToSpace());
          }, widget);

        widget.viewportWidth($(window).width());
        if (widget.product()) {
          widget.imgGroups(widget.groupImages(widget.product().mediumImageURLs()));
          widget.mainImgUrl(widget.product().primaryFullImageURL());
        }

        if (true || widget.skipTheContent()) {
          var focusFirstItem = function () {
            $('#cc-product-details :focusable').first().focus();
            widget.skipTheContent(false);
          };

          focusFirstItem();
          setTimeout(focusFirstItem, 1); // Daft IE fix.
        }

      },

      reserveSkuInventory: function(){
          var skus = "";
          var localArr = [];
            widgetModel.product().childSKUs().forEach(function(item, index){
                localArr.push(item.repositoryId());
            });
            skus = localArr.join();
            var inputData = { skus: skus };
            $.ajax({
                url: widgetModel.translate(widgetModel.GET_RESERVE_QUANTITY),
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(inputData),
                success: function(resp) {
                    var reservedArrayJS = {};
                    for (var j = 0; j < resp.inventories.length; j++) {
                        reservedArrayJS[resp.inventories[j].skuId] = resp.inventories[j].quantity;
                    }
                    widgetModel.reservedArray(ko.mapping.fromJS(reservedArrayJS));
                    widgetModel.setUpSKuSizes(widgetModel);
                },
                error : function error(error){
                        logger.info("ERROR : ", error);
                    }
            });
      },
      handleAddToSelection: function(data){
          
      
        var product = data.product();
        var user = data.user();
        var products = [];
        var productSkuData = [];

        var productArrForSku = [];
        //   spinner.create(productLoadingOptions);
            widgetModel.showLoader(true);
        widgetModel.displayMultipleSkus().forEach(function (val, i) {
            if(val.records[0].attributes["product.x_collectionType"][0].toLowerCase() == widgetModel.bannerJsonObject().upcoming.collectionType.toLowerCase()){
                productArrForSku.push(val.records[0].attributes["product.repositoryId"][0])
        }});
        var input = {};
        input[CCConstants.PRODUCT_IDS] = productArrForSku;
        input[CCConstants.FIELDS_QUERY_PARAM] = ["id","displayName","primaryFullImageURL","route","childSKUs.repositoryId","childSKUs.x_actualSize","childSKUs.listPrice","childSKUs.salePrice","childSKUs.x_fabLongDesc","parentCategories","parentCategory","listPrice","salePrice","x_collectionType","x_categoryType","x_webStyleId","x_primaryCategory","x_filterColor","x_pseStyleCode"];
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, input, function (successData) {
          successData.forEach(function (val, i) {
            val.childSKUs.forEach(function (value, index) {
                var parentCategoryArr = [];
                 val.parentCategories.forEach(function(item,index){
                parentCategoryArr.push(item.repositoryId)}),
              productSkuData.push({
                productId: val.id ? val.id : '',
                skuId: value.repositoryId ? value.repositoryId : '',
                listPrice: value.listPrice ? value.listPrice.toString() : '',
                salePrice: value.salePrice ? value.salePrice.toString() : '',
                longDescription: value.x_fabLongDesc ? value.x_fabLongDesc : '', 
                size: value.x_actualSize ? value.x_actualSize : '',
                categoryType: val.x_categoryType ? val.x_categoryType : '',
                webStyleId: val.x_webStyleId ? val.x_webStyleId : '',
                pscStyleCode:  val.x_pseStyleCode ?  val.x_pseStyleCode : '',
                parentCategory: val.x_primaryCategory ? 'b2b-'+ val.x_primaryCategory : val.parentCategory.repositoryId ? val.parentCategory.repositoryId : '',
                parentCategories : parentCategoryArr,
                x_filterColor : val.x_filterColor,
                quantity: '1',
                displayName: val.displayName ? val.displayName : '',
                productImg: val.primaryFullImageURL ? val.primaryFullImageURL : '',
                productLink: val.route ? val.route : '',
                productListPrice: val.listPrice ? val.listPrice.toString() : '',
                productSalePrice: val.salePrice ? val.salePrice.toString() : ''
              });
            })

          });
          var currentTotal = productSkuData.reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.listPrice)},0);
          var availableCreditLimit =  widgetModel.effectiveCreditLimit() - parseInt(currentTotal, 10);
          if( parseInt(availableCreditLimit, 10) < 1 ){
            widgetModel.creditLimitExceeded(true);
            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
            widgetModel.showLoader(false);
            // spinner.destroyWithoutDelay(productLoadingOptions.parent);
            return;
          } else {
          var json = {
            isPDP : true,
            accountId: user.parentOrganization.id(),
            emailId: user.emailAddress(),
            categoryType: product.x_categoryType(),
            products: productSkuData
          };
          widgetModel.addToSelection(widgetModel.translate(widgetModel.ADD_RECORDS_TO_SELECTION_URL), json).then(function (successData) {
                 if(successData.Status){
                            // self.purchaseSkus.removeAll();
                            // self.displayMultipleSkus.removeAll();
                        // self.showLoader(false);
                            notifier.sendError(self.WIDGET_ID,successData.Data, true);
                        }else{
                        // self.purchaseSkus.removeAll();
                            // self.displayMultipleSkus.removeAll();
                            // self.showLoader(false);
                            notifier.sendSuccess(self.WIDGET_ID,successData.Data, true);
                        }
                        setTimeout(function(){ 
                            notifier.clearError(self.WIDGET_ID);
                            notifier.clearSuccess(self.WIDGET_ID); 
                        }, 3000);
                        
                            // spinner.destroyWithoutDelay(productLoadingOptions.parent);
                            widgetModel.showLoader(false);
          }, function (error) {
                console.log("ERROR:: ",error);
                    // spinner.destroyWithoutDelay(productLoadingOptions.parent);
                    widgetModel.showLoader(false);
                notifier.clearError(widgetModel.WIDGET_ID);
                notifier.clearSuccess(widgetModel.WIDGET_ID);
                notifier.sendError(widgetModel.WIDGET_ID,'Error in adding products to Selection List', true);
          });
            }

        });
      },


      addToSelection: function (addToSelectUrl, jsonData) {
        var selectedProductDetails = {
          url: addToSelectUrl,
          dataType: "json",
          type: 'POST',
          contentType: "application/json",
          data: JSON.stringify(jsonData)
        };
        return Promise.resolve($.ajax(selectedProductDetails));
      },


      loadSwatch: function () {
        var self = this;
        var i, input = {}, products = [];

        // List products request to search
        input[CCConstants.SEARCH_NAV_RECORD_FILTER_KEY] = "product.x_webStyleId:" + widgetModel.product().x_webStyleId();
        ccRestClient.request(CCConstants.ENDPOINT_SEARCH_SEARCH,
          input,
          function (successData) {
            self.displayMultipleSkus(successData.resultsList.records);
          },
          function (errorData) {
            console.log(errorData);
          },
          null);

      },

      // start of creating micro data for Google Schema
      loadMeta: function () {

        var input = {}

        input[CCConstants.PRODUCT_IDS] = widgetModel.product().id();
        input[CCConstants.FIELDS_QUERY_PARAM] = ["childSKUs"];
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS,
          input,
          function (successData) {
            widgetModel.SKUDetailArr(successData[0].childSKUs);
          },
          function (errorData) {
            console.log(errorData);
          },
          null);

      },
      // end of creating micro data for Google Schema


      beforeAppear: function (page) {
        var widget = this;
        widget.showLoader(true);
        widgetModel.getCreditLimitValues();
        widgetModel.buyButtonText('Add to Cart');
        widgetModel.reservedArray([]);
        if ((widgetModel.continueBtnTxt() == true) && ($(window).width() < 800)) {
          widgetModel.continueBtnTxt(false);
        }
        //reset the values of COD
        widgetModel.showMessage(false);
        widgetModel.codAvailable(null);
        widgetModel.furnitureServiceAvailbale(null);
        widgetModel.validentry(true);
        widgetModel.catInfo(null);
        widgetModel.sizeCHrtOb(null);
        widgetModel.staticsizeCHrtOb(null);
        widgetModel.pincode(null);
        widgetModel.showSizeMsg(null);

        

        //Check if  the product is upcoming product
        if(widget.product() && widget.product().x_collectionType() && (widget.product().x_collectionType().toLowerCase() === widgetModel.bannerJsonObject().upcoming.collectionType.toLowerCase())){
          widgetModel.isUpcomingCollection(true);
        } else {
          widgetModel.isUpcomingCollection(false);
        }

        if (widget.product && widget.product()) {
          for (var i = 0; i < widget.product().parentCategories().length; i++) {
            if (widget.product().parentCategories()[i].longDescription() && widget.product().parentCategories()[i].longDescription().includes("sizeChartName")) {
              var isSizeDropdown = widget.product().parentCategories()[i].longDescription().replace(/<(.|\n)*?>/g, '');
              var txt = document.createElement("textarea");
              txt.innerHTML = isSizeDropdown;
              isSizeDropdown = JSON.parse(txt.value);
              widget.catInfo(isSizeDropdown);
              widget.isSizeDropdown(isSizeDropdown.dropdown);
              break;
            }
          }

          widget.checkResponsiveFeatures($(window).width());
          this.backLinkActive(true);
          if (!widget.isPreview() && !widget.historyStack.length) {
            this.backLinkActive(false);
          }

          /* reset active img index to 0 */
          widget.shippingSurcharge(null);
          widget.secondaryCurrencyShippingSurcharge(null);
          widget.activeImgIndex(0);
          widget.firstTimeRender = true;
          if(!widget.isUpcomingCollection()){
              widget.reserveSkuInventory();
          }else{
              this.setUpSKuSizes(widget);
          }
          this.populateVariantOptions(widget);
          if (widget.product()) {
            widget.imgGroups(widget.groupImages(widget.product().mediumImageURLs()));
          }
          widget.loaded(true);
          this.itemQuantity(1);
          // the dropdown values should be pre-selected if there is only one sku
          if (widget.product() && widget.product().childSKUs().length == 1) {
            this.filtered(false);
            this.filterOptionValues(null);
          }
          notifier.clearSuccess(this.WIDGET_ID);
          notifier.clearError(this.WIDGET_ID);
          var catalogId = null;
          if (widget.user().catalog) {
            catalogId = widget.user().catalog.repositoryId;
          }
          widget.listPrice(widget.product().listPrice());
          widget.salePrice(widget.product().salePrice());

          if (widget.product()) {
            widget.product().stockStatus.subscribe(function (newValue) {
              if ((widget.product().stockStatus().stockStatus === CCConstants.IN_STOCK ||
                widget.product().stockStatus().stockStatus === CCConstants.PREORDERABLE ||
                widget.product().stockStatus().stockStatus === CCConstants.BACKORDERABLE) &&
                (widget.product().stockStatus().orderableQuantity != undefined ||
                  widget.product().stockStatus().productSkuInventoryStatus != undefined)) {
                if (widget.product().stockStatus().orderableQuantity && widget.product().childSKUs().length > 1) {
                  widget.stockAvailable(widget.product().stockStatus().orderableQuantity);
                } else if (widget.product().childSKUs().length == 1) {
                  if (widget.product().stockStatus().productSkuInventoryStatus && widget.product().stockStatus().productSkuInventoryStatus[widget.product().childSKUs()[0].repositoryId()]) {
                    widget.stockAvailable(widget.product().stockStatus().productSkuInventoryStatus[widget.product().childSKUs()[0].repositoryId()]);
                  } else {
                    if (widget.product().stockStatus().orderableQuantity) {
                      widget.stockAvailable(widget.product().stockStatus().orderableQuantity);
                    }
                  }
                }
                else {
                  widget.stockAvailable(1);
                }
                widget.disableOptions(false);
                widget.stockStatus(true);
                widget.stockState(widget.product().stockStatus().stockStatus);
                widget.availabilityDate(widget.product().stockStatus().availabilityDate);
              } else {
                widget.stockAvailable(0);
                widget.stockState(CCConstants.OUT_OF_STOCK);
                widget.disableOptions(true);
                widget.stockStatus(false);
              }
              widget.showStockStatus(true);
            });
            var firstchildSKU = widget.product().childSKUs()[0];
            if (firstchildSKU) {
              var skuId = firstchildSKU.repositoryId();
              if (this.variantOptionsArray().length > 0) {
                skuId = '';
              }
              this.showStockStatus(false);
              widget.product().getAvailability(widget.product().id(), skuId, catalogId);
              widget.product().getPrices(widget.product().id(), skuId);
            } else {
              widget.stockStatus(false);
              widget.disableOptions(true);
              widget.showStockStatus(true);
            }
            this.priceRange(this.product().hasPriceRange);
            widget.mainImgUrl(widget.product().primaryFullImageURL());

            $.Topic(pubsub.topicNames.PRODUCT_VIEWED).publish(widget.product());
            $.Topic(pubsub.topicNames.PRODUCT_PRICE_CHANGED).subscribe(function () {
              widget.listPrice(widget.product().listPrice());
              widget.salePrice(widget.product().salePrice());
              widget.shippingSurcharge(widget.product().shippingSurcharge());
              widget.secondaryCurrencyShippingSurcharge(widget.product().secondaryCurrencyShippingSurcharge && widget.product().secondaryCurrencyShippingSurcharge() ? widget.product().secondaryCurrencyShippingSurcharge() : null);
            });
          }

          // Load spaces
          if (widget.user().loggedIn()) {
            widget.getSpaces(function () { });
          }
        }


        //Product Details list view
        if (widgetModel.product().longDescription()) {
          var prodctDetails = widgetModel.product().longDescription().replace(/<(.|\n)*?>/g, '');

          //To decode the special characters
          var txt = document.createElement("textarea");
          txt.innerHTML = prodctDetails;
          prodctDetails = txt.value;

          var prodctDetailsArray = prodctDetails.split(';');
          widgetModel.productDetailsArray(prodctDetailsArray);
        } else {
          widgetModel.productDetailsArray([]);
        }

        function CategoryAttribute() {
          this.attr = [
            {
              key: "x_sCAcrossShoulder",
              labelWomen: "Across Shoulder",
              labelMen: "Across Shoulder",
              labelKids: "Across Shoulder",
              topWear: 4,
              bottomWear: 0,
              sets: 4,
              footWear: 0
            },
            {
              key: "x_sCBust",
              labelWomen: "Bust",
              labelMen: "Chest",
              labelKids: "Chest",
              topWear: 1,
              bottomWear: 0,
              sets: 1,
              footWear: 0
            },
            {
              key: "x_sCFrontLengthShoulderSeam",
              labelWomen: "Front Length",
              labelMen: "Front Length",
              labelKids: "Front Length",
              topWear: 5,
              bottomWear: 0,
              sets: 5,
              footWear: 0
            },
            {
              key: "x_sCHips",
              labelWomen: "Hips",
              labelMen: "Hips",
              labelKids: "Hips",
              topWear: 3,
              bottomWear: 4,
              sets: 3,
              footWear: 0
            },
            {
              key: "x_sCWaist",
              labelWomen: "Waist",
              labelMen: "Waist",
              labelKids: "Waist",
              topWear: 2,
              bottomWear: 1,
              sets: 2,
              footWear: 0
            },
            {
              key: "x_sCHemline",
              labelWomen: "Hemline",
              labelMen: "Hemline",
              labelKids: "Hemline",
              topWear: 6,
              bottomWear: 7,
              sets: 6,
              footWear: 0
            },
            {
              key: "x_sCBottomsOutseamSideseam",
              labelWomen: "Bottom Length",
              labelMen: "Bottom Length",
              labelKids: "Bottom Length",
              topWear: 0,
              bottomWear: 5,
              sets: 16,
              footWear: 0
            },
            {
              key: "x_sCBottomsWaistRELAXED",
              labelWomen: "Waist Relaxed",
              labelMen: "Waist Relaxed",
              labelKids: "Waist Relaxed",
              topWear: 0,
              bottomWear: 2,
              sets: 14,
              footWear: 0
            },
            {
              key: "x_sCBottomsWaistSTRETCHED",
              labelWomen: "Waist Stretched",
              labelMen: "Waist Stretched",
              labelKids: "Waist Stretched",
              topWear: 0,
              bottomWear: 3,
              sets: 15,
              footWear: 0
            },
            {
              key: "x_sCSkirtLength",
              labelWomen: "Skirt Length",
              labelMen: "Skirt Length",
              labelKids: "Skirt Length",
              topWear: 0,
              bottomWear: 6,
              sets: 17,
              footWear: 0
            },
            {
              key: "x_sCDupttaLength",
              labelWomen: "Duptta length",
              labelMen: "Duptta length",
              labelKids: "Duptta length",
              topWear: 7,
              bottomWear: 0,
              sets: 12,
              footWear: 0
            },
            {
              key: "x_sCWidth",
              labelWomen: "Duptta Width",
              labelMen: "Duptta Width",
              labelKids: "Duptta Width",
              topWear: 8,
              bottomWear: 0,
              sets: 13,
              footWear: 0
            },
            {
              key: "x_sCWaistSlipJacket",
              labelWomen: "Slip Waist",
              labelMen: "Jacket Waist",
              labelKids: "Jacket Waist",
              topWear: 10,
              bottomWear: 0,
              sets: 8,
              footWear: 0
            },
            {
              key: "x_sCHipsSlipJacket",
              labelWomen: "Slip Hips",
              labelMen: "Jacket Hip",
              labelKids: "Jacket Hip",
              topWear: 11,
              bottomWear: 0,
              sets: 9,
              footWear: 0
            },
            {
              key: "x_sCBustSlipJacket",
              labelWomen: "Slip Bust",
              labelMen: "Jacket Chest",
              labelKids: "Jacket Chest",
              topWear: 9,
              bottomWear: 0,
              sets: 7,
              footWear: 0
            },
            {
              key: "x_sCShoulderSlipJacket",
              labelWomen: "Slip Shoulder",
              labelMen: "Jacket Shoulder",
              labelKids: "Jacket Shoulder",
              topWear: 12,
              bottomWear: 0,
              sets: 10,
              footWear: 0
            },
            {
              key: "x_sCFrontLengthSlipJacket",
              labelWomen: "Slip Length",
              labelMen: "Jacket length",
              labelKids: "Jacket length",
              topWear: 13,
              bottomWear: 0,
              sets: 11,
              footWear: 0
            },
            {
              key: "x_sCFootwearUK",
              labelWomen: "UK",
              labelMen: "UK",
              labelKids: "UK",
              topWear: 0,
              bottomWear: 0,
              sets: 0,
              footWear: 1
            },
            {
              key: "x_sCFootwearUS",
              labelWomen: "US",
              labelMen: "US",
              labelKids: "US",
              topWear: 0,
              bottomWear: 0,
              sets: 0,
              footWear: 2
            },
            {
              key: "x_sCFootwearEuro",
              labelWomen: "Euro",
              labelMen: "Euro",
              labelKids: "Euro",
              topWear: 0,
              bottomWear: 0,
              sets: 0,
              footWear: 3
            },
            {
              key: "x_sCFootwearLength",
              labelWomen: "Length (in cm)",
              labelMen: "Length (in cm)",
              labelKids: "Length (in cm)",
              topWear: 0,
              bottomWear: 0,
              sets: 0,
              footWear: 4
            }

          ],

            this.getSequenceType = function () {
              var attr = this.attr;
              var sequenceType = [];
              var sequenceAttribute = widgetModel.catInfo().sequenceType;

              var data = attr.filter(function (element, index) {
                return element[sequenceAttribute] ? true : false;
              });

              return data.sort(function (element1, element2) {
                return element1[sequenceAttribute] > element2[sequenceAttribute] ? 1 : -1;
              });
            }
        }
        if (widgetModel.catInfo()) {
          if (widgetModel.catInfo().sizeChartName === 'dynamic') {
            var catAttr = new CategoryAttribute();
            var sequenceTypeObj = catAttr.getSequenceType();
            //Setting up size chart model
            var childSkus = widgetModel.product().childSKUs();
            if (childSkus) {
              var sizesObj = {};
              sizesObj.size = [];
              sizesObj.head = [];
              for (var index = 0; index < childSkus.length; index++) {
                var childSku = childSkus[index];
                var temp = [];
                if (widgetModel.catInfo().sequenceType !== 'footWear') {
                  temp.push(childSku.x_actualSize());
                }
                for (var attrIndex = 0; attrIndex < sequenceTypeObj.length; attrIndex++) {
                  var key = sequenceTypeObj[attrIndex].key;
                  if (childSku.hasOwnProperty(sequenceTypeObj[attrIndex].key) && childSku[sequenceTypeObj[attrIndex].key]()) {
                    if (widgetModel.product().x_categoryType() == "IK" || widgetModel.product().x_categoryType() == "IK-G" || widgetModel.product().x_categoryType() == "IK-B" || widgetModel.product().x_categoryType() == "KW") {
                      if (sizesObj.head.indexOf(sequenceTypeObj[attrIndex].labelKids) === -1) {
                        sizesObj.head.push(sequenceTypeObj[attrIndex].labelKids);
                      }
                    }
                    if (widgetModel.product().x_categoryType() == "WI" || widgetModel.product().x_categoryType() == "WW" || widgetModel.product().x_categoryType() == "GL") {
                      if (sizesObj.head.indexOf(sequenceTypeObj[attrIndex].labelWomen) === -1) {
                        sizesObj.head.push(sequenceTypeObj[attrIndex].labelWomen);
                      }
                    }
                    if (widgetModel.product().x_categoryType() == "MN" || widgetModel.product().x_categoryType() == "MW") {
                      if (sizesObj.head.indexOf(sequenceTypeObj[attrIndex].labelMen) === -1) {
                        sizesObj.head.push(sequenceTypeObj[attrIndex].labelMen);
                      }
                    }

                    if (widgetModel.product().x_categoryType() == "FT") {
                      if (sizesObj.head.indexOf(sequenceTypeObj[attrIndex].labelMen) === -1) {
                        sizesObj.head.push(sequenceTypeObj[attrIndex].labelMen);
                      }
                    }
                    temp.push(childSku[sequenceTypeObj[attrIndex].key]());
                  }
                }
                sizesObj['size'].push(temp);
              }
              widgetModel.sizeCHrtOb(sizesObj);
            } else {
              widgetModel.sizeCHrtOb({});
            }
          } else {
            var sizeChartDataStatic = widgetModel.sizeChartData() ? JSON.parse(widgetModel.sizeChartData()) : null;
            if (sizeChartDataStatic && sizeChartDataStatic.hasOwnProperty(widgetModel.catInfo().sizeChartName)) {
              widgetModel.staticsizeCHrtOb(sizeChartDataStatic[widgetModel.catInfo().sizeChartName]);
            }
          }


        }
      },
      indentDescription: function () {
        var valuesInArray = widgetModel.product().description().split(/<|>/);
        var finalString = "";
        for (var j = 0; j < valuesInArray.length; j++) {
          var vals = valuesInArray[j].split(";");
          if (vals.length > 1) {
            var text2 = '<ul class="align-bullet">';
            for (var k = 0; k < vals.length; k++) {
              text2 += "<li  style = 'list-style-type:initial'>" + vals[k];
            }
            text2 += "</ul>";
            finalString += text2;
          } else {
            finalString += "<div class='align-desc'>" + valuesInArray[j] + "</div>";
          }
        }
        widgetModel.product().description(finalString);
      },

      
      //default cart text
      defaultCartText: function () {
        widgetModel.buyButtonText('Add to Cart');
      },
     
      //This method is implemented to add active class on selected sku from size tabs.
      selectVariants: function (enableGetPrice) {
        //if there is only one size than add a class for defaut selection
        if (widgetModel.variantOptionsArray()[0].optionValues().length == 1 && widgetModel.variantOptionsArray()[0].optionValues()[0].status != 'OUT_OF_STOCK' && !widgetModel.isUpcomingCollection()) {
          $(".size_box").addClass("activeAlpha");
        }
        $('.size_box').click(widgetModel.selectVariantOptions);
      },
      
      selectVariantOptions: function () {
        var btns = document.getElementsByClassName("size_box");
        widgetModel.showSizeMsg(false);
        for (var i = 0; i < btns.length; i++) {
          var current = document.getElementsByClassName("activeAlpha");
          if($(btns[i]).hasClass("activeAlpha"))
          {
              $(btns[i]).removeClass("activeAlpha");
          }
          if(!widgetModel.isUpcomingCollection()){
          $(this).addClass("activeAlpha");
               if(($(btns[i]).html()==$(this).html()))
               {
                   btns[i].className += " activeAlpha";
               }
          }
        }
      },

      //This method is implemented to fetch selected dimension from size dropDown.
      selectDimensions: function () {
        $('select').on("change", function () {
          //Sets dataLayer object for 'productView' event after selecting dimension from size dropDown.
          // widgetModel.setDataLayerObject();
        });
      },

      //This method is implemented to set 'productView' event after selecting sizes from tabs or dropDown.
      setDataLayerObject: function () {
        var dimension = "", variant = "", skuId = "", skuPrice = "";
        var currencycode = widgetModel.site().selectedPriceListGroup().currency.currencyCode;
        if (typeof window.dataLayer !== 'undefined') {
          if (widgetModel.selectedSku() && widgetModel.selectedSku().x_actualSize) {
            if (widgetModel.isSizeDropdown()) {
              dimension = widgetModel.selectedSku().x_actualSize;
            } else {
              variant = widgetModel.selectedSku().x_actualSize;
            }
            skuId = widgetModel.selectedSku().repositoryId;
            skuPrice = currencycode === "INR" ? Math.round(widgetModel.selectedSku().salePrice ? widgetModel.selectedSku().salePrice : widgetModel.selectedSku().listPrice) : (widgetModel.selectedSku().salePrice ? widgetModel.selectedSku().salePrice : widgetModel.selectedSku().listPrice);
          } else {

            if (!widgetModel.product().productSalePrice || widgetModel.product().productSalePrice === 0 || isNaN(widgetModel.product().productSalePrice)) {
              if (widgetModel.product().productListPrice) {
                skuPrice = currencycode === "INR" ? Math.round(widgetModel.product().productListPrice) : widgetModel.product().productListPrice;
              } else { skuPrice = ""; }
            } else { skuPrice = currencycode === "INR" ? Math.round(widgetModel.product().productSalePrice) : widgetModel.product().productListPrice; }
          }
          var products = {
            "brand": "Fabindia",
            "category": widgetModel.product().parentCategories()[0].displayName(),
            "dimension1": dimension,
            "id": widgetModel.product().id(),
            "name": widgetModel.product().displayName(),
            "position": "1",
            "price": skuPrice,
            "sku": skuId,
            "variant": variant
          };
       
        }
      },

      goBack: function () {
        $(window).scrollTop($(window).height());
        window.history.go(-1);
        return false;
      },

      // Handles loading a default 'no-image' as a fallback
      cancelZoom: function (element) {
        $(element).parent().removeClass('zoomContainer-CC');
      },

      //this method populates productVariantOption model to display the variant options of the product
      populateVariantOptions: function (widget, allStockStatus) {
        var options = widget.productVariantOptions();
        var skusOptions = widget.product().childSKUs();
        if (options && options !== null && options.length > 0) {
          var optionsArray = [], productLevelOrder, productTypeLevelVariantOrder = {}, optionValues, productVariantOption, variants;
          for (var typeIdx = 0, typeLen = widget.productTypes().length; typeIdx < typeLen; typeIdx++) {
            if (widget.productTypes()[typeIdx].id == widget.product().type()) {
              variants = widget.productTypes()[typeIdx].variants;
              for (var variantIdx = 0, variantLen = variants.length; variantIdx < variantLen; variantIdx++) {
                productTypeLevelVariantOrder[variants[variantIdx].id] = variants[variantIdx].values;
              }
            }
          }
          for (var i = 0; i < options.length; i++) {
            if (widget.product().variantValuesOrder[options[i].optionId]) {
              productLevelOrder = widget.product().variantValuesOrder[options[i].optionId]();
            }
            optionValues = this.mapOptionsToArray(options[i].optionValueMap, productLevelOrder ? productLevelOrder : productTypeLevelVariantOrder[options[i].optionId], skusOptions, allStockStatus);
            productVariantOption = this.productVariantModel(options[i].optionName, options[i].mapKeyPropertyAttribute, optionValues, widget, options[i].optionId);
            optionsArray.push(productVariantOption);
          }
          widget.variantOptionsArray(optionsArray);
        } else {
          widget.imgMetadata = widget.product().product.productImagesMetadata;
          widget.variantOptionsArray([]);
        }
      },

      /*this create view model for variant options this contains
      name of the option, possible list of option values for the option
      selected option to store the option selected by the user.
      ID to map the selected option*/
      productVariantModel: function (optionDisplayName, optionId, optionValues, widget, actualOptionId) {
        var productVariantOption = {};
        var productImages = {};
        productVariantOption.optionDisplayName = optionDisplayName;
        productVariantOption.parent = this;
        productVariantOption.optionId = optionId;
        productVariantOption.originalOptionValues = ko.observableArray(optionValues);
        productVariantOption.actualOptionId = actualOptionId;

        var showOptionCation = ko.observable(true);
        if (optionValues.length === 1) {
          showOptionCation(this.checkOptionValueWithSkus(optionId, optionValues[0].value));
        }
        //If there is just one option value in all Skus we dont need any caption
        if (showOptionCation()) {
          productVariantOption.optionCaption = widget.translate('optionCaption', { optionName: optionDisplayName }, true);
        }
        productVariantOption.selectedOptionValue = ko.observable();
        productVariantOption.countVisibleOptions = ko.computed(function () {
          var count = 0;
          for (var i = 0; i < productVariantOption.originalOptionValues().length; i++) {
            if (optionValues[i].visible() == true) {
              count = count + 1;
            }
          }
          return count;
        }, productVariantOption);
        productVariantOption.disable = ko.computed(function () {
          if (productVariantOption.countVisibleOptions() == 0) {
            return true;
          } else {
            return false;
          }
        }, productVariantOption);
        productVariantOption.selectedOption = ko.computed({
          write: function (option) {
            this.parent.filtered(false);
            productVariantOption.selectedOptionValue(option);
            if (productVariantOption.actualOptionId === this.parent.listingVariant()) {
              if (option && option.listingConfiguration) {
                this.parent.imgMetadata = option.listingConfiguration.imgMetadata;
                this.parent.assignImagesToProduct(option.listingConfiguration);
              } else {
                this.parent.imgMetadata = this.parent.product().product.productImagesMetadata;
                this.parent.assignImagesToProduct(this.parent.product().product);
              }
            }
            this.parent.filterOptionValues(productVariantOption.optionId);
          },
          read: function () {
            return productVariantOption.selectedOptionValue();
          },
          owner: productVariantOption
        });
        productVariantOption.selectedOption.extend({
          required: { params: true, message: widget.translate('optionRequiredMsg', { optionName: optionDisplayName }, true) }
        });
        productVariantOption.optionValues = ko.computed({
          write: function (value) {
            productVariantOption.originalOptionValues(value);
          },
          read: function () {
            return ko.utils.arrayFilter(
              productVariantOption.originalOptionValues(),
              function (item) { return item.visible() == true; }
            );
          },
          owner: productVariantOption
        });


        //The below snippet finds the product display/listing variant (if available)
        //looping through all the product types
        for (var productTypeIdx = 0; productTypeIdx < widget.productTypes().length; productTypeIdx++) {
          //if the product type matched with the current product
          if (widget.product().type() && widget.productTypes()[productTypeIdx].id == widget.product().type()) {
            var variants = widget.productTypes()[productTypeIdx].variants;
            //Below FOR loop is to iterate over the various variant types of that productType
            for (var productTypeVariantIdx = 0; productTypeVariantIdx < variants.length; productTypeVariantIdx++) {
              //if the productType has a listingVariant == true, hence this is the product display variant
              if (variants[productTypeVariantIdx].listingVariant) {
                widget.listingVariant(variants[productTypeVariantIdx].id);
                break;
              }
            }
            break;
          }
        }
        productImages.thumbImageURLs = (widget.product().product.thumbImageURLs.length == 1 && widget.product().product.thumbImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ?
          [] : (widget.product().product.thumbImageURLs);
        productImages.smallImageURLs = (widget.product().product.smallImageURLs.length == 1 && widget.product().product.smallImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ?
          [] : (widget.product().product.smallImageURLs);
        productImages.mediumImageURLs = (widget.product().product.mediumImageURLs.length == 1 && widget.product().product.mediumImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ?
          [] : (widget.product().product.mediumImageURLs);
        productImages.largeImageURLs = (widget.product().product.largeImageURLs.length == 1 && widget.product().product.largeImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ?
          [] : (widget.product().product.largeImageURLs);
        productImages.fullImageURLs = (widget.product().product.fullImageURLs.length == 1 && widget.product().product.fullImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ?
          [] : (widget.product().product.fullImageURLs);
        productImages.sourceImageURLs = (widget.product().product.sourceImageURLs.length == 1 && widget.product().product.sourceImageURLs[0].indexOf("/img/no-image.jpg") > 0) ?
          [] : (widget.product().product.sourceImageURLs);

        var prodImgMetadata = [];
        if (widget.product().thumbImageURLs && widget.product().thumbImageURLs().length > 0) {
          for (var index = 0; index < widget.product().thumbImageURLs().length; index++) {
            prodImgMetadata.push(widget.product().product.productImagesMetadata[index]);
          }
        }

        ko.utils.arrayForEach(productVariantOption.originalOptionValues(), function (option) {
          if (widget.listingVariant() === actualOptionId) {
            for (var childSKUsIdx = 0; childSKUsIdx < widget.product().childSKUs().length; childSKUsIdx++) {
              if (widget.product().childSKUs()[childSKUsIdx].productListingSku()) {
                var listingConfiguration = widget.product().childSKUs()[childSKUsIdx];
                if (listingConfiguration[actualOptionId]() == option.key) {
                  var listingConfig = {};
                  listingConfig.thumbImageURLs = $.merge($.merge([], listingConfiguration.thumbImageURLs()), productImages.thumbImageURLs);
                  listingConfig.smallImageURLs = $.merge($.merge([], listingConfiguration.smallImageURLs()), productImages.smallImageURLs);
                  listingConfig.mediumImageURLs = $.merge($.merge([], listingConfiguration.mediumImageURLs()), productImages.mediumImageURLs);
                  listingConfig.largeImageURLs = $.merge($.merge([], listingConfiguration.largeImageURLs()), productImages.largeImageURLs);
                  listingConfig.fullImageURLs = $.merge($.merge([], listingConfiguration.fullImageURLs()), productImages.fullImageURLs);
                  listingConfig.sourceImageURLs = $.merge($.merge([], listingConfiguration.sourceImageURLs()), productImages.sourceImageURLs);
                  listingConfig.primaryFullImageURL = listingConfiguration.primaryFullImageURL() ? listingConfiguration.primaryFullImageURL() : widget.product().product.primaryFullImageURL;
                  listingConfig.primaryLargeImageURL = listingConfiguration.primaryLargeImageURL() ? listingConfiguration.primaryLargeImageURL() : widget.product().product.primaryLargeImageURL;
                  listingConfig.primaryMediumImageURL = listingConfiguration.primaryMediumImageURL() ? listingConfiguration.primaryMediumImageURL() : widget.product().product.primaryMediumImageURL;
                  listingConfig.primarySmallImageURL = listingConfiguration.primarySmallImageURL() ? listingConfiguration.primarySmallImageURL() : widget.product().product.primarySmallImageURL;
                  listingConfig.primaryThumbImageURL = listingConfiguration.primaryThumbImageURL() ? listingConfiguration.primaryThumbImageURL() : widget.product().product.primaryThumbImageURL;

                  //storing the metadata for the images
                  var childSKUImgMetadata = [];
                  if (listingConfiguration.images && listingConfiguration.images().length > 0) {
                    for (var index = 0; index < listingConfiguration.images().length; index++) {
                      childSKUImgMetadata.push(widget.product().product.childSKUs[childSKUsIdx].images[index].metadata);
                    }
                  }
                  listingConfig.imgMetadata = $.merge($.merge([], childSKUImgMetadata), prodImgMetadata);
                  option.listingConfiguration = listingConfig;
                }
              }
            }
          }
          if (widget.variantName() === actualOptionId && option.key === widget.variantValue()) {
            productVariantOption.selectedOption(option);
          }
        });

        return productVariantOption;
      },

      //this method is triggered to check if the option value is present in all the child Skus.
      checkOptionValueWithSkus: function (optionId, value) {
        var childSkus = this.product().childSKUs();
        var childSkusLength = childSkus.length;
        for (var i = 0; i < childSkusLength; i++) {
          if (!childSkus[i].dynamicPropertyMapLong[optionId] || childSkus[i].dynamicPropertyMapLong[optionId]() === undefined) {
            return true;
          }
        }
        return false;
      },

      //this method is triggered whenever there is a change to the selected option.
      filterOptionValues: function (selectedOptionId) {
        if (this.filtered()) {
          return;
        }
        var variantOptions = this.variantOptionsArray();
        for (var i = 0; i < variantOptions.length; i++) {
          var currentOption = variantOptions[i];
          var matchingSkus = this.getMatchingSKUs(variantOptions[i].optionId);
          var optionValues = this.updateOptionValuesFromSku(matchingSkus, selectedOptionId, currentOption);
          variantOptions[i].optionValues(optionValues);
          this.filtered(true);
        }
        this.updateSingleSelection(selectedOptionId);
      },

      // get all the matching SKUs
      getMatchingSKUs: function (optionId) {
        var childSkus = this.product().childSKUs();
        var matchingSkus = [];
        var variantOptions = this.variantOptionsArray();
        var selectedOptionMap = {};
        for (var j = 0; j < variantOptions.length; j++) {
          if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined) {
            selectedOptionMap[variantOptions[j].optionId] = variantOptions[j].selectedOption().value;
          }
        }
        for (var i = 0; i < childSkus.length; i++) {
          var skuMatched = true;
          for (var key in selectedOptionMap) {
            if (selectedOptionMap.hasOwnProperty(key)) {
              if (!childSkus[i].dynamicPropertyMapLong[key] ||
                childSkus[i].dynamicPropertyMapLong[key]() != selectedOptionMap[key]) {
                skuMatched = false;
                break;
              }
            }
          }
          if (skuMatched) {
            matchingSkus.push(childSkus[i]);
          }
        }
        return matchingSkus;
      },

      //this method constructs option values for all the options other than selected option
      //from the matching skus.
      updateOptionValuesFromSku: function (skus, selectedOptionID, currentOption) {
        var optionId = currentOption.optionId;
        var options = [];
        var optionValues = currentOption.originalOptionValues();
        for (var k = 0; k < skus.length; k++) {
          var optionValue = skus[k].dynamicPropertyMapLong[optionId];
          if (optionValue != undefined) {
            options.push(optionValue());
          }
        }
        for (var j = 0; j < optionValues.length; j++) {
          var value = optionValues[j].value;
          var visible = false;
          var index = options.indexOf(value);
          if (index != -1) {
            visible = true;
          }
          optionValues[j].visible(visible);
        }
        return optionValues;
      },

      //This method returns true if the option passed is the only one not selected
      //and all other options are either selected or disabled.
      validForSingleSelection: function (optionId) {
        var variantOptions = this.variantOptionsArray();
        for (var j = 0; j < variantOptions.length; j++) {
          if (variantOptions[j].disable() || (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined)) {
            return true;
          }
          if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() == undefined && variantOptions[j].countVisibleOptions() == 1) {
            return true;
          }
        }
        return false;
      },

      //This method updates the selection value for the options wiht single option values.
      updateSingleSelection: function (selectedOptionID) {
        var variantOptions = this.variantOptionsArray();
        for (var i = 0; i < variantOptions.length; i++) {
          var optionId = variantOptions[i].optionId;
          if (variantOptions[i].countVisibleOptions() == 1 && variantOptions[i].selectedOption() == undefined && optionId != selectedOptionID) {
            var isValidForSingleSelection = this.validForSingleSelection(optionId);
            var optionValues = variantOptions[i].originalOptionValues();
            for (var j = 0; j < optionValues.length; j++) {
              if (optionValues[j].visible() == true) {
                variantOptions[i].selectedOption(optionValues[j]);
                break;
              }
            }
          }
        }
      },

      //this method convert the map to array of key value object and sort them based on the enum value
      //to use it in the select binding of knockout
      mapOptionsToArray: function (variantOptions, order, skuData, allStockStatus) {
        var optionArray = [];
        var skuViewModel = skuData;
        var skuSize;
        var stockStatus;

        for (var idx = 0, len = order.length; idx < len; idx++) {
          if (variantOptions.hasOwnProperty(order[idx])) {
            for (var index = 0; index < skuViewModel.length; index++) {
              if (order[idx] == skuViewModel[index].x_size()) {
                skuSize = skuViewModel[index].x_actualSize();
                if (allStockStatus) {
                  stockStatus = allStockStatus[skuViewModel[index].repositoryId()];
                }
              }
            }
            optionArray.push({ key: order[idx], value: variantOptions[order[idx]], visible: ko.observable(true), title: skuSize, status: stockStatus });
          }
        }
        return optionArray;
      },

      //this method returns the selected sku in the product, Based on the options selected
      getSelectedSku: function (variantOptions) {
        var childSkus = [];
        if (this.product()) {
          childSkus = this.product().product.childSKUs;
        }
        var selectedSKUObj = {};
        for (var i = 0; i < childSkus.length; i++) {
          selectedSKUObj = childSkus[i];
          for (var j = 0; j < variantOptions.length; j++) {
            if (!variantOptions[j].disable() && childSkus[i].dynamicPropertyMapLong[variantOptions[j].optionId] != variantOptions[j].selectedOption().value) {
              selectedSKUObj = null;
              break;
            }
          }
          if (selectedSKUObj !== null) {
            $.Topic(pubsub.topicNames.SKU_SELECTED).publish(this.product(), selectedSKUObj, variantOptions);
            return selectedSKUObj;
          }
        }
        return null;
      },

      //refreshes the prices based on the variant options selected
      refreshSkuPrice: function (selectedSKUObj) {
        if (selectedSKUObj === null) {
          if (this.product().hasPriceRange) {
            this.priceRange(true);
          } else {
            this.listPrice(this.product().listPrice());
            this.salePrice(this.product().salePrice());
            this.priceRange(false);
          }
        } else {
          this.priceRange(false);
          var skuPriceData = this.product().getSkuPrice(selectedSKUObj);
          this.listPrice(skuPriceData.listPrice);
          this.salePrice(skuPriceData.salePrice);
        }
      },

      //refreshes the stockstatus based on the variant options selected
      refreshSkuStockStatus: function (selectedSKUObj) {
        var key, stock;
        var orderable = true;
        var stockStatusMap = this.product().stockStatus();
        if (selectedSKUObj === null) {
          key = 'stockStatus';
        } else {
          key = ko.unwrap(selectedSKUObj.repositoryId);
          if (stockStatusMap != undefined && stockStatusMap.productSkuInventoryStatus != undefined) {
          if(widgetModel.reservedArray() && Object.keys(widgetModel.reservedArray()).length > 0){
              var stock = stockStatusMap.productSkuInventoryStatus[key] - widgetModel.reservedArray()[key]();
                orderable = stock > 0 ? true : false;
                selectedSKUObj.quantity = stock;
            }else{
            orderable = stockStatusMap.productSkuInventoryStatus[key] > 0 ? true : false;
            }
          }
        }
        for (var i in stockStatusMap) {
          if (i == key) {
            if ((stockStatusMap[key] == 'IN_STOCK' ||
              stockStatusMap[key] == 'PREORDERABLE' ||
              stockStatusMap[key] == 'BACKORDERABLE') && orderable) {
              this.stockStatus(true);
              this.stockState(stockStatusMap[key]);
              this.availabilityDate(this.getAvailabilityDate(key));
              if (selectedSKUObj === null) {
                this.stockAvailable(1);
              }
              else {
                this.stockAvailable(selectedSKUObj.quantity);
              }
            } else {
              this.stockStatus(false);
              this.stockAvailable(0);
              this.stockState('OUT_OF_STOCK');
            }
            return;
          }
        }
      },

      refreshSkuData: function (selectedSKUObj) {
        this.refreshSkuPrice(selectedSKUObj);
        this.refreshSkuStockStatus(selectedSKUObj);
      },

      // this method returns the availabilityDate if present for the selected variant of the product
      getAvailabilityDate: function (pSkuId) {
        var date = null;
        var skuInventoryList = this.product().stockStatus().productSkuInventoryDetails;
        for (var i in skuInventoryList) {
          var skuInfo = skuInventoryList[i];
          if (skuInfo["catRefId"] === pSkuId) {
            date = skuInfo["availabilityDate"];
            break;
          }
        }
        return date;
      },

      // this method  returns a map of all the options selected by the user for the product
      getSelectedSkuOptions: function (variantOptions) {
        var selectedOptions = [], listingVariantImage;
        for (var i = 0; i < variantOptions.length; i++) {
          if (!variantOptions[i].disable()) {
            selectedOptions.push({ 'optionName': variantOptions[i].optionDisplayName, 'optionValue': variantOptions[i].selectedOption().key, 'optionId': variantOptions[i].actualOptionId, 'optionValueId': variantOptions[i].selectedOption().value });
          }
        }
        return selectedOptions;
      },

      // this function  assign  sku specific image for style based item
      assignSkuIMage: function (newProduct, selectedSKU) {
        var variants, listingVariantId, listingVariantValues = {};
        for (var typeIdx = 0, typeLen = this.productTypes().length; typeIdx < typeLen; typeIdx++) {
          if (this.productTypes()[typeIdx].id == newProduct.type) {
            variants = this.productTypes()[typeIdx].variants;
            for (var variantIdx = 0; variantIdx < variants.length; variantIdx++) {
              if (variants[variantIdx].listingVariant) {
                listingVariantId = variants[variantIdx].id;
                listingVariantValues = variants[variantIdx].values;
                break;
              }
            }
          }
        }
        if (newProduct.childSKUs) {
          for (var childSKUsIdx = 0; childSKUsIdx < newProduct.childSKUs.length; childSKUsIdx++) {
            if (newProduct.childSKUs[childSKUsIdx][listingVariantId] === selectedSKU[listingVariantId]
              && !selectedSKU.primaryThumbImageURL) {
              selectedSKU.primaryThumbImageURL = newProduct.childSKUs[childSKUsIdx].primaryThumbImageURL;
              break;
            }

          }
        }
      },

      allOptionsSelected: function () {
        var allOptionsSelected = true;
        if (this.variantOptionsArray().length > 0) {
          var variantOptions = this.variantOptionsArray();
          for (var i = 0; i < variantOptions.length; i++) {
            if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
              allOptionsSelected = false;
              this.selectedSku(null);
              break;
            }
          }

          if (allOptionsSelected) {
            // get the selected sku based on the options selected by the user
            var selectedSKUObj = this.getSelectedSku(variantOptions);
            if (selectedSKUObj === null) {
              return false;
            }
            this.selectedSku(selectedSKUObj);
          }
          this.refreshSkuData(this.selectedSku());
        }

        return allOptionsSelected;
      },

      quantityIsValid: function () {
        return this.itemQuantity() > 0 && this.itemQuantity() <= this.stockAvailable();
      },

      // this method validated if all the options of the product are selected
      validateAddToCart: function () {

        var AddToCartButtonFlag = this.allOptionsSelected() && this.stockStatus() && this.quantityIsValid() && (this.listPrice() != null);
        // Requirement for configurable items. Do not allow item to be added to cart.
        if ((this.variantOptionsArray().length > 0) && this.selectedSku()) {
          AddToCartButtonFlag = AddToCartButtonFlag
            && !this.selectedSku().configurable;
        } else {
          // Check if the product is configurable. Since the product has only
          // one sku,
          // it should have the SKU as configurable.
          AddToCartButtonFlag = AddToCartButtonFlag
            && !this.product().isConfigurable();
        }
        if (!AddToCartButtonFlag) {
          $('#cc-prodDetailsAddToCart').attr("aria-disabled", "true");
        }

        return AddToCartButtonFlag;

      },

      handleChangeQuantity: function (data, event) {
        var quantity = this.itemQuantity();

        if (quantity < 1) {
          console.log('<= 0');
        } else if (quantity > this.stockAvailable()) {
          console.log('> orderable quantity');
        }

        return true;
      },
  
  getCreditLimitValues: function(){
                
             $.ajax({
                url: widgetModel.translate(widgetModel.GET_ORDER_SSE)  + '?fields=items.priceInfo.total&q=organizationId="' + widgetModel.user().parentOrganization.id() + '" AND (state="PENDING_APPROVAL")',
                dataType: "json",
                type: 'GET',
                contentType: "application/json",
                headers: {
                  'X-CCOrganization': widgetModel.user().parentOrganization.id(),
                  'x-ccsite': widgetModel.site().siteInfo.repositoryId
                },
            async: true,
            success: function(data) {
                var pendingOrders = data.items ? data.items : '';
                var orderTotal = pendingOrders.reduce(function(orderVal, itemVal){return orderVal+Number(itemVal.priceInfo.total)},0);
                if(widgetModel.isUpcomingCollection()){
                    widgetModel.getCurrentSelectionTotal().then(function(successData){
                        widgetModel.effectiveCreditLimit(Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(orderTotal) - Number(successData.totalPrice));
                        if(widgetModel.effectiveCreditLimit() > 0){
                            widgetModel.creditLimitExceeded(false);
                        } else {
                            notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                            widgetModel.creditLimitExceeded(true);
                        }
                        widgetModel.showLoader(false);
                    }, function(errorData){
                        logger.log(errorData);
                        widgetModel.showLoader(false);
                    });
                    
                } else {
                    //effective total with cart value will be checked while adding item to cart
                    widgetModel.effectiveCreditLimit(Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(orderTotal));
                    var effectiveCreditLimit = Number(widgetModel.user().currentOrganization().x_creditLimit) - Number(orderTotal) -  widgetModel.cart().amount();
                    if(effectiveCreditLimit > 0){
                        widgetModel.creditLimitExceeded(false);
                    } else {
                        notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
                        widgetModel.creditLimitExceeded(true);
                    }
                    widgetModel.showLoader(false);
                }
               
            }
        });
        },
        getCurrentSelectionTotal: function(){
          var getDetails = {
                url: widgetModel.translate('selectionTotalUrl') + "?accountId=" + widgetModel.user().parentOrganization.id() + "&emailId="  + widgetModel.user().emailAddress() ,
                dataType: "json",
                type: 'GET',
                contentType: "application/json"
            };
          
            return Promise.resolve($.ajax(getDetails));  
        },
      // Sends a message to the cart to add this product
      handleAddToCart: function () {
       
        notifier.clearError(this.WIDGET_ID);
        var variantOptions = this.variantOptionsArray();
        notifier.clearSuccess(this.WIDGET_ID);
        //get the selected options, if all the options are selected.
        if (!variantOptions[0].selectedOption() && variantOptions[0].optionValues().length > 1 && this.product().childSKUs()[0].x_actualSize()) {
          this.showSizeMsg(true);
          return;
        }
        if (variantOptions[0].optionValues().length == 1) {
          var selectedOptions = variantOptions[0].optionValues()[0];
          widgetModel.refreshSkuData(this.product().childSKUs()[0]);
        } else {
          var selectedOptions = this.getSelectedSkuOptions(variantOptions);
        }
        
        var salePrice = 0;
        if (variantOptions[0].optionValues().length == 1) {
            salePrice = this.product().childSKUs()[0].salePrice;
        } else {
            salePrice = this.selectedSku().salePrice ?  this.selectedSku().salePrice : 0 ;
        }
        var availableCreditLimit =  widgetModel.effectiveCreditLimit() - salePrice * parseInt(this.itemQuantity(), 10) -  widgetModel.cart().amount();        
        if( parseInt(availableCreditLimit, 10) < 1 ){
          widgetModel.creditLimitExceeded(true);
          notifier.sendError(widgetModel.WIDGET_ID, widgetModel.translate('cLExceeded'), true);
          return;
        }

        var selectedOptionsObj = { 'selectedOptions': selectedOptions };

        //adding availabilityDate for product object to show in the edit summary
        //dropdown for backorder and preorder
        var availabilityDateObj = { 'availabilityDate': this.availabilityDate() };
        var stockStateObj = { 'stockState': this.stockState() };

        

        var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj,
          availabilityDateObj, stockStateObj);

        if (this.selectedSku() && !this.selectedSku().primaryThumbImageURL) {
          this.assignSkuIMage(newProduct, this.selectedSku());
        }
        if (this.variantOptionsArray().length > 0 && variantOptions[0].optionValues().length > 1) {
          //assign only the selected sku as child skus
          newProduct.childSKUs = [this.selectedSku()];
        }

        newProduct.orderQuantity = parseInt(this.itemQuantity(), 10);
        
        // If the sum of item quantity available in cart and the item quantity being added, is greater than the available stock,
        // show a notifier error and stop the add to cart.
        var itemQuantityInCart = this.itemQuantityInCart(newProduct);
        var stockAvailable = newProduct.orderLimit && newProduct.orderLimit < this.stockAvailable() ? newProduct.orderLimit : this.stockAvailable();
        if ((itemQuantityInCart + parseInt(this.itemQuantity(), 10)) > stockAvailable) {
          if (stockAvailable > 0) {
            var notificationMsg = CCi18n.t('ns.productdetails:resources.totalItemQuantityExceeded', { stockAvailable: stockAvailable, itemQuantityInCart: itemQuantityInCart });
          } else {
            var notificationMsg = 'This product is out of stock';
          }
          notifier.sendError(this.WIDGET_ID, notificationMsg, true);
          return;
        }

        widgetModel.addToCartSuccessMsg(true);
        if ($(window).width() < 800) {
          $('.cc-item-price').hide();
          widgetModel.continueBtnTxt(true);
        }
        notifier.sendSuccess(widgetModel.WIDGET_ID,widgetModel.translate('addCartSuccess'), true);
        setTimeout(function () {
            widgetModel.addToCartSuccessMsg(false);
            notifier.clearSuccess(widgetModel.WIDGET_ID);
          }, 3000);
          
        if ($(".notificatonMsgAddToCart").is(":visible")) {
          setTimeout(function () {
            $(".notificatonMsgAddToCart").fadeOut();
            widgetModel.addToCartSuccessMsg(false);
          }, 3000);
        }

        $.Topic(pubsub.topicNames.CART_ADD).publishWith(
          newProduct, [{ message: "success" }]);

        //'addToCart' event will be fired when product is added to cart.
        if (typeof window.dataLayer !== 'undefined') {
          var dimension = "", variant = "", skuId = "", skuPrice = "";
          var productSalePrice = widgetModel.product().productSalePrice;
          var productListPrice = widgetModel.product().productListPrice;
          var childSku = widgetModel.product().childSKUs()[0];
          var selectedSkuSalePrice = widgetModel.selectedSku() ? widgetModel.selectedSku().salePrice : '';
          var selectedSkuListPrice = widgetModel.selectedSku() ? widgetModel.selectedSku().listPrice : '';
          var selectedSkuSize = widgetModel.selectedSku() ? widgetModel.selectedSku().x_actualSize : '';
          var currencyCode = widgetModel.site().selectedPriceListGroup().currency.currencyCode;
          if (widgetModel.selectedSku() && selectedSkuSize) {
            if (widgetModel.isSizeDropdown()) {
              dimension = selectedSkuSize;
            } else {
              variant = selectedSkuSize;
            }
            skuId = widgetModel.selectedSku().repositoryId;
            skuPrice = currencyCode === "INR" ? Math.round(selectedSkuSalePrice ? selectedSkuSalePrice : selectedSkuListPrice) : (selectedSkuSalePrice ? selectedSkuSalePrice : selectedSkuListPrice);
          } else {
            if (!productSalePrice) {
              if (productListPrice) {
                skuPrice = currencyCode === "INR" ? Math.round(productListPrice) : productListPrice;
              } else {
                skuPrice = "";
              }
            } else {
              skuPrice = currencyCode === "INR" ? Math.round(productSalePrice) : productSalePrice;
            }
            if (widgetModel.product().childSKUs().length == 1) {
              skuId = childSku.repositoryId();
              skuPrice = currencyCode === "INR" ? Math.round(childSku.salePrice() ? childSku.salePrice() : childSku.listPrice()) : (childSku.salePrice() ? childSku.salePrice() : childSku.listPrice());
              if (widgetModel.isSizeDropdown()) {
                dimension = childSku.x_actualSize();
              } else {
                variant = childSku.x_actualSize();
              }
            }
          }
          var products = {
            "brand": "Fabindia",
            "category": widgetModel.product().parentCategories()[0].displayName(),
            "dimension1": dimension,
            "id": widgetModel.product().id(),
            "name": widgetModel.product().displayName(),
            "price": skuPrice,
            "sku": skuId,
            "variant": variant
          };
          
        }

        // To disable Add to cart button for three seconds when it is clicked and enabling again
        this.isAddToCartClicked(true);
        var self = this;
        setTimeout(enableAddToCartButton, 3000);

        function enableAddToCartButton() {
          self.isAddToCartClicked(false);
        };

        if (self.isInDialog()) {
          $(".modal").modal("hide");
        }
      },
     
      /**
       * Retrieve list of spaces for a user
       */
      getSpaces: function (callback) {
        var widget = this;
        var successCB = function (result) {
          var mySpaceOptions = [];
          var joinedSpaceOptions = [];
          if (result.response.code.indexOf("200") === 0) {

            //spaces
            var spaces = result.items;
            spaces.forEach(function (space, index) {
              var spaceOption = {
                spaceid: space.spaceId,
                spaceNameFull: ko.observable(space.spaceName),
                spaceNameFormatted: ko.computed(function () {
                  return space.spaceName + " (" + space.creatorFirstName + " " + space.creatorLastName + ")";
                }, widget),
                creatorid: space.creatorId,
                accessLevel: space.accessLevel,
                spaceOwnerFirstName: space.creatorFirstName,
                spaceOwnerLastName: space.creatorLastName
              };

              // if user created the space, add it to My Spaces, otherwise add it to Joined Spaces
              if (space.creatorId == swmRestClient.apiuserid) {
                mySpaceOptions.push(spaceOption);
              }
              else {
                joinedSpaceOptions.push(spaceOption);
              }
            });

            // sort each group alphabetically
            mySpaceOptions.sort(mySpacesComparator);
            joinedSpaceOptions.sort(joinedSpacesComparator);

            widget.spaceOptionsGrpMySpacesArr(mySpaceOptions);
            widget.spaceOptionsGrpJoinedSpacesArr(joinedSpaceOptions);

            var groups = [];
            var mySpacesGroup = { label: widget.translate('mySpacesGroupText'), children: ko.observableArray(widget.spaceOptionsGrpMySpacesArr()) };
            var joinedSpacesGroup = { label: widget.translate('joinedSpacesGroupText'), children: ko.observableArray(widget.spaceOptionsGrpJoinedSpacesArr()) };

            var createOptions = [];
            var createNewOption = { spaceid: "createnewspace", spaceNameFull: ko.observable(widget.translate('createNewSpaceOptText')) };
            createOptions.push(createNewOption);
            var createNewSpaceGroup = { label: "", children: ko.observableArray(createOptions) };

            groups.push(mySpacesGroup);
            groups.push(joinedSpacesGroup);
            groups.push(createNewSpaceGroup);
            widget.spaceOptionsArray(groups);
            widget.mySpaces(mySpaceOptions);

            if (callback) {
              callback();
            }
          }
        };
        var errorCB = function (resultStr, status, errorThrown) {
        };

        swmRestClient.request('GET', '/swm/rs/v1/sites/{siteid}/spaces', '', successCB, errorCB, {});
      },

      // SC-4166 : ajax success/error callbacks from beforeAppear does not get called in IE9, ensure dropdown options are populated when opening dropdown
      openAddToWishlistDropdownSelector: function () {
        var widget = this;
        if (widget.spaceOptionsArray().length === 0) {
          widget.getSpaces();
        }
      },
      // this method validates if all the options of the product are selected before allowing
      // add to space. Unlike validateAddToCart, however, it does not take into account inventory.
      validateAddToSpace: function () {
        var allOptionsSelected = true;
        if (this.variantOptionsArray().length > 0) {
          var variantOptions = this.variantOptionsArray();
          for (var i = 0; i < variantOptions.length; i++) {
            if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
              allOptionsSelected = false;
              break;
            }
          }
          if (allOptionsSelected) {
            // get the selected sku based on the options selected by the user
            var selectedSKUObj = this.getSelectedSku(variantOptions);

            if (selectedSKUObj == null) {
              return false;
            }
            var skuPriceData = this.product().getSkuPrice(selectedSKUObj);
            if (skuPriceData.listPrice === null) {
              return false;
            }
          }
        } else {//if no variants are there check product listPrice is not null
          if (this.listPrice() == null) {
            return false;
          }
        }

        // Requirement for configurable items. Do not allow item to be added to WL.
        if ((this.variantOptionsArray().length > 0) && this.selectedSku()) {
          allOptionsSelected = allOptionsSelected
            && !this.selectedSku().configurable;
        } else {
          // Check if the product is configurable. Since the product has only
          // one sku,
          // it should have the SKU as configurable.
          allOptionsSelected = allOptionsSelected
            && this.product() && !this.product().isConfigurable();
        }

        // get quantity input value
        var quantityInput = this.itemQuantity();
        if (quantityInput.toString() != "") {
          if (!quantityInput.toString().match(/^\d+$/) || Number(quantityInput) < 0) {
            return false;
          }
        }

        var addToSpaceButtonFlag = allOptionsSelected && this.product().childSKUs().length > 0;
        if (!addToSpaceButtonFlag) {
          $('#cc-prodDetailsAddToSpace').attr("aria-disabled", "true");
        }

        return addToSpaceButtonFlag;
      },

      //check whether all the variant options are selected and if so, populate selectedSku with the correct sku of the product.
      //this is generic method, can be reused in validateAddToSpace and validateAddToCart in future
      validateAndSetSelectedSku: function (refreshRequired) {
        var allOptionsSelected = true;
        if (this.variantOptionsArray().length > 0) {
          var variantOptions = this.variantOptionsArray();
          for (var i = 0; i < variantOptions.length; i++) {
            if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
              allOptionsSelected = false;
              this.selectedSku(null);
              break;
            }
          }
          if (allOptionsSelected) {
            // get the selected sku based on the options selected by the user
            var selectedSKUObj = this.getSelectedSku(variantOptions);
            if (selectedSKUObj === null) {
              return false;
            }
            this.selectedSku(selectedSKUObj);
          }
          if (refreshRequired) {
            this.refreshSkuData(this.selectedSku());
          }
        }
        return allOptionsSelected;
      },

      // displays Add to Space modal
      addToSpaceClick: function (widget) {
        var variantOptions = this.variantOptionsArray();
        notifier.clearSuccess(this.WIDGET_ID);

        //get the selected options, if all the options are selected.
        var selectedOptions = this.getSelectedSkuOptions(variantOptions);
        var selectedOptionsObj = { 'selectedOptions': selectedOptions };
        var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
        newProduct.desiredQuantity = this.itemQuantity();

        if (this.variantOptionsArray().length > 0) {
          //assign only the selected sku as child skus
          newProduct.childSKUs = [this.selectedSku()];
        }
        newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
        $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD).publishWith(newProduct, [{ message: "success" }]);
      },

      // displays Add to Space modal, triggered from selector button
      addToSpaceSelectorClick: function (widget) {
        var variantOptions = this.variantOptionsArray();
        notifier.clearSuccess(this.WIDGET_ID);

        //get the selected options, if all the options are selected.
        var selectedOptions = this.getSelectedSkuOptions(variantOptions);
        var selectedOptionsObj = { 'selectedOptions': selectedOptions };
        var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
        newProduct.desiredQuantity = this.itemQuantity();

        if (this.variantOptionsArray().length > 0) {
          //assign only the selected sku as child skus
          newProduct.childSKUs = [this.selectedSku()];
        }
        newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
        $.Topic(pubsub.topicNames.SOCIAL_SPACE_SELECTOR_ADD).publishWith(newProduct, [{ message: "success" }]);
      },

      // automatically add product to selected space
      addToSpaceSelect: function (widget, spaceId) {
        var variantOptions = this.variantOptionsArray();
        notifier.clearSuccess(this.WIDGET_ID);

        //get the selected options, if all the options are selected.
        var selectedOptions = this.getSelectedSkuOptions(variantOptions);
        var selectedOptionsObj = { 'selectedOptions': selectedOptions };
        var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
        newProduct.desiredQuantity = this.itemQuantity();

        if (this.variantOptionsArray().length > 0) {
          //assign only the selected sku as child skus
          newProduct.childSKUs = [this.selectedSku()];
        }
        newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
        $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_TO_SELECTED_SPACE).publishWith(newProduct, [spaceId]);
      },

      /**
       * Fetch Facebook app id
       */
      fetchFacebookAppId: function () {
        var widget = this;
        var serverType = CCConstants.EXTERNALDATA_PRODUCTION_FACEBOOK;
        if (widget.isPreview()) {
          serverType = CCConstants.EXTERNALDATA_PREVIEW_FACEBOOK;
        }
        ccRestClient.request(CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA,
          null, widget.fetchFacebookAppIdSuccessHandler.bind(widget),
          widget.fetchFacebookAppIdErrorHandler.bind(widget),
          serverType);
      },

      /**
       * Fetch Facebook app id successHandler, update local and global scope data
       */
      fetchFacebookAppIdSuccessHandler: function (pResult) {
        var widget = this;
        widget.siteFbAppId(pResult.serviceData.applicationId);

        //if (widget.siteFbAppId()) {
        //  facebookSDK.init(widget.siteFbAppId());
        //}
      },

      /**
       * Fetch Facebook app id error handler
       */
      fetchFacebookAppIdErrorHandler: function (pResult) {
        logger.debug("Failed to get Facebook appId.", result);
      },

      // Share product to FB
      shareProductFbClick: function () {
        var widget = this;

        // open fb share dialog
        var protocol = window.location.protocol;
        var host = window.location.host;
        var siteBaseUrl = "";
        if (window.siteBaseURLPath && window.siteBaseURLPath !== "/") {
          siteBaseUrl = window.siteBaseURLPath;
        }
        var productUrlEncoded = encodeURIComponent(protocol + "//" + host + siteBaseUrl + widget.product().route());

        var appID = widget.siteFbAppId();
        // NOTE: Once we can support the Facebook Crawler OG meta-tags, then we should try and use the newer Facebook Share Dialog URL
        //       (per https://developers.facebook.com/docs/sharing/reference/share-dialog).  Until then, we will use a legacy
        //       share URL.  Facebook may eventually not support this older URL, so would be good to replace it as soon as possible.
        //var fbShareUrl = "https://www.facebook.com/dialog/share?app_id=" + appID + "&display=popup&href=" + spaceUrlEncoded + "&redirect_uri=https://www.facebook.com";
        var fbShareUrl = "https://www.facebook.com/sharer/sharer.php?app_id=" + appID + "&u=" + productUrlEncoded;
        var facebookWin = window.open(fbShareUrl, 'facebookWin', 'width=720, height=500');
        if (facebookWin) {
          facebookWin.focus();
        }
      },

      // Share product to Twitter
      shareProductTwitterClick: function () {
        var widget = this;
        var productNameEncoded = encodeURIComponent(widget.product().displayName());
        var protocol = window.location.protocol;
        var host = window.location.host;
        var siteBaseUrl = "";
        if (window.siteBaseURLPath && window.siteBaseURLPath !== "/") {
          siteBaseUrl = window.siteBaseURLPath;
        }
        var productUrlEncoded = encodeURIComponent(protocol + "//" + host + siteBaseUrl + widget.product().route());
        var twitterWin = window.open('https://twitter.com/share?url=' + productUrlEncoded + '&text=' + productNameEncoded, 'twitterWindow', 'width=720, height=500');
        if (twitterWin) {
          twitterWin.focus();
        }
      },

      // Share product to Pinterest
      shareProductPinterestClick: function () {
        var widget = this;
        var productNameEncoded = encodeURIComponent(widget.product().displayName());
        var protocol = window.location.protocol;
        var host = window.location.host;
        var siteBaseUrl = "";
        if (window.siteBaseURLPath && window.siteBaseURLPath !== "/") {
          siteBaseUrl = window.siteBaseURLPath;
        }
        var productUrlEncoded = encodeURIComponent(protocol + "//" + host + siteBaseUrl + widget.product().route());
        var productMediaEncoded = encodeURIComponent(protocol + "//" + host + siteBaseUrl + widget.product().primaryLargeImageURL());
        var pinterestWin = window.open('https://pinterest.com/pin/create/button/?url=' + productUrlEncoded + '&description=' + productNameEncoded + '&media=' + productMediaEncoded, 'pinterestWindow', 'width=720, height=500');
        if (pinterestWin) {
          pinterestWin.focus();
        }
      },

      // Share product by Email
      shareProductEmailClick: function () {
        var widget = this;
        var mailto = [];
        var protocol = window.location.protocol;
        var host = window.location.host;
        var siteBaseUrl = "";
        if (window.siteBaseURLPath && window.siteBaseURLPath !== "/") {
          siteBaseUrl = window.siteBaseURLPath;
        }
        var productUrl = protocol + "//" + host + siteBaseUrl + widget.product().route();
        mailto.push("mailto:?");
        mailto.push("subject=");
        mailto.push(encodeURIComponent(widget.translate('shareProductEmailSubject', { 'productName': widget.product().displayName() })));
        mailto.push("&body=");
        var body = [];
        body.push(widget.translate('shareProductEmailBodyIntro', { 'productName': widget.product().displayName() }));
        body.push("\n\n");
        body.push(productUrl);
        mailto.push(encodeURIComponent(body.join("")));
        window.location.href = mailto.join("");
      },



      handleLoadEvents: function (eventName) {
        if (eventName.toUpperCase() === LOADING_EVENT) {
          spinner.create(productLoadingOptions);
          $('#cc-product-spinner').css('z-index', 1);
        } else if (eventName.toUpperCase() === LOADED_EVENT) {
          this.removeSpinner();
        }
      },
      // Loads the Magnifier and/or Viewer, when required
      loadImage: function () {
        if (resourcesAreLoaded) {
          var contents = $('#cc-image-viewer').html();
          if (!contents) {
            if (this.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
              this.loadMagnifier();
            } else if (this.viewportWidth() >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
              this.loadZoom();
            } else {
              //Load zoom on carousel
              this.loadCarouselZoom();
            }
          } else {
            this.loadViewer(this.handleLoadEvents.bind(this));
          }
        } else if (resourcesNotLoadedCount++ < resourcesMaxAttempts) {
          setTimeout(this.loadImage, 500);
        }
      },

      groupImages: function (imageSrc) {
        var self = this;
        var images = [];
        if (imageSrc) {
          for (var i = 0; i < imageSrc.length; i++) {
            if (i % 3 == 0) {
              images.push(ko.observableArray([imageSrc[i]]));
            } else {
              images[images.length - 1]().push(imageSrc[i]);
            }
          }
        }
        return images;
      },

      handleCarouselArrows: function (data, event) {
        // Handle left key
        if (event.keyCode == 37) {
          $('#prodDetails-imgCarousel').carousel('prev');
        }
        // Handle right key
        if (event.keyCode == 39) {
          $('#prodDetails-imgCarousel').carousel('next');
        }
      },

      handleCycleImages: function (data, event, index, parentIndex) {
        var absoluteIndex = index + parentIndex * 4;
        // Handle left key
        if (event.keyCode == 37) {
          if (absoluteIndex == 0) {
            $('#prodDetails-imgCarousel').carousel('prev');
            $('#carouselLink' + (this.product().thumbImageURLs.length - 1)).focus();
          } else if (index == 0) {
            // Go to prev slide
            $('#prodDetails-imgCarousel').carousel('prev');
            $('#carouselLink' + (absoluteIndex - 1)).focus();
          } else {
            $('#carouselLink' + (absoluteIndex - 1)).focus();
          }
        }
        // Handle right key
        if (event.keyCode == 39) {
          if (index == 3) {
            $('#prodDetails-imgCarousel').carousel('next');
            $('#carouselLink' + (absoluteIndex + 1)).focus();
          } else if (absoluteIndex == (this.product().thumbImageURLs.length - 1)) {
            // Extra check when the item is the last item of the carousel
            $('#prodDetails-imgCarousel').carousel('next');
            $('#carouselLink0').focus();
          } else {
            $('#carouselLink' + (absoluteIndex + 1)).focus();
          }
        }
      },

      loadImageToMain: function (data, event, index) {
        this.activeImgIndex(index);
        this.mainImgUrl(this.product().fullImageURLs[index]);

        return false;
      },

      assignImagesToProduct: function (pInput) {
        if (this.firstTimeRender == true) {
          this.product().primaryFullImageURL(pInput.primaryFullImageURL);
          this.product().primaryLargeImageURL(pInput.primaryLargeImageURL);
          this.product().primaryMediumImageURL(pInput.primaryMediumImageURL);
          this.product().primarySmallImageURL(pInput.primarySmallImageURL);
          this.product().primaryThumbImageURL(pInput.primaryThumbImageURL);
          this.firstTimeRender = false;
        }
        this.product().thumbImageURLs(pInput.thumbImageURLs);
        this.product().smallImageURLs(pInput.smallImageURLs);
        this.product().mediumImageURLs(pInput.mediumImageURLs);
        this.product().largeImageURLs(pInput.largeImageURLs);
        this.product().fullImageURLs([]);
        this.product().fullImageURLs(pInput.fullImageURLs);
        this.product().sourceImageURLs(pInput.sourceImageURLs);

        this.mainImgUrl(pInput.primaryFullImageURL);
        this.imgGroups(this.groupImages(pInput.mediumImageURLs));
        this.activeImgIndex(0);
        this.activeImgIndex.valueHasMutated();
      },

      checkResponsiveFeatures: function (viewportWidth) {
        if (viewportWidth > 978) {
          this.isMobile(false);
        }
        else if (viewportWidth <= 978) {
          this.isMobile(true);
        }
      },
      priceUnavailableText: function () {
        return CCi18n.t('ns.productdetails:resources.priceUnavailable');
      },
      isInDialog: function () {
        return $("#CC-prodDetails-addToCart").closest(".modal").length;
      },


      /**
      *
      */
      getSelectedProducts: function () {
        if (!this.validateAddToPurchaseList()) {
          notifier.sendError(this.WIDGET_ID, this.translate('productAddError'));
        } else {
          var variantOptions = this.variantOptionsArray();
          var selectedOptions = this.getSelectedSkuOptions(variantOptions);
          var selectedOptionsObj = { 'selectedOptions': selectedOptions };
          var selectedProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
          selectedProduct.desiredQuantity = parseInt(this.itemQuantity(), 10);
          if (this.variantOptionsArray().length > 0) {
            //assign only the selected sku as child skus
            selectedProduct.childSKUs = [this.selectedSku()];
          }
          var productItem = {
            "productId": selectedProduct.id,
            "catRefId": selectedProduct.childSKUs[0].repositoryId,
            "quantityDesired": selectedProduct.desiredQuantity,
            "displayName": selectedProduct.displayName
          };
          var productItemArray = [];
          productItemArray.push(productItem);
          return productItemArray;
        }
      },


      // this method validates if all the options of the product are selected before allowing
      // add to purchase list.
      validateAddToPurchaseList: function () {
        //var self = this;
        var allOptionsSelected = true;
        if (this.variantOptionsArray().length > 0) {
          var variantOptions = this.variantOptionsArray();
          for (var i = 0; i < variantOptions.length; i++) {
            if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
              allOptionsSelected = false;
              break;
            }
          }
          if (allOptionsSelected) {
            // get the selected sku based on the options selected by the user
            var selectedSKUObj = this.getSelectedSku(variantOptions);

            //var skuPriceData = this.product().getSkuPrice(selectedSKUObj);
            if (selectedSKUObj === null) {
              return false;
            }
          }
        } /*else { //if no variants are there check product listPrice is not null
          if (this.listPrice() == null) {
            return false;
          }
        }*/

        // Requirement for configurable items. Do not allow item to be added to WL.
        if ((this.variantOptionsArray().length > 0) && this.selectedSku()) {
          allOptionsSelected = allOptionsSelected &&
            !this.selectedSku().configurable;
        } else {
          // Check if the product is configurable. Since the product has only
          // one sku,
          // it should have the SKU as configurable.
          allOptionsSelected = allOptionsSelected &&
            !this.product().isConfigurable();
        }

        // get quantity input value
        var quantityInput = this.itemQuantity();
        if (quantityInput.toString() != "") {
          if (!quantityInput.toString().match(/^\d+$/) || Number(quantityInput) < 0) {
            return false;
          }
        }

        var addToPurchaseListFlag = allOptionsSelected && this.product().childSKUs().length > 0;

        return addToPurchaseListFlag;
      },
      quantityIncrease: function () {

        widgetModel.itemQuantity(widgetModel.itemQuantity() + 1);
        $('#itemQuantityValue').val(widgetModel.itemQuantity());

      },
      quantityDecrease: function () {

        widgetModel.itemQuantity(widgetModel.itemQuantity() - 1);

        $('#itemQuantityValue').val(widgetModel.itemQuantity());

        if (widgetModel.itemQuantity() <= 1) {
          return widgetModel.itemQuantity(1);
        }
      },

      //Check the COD availability
      checkCODAvailability: function () {
        var url = "/ccstorex/custom/v1/serviceabilityCheck";
        var pattern = /^([0-9]{5,})$/;
        widgetModel.validentry(pattern.test(widgetModel.pincode()));
        widgetModel.showMessage(false);
        var inputData = {
          postalCode: widgetModel.pincode()
        }
        if (widgetModel.validentry()) {
          ccRestClient.request(url,
            inputData,
            function (result) {
              widgetModel.showMessage(true);
              widgetModel.codAvailable(result.message.cod || result.message.prepaid);
              widgetModel.furnitureServiceAvailbale(result.message.furniture);
            },
            function () {
              console.log("error");
            });
        }
      },

      //Size Guide animation functionality
      gotoSizeChart: function (event, data) {
        if (event.currentTarget.className === "visible-xs visible-sm") {
          $('#size-chart-anchor').click();
          $('html, body').animate({
            scrollTop: $("#accordion").offset().top
          }, 200);
        } else {
          $('#product-tabs a[href="#menu5"]').tab('show');
          $('html, body').animate({
            scrollTop: $(".relatedcontainer").offset().top - 300
          }, 200);
        }
      },
      setUpSKuSizes: function (widget) {
        var url = CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY;
        var pathParameter = widget.product().id();

        ccRestClient.request(url, {},
          function (data) {
            if(widgetModel.reservedArray() && Object.keys(widgetModel.reservedArray()).length > 0){
                var key, stock;
                for(var index = 0; index < data.productSkuInventoryDetails.length; index++){
                    key = data.productSkuInventoryDetails[index].catRefId;
                    stock = parseInt(data.productSkuInventoryStatus[key], 10) - parseInt(widgetModel.reservedArray()[key](), 10);
                    data.productSkuInventoryStatus[key] = stock;
                    data.stockStatus = stock > 0 ? "IN_STOCK" : "OUT_OF_STOCK";
                    data[key] = stock > 0 ? "IN_STOCK" : "OUT_OF_STOCK";
                }
            }
            if (data["productSkuInventoryStatus"] != null) {
              widget.populateVariantOptions(widget, data);
            } else {
              widget.populateVariantOptions(widget);
            }
          },
          //error callback
          function (data) {
            widget.populateVariantOptions(widget);
          },
          pathParameter);
      },

      stringifyArrayValue: function (cartArray, arrKey) {
        return cartArray.map(function (elem) { if (elem[arrKey] != null) { return elem[arrKey]; } }, { arrKey: arrKey }).join(",");
      },
      
    };
  }
);
